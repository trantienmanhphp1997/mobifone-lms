import { Component, HostListener, Input, OnInit } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { BadgeService } from 'src/app/shared/services/badge.service';
import { CourseService } from 'src/app/shared/services/course.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { FileService } from 'src/app/shared/services/file.service';
import { PdfViewerModule } from 'ng2-pdf-viewer';
@Component({
    selector: 'app-eu-course-upload-badge',
    templateUrl: 'eu-course-upload-badge.component.html',
    styleUrls: ['./eu-course-upload-badge.component.css']
})

export class EuCourseUploadBadgeComponent implements OnInit {
    @Input() course: any;
    showUpload :any;
    showBadge :any;
    mimeType: string;
    fileName: string;
    badgeFile: string;
    imageType: any = ['png', 'jpeg', 'jpg', 'csv', 'gif', 'jfif'];
    fileType: any = ['pdf'];
    pdfSrc: any;
    pageVariable: number = 1;
    action: any = [];
    constructor(
        private modalService: NgbModal,
        private badgeService: BadgeService,
        private courseService: CourseService,
        private spinner: NgxSpinnerService,
        private toastrService: ToastrCustomService,
        private translateService: TranslateService,
        private fileservice: FileService,
    ) { 
        this.showUpload=true;
    }

    ngOnInit() {
        this.dragAreaClass = "dragarea";
        if(this.course && this.course.badgefilename){
            var name = this.course.badgefilename.split('.');
            if(name) {
                this.mimeType = name[name.length - 1];
                this.fileName = name[0].length > 170 ? name[0].substring(0,170) + '[...].' + this.mimeType :  name[0] + '.' + this.mimeType
            }
        }

        this.action = [
            {
                name: "zoomIn",
                icon: "",
                f: function() {

                }
            },
            {
                name: "zoomOut",
                icon: "",
                f: function() {
                    
                }
            },
            {
                name: "page",
                icon: "",
                f: function() {
                    
                }
            },
            {
                name: "zoomIn",
                icon: "",
                f: function() {
                    
                }
            }
        ]
    }

    /**
     * Drag and drop file
     *
     * use HostListener: dragover, dragenter, dragend, dragleave. drop
     */
    error: string;
    dragAreaClass: string;
    fileSelected: any = null;
    courseBadge:any;
    imageSrc: any;

    onFileChange(event: any) {
        let files: FileList = event.target.files;
        this.saveFiles(files);
    }
    @HostListener("dragover", ["$event"]) onDragOver(event: any) {
        this.dragAreaClass = "droparea";
        event.preventDefault();
    }
    @HostListener("dragenter", ["$event"]) onDragEnter(event: any) {
        this.dragAreaClass = "droparea";
        event.preventDefault();
    }
    @HostListener("dragend", ["$event"]) onDragEnd(event: any) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
    }
    @HostListener("dragleave", ["$event"]) onDragLeave(event: any) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
    }
    @HostListener("drop", ["$event"]) onDrop(event: any) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
        event.stopPropagation();
        if (event.dataTransfer.files) {
            let files: FileList = event.dataTransfer.files;
            this.saveFiles(files);
        }
    }

    /**
     * Process file
     *
     * Method:
     * + saveFiles(): check validate to file and save to global variable
     * + dowloadURL(): ...
     * + dowloadTemplate(): download training plan template (excel)
     */
    saveFiles(files: FileList) {
        // let allowedExtensions = /(\.xls|\.xlsx)$/i;
        if (files.length > 1) {
            this.error = "Hệ thống chỉ cho phép tải lên từng tệp. Vui lòng kiểm tra lại";
        } else if(files.length) {
            this.error = "";
            this.fileSelected = files[0];
            const reader = new FileReader();
            if(this.fileSelected.type == "application/pdf"){
                if (typeof (FileReader) !== 'undefined') {
                    reader.onload = (e: any) => {
                        this.imageSrc = e.target.result;
                    };
                    reader.readAsArrayBuffer(this.fileSelected);
                }
            }else{
                reader.onload = e => this.imageSrc = reader.result;
                reader.readAsDataURL(this.fileSelected);
            }
            this.showUpload=false;
            if(this.fileSelected.name) {
                var name = this.fileSelected.name.split('.');
                if(name) {
                    this.mimeType = name[name.length - 1];
                    this.fileName = name[0].length > 170 ? name[0].substring(0,170) + '[...].' + this.mimeType :  name[0] + '.' + this.mimeType
                }
            }
        }
    }
    uploadBadge() {
        this.spinner.show();
        const params = {
            //   userid: this.studentId.toString(),
            badgeid: this.course.badgeid
        }
        this.badgeService.assignStudentToBadge(params, this.fileSelected).subscribe(res => {
            this.spinner.hide();
            this.showBadge=true;
            this.updateCouser();
            this.toastrService.success("Upload chứng chỉ thành công! Đang chờ phê duyệt");
            // this.onreloadPage();
        }, err => {
            this.spinner.hide();
            this.toastrService.handlerError(err);
        })
        this.fileSelected=null;
    }
    //  public downloadURI(uri: string, name?: string): void {
    //      const link = document.createElement('a');
    //      if (name) {
    //          link.setAttribute('download', name);
    //      }
    //      link.href = this.fileservice.getFileFromPathUrl(uri);
    //      document.body.appendChild(link);
    //      link.click();
    //      link.remove();
    //  }

    //  dowloadTemplate() {
    //      this.spinner.show();
    //      this.trainingPlanService.downloadTemplateTrainingPlanFile().subscribe(
    //          res => {
    //              this.spinner.hide()
    //              this.downloadURI(res.body.path);
    //          },
    //          err => {
    //              this.spinner.hide();
    //              this.toastrService.error('Đã có lỗi xảy ra trong quá trình tải xuống');
    //          }
    //      )
    //  }
    getDateFromUnix(date) {
        if (date) {
            return moment.unix(date);
        }
        return null;
    }
    deleteBadge(){
        this.fileSelected = null;
        this.showUpload = true;
    }
    updateCouser(){
        return this.courseService.getCoursesInfoEnduser(this.course.id).subscribe((data) => {
            this.course = data.body;
        },
        (err) => {
        });
    }
    viewFile() {
        var file = null;
        if (this.course) {
            file = this.fileservice.badgeFileUrl(this.course.badgecontextid, this.course.badgefilename, '/external_badge/overviewfiles/', '0/');
        }
        return file;
    }
    checkType(type :any) {
        if(type.includes(this.mimeType))
            return true;
    }
    pageRendered(e: CustomEvent) {
    }
    onProgress(progressData) {
    }
}