import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-badge-category-confirm-delete.component',
  templateUrl: './badge-category-confirm-delete.component.html',
  styleUrls: ['./badge-category-confirm-delete.component.css'],
  providers: [NgbModal, NgbModalConfig]
})
export class BadgeCategoryConfirmDeleteComponent implements OnInit {

  @Input() body: string;
  @Input() confirmButton: string;
  data:any;
  constructor(
    public activeModal: NgbActiveModal,
  ) {}

  ngOnInit(): void {
      this.data = JSON.parse(this.body);
  }
}
