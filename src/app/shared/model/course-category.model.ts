export interface CourseCategory {
  id?: number;
  name: string;
  code:string;
  description?: string;
  parent?: number;
  idnumber?: number;
  descriptionformat?: number;
  sortorder?: number;
  coursecount?: number;
  visible?: boolean;
  visibleold?: boolean;
  timemodified?: Date;
  depth?: number;
  path?: string;
  theme?: string;
  completed?: any;
  parentname?: string;
  isroot?: any;
}


