import {
  LOCAL_STORAGE_KEY,
  USER_ROLE,
} from './../../../shared/constants/base.constant';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalStorageService } from 'ngx-webstorage';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { ReportService } from '../../../shared/services/report.service';
import { FileService } from '../../../shared/services/file.service';
import { CourseService } from 'src/app/shared/services/course.service';
import { element } from 'protractor';
import { RoleSystem } from 'src/app/shared/model/role-system';
import { RoleService } from 'src/app/shared/services/role.service';
import { TranslateService } from '@ngx-translate/core';
import { UserService } from 'src/app/shared/services/user.service';
import { UserInfo } from 'src/app/shared/model/user-info.model';
import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { Department } from 'src/app/shared/model/department.model';

@Component({
  selector: 'app-result-exam',
  templateUrl: './result-exam.component.html',
  styleUrls: ['./result-exam.component.css'],
})
export class ResultExamComponent implements OnInit {
  @ViewChild('keyword') keyword: ElementRef;
  searchExam = {
    search: '',
    userids: '',
    //  startDate: null,
    //  endDate: null,
    //  categoryid: null,
    pageIndex: 1,
    pageSize: 10,
    //  sortColumn: 'timecreated',
    //  sortType: 'desc',
    coursetype: '',
    deparmentIds: '',
  };
  reportTypeAll = new Set<number>();
  courses = [];
  studentSelected: any[] = [];
  studentData: any[] = [];
  totalRecord: number;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  pageIndex = 1;
  pageSize = 10;
  showFullRoles = false;
  selectedRole = [];
  selectedStudents = [];
  selectRoleEvent = false;
  roleList: RoleSystem[] = [];
  roleListRemoveRoleManager: RoleSystem[] = [];
  userIds = [];
  userIdsChecked = [];
  keyWord: string;
  selectedPositionIds = [];
  checkedDepartmentIds = [];
  checkBoxAllUserDelete = false;
  sortcolumn = 'timecreated';
  sorttype = 'desc';
  userInfors: UserInfo[] = [];
  limit = 400;
  filterUserReq = "";
  keywordStudent: string;
  dropdownStudentSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'fullname',
    selectAllText: 'Chọn tất cả',
    unSelectAllText: 'Bỏ chọn tất cả',
    searchPlaceholderText: 'Tìm kiếm',
    itemsShowLimit: 5,
    allowSearchFilter: true,
  };
  reqQuestionUpdate = new Subject<string>();
  elements:any ;
  @ViewChild('departmentTree')
  public departmentTree: TreeViewComponent;
  departmentList: Department[] = [];
  departmentTreeData: any;
  constructor(
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    private $localStorage: LocalStorageService,
    private reportService: ReportService,
    private fileService: FileService,
    private courseService: CourseService,
    private translateService: TranslateService,
    private userService: UserService,
    private departmentService: DepartmentService
  ) {}

  ngOnInit(): void {
    this.reqQuestionUpdate.pipe(
      debounceTime(800),
      distinctUntilChanged())
      .subscribe(value => {
        if (this.filterUserReq != value) {
          this.onSearchData('user', value)
        }
      });
    const currentSearch = this.$localStorage.retrieve(
      LOCAL_STORAGE_KEY.SEARCH.REPORT_EXAM_SEARCH
    );
    if (currentSearch) {
      // Kiem tra xem truoc do co luu du lieu tim kiem khong
      this.searchExam.search = currentSearch.search;
      this.searchExam.pageIndex = currentSearch.pageIndex;
      this.searchExam.pageSize = currentSearch.pageSize;
      //  this.searchExam.sortColumn = currentSearch.sortColumn;
      //  this.searchExam.sortType = currentSearch.sortType;
      // Xoa di sau khi su dung
      this.$localStorage.clear(LOCAL_STORAGE_KEY.SEARCH.REPORT_EXAM_SEARCH);
    }
    this.getListExam();
    this.getListStudentData();
    this.getListAllTree();
  }
  onSearchData(type: any, $event: any) {
      this.keywordStudent = $event;
      this.getListStudentData()
    
  }
  ngAfterViewInit() {
    this.keyword.nativeElement.value = this.searchExam?.search;
  }

  onSelectDeStudentEvent() {
    this.userIds = [];
    this.userIdsChecked = [];
    this.selectRoleEvent = true;
    this.keywordStudent = '';
    this.getListStudentData();
  }
  onSelectStudentEvent() {
    this.userIds = [];
    this.userIdsChecked = [];
    this.selectRoleEvent = true;
  }
  onSearchStudent(){
    this.elements = document.getElementsByTagName("input");
    this.elements[1].addEventListener('keydown', (event)=>{
      setTimeout(() => {
        this.keywordStudent =  this.elements[1].value;
        this.getListStudentData();
      }, 100)
    });
  }
  changeRole() {
    if (this.selectRoleEvent) {   
      // Vi su kien onDropDownClose luon ban ra, vi vay co them co de kiem tra truoc do co hanh vi thay doi chuc danh khong
      this.selectRoleEvent = false;
      this.selectedStudents = [];
      for (const role of this.selectedRole) {
        this.selectedStudents.push(
          role.id
          
        );
      }
      this.pageIndex = 1;
      this.userIds = [];
      this.userIdsChecked = [];
      this.searchUser();
    }
  }
  changeStudent(data:any){
    this.selectedStudents = data;
    this.searchUser();
  }
 
  getListStudentData() {
    const params = {
      limit: this.limit,
      keyword: this.keywordStudent
    };

    return this.courseService.getUserList(params).subscribe((roleList) => {
      this.roleList = roleList.body.results;
      this.roleList.forEach((role) => {
        role.shortnameTranslate = this.getShortName(role.shortname);
      });
      this.roleListRemoveRoleManager = this.roleList.filter((role) => {
        return role.shortname !== USER_ROLE.STUDENT;
      });
      const roleUser = this.$localStorage.retrieve('roles')[0].shortname;
      if (roleUser === USER_ROLE.MANAGER) {
        this.showFullRoles = false;
      } else if (roleUser === USER_ROLE.STUDENT) {
        this.showFullRoles = true;
      }
    });
  }

  getShortName(shortname) {
    if (shortname) {
      return this.translateService.instant('user.' + shortname);
    }
    return '';
  }

  searchUser() {
    const params = {
      limit: this.pageSize,
      page: this.pageIndex,
      userids: this.selectedStudents.toString(),
    };
    this.spinner.show();
    this.userIds = [];
    this.userService.getUserListUser(params).subscribe(
      (res) => {
        this.courses = res.body.results;
        this.keywordStudent = '';
        this.getListStudentData(); 
        this.pageIndex = this.searchExam.pageIndex;
        this.pageSize = this.searchExam.pageSize;
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      }
    );
  }

  getListExam() {
    const params = {
      limit: this.searchExam.pageSize,
      page: this.searchExam.pageIndex,
      search: this.searchExam.search,
      userids: this.searchExam.userids,
      coursetype: this.searchExam.coursetype,
      departmentids: this.searchExam.deparmentIds,
    };
    this.spinner.show();
    this.reportService.getListExamReport(params).subscribe(
      (data) => {
        this.spinner.hide();
        this.courses = data.body.results;

        this.totalRecord = data.body.total;
        this.pageIndex = this.searchExam.pageIndex;
        this.pageSize = this.searchExam.pageSize;
      },
      (err) => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      }
    );
  }

  changePage(event) {
    this.searchExam.pageIndex = event.pageIndex + 1;
    this.searchExam.pageSize = event.pageSize;
    this.searchExam.userids = this.selectedStudents.toString();
    this.getListExam();
  }

  onChangeType(event) {
    this.searchExam.pageIndex = 1;
    this.searchExam.coursetype = event.target.value;
    this.getListExam();
  }

  goToDetail() {
    this.$localStorage.store(
      LOCAL_STORAGE_KEY.SEARCH.REPORT_EXAM_SEARCH,
      this.searchExam
    );
  }

  onSearch(event) {
    this.searchExam.pageIndex = 1;
    this.searchExam.search = event.target.value;
    this.getListExam();
  }

  onExportListExamReport() {
    const params = {
      limit: this.searchExam.pageSize,
      page: this.searchExam.pageIndex,
      search: this.searchExam.search,
      coursetype: this.searchExam.coursetype,
      departmentids: this.searchExam.deparmentIds,
    };

    this.reportService.exportListExamReport(params).subscribe(
      (res) => {
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
      },
      (err) => {
        this.toastrService.handlerError(err);
      }
    );
  }
  onScrollDown(){
    this.limit += 40;
    this.getListStudentData();
}

  onExport(id: any) {
    const reportType =
      Array.from(this.reportTypeAll).filter((type) => type === id).length > 0;
    const params = {
      courseid: id,
      reporttype: reportType ? 1 : '',
    };
    this.reportService.exportExamReportDetail(params).subscribe(
      (res) => {
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
      },
      (err) => {
        this.toastrService.handlerError(err);
      }
    );
  }

  onChangeReportType(event, courseId) {
    if (event.target.value) {
      this.reportTypeAll.add(courseId);
    } else {
      this.reportTypeAll.delete(courseId);
    }
  }

  getListAllTree() {
    this.spinner.show();
    this.departmentService.getUserDepartmentTree(0).subscribe(
      (data) => {
        this.departmentList = data.body;
        if (this.departmentList) {
          this.departmentList[0].parentid = null;
        }
        this.departmentTreeData = {
          dataSource: this.departmentList,
          id: 'id',
          parentID: 'parentid',
          text: 'name',
          hasChildren: 'haschild',
        };
        // this.getListDepartmentDefault();
        this.spinner.hide();
      },
      (error) => {
        this.spinner.hide();
        this.toastrService.handlerError(error);
      }
    );
  }
  getListDepartmentDefault() {
    this.reportService.getListDefaultParameterSource().subscribe((data) => {
     data.body.departmentids.forEach((item) =>{
        this.departmentTree.checkedNodes =[...this.departmentTree?.checkedNodes, item];
      })
      this.spinner.hide();
    });
  }
  public nodeCheck(args: any): void {
    const checkedNode: any = [args.node];
    if (
      args.event.target.classList.contains('e-fullrow') ||
      args.event.key == 'Enter'
    ) {
      // @ts-ignore
      const getNodeDetails: any = this.departmentTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.departmentTree.uncheckAll(checkedNode);
      } else {
        this.departmentTree.checkAll(checkedNode);
      }
    }
  }

  // chọn 1 đơn vị
  nodeDepartmentSelected(e: any) {
    // code
  }

  // chọn nhiều đơn vị
  nodeDepartmentChecked(e: any) {
    this.checkedDepartmentIds = [];
    this.departmentTree.checkedNodes.forEach((node) => {
      this.checkedDepartmentIds.push(parseInt(node, 10));
    });
    this.searchExam.deparmentIds = this.checkedDepartmentIds.toString();
    this.getListExam();
  }
}
