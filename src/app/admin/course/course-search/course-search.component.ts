import { DepartmentService } from '../../../shared/services/department.service';
import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { PositionService } from '../../../shared/services/position.service';
import * as moment from 'moment';
import { CourseCategoryService } from 'src/app/shared/services/course-category.service';
import CommonUtil from 'src/app/shared/utils/common-util';

@Component({
  selector: 'app-course-search',
  templateUrl: './course-search.component.html',
  styleUrls: ['./course-search.component.css']
})
export class CourseSearchComponent implements OnInit {

  @Input() searchCourse;

  categoryMap: any[] = [];
  categoryData: any;
  courseCategoryList: any[] = [];
  departmentMap: any[] = [];
  positionList: any[] = [];
  positionData: number [] = [];
  startDate: any;
  endDate: any;
  statusSearch: any;
  departmentData: number [] = [];
  statusSearchData: any;
  // planidData: any;
  plancourse: any = 0;

  dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 5,
    allowSearchFilter: true,
    searchPlaceholderText: 'Tìm kiếm'
  };

  constructor(
    public activeModal: NgbActiveModal,
    private positionService: PositionService,
    private departmentService: DepartmentService,
    private courseCategoryService: CourseCategoryService
  ) {}

  ngOnInit(): void {
    this.statusSearchData = [
      {name: 'Chưa hiển thị', id: 1},
      {name: 'Chưa diễn ra', id: 2},
      {name: 'Đang diễn ra', id: 3},
      {name: 'Đã kết thúc', id: 4}
    ];
    // this.planidData=[
    //   {name:'Theo kế hoạch', id:1},
    //   {name:'Không theo kế hoạch',id:0}
    // ];

    if(this.searchCourse.status){
      this.statusSearch = this.searchCourse.status;
    }else{
      this.statusSearch = null;
    }

    if(this.searchCourse.isplancourse){
      this.plancourse = this.searchCourse.isplancourse * 1;
    }else{
      this.plancourse = null;
    }

    if(this.searchCourse.departmentData){
      this.departmentData = this.searchCourse.departmentData;
    }else{
      this.departmentData = [];
    }

    if(this.searchCourse.positionData){
      this.positionData = this.searchCourse.positionData;
    }else{
      this.positionData = null;
    }

    if (this.searchCourse.startDate) {
      this.startDate = moment.unix(this.searchCourse.startDate).toDate();
    }
    if (this.searchCourse.endDate) {
      this.endDate = moment.unix(this.searchCourse.endDate).toDate();
    }

    if(this.searchCourse.categoryData) {
      this.categoryData = this.searchCourse.categoryData
    }else{
      this.categoryData = [];
    }

    this.getListPosition();
    this.listAllTree();
    this.listAllTreeCategory();
  }

  getListPosition() {
    return this.positionService.getPosition().subscribe(positionList => {
      this.positionList = positionList.body.results;
    });
  }

  onSearchCourses() {
    if(this.endDate){
      //lay cuoi ngay
      this.endDate.setHours(23);
      this.endDate.setMinutes(59);
      this.endDate.setSeconds(59);
    }
    const searchParams = {
      positionData: this.positionData,
      startDate: this.startDate,
      endDate: this.endDate,
      departmentData: this.departmentData,
      status: (this.statusSearch==="null") ? null : this.statusSearch,
      isplancourse:(this.plancourse==="null")? null: this.plancourse * 1,
      categoryData: this.categoryData
    }
    this.activeModal.close(searchParams);
  }

  resetSearch() {
    this.startDate = null;
    this.endDate = null;
    this.positionData = [];
    this.departmentData = null;
    this.statusSearch = null;
    this.plancourse= null;
    this.categoryData= []
  }

  onDeSelectPosition($event) {
    this.positionData = [...this.positionData]?.filter(id => id !== $event.id);
  }

  onDeSelectAllPosition() {
    this.positionData = [];
  }

  onDeSelectDepartment($event) {
    this.departmentMap = [...this.departmentMap]?.filter(id => id !== $event.id);
  }

  onDeSelectAllDepartment() {
    this.departmentData = [];
  }

  onDeSelectCategory($event) {
    this.categoryMap = [...this.categoryMap]?.filter(id => id !== $event.id);
  }

  onDeSelectAllCategory() {
    this.categoryData = [];
  }

  listAllTree() {
    this.departmentService.getUserDepartmentTree(1).subscribe((data) => {
      const e = [];
      data.body.forEach(department => {
        if (department.parentid !== null) {
          const parentName = e.filter(x => x.id == department.parentid)[0]?.name;
          let name = '';
          if (parentName) {
            name = parentName + ' / ' + department.name;
          } else {
            name = department.name;
          }
          e.push({id: department.id, name: name});
        } else {
          e.push({id: department.id, name: department.name});
        }
      });
      this.departmentMap = e;
    });
  }

  listAllTreeCategory() {
    this.courseCategoryService.getCourseCategoryTree(null).subscribe(response => {
      this.courseCategoryList = response.body;
      const e = [];
      this.courseCategoryList.forEach(category => {
        if (category.parent !== null) {
          const parentName = e.filter(x => x.id == category.parent)[0]?.name;
          let name = '';
          if (parentName) {
            name = parentName + ' / ' + category.name;
          } else {
            name = category.name;
          }
          e.push({id: category.id, name: name});
        } else {
          e.push({id: category.id, name: category.name});
        }
      });
      this.categoryMap = e;
    });
  }

  onCheckBox(value) {
    // console.log(this.plancourse);
    // var e = this.plancourse;
    // this.plancourse = e ? 0 : 1;
    // console.log(e, this.plancourse);
  }

}
