import {Component, OnInit, Input} from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrCustomService} from '../../../../shared/services/toastr-custom.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {BadgeService} from '../../../../shared/services/badge.service';
import {ActivatedRoute} from '@angular/router';
import {Sort} from '@angular/material/sort';
import CommonUtil from 'src/app/shared/utils/common-util';
import * as moment from 'moment';
import { STATUS_BADGES } from 'src/app/shared/constants/base.constant';
@Component({
    selector: 'app-bade-level-badge-addForm',
    templateUrl: './badege-level-badge-addForm.component.html',
    styleUrls: ['./badege-level-badge-addForm.component.css'],
})

export class BadgeLevelBadgeAddFormComponent implements OnInit {
    constructor(
        public activeModal: NgbActiveModal,
        private badgeService: BadgeService,
        private route: ActivatedRoute,
        private spinner: NgxSpinnerService,
        private toastrService: ToastrCustomService
    ) {}
    
    @Input() badgeActive: any;
    @Input() id: number;
    public hasDelete=true;
    public hasAction=true;
    public isReloadPage=false;
    public dataTable = [];
    public selectedRow = 0;
    public totalRecord: number;
    public pageSizeOptions: number[] = [5, 10, 25, 50, 100];
    public selectedIds = [];
    public completeAll = false;
    public searchData = {
        search: '',
        levelid: null,
        pageIndex: 1,
        pageSize: 5,
        sortColumn: 'id',
        sortType: 'desc'
    };

    ngOnInit(){
        this.onChangeContent(this.badgeActive);
    }

    onLoadBadge(){
        this.spinner.show();
        const params = {
            limit: this.searchData.pageSize,
            page: this.searchData.pageIndex,
            keyword: this.searchData.search,
            sortcolumn: this.searchData.sortColumn,
            sorttype: this.searchData.sortType,
            excludelevelid: this.id,
            type: this.badgeActive == 1 ? 1 : 0 
        };
        this.badgeService.getBadgeByLevel(params).subscribe(data => {
            if(data && data.body){
                this.dataTable = data.body.results;
            }
            this.totalRecord = data.body.total;
            this.spinner.hide();
        }, error => {
            this.spinner.hide();
        });
    }

    onChangeContent(id){
        this.badgeActive = id;
        this.onLoadBadge();
        this.selectedRow = 0;
        this.selectedIds = [];
        this.completeAll = false;
    }

    submitForm(){
        this.spinner.show();
        const params = {
            levelid: this.id,
            badgeids: this.selectedIds.toString().replace(/\,$/,'')
        };
        this.badgeService.assignBadgeToLevel(params).subscribe(data => {
            this.onChangeContent(this.badgeActive);
            this.toastrService.success(`common.noti.update_success`);
            this.spinner.hide();
        }, err => {
            this.spinner.hide();
            this.toastrService.handlerError(err);
        });
    }

    closeForm(){
        this.activeModal.close('close');
    }

    clear(){
        this.searchData.search = '';
        this.searchData.pageSize = 5;
        this.searchData.pageIndex = 1;
        this.onChangeContent(this.badgeActive);
    }
    
    sortData(sort: Sort) {
        this.searchData.pageIndex = 1;
        this.searchData.sortColumn = sort.active;
        this.searchData.sortType = sort.direction;
        this.onChangeContent(this.badgeActive);
    }
    
    changePage(event) {
        this.searchData.pageIndex = event.pageIndex + 1;
        this.searchData.pageSize = event.pageSize;
        this.onChangeContent(this.badgeActive);
    }
    
    onChangeKeyWord() {
        this.searchData.pageIndex = 1;
        this.onChangeContent(this.badgeActive);
    }

    oncheckboxAll(event: any){
        this.selectedIds = new Array();
        if(this.completeAll){
            this.dataTable.forEach(t => {
                var a = t['id'];
                var e = this.selectedIds.includes(a);
                if(!e)
                    this.selectedIds.push(a);
            });
        }
        this.completeData();
        this.selectedRow = this.selectedIds.length;
    }

    oncheckboxItem(ids: any, event: any){
        var e = this.selectedIds.includes(ids);
        if(e){
            this.selectedIds.splice(this.selectedIds.indexOf(ids), 1);
        }else{
            this.selectedIds.push(ids);
        }
        this.completeData();
        this.selectedRow = this.selectedIds.length;
        this.completeAll = (this.selectedIds.length == this.dataTable.length);
    }

    completeData(){
        this.dataTable.forEach((t,i) => {
            var a = t["id"];
            var e = this.selectedIds.includes(a);
            this.dataTable[i]["completed"] = (e);
        });
    }

    limitString(str: string){
        return CommonUtil.limitWord(str, 30);
      }
      
      getDateFromUnix(date) {
        if (date) {
          return moment.unix(date);
        }
        return null;
      }
    
      convertDays(sec: any) {
        if (sec !== null) {
          return Math.floor(sec / 86400);
        }
        return '';
      }
    
      getStatusBadge(status: number) {
        const statusBadge = STATUS_BADGES.find(s => s.statusId === status) || STATUS_BADGES[0];
        return statusBadge;
      }
}
