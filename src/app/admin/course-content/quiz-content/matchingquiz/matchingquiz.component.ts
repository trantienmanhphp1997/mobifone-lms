import { Component, OnInit } from '@angular/core';
import * as ClassicEditor from 'src/assets/ckeditor5-build-classic';
import {CKEDITOR_CONFIG} from '../../../../shared/constants/ckeditor.constant';

@Component({
  selector: 'app-matchingquiz',
  templateUrl: './matchingquiz.component.html',
  styleUrls: ['./matchingquiz.component.css']
})
export class MatchingquizComponent implements OnInit {
  public Editor = ClassicEditor;

  editorConfig = CKEDITOR_CONFIG.NOT_UPLOAD;
  constructor() { }

  ngOnInit(): void {
  }

}
