export interface TopicModel{
  hiddenbynumsections?: number;
  id: number;
  name?: number;
  section?: number;
  modules?: any[];
  summary?: string;
  summaryformat?: number;
  uservisible?: boolean;
  visible?: number;
  modname?: string;
  checked?: boolean;
  checkedMany?: boolean;
  listquiz?: any[];
}
