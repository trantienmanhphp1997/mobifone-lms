import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';;
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Course } from 'src/app/shared/model/course.model';
import { CourseService } from '../../../shared/services/course.service';
import { CourseQrcodeDetailComponent } from './course-qrcode-detail/course-qrcode-detail.component';
@Component({
  selector: 'app-course-qrcode',
  templateUrl: './course-qrcode.component.html',
  styleUrls: ['./course-qrcode.component.css']
})
export class CourseQrcodeComponent implements OnInit {
  shortname: any;
  categoryname: any;
  qrcodeid:any;
  date = new Date();
  @Input() course: any;
  constructor(
    public activeModal: NgbActiveModal,
    private courseService: CourseService,
    private modalService: NgbModal,
  ) { }
  openCourseDetailQR(course?: any){
    this.activeModal.dismiss();
    const modalDep = this.modalService.open(CourseQrcodeDetailComponent, {
      size: 'xs',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.course = course;
    modalDep.componentInstance.qrcodeid= this.qrcodeid;
    modalDep.result.then((result) => {
      if (result) {}
    });
  }
  ngOnInit(): void {
    this.createQRcodeInfo();
  }

  createQRcodeInfo(){
    this.courseService.QRcode(this.course.id).subscribe(data => {
      this.qrcodeid = data.body.id
    });
  }

}
