import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalStorageService } from 'ngx-webstorage';
import { USER_INFO } from 'src/app/shared/constants/base.constant';
import { CourseService } from 'src/app/shared/services/course.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';

@Component({
  selector: 'app-course-assessment-detail',
  templateUrl: './course-assessment-detail.component.html',
  styleUrls: ['./course-assessment-detail.component.css'],
})
export class CourseAssessmentDetailComponent implements OnInit {
  IsmodelShow: boolean;
  @Input() courseId;
  @Input() user;
  @Output() emitService: EventEmitter<any> = new EventEmitter();
  courseInfo: any;
  fullname = null;
  checkstudentcanwatchevaluation: boolean = true;
  evaluationInfo3M: any = [];
  st3M: any;
  evaluationInfo6M: any = [];
  st6M: any;
  listCommitmentByUser:any =[];
  listCommitmentThreeMonth: any = [];
  listCommitmentSixMonth: any = [];

  constructor(
    public activeModal: NgbActiveModal,
    private spinner: NgxSpinnerService,
    private courseService: CourseService,
    private toartService: ToastrCustomService
  ) {}

  ngOnInit(): void {
    var string = this.user.fullname;
    this.fullname = string.slice(0, string.search('-'));
    this.listCourseBeAsssessed();
    this.getDetailEvaluationEndUser();
    this.getListCommitmentActionCourseByUser();
  }

  close() {
    this.IsmodelShow = true;
  }
  getDetailEvaluationEndUser() {
    this.spinner.show();
    this.courseService
      .getDetailEvaluationEndUser(this.courseId, 3, this.user.id, 2)
      .subscribe(
        (res) => {
          this.spinner.hide();
          this.evaluationInfo3M = res.body;
          this.evaluationInfo3M?.checkstudentcanwatchevaluation == 1
            ? (this.checkstudentcanwatchevaluation = true)
            : (this.checkstudentcanwatchevaluation = false);
          this.st3M = this.evaluationInfo3M.criterias.map((c: any) => {
            return { criteriaid: c.id, status: c.status || 0 };
          });
        },
        (err) => {
          this.spinner.hide();
          this.toartService.handlerError(err);
        }
      );
    //6 month
    this.courseService
      .getDetailEvaluationEndUser(this.courseId, 6, this.user.id, 2)
      .subscribe(
        (res) => {
          this.spinner.hide();
          this.evaluationInfo6M = res.body;
          this.evaluationInfo6M?.checkstudentcanwatchevaluation == 1
            ? (this.checkstudentcanwatchevaluation = true)
            : (this.checkstudentcanwatchevaluation = false);
          this.st6M = this.evaluationInfo6M.criterias.map((c: any) => {
            return { criteriaid: c.id, status: c.status || 0 };
          });
        },
        (err) => {
          this.spinner.hide();
          this.toartService.handlerError(err);
        }
      );
  }
  changeEvaluation(id, $event, type) {
    if (type == 3) {
      this.st3M = this.st3M.map((c) => {
        if (c.criteriaid == id) {
          return {
            criteriaid: c.criteriaid,
            status: parseInt($event.target.value),
          };
        } else {
          return { criteriaid: c.criteriaid, status: c.status };
        }
      });
    } else if (type == 6) {
      this.st6M = this.st6M.map((c) => {
        if (c.criteriaid == id) {
          return {
            criteriaid: c.criteriaid,
            status: parseInt($event.target.value),
          };
        } else {
          return { criteriaid: c.criteriaid, status: c.status };
        }
      });
    }
  }
  evaluatationUser(type) {
    this.spinner.show();
    if (
      type == 3 &&
      this.evaluationInfo3M.criterias.length !=
        this.st3M.filter((e) => e.status !== 0).length
    ) {
      this.spinner.hide();
      this.toartService.error('Bạn chưa đánh giá đủ theo các tiêu chí');
      return;
    } else if (
      type == 6 &&
      this.evaluationInfo6M.criterias.length !=
        this.st6M.filter((e) => e.status !== 0).length
    ) {
      this.spinner.hide();
      this.toartService.error('Bạn chưa đánh giá đủ theo các tiêu chí');
      return;
    }
    const evaluation = {
      courseid: this.courseInfo.id,
      userids: this.user.id,
      criteria: type == 3 ? this.st3M : this.st6M,
      type: type,
      checkstudentcanwatchevaluation:
        this.checkstudentcanwatchevaluation == true ? 1 : 0,
    };
    this.courseService.evaluatationUser(evaluation).subscribe(
      (res) => {
        this.spinner.hide();
        this.listCourseBeAsssessed();
        this.emitService.next(null);
        this.toartService.success('Đánh giá thành công');
      },
      (err) => {
        this.spinner.hide();
        this.toartService.handlerError(err);
      }
    );
  }
  listCourseBeAsssessed() {
    const params = {
      userid: this.user.id,
    };
    this.courseService.getListCourseEvaluation(params).subscribe(
      (res) => {
        this.courseInfo = res.body.results.find((x) => x.id == this.courseId);
      },
      (err) => {
        this.toartService.handlerError(err);
      }
    );
  }

  getListCommitmentActionCourseByUser() {
    const param = {
      courseid: this.courseId,
      userid: this.user.id,
    };

    this.courseService.getListCommitment(param).subscribe((data) => {
      this.listCommitmentByUser = data.body;
      this.listCommitmentByUser['results'].forEach(
        (item) => {
          if(item.completetime === 3) {
            this.listCommitmentThreeMonth.push(item);
          } else {
            this.listCommitmentSixMonth.push(item);
          }
        }
      )
      }, err => {
        // this.toastrService.handlerError(err);
      }
    );
  }

  changeStudentWatch(event: any) {
    this.checkstudentcanwatchevaluation = event.checked;
  }
}
