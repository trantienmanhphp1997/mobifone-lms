import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AbstractControl, FormBuilder, Validators} from '@angular/forms';
import {ToastrCustomService} from '../../../shared/services/toastr-custom.service';
import CommonUtil from '../../../shared/utils/common-util';
import {PositionService} from '../../../shared/services/position.service';
import {Position} from '../../../shared/model/position.model';
import {CustomValidators} from '../../../shared/utils/custom-validators';
import { MatRadioChange } from '@angular/material/radio';
import * as moment from 'moment';

@Component({
  selector: 'app-course-category-edit',
  templateUrl: './position-edit.component.html',
  styleUrls: ['../position.component.css']
})
export class PositionEditComponent implements OnInit {

  @Input() positions: Position[];
  // @Input() courseCategoryList: CourseCategory[];
  @Input() position: Position;

  @Output() newPosition = new EventEmitter<any>();
  isEnableEvaluation: any = 0;
  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private modalService: NgbModal,
    private toastrService: ToastrCustomService,
    private programPositionService: PositionService
  ) {
  }

  validateCodePos = true;
  positionsName: any;
  editProgramForm = this.fb.group({
  name: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(100)]],
  description: [''],
  code: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(50)]],
  enableevaluation: 0,
  completeTime: ['', Validators.required],
  completedat: ['']
  });
  positionClone: Position;

  titleForm = 'common.update';
  currentDate: Date = new Date();

  ngOnInit(): void {
    this.positionClone = Object.assign({}, this.position);
    this.positionClone = this.position.enableevaluation ? {...this.position} : {...this.position, enableevaluation: 0}
    // this.fetch();
    if (this.position.id === undefined) {
      this.titleForm = 'position.create_title';
    } else {
      if (this.position.completedat) {
        const completedat = moment.unix(this.position.completedat).toDate();
        this.editProgramForm.patchValue({
          completeTime: completedat,
        })
      }
    }
  }

  mustTypeIdDepartment() {
    const code =  this.positionClone.code;
    if (code === '') {
      this.validateCodePos = true;
    } else if (!code.match(/^[A-Za-z0-9-_]+$/g)){
       this.validateCodePos = false;
     } else {
      this.validateCodePos = true;
    }
  }

  editProgram() {
    if (this.editProgramForm.invalid) {
      // Danh dau la da cham de hien thi message loi
      CommonUtil.markFormGroupTouched(this.editProgramForm);
      return;
    }
    if (this.validateCodePos === false){
      return;
    }
    this.editProgramForm.value.id = this.positionClone.id;

    if (this.editProgramForm.value.completeTime) {
      this.editProgramForm.patchValue({completedat: CommonUtil.convertDateToTime(this.editProgramForm.value.completeTime, 0, 0)})
    }

    if (this.positionClone.id !== undefined) {
      this.programPositionService.updatePosition(this.editProgramForm.value).subscribe(res => {
          this.toastrService.success(`common.noti.update_success`);
          this.newPosition.emit(this.positionClone.id);
          this.modalService.dismissAll();
        }, err => {
          this.toastrService.handlerError(err);
        }
      );
    } else {
      this.programPositionService.createPosition(this.editProgramForm.value).subscribe(res => {
          this.newPosition.emit(res.body.id);
          this.toastrService.success(`common.noti.create_success`);
          this.modalService.dismissAll();
        }, err => {
          this.toastrService.handlerError(err);
        }
      )
      ;
    }
  }
  enableEvaluation (event: MatRadioChange){
    this.isEnableEvaluation = event.value;
  }
}
