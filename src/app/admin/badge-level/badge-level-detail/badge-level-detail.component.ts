import {BadgeService} from '../../../shared/services/badge.service';
import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {BadgeCategory} from '../../../shared/model/badge-category.model';
import {NgxSpinnerService} from 'ngx-spinner';
import {ToastrCustomService} from 'src/app/shared/services/toastr-custom.service';
import { BadgeCategoryService } from 'src/app/shared/services/badge-category.service';

@Component({
  selector: 'app-level-badge-detail',
  templateUrl: './badge-level-detail.component.html',
  styleUrls: ['./badge-level-detail.component.css']
})
export class BadgeLevelDetailComponent implements OnInit {
  id: any;
  selectedId:any;
  constructor(
    private _Activatedroute: ActivatedRoute,
    private categoryService: BadgeCategoryService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
  ) {
  }

  ngOnInit(): void {
      this._Activatedroute.paramMap.subscribe(params => {
         this.id = params.get('id');
      });
   }
   selectedIndex(value: number){
     this.selectedId = value;
   }
}
