import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { StaffAssessmentService } from 'src/app/shared/services/staff-assessment.service';
import { EvaluationPopupComponent } from './evaluation-popup/evaluation-popup.component';

@Component({
    selector: 'app-name',
    templateUrl: './staff-assessment.component.html',
    styleUrls: ['./staff-assessment.component.css']
})
export class StaffAssessmentComponent implements OnInit {
    listStudent: any = [];
    totalRecord: number;
    pageSize = 10;
    pageIndex = 1;
    pageSizeOptions: number[] = [10, 25, 50, 100];
    keyword: string = null
    status: any;
    constructor(
        private staffAssessmentService: StaffAssessmentService,
        private modalService: NgbModal,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit(): void {
        this.getListStdCompleteOB();
    }

    getListStdCompleteOB() {
        this.spinner.show();
        const params = {
            limit: this.pageSize,
            page: this.pageIndex,
            search: this.keyword,
            status: this.status,
        }
        this.staffAssessmentService.getListStdCplteOnB(params).subscribe(res => {
            this.listStudent = res.body.results
            this.totalRecord = res.body.total
            this.spinner.hide();
        },
            err => {
                this.spinner.hide();
            })
    }

    openEvaluationForm(std: any) {
        const modalDep = this.modalService.open(EvaluationPopupComponent, {
            size: 'xl',
            centered: true,
            backdrop: 'static'
        });

        modalDep.componentInstance.student = std;
        //   modalDep.componentInstance.body = this.translateService.instant('badge.unassign_course_confirm_content');
        //   modalDep.componentInstance.confirmButton = this.translateService.instant('common.unassign');

        modalDep.componentInstance.newStaffAss.subscribe(($e) => {
            this.getListStdCompleteOB();
        });
    }
    changePage(event: any) {
        this.pageIndex = event.pageIndex + 1;
        this.pageSize = event.pageSize;
        this.getListStdCompleteOB();
    }
    onChangeSearch(e: any) {
        this.pageIndex = 1;
        this.getListStdCompleteOB();
    }
    onChangeStatus(e: any) {
        this.status = e.target.value;
        this.getListStdCompleteOB();
    }
}
