import { ToastrCustomService } from './../../shared/services/toastr-custom.service';
import { ReportService } from '../../shared/services/report.service';
import {Component, AfterViewInit, NgZone, OnInit, ViewChild} from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { Label } from 'ng2-charts';
import * as pluginDataLabels from 'chartjs-plugin-datalabels';
import { QUARTER } from 'src/app/shared/constants/base.constant';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatInput } from '@angular/material/input';
import { MonthPickerComponent } from 'src/app/shared/month-picker-component/month-picker.component';
import { DisabledTimeFn, DisabledTimePartial } from 'ng-zorro-antd/date-picker';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements AfterViewInit, OnInit {
  @ViewChild('fromInput', {
    read: MatInput
  }) fromInput: MatInput;
  @ViewChild('toInput', {
    read: MatInput
  }) toInput: MatInput;
  @ViewChild('fromInputDV', {
    read: MatInput
  }) fromInputDV: MatInput;
  @ViewChild('toInputDV', {
    read: MatInput
  }) toInputDV: MatInput;
  @ViewChild('fromInputGV', {
    read: MatInput
  }) fromInputGV: MatInput;
  @ViewChild('toInputGV', {
    read: MatInput
  }) toInputGV: MatInput;
  @ViewChild('defaultYear', {
    read: MatInput
  }) defaultYear: MonthPickerComponent;
  public activeIndex: any = 0;
  inputYear: 2022;
  overviewSystem: any;
  issearchbydate: any;
  barChartData = [
    {data: [0, 0, 0], label: 'Số khóa học'},
    {data: [0, 0, 0], label: 'Số học viên'},
  ];
  currentDate: Date = new Date();
  minDate: Date = new Date();
  searchTeacher = {
    teacherids: null,
    departmentIds: null,
    departmentIdsDV: null,
    categoryIds: null,
    startDate: null,
    endDate: null,
    startDateGV: null,
    endDateGV: null,
    startDateDV: null,
    endDateDV: null,
   };
  validYearDV: Date;
  form: FormGroup = new FormGroup({});
  formQuarter: FormGroup = new FormGroup({});
  formYear: FormGroup = new FormGroup({});
  searchQuarter = 0 ;
  searchYear: any;
  quarterData = QUARTER;
  filterOption: any;

  searchFormDV = this.fb.group({
    startDateDV: [],
    endDateDV: [],
  });

  barChartData_exam = [
    {data: [150, 110, 150], label: 'Số học viên đạt'},
    {data: [80, 250, 100], label: 'Số học viên không đạt'},
    {data: [0, 0, 0], label: ''}
  ];

  public barChartOptions = {
    scaleShowVerticalLines: true,
    responsive: true
  };

  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'left',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };

  public barChartLabels = [];
  public barChartType = 'bar';
  public barChartLegend = true;
  public pieChartLabels: Label[] = [];
  public pieChartData: number[] = [];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartPlugins = [];
  public pieChartColors: any[];
  yearFormat = 'yyyy';
  date1 = null;
  resultYear: any;
  public isCheckDataPieChart: boolean = true;

  dropdownRoleSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Chọn tất cả',
    unSelectAllText: 'Bỏ chọn tất cả',
    searchPlaceholderText: 'Tìm kiếm',
    itemsShowLimit: 5,
    allowSearchFilter: true
  };
  selectedDepartment = [];
  listDepartment = [];
  isCheckDeparmentEvent: boolean = false;
  departmentids = [];

  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    // console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    // console.log(event, active);
  }

 
    // Can not select days before today and today
   
  
  initForm(): void {
    this.form = this.fb.group({
      precious: '',
    });
    this.formQuarter = this.fb.group({
      precious: '',
      yearQuarterSelected: '',
    });
  }

  constructor(
    private fb: FormBuilder,
    private ngZone: NgZone,
    private reportService: ReportService,
    private toastrService: ToastrCustomService,
    private departmentService: DepartmentService,
    
  ) {}

  ngOnInit(): void {
    this.onLoadDepartment();
    this.reportCourseInSystem(0);  // category id = 0 => get all
    this.reportOverviewSystemData();
    this.initForm();
    this.filterOption = '';
  }


  disabledDate = (current: Date): void => {
   
  }

  onChangeYear(result: Date): void {
    this.resultYear = result;
  }

  onChangeYearWidthQuarter(event) {
    console.log('event: ' + event);
    
    this.searchYear = event.getFullYear();
    if (this.departmentids.length > 0) {
      this.reportCourseInSystemDepartment(0);
      this.reportOverviewSystemDataDepartment();
    } else {
      this.reportCourseInSystem(0);
      this.issearchbydate = 0;
      this.reportOverviewSystemData();
    }
  }

  /**
   * @method ngAfterViewInit
   */
  ngAfterViewInit() {
  }

  reportCourseInSystem(categoryId: number) {
    this.reportService.reportCourseInSystem(1, categoryId, this.searchYear, this.searchQuarter).subscribe(
      res => {
      this.barChartLabels = [];
      for (let i = 0; i < res.body.length; i++) {
        this.barChartLabels.push(res.body[i].year);
        this.barChartData[0].data[i] = res.body[i].result.totalcourse;
        this.barChartData[1].data[i] = res.body[i].result.totalstudent;
      }
    },
    err => {
      this.toastrService.handlerError(err);
    });
  }

  _parseInt(val: any) {
    return parseInt(val);
  }

  reportOverviewSystemData(){
    if (this.issearchbydate === 1){
      if (this.searchTeacher.endDateDV != null){
        const fromdate = Number(this.searchTeacher.startDateDV);
        const todate = Number( this.searchTeacher.endDateDV);
        this.reportService.overviewSystemData2(fromdate, todate, this.issearchbydate).subscribe(
          data => {
            this.overviewSystem = data.body;
            this.pieChartLabels = ['Học viên đạt', 'Học viên không đạt'];
            this.pieChartData = [this.overviewSystem.completedstudent, this.overviewSystem.uncompletedstudent];
            if (this.pieChartData[0] === 0 && this.pieChartData[1] === 0) {
              this.isCheckDataPieChart = false;
            } else {
              this.isCheckDataPieChart = true;
              this.issearchbydate = 0;
            }
            this.pieChartPlugins = [pluginDataLabels];
            this.pieChartColors =
            [
              {
                backgroundColor: [
                  '#86c7f3',
                  '#ffa1b5'
                ]
              }
            ];
        },
        err => {
          this.toastrService.handlerError(err);
        });
      }     
      
    }else {
      this.issearchbydate = 0;
      this.reportService.overviewSystemData(this.searchYear, this.searchQuarter, this.issearchbydate).subscribe(
        data => {
          this.overviewSystem = data.body;
          this.pieChartLabels = ['Học viên đạt', 'Học viên không đạt'];
          this.pieChartData = [this.overviewSystem.completedstudent, this.overviewSystem.uncompletedstudent];
          if (this.pieChartData[0] === 0 && this.pieChartData[1] === 0) {
            this.isCheckDataPieChart = false;
          } else {
            this.isCheckDataPieChart = true;
            this.issearchbydate = 0;
          }
          this.pieChartPlugins = [pluginDataLabels];
          this.pieChartColors =
          [
            {
              backgroundColor: [
                '#86c7f3',
                '#ffa1b5'
              ]
            }
          ];
      },
      err => {
        this.toastrService.handlerError(err);
      });
    }

    
  }
  yearSelected(event) {
    this.searchYear = event.getFullYear();
    if (this.departmentids.length > 0) {
      this.reportCourseInSystemDepartment(0);
      this.reportOverviewSystemDataDepartment();
    } else {
      this.reportCourseInSystem(0);
      this.issearchbydate = 0;
      this.reportOverviewSystemData();
    }
    
  }

  clearChangeYearOrQuarter(){
    this.searchTeacher.endDateDV = null;
    this.toInputDV.value = null;
    this.searchTeacher.startDateDV = null;
    this.fromInputDV.value = null;
  }

  clearDateDV(e: any, type: any) {
    e.stopPropagation();
    switch (type) {
      case 'endDateDV':
        this.searchTeacher.endDateDV = null;
        this.toInputDV.value = null;
        break;
      case 'startDateDV':
        this.searchTeacher.startDateDV = null;
        this.fromInputDV.value = null;
        break;
      default:
        break;
    }
    // this.getListTeacherInterDV();
  }

  changeStartDateDV(e: any) {
    this.searchTeacher.startDateDV = Math.round(((this.searchFormDV.value.startDateDV).getTime()) / 1000) ;
    const yearStartDateDV = new Date(this.searchTeacher.startDateDV).getFullYear();
    this.validYearDV = new Date(yearStartDateDV, 12, 0, 23, 59, 59);
    const yearEndDateDV = new Date(this.searchTeacher.endDateDV).getFullYear();
    if (yearStartDateDV !== yearEndDateDV) {
      this.toInputDV.value = '';
      this.searchTeacher.endDateDV = null;
      this.searchFormDV.value.endDateDV = null;
    }
    this.issearchbydate = 1; 
    this.reportOverviewSystemData();
  
  }

  changeEndDateDV(e: any) {
    this.searchTeacher.endDateDV = Math.round(((this.searchFormDV.value.endDateDV).getTime()) / 1000);
    this.issearchbydate = 1; 
    this.reportOverviewSystemData();
  }

  onChangeFilter(event) {
    this.filterOption = event.target.value;
    if (event.target.value == 1) {
      this.formQuarter.reset();
      this.searchQuarter = 0;
      this.searchYear = null;
    }else if(event.target.value == '') {
      this.formQuarter.reset();
      this.searchYear = null;
    }
  }

  onChange(event){
    this.searchQuarter = event.target.value;
    if (this.departmentids.length > 0) {
      this.reportOverviewSystemDataDepartment();
      this.reportCourseInSystemDepartment(0);
    } else {
      this.reportCourseInSystem(0);
      this.issearchbydate = 0;
      this.reportOverviewSystemData();
    }
    this.clearChangeYearOrQuarter();
  }

  clearQuarterAndChange = () => {
    this.form.reset();
    
  } 

  onSelectDepartmentEvent(): void {
    //Select
    this.isCheckDeparmentEvent = true;
  }

  changeDepartment(): void {
    //Filter cuoi
    if (this.isCheckDeparmentEvent) {
      this.isCheckDeparmentEvent = false;
      this.departmentids = [];
      this.selectedDepartment.forEach((item) => {
        this.departmentids.push(item.id);
      });
      
      if (this.departmentids.length > 0) {
        this.reportCourseInSystemDepartment(0);
        this.reportOverviewSystemDataDepartment();
      } else {
        this.reportCourseInSystem(0);
        this.reportOverviewSystemData();
      }
    }
    this.isCheckDeparmentEvent = false;
  }

  onLoadDepartment(): void {
    this.departmentService.getUserDepartmentTreeDashboard(0, 1).subscribe((res) => {
      this.listDepartment = res.body;
    });
  }

  reportCourseInSystemDepartment(categoryId: number) {
    this.reportService.reportCourseInSystemDeparment(1, categoryId, this.departmentids.toString(), this.searchYear).subscribe(
      res => {
      this.barChartLabels = [];
      for (let i = 0; i < res.body.length; i++) {
        this.barChartLabels.push(res.body[i].year);
        this.barChartData[0].data[i] = res.body[i].result.totalcourse;
        this.barChartData[1].data[i] = res.body[i].result.totalstudent;
      }
    },
    err => {
      this.toastrService.handlerError(err);
    });
  }


  reportOverviewSystemDataDepartment(){
    this.reportService.overviewSystemDataDeparment(this.searchYear, this.searchQuarter, this.departmentids.toString()).subscribe(
      data => {
        this.overviewSystem = data.body;
        this.pieChartLabels = ['Học viên đạt', 'Học viên không đạt'];
        this.pieChartData = [this.overviewSystem.completedstudent, this.overviewSystem.uncompletedstudent];
        if (this.pieChartData[0] === 0 && this.pieChartData[1] === 0) {
          this.isCheckDataPieChart = false;
        } else {
          this.isCheckDataPieChart = true;
        }
        this.pieChartPlugins = [pluginDataLabels];
        this.pieChartColors =
        [
          {
            backgroundColor: [
              '#86c7f3',
              '#ffa1b5'
            ]
          }
        ];
    },
    err => {
      this.toastrService.handlerError(err);
    });
  }
}
