import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {HTTP_INTERCEPTORS, HttpClient, HttpClientModule} from '@angular/common/http';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {ToastrModule} from 'ngx-toastr';
import {NgxWebstorageModule} from 'ngx-webstorage';
import {ToastrCustomService} from './shared/services/toastr-custom.service';
import {HttpErrorInterceptor} from './shared/interceptor/http-error-interceptor.service';
import {LoginComponent} from './login/login.component';
import {NgSelectModule} from '@ng-select/ng-select';
import {CommonModule, registerLocaleData} from '@angular/common';
import {AdminModule} from './admin/admin.module';
import {EndUserModule} from './end-user/end-user.module';
import {NgxSpinnerModule} from 'ngx-spinner';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {ConfirmModalComponent} from './shared/modal/confirm-modal/confirm-modal.component';
import {MatListModule} from '@angular/material/list';
import {MAT_DATE_LOCALE} from '@angular/material/core';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatExpansionModule} from '@angular/material/expansion';
import {ResetPasswordComponent} from './reset-password/reset-password.component';
import {InfoModalComponent} from './shared/modal/info-modal/info-modal.component';
import { BbbLogoutComponent } from './bbb-logout/bbb-logout.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import {MatRadioModule} from '@angular/material/radio';
import { ExportAsModule } from 'ngx-export-as';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { vi_VN } from 'ng-zorro-antd/i18n';
import vi from '@angular/common/locales/vi';
import { NzDatePickerModule } from 'ng-zorro-antd';

registerLocaleData(vi);


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ResetPasswordComponent,
    ForgotPasswordComponent,
    ConfirmModalComponent,
    InfoModalComponent,
    BbbLogoutComponent,
  ],
  imports: [
    AdminModule,
    EndUserModule,
    CommonModule,
    BrowserModule,
    AppRoutingModule,
    NgSelectModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSpinnerModule,
    BrowserAnimationsModule,
    NgbModule,
    HttpClientModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatCheckboxModule,
    MatListModule,
    MatExpansionModule,
    ExportAsModule,
    MatListModule,
    NgxWebstorageModule.forRoot({prefix: 'lms', separator: '-'}),
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient],
      },
      useDefaultLang: true,
    }),
    ToastrModule.forRoot(),
    MatRadioModule,
    NzDatePickerModule
  ],
  providers: [
    ToastrCustomService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpErrorInterceptor,
      multi: true
    },
    { provide: MAT_DATE_LOCALE, useValue: 'vi-VN' },
    { provide: NZ_I18N, useValue: vi_VN }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, `./assets/i18n/`, '.json');
}
