import { Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { COST_SOURCE, DEFAULT_COURSE_IMAGE, LIST_HOURS, LIST_MINUTES, TRAINING_TYPE } from 'src/app/shared/constants/base.constant';
import { CourseCategory } from 'src/app/shared/model/course-category.model';
import { Course } from 'src/app/shared/model/course.model';
import { Position } from 'src/app/shared/model/position.model';
import { CourseCategoryService } from 'src/app/shared/services/course-category.service';
import { CourseService } from 'src/app/shared/services/course.service';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { FileService } from 'src/app/shared/services/file.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import CommonUtil from 'src/app/shared/utils/common-util';
import { CustomValidators } from 'src/app/shared/utils/custom-validators';
import { QUARTER } from 'src/app/shared/constants/base.constant';

@Component({
    selector: 'app-course-plan-detail',
    templateUrl: 'course-plan-detail.component.html',
    styleUrls: ['./course-plan-detail.component.css']
})

export class CoursePlanDetailComponent implements OnInit {
    @Input() courseClone: any;
    @Input() action: any;
    @Input() planId: any;
    @Input() canEdit: any;
    @Input() positionList: Position[] = [];
    @Output() loadAll = new EventEmitter();
    quarterData = QUARTER;
    isCreateExam = false;
    dropdownPositionSettings = {
        singleSelection: false,
        idField: 'id',
        searchPlaceholderText: 'Tìm kiếm',
        textField: 'name',
        selectAllText: 'Chọn hết',
        unSelectAllText: 'Bỏ chọn hết',
        itemsShowLimit: 5,
        allowSearchFilter: true
    };
    courseType = 1;
    listTrainingType = TRAINING_TYPE;
    courseid: null;
    listCostSource = COST_SOURCE;
    courseForm = this.fb.group({
        code: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(200)]],
        name: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(200)]],
        categoryid: [null, Validators.required],
        description: [''],
        trainingtype: [null, Validators.required],
        trainingobject: [],
        numberstudent: ['', [Validators.required,Validators.min(0)]],
        numberclass: ['',[Validators.min(0)]],
        location: [],
        courseid: null,
        cost: [, Validators.required],
        // startdate: [, Validators.required],
        // enddate: [, Validators.required],
        duration: ['',[Validators.min(0)]],
        timeexpect: [, [Validators.required, CustomValidators.isNumberic]],
        resource: [],
        // hourStartDate: ['', Validators.required],
        // minuteStartDate: ['', Validators.required],
        // hourEndDate: ['', Validators.required],
        // minuteEndDate: ['', Validators.required],
        selectedPositions: [],
        other: [''],
    });

    hoursList = LIST_HOURS;
    minutesList = LIST_MINUTES;
    courseCategoryList: CourseCategory[] = [];
    categoryMap = new Map();

    course: any = {
        name: '',
        startdate: null,
        enddate: null
    };
    courseDetail: any;
    courseFile: any;
    avatarCourseInvalid: any;
    avatarCourseErrorRequired: any;
    startTimeInvalid = false;
    endTimeInvalid = false;
    selectedPositions = [];
    selectedPositionIds = [];
    currentDate: Date = new Date();
    constructor(
        private fb: FormBuilder,
        private toastrService: ToastrCustomService,
        private courseService: CourseService,
        private courseCategoryService: CourseCategoryService,
        public activeModal: NgbActiveModal,
        private router: Router,
        private spinner: NgxSpinnerService,
        private departmentService: DepartmentService,
        private modalService: NgbModal,
    ) { }

    ngOnInit(): void {
        this.courseCategoryService.getCourseCategoryTree(null).subscribe(response => {
            this.courseCategoryList = response.body;
            CommonUtil.convertCategoryListToMap(this.courseCategoryList, this.categoryMap);
        });
        if (this.courseClone) {
            this.initFormCourseDetail(this.courseClone);
            this.action = 'update'
        }
        else {

        }
    }

    initFormCourseDetail(course: any) {
        // const startDate = moment.unix(course.startdate).toDate();
        // const endDate = moment.unix(course.enddate).toDate();
        this.courseid = course.courseid;
        this.courseForm.patchValue({
            code: course.code,
            name: course.name,
            courseid: course.courseid,
            description: course.description,
            duration: course.duration,
            timeexpect: course.timeexpect,
            // startdate: startDate,
            // hourStartDate: startDate.getHours(),
            // minuteStartDate: startDate.getMinutes(),
            // enddate: endDate,
            // hourEndDate: endDate.getHours(),
            // minuteEndDate: endDate.getMinutes(),
            requireenglish: course.requireenglish,
            categoryid: course.categoryid,
            trainingtype: course.trainingtype || 1,
            trainingobject: course.trainingobject,
            numberstudent: course.numberstudent,
            numberclass: course.numberclass,
            location: course.location,
            resource: course.resource,
            cost: CommonUtil.converCostWithDots(course.cost),
            other: course.otherresource,
        });
        if (course.trainingobject) {
            const positionIdArray = course.trainingobject.split(',');
            for (const positionId of positionIdArray) {
                const foundPosition = this.positionList.find(value => value.id === +positionId);
                if (foundPosition) {
                    this.selectedPositions.push(foundPosition);
                }
            }
            this.courseForm.patchValue({
                selectedPositions: this.selectedPositions,
            });
        } else {
            this.selectedPositions = [];
        }
        if (course.other == "OTHERS") {
            this.courseForm.controls['other'].setValidators([Validators.required]);
            this.courseForm.get('other').updateValueAndValidity();
        } else {
            this.courseForm.get('other').clearValidators();
        }
    }
    createNewCourse() {
        // Danh dau la da cham de hien thi message loi
        if (this.courseForm.invalid) {
            CommonUtil.markFormGroupTouched(this.courseForm);
            return;
        }

        //Set data to create/update
        this.course = {
            name: this.courseForm.value.name.trim(),
            planid: this.planId,
            categoryid: this.courseForm.value.categoryid,
            description: this.courseForm.value.description,
            duration: this.courseForm.value.duration,
            timeexpect: this.courseForm.value.timeexpect,
            trainingtype: this.courseForm.value.trainingtype,
            trainingobject: this.courseForm.value.selectedPositions?.map(e => e?.id).toString(),
            numberstudent: this.courseForm.value.numberstudent,
            numberclass: this.courseForm.value.numberclass,
            location: this.courseForm.value.location,
            cost: this.courseForm.value.cost.replace(/[^\d]/g, '') * 1,
            resource: this.courseForm.value.resource,
            otherresource: this.courseForm.value.resource == 'OTHERS' ? this.courseForm.value.other : null
        }
        // this.course.startdate = CommonUtil.convertDateToTime(this.courseForm.value.startdate,
        //     this.courseForm.value.hourStartDate, this.courseForm.value.minuteStartDate);

        // this.course.enddate = CommonUtil.convertDateToTime(this.courseForm.value.enddate,
        //     this.courseForm.value.hourEndDate, this.courseForm.value.minuteEndDate);

        // if (this.course.startdate < this.currentDate.getTime() / 1000) {
        //     this.startTimeInvalid = true;
        //     return;
        // } else {
        //     this.startTimeInvalid = false;
        // }

        // if (this.course.enddate < this.course.startdate) {
        //     this.endTimeInvalid = true;
        //     return;
        // } else {
        //     this.endTimeInvalid = false;
        // }

        this.spinner.show();

        if (this.courseClone) {
            this.course.proposecourseid = this.courseClone.id;
        }
        else {
            this.course.code = this.courseForm.value.code.trim();
        }

        this.courseService.createOrUpdateCourseRecommend(this.course).subscribe(res => {
            this.activeModal.dismiss();
            this.loadAll.emit('load');
            this.toastrService.success('Lưu thành công');
            this.spinner.hide();
        },
            err => {
                this.spinner.hide();
                this.toastrService.handlerError(err);
            }
        );
    }

    startDateChange($event) {
        if (this.courseForm.value.startdate) {
            this.course.startdate = CommonUtil.convertDateToTime(this.courseForm.value.startdate,
                this.courseForm.value.hourStartDate, this.courseForm.value.minuteStartDate);
        }
    }

    endDateChange($event) {
        if (this.courseForm.value.enddate) {
            this.course.enddate = CommonUtil.convertDateToTime(this.courseForm.value.enddate,
                this.courseForm.value.hourEndDate, this.courseForm.value.minuteEndDate);
        }
    }

    convertCost(value: any) {
        let price = value.replace(/[^\d]/g, '') * 1
        this.courseForm.patchValue({
            cost: Intl.NumberFormat('vi-VN').format(price)
        });
    }

    onChangePosition() {
        if (this.courseForm.get('selectedPositions').value) {
            if (this.courseForm.get('selectedPositions').value.length > 0) {
                this.selectedPositionIds = [];
                for (const position of this.courseForm.get('selectedPositions').value) {
                    if (position) {
                        this.selectedPositionIds.push(position.id);
   
                    }
                    console.log(this.selectedPositionIds);
                }
            } else {
                this.selectedPositionIds = [];
            }
        } else {
            this.selectedPositionIds = [];
        }
    }
    onChangeResourse($event) {
        if ($event.target.value == "OTHERS") {
            this.courseForm.controls['other'].setValidators([Validators.required]);
            this.courseForm.get('other').updateValueAndValidity();
        } else {
            this.courseForm.patchValue({
                other: null,
            })
            this.courseForm.get('other').clearValidators();
        }
    }
}