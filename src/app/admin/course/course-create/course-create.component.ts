import { DepartmentService } from '../../../shared/services/department.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { CourseCategoryService } from '../../../shared/services/course-category.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CourseService } from '../../../shared/services/course.service';
import { ToastrCustomService } from '../../../shared/services/toastr-custom.service';
import { CourseCategory } from '../../../shared/model/course-category.model';
import CommonUtil from '../../../shared/utils/common-util';
import { BADGE_TYPE, COST_SOURCE, DEFAULT_COURSE_IMAGE, LIST_HOURS, LIST_MINUTES, PLAN_ROLE, TRAINING_TYPE, USER_INFO, USER_ROLE } from '../../../shared/constants/base.constant';
import { Course } from '../../../shared/model/course.model';
import { CustomValidators } from '../../../shared/utils/custom-validators';
import * as moment from 'moment';
import { FileService } from 'src/app/shared/services/file.service';
import { ProcessCloneCourseComponent } from '../process-clone-course/process-clone-course.component';
import { TrainingPlanService } from 'src/app/shared/services/training-plan.service';
import { Position } from 'src/app/shared/model/position.model';
import { LocalStorageService } from 'ngx-webstorage';


@Component({
  selector: 'app-course-create',
  templateUrl: './course-create.component.html',
  styleUrls: ['./course-create.component.css']
})
export class CourseCreateComponent implements OnInit {
  @Input() courseClone: any;
  @Input() coursePlan: any;
  @Input() creFollwingPlan: any = false;
  @Input() positionList: Position[] = [];
  @Input() removePlan: any = false;
  isCreateExam = false;
  isCreatePlanCourse = false;
  checkTypeTrain = false;
  courseType = 1;
  maxCostPlan = 0;
  selectedPositions = [];
  selectedPositionIds = [];
  selectedBadgeIds=[];
  startdateClone = null;
  enddateClone = null;

  courseForm: FormGroup = new FormGroup({});
  badgeList: any;
  hoursList = LIST_HOURS;
  minutesList = LIST_MINUTES;
  courseCategoryList: CourseCategory[] = [];
  categoryMap = new Map();


  course: Course = {
    fullname: '',
    startdate: null,
    enddate: null,
    otherresource: null,
    badgerequire: null,
  };
  courseDetail: any;
  courseFile: any;
  avatarCourseInvalid: any;
  avatarCourseErrorRequired: any;
  startTimeInvalid = false;
  endTimeInvalid = false;
  startTimeCloneInvalid = false;
  dateTimeCloneInvalid = false;
  endTimeCloneInvalid = false;
  // creFollwingPlan = false;
  currentDate: Date = new Date();
  listTrainingType = TRAINING_TYPE;
  listCostSource = COST_SOURCE;
  listCoursePlan = [];
  planYear = '';
  showOpExamCheckbox = false
  dropdownPositionSettings = {
    singleSelection: false,
    idField: 'id',
    searchPlaceholderText: 'Tìm kiếm',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 5,
    allowSearchFilter: true
  };
  departmentMap = new Map();
  departmentId: any;
  isManagerNotRoot = false;
  isTeacher = false;
  isShowLocation = false;
  isShowCreateBadge = false;
  islimited = true;
  costInvalid = false;
  durationInvalid = false;
  disableButton = false;
  isOpen = false;
  originalCourseList = [];
  listTrainingPlan = [];
  isClass = false
  checkApplyTrainingPlan = false;
  disabledClass: boolean = false;
  disabledPlan: boolean = false;
  listTrainingPlanCourse: any = []
  fullname: string = '';
  maxCostValue: any;
  isCheckisforcepassexam = false;
  isCheckComplete : boolean = false;
  @ViewChild('imgPreview') imgPreview: ElementRef;
  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrCustomService,
    private courseService: CourseService,
    private courseCategoryService: CourseCategoryService,
    public activeModal: NgbActiveModal,
    private router: Router,
    private spinner: NgxSpinnerService,
    private departmentService: DepartmentService,
    private trainingPlanService: TrainingPlanService,
    private fileservice: FileService,
    private modalService: NgbModal,
    private $localStorage: LocalStorageService,
  ) { }

  ngOnInit(): void {
    this.courseForm = this.fb.group({
      fullname: [null, [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(200)]],
      categoryid: [null, Validators.required],
      summary: [''],
      requireenglish: [],
      isopen: [],
      startdate: [, Validators.required],
      enddate: [, Validators.required],
      hourStartDate: ['', Validators.required],
      minuteStartDate: ['', Validators.required],
      hourEndDate: ['', Validators.required],
      minuteEndDate: ['', Validators.required],
      iscreatebadge: [],
      location: [],
      trainingtype: [, Validators.required],
      trainingobject: [],
      numberstudent: ['', [Validators.required, Validators.maxLength(11), Validators.min(0)]],
      cost: ['', [Validators.required, Validators.maxLength(16)]],
      costDisplay: ['', [Validators.required, Validators.maxLength(16)]],
      resource: [null, Validators.required],
      hasquiz: [],
      selectedPositions: [, Validators.required],
      duration: ['',[Validators.required, Validators.min(0)]],
      planid: [],
      proposecourseid: [''],
      other:[''],
      badgerequire:[],
      istrainingforeign:[],
      isforcepassexam: [ ],
      completealllessions: [],
      isjoinsurvey: [],
      // departmentId: ['', Validators.required],
      classofcourseid: [],
      trainingplanid: [],
      isClass: [],
      checkApplyTrainingPlan: [],
    });

    this.courseForm.patchValue({
      completealllessions: true
    });
    if (this.isCreateExam) {
      this.courseType = 2;
      this.courseForm.controls['enddate'].setValidators([Validators.required]);
      this.courseForm.controls['enddate'].updateValueAndValidity();
      this.courseForm.controls['hourEndDate'].setValidators([Validators.required]);
      this.courseForm.controls['hourEndDate'].updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].setValidators([Validators.required]);
      this.courseForm.controls['minuteEndDate'].updateValueAndValidity();
      this.courseForm.get('numberstudent').clearValidators();
      this.courseForm.get('cost').clearValidators();
      this.courseForm.get('trainingobject').clearValidators();
      this.courseForm.get('trainingtype').clearValidators();
      this.courseForm.get('resource').clearValidators();
      this.courseForm.get('selectedPositions').clearValidators();
      this.courseForm.get('classofcourseid').clearValidators();
      this.courseForm.get('planid').clearValidators();
      this.courseForm.get('costDisplay').clearValidators();
      this.courseForm.get('duration').clearValidators();
    }
    this.courseCategoryService.getCourseCategoryTree(null).subscribe(response => {
      this.courseCategoryList = response.body;
      CommonUtil.convertCategoryCodeListToMap(this.courseCategoryList, this.categoryMap);
    });
    if (this.courseClone) {
      this.getInfoCourse(this.courseClone)
    }
    if (this.coursePlan) {
      this.isCreatePlanCourse = true;
      this.initFormCourseDetail(this.coursePlan);
    }
    this.isManagerNotRoot = this.$localStorage.retrieve(USER_INFO.INFO).planrole == PLAN_ROLE.CREATE;
    this.isTeacher = this.$localStorage.retrieve(USER_INFO.ROLES)[0].shortname == 'teacher'
    this.listAllTree()
    this.getListBadge()
    this.getListOriginalCourse()
    this.getListTrainingPlan()
  }
  getInfoCourse(courseId: any) {
    this.courseService.getCoursesInfo(courseId).subscribe(response => {
      this.courseDetail = response.body;
      let checkClass = false
      if (this.removePlan) {
        checkClass = this.courseDetail.planid ? true : false
        this.courseDetail.planid = null
        this.courseDetail.proposecourseid = null
        this.courseDetail.isclass = 0
        this.courseDetail.classofcourseid = null
      } else {
        this.fullname = this.courseDetail.fullname
        this.courseDetail.isclass = 0
        this.courseDetail.classofcourseid = null
      }
      this.isClass = checkClass == true ? false : (this.courseDetail.isclass == 1 ? true : false);
      this.checkApplyTrainingPlan = this.courseDetail.planid ? true : false;
      this.startdateClone = this.courseDetail.startdate;
      this.enddateClone = this.courseDetail.enddate;
      this.initFormCourseDetail(response.body);
    });
  }
  initFormCourseDetail(course: any) {
    this.maxCostPlan = course.cost;
    const cost = CommonUtil.converCostWithDots(course.cost);
    const startDate = course.startdate ? moment.unix(course.startdate).toDate() : null;
    const endDate = course.enddate ? moment.unix(course.enddate).toDate() : null;
    this.courseForm.patchValue({
      fullname: course?.fullname || course?.name,
      summary: course.summary,
      startdate: startDate || null,
      hourStartDate: startDate ? startDate.getHours() : '',
      minuteStartDate: startDate ? startDate.getMinutes() : '',
      enddate: endDate || null,
      hourEndDate: endDate ? endDate.getHours() : '',
      minuteEndDate: endDate ? endDate.getMinutes() : '',
      requireenglish: course.requireenglish || 0,
      categoryid: course.categoryid,
      isopen: course.isopen || 0,
      iscreatebadge: course.badgeid ? 1 : 0,
      trainingtype: course.trainingtype || 1,
      trainingobject: course.trainingobject,
      numberstudent: course.numberstudent,
      numberclass: course.numberclass,
      location: course.location,
      resource: course.resource,
      hasquiz: course.hasquiz || 0,
      cost: cost == '' ? 0 : cost,
      costDisplay: cost == '' ? 0 : cost,
      duration: course.duration,
      planid: course.planid,
      proposecourseid: course.id,
      other: course.otherresource,
      istrainingforeign: course.istrainingforeign ? 1 :  0,
      isforcepassexam: course.isforcepassexam ? 1 : 0,
      isjoinsurvey: course.isjoinsurvey ? 1 : 0,
      completealllessions: course.completealllessions ? 1 : 0,
      // departmentId: course.departmentid
      // departmentId: course.departmentid,
      classofcourseid: course.classofcourseid,
      isClass: course.isclass || 0,
      checkApplyTrainingPlan: course.planid || 0,
    });

    this.disabledPlan = course.planid != null ? true : false
    this.disabledClass = course.planid != null ? true : false
    this.showOpExamCheckbox = [TRAINING_TYPE[0].value, TRAINING_TYPE[1].value, TRAINING_TYPE[2].value].includes(course.trainingtype);
    this.isShowLocation = [TRAINING_TYPE[0].value, TRAINING_TYPE[1].value, TRAINING_TYPE[2].value].includes(course.trainingtype);
    this.isShowCreateBadge = ![TRAINING_TYPE[0].value, TRAINING_TYPE[1].value, TRAINING_TYPE[2].value].includes(course.trainingtype);
    if (course.trainingobject) {
      const positionIdArray = course.trainingobject.split(',');
      for (const positionId of positionIdArray) {
        const foundPosition = this.positionList.find(value => value.id === +positionId);
        this.selectedPositions.push(foundPosition);
      }
      this.courseForm.patchValue({
        selectedPositions: this.selectedPositions,
      });
    } else {
      this.selectedPositions = [];
    }

    if (!this.creFollwingPlan) {
      if (course.filename) {
        this.courseDetail.img = this.fileservice.getFileUrl(course.contextid, course.filename, '/course/overviewfiles/');
      } else {
        this.courseDetail.img = DEFAULT_COURSE_IMAGE;
      }
      this.imgPreview.nativeElement.src = this.courseDetail.img;
    }
    if (course.isopen) {
      this.courseForm.controls['numberstudent'].setValidators([Validators.required]);
      this.courseForm.get('numberstudent').updateValueAndValidity();
    }

    this.islimited = endDate ? true : false;
    if(!this.islimited){
      this.courseForm.get('enddate').clearValidators();
      this.courseForm.get('hourEndDate').clearValidators();
      this.courseForm.get('minuteEndDate').clearValidators();
      this.courseForm.controls['enddate'].updateValueAndValidity();
      this.courseForm.controls['hourEndDate'].updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].updateValueAndValidity();
      this.courseForm.patchValue({
        enddate: 0,
        hourEndDate: 0,
        minuteEndDate: 0,
      })
    }else{
      this.courseForm.controls['enddate'].setValidators([Validators.required]);
      this.courseForm.controls['enddate'].updateValueAndValidity();
      this.courseForm.controls['hourEndDate'].setValidators([Validators.required]);
      this.courseForm.controls['hourEndDate'].updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].setValidators([Validators.required]);
      this.courseForm.controls['minuteEndDate'].updateValueAndValidity();
    }

    if (course.trainingtype == "OFFI" || course.trainingtype == "OFFE" || course.trainingtype == "OFFP") {
      this.checkTypeTrain = true
    } else {
      this.checkTypeTrain = false
    }
  }
  createNewCourse() {
    // this.disableButton = true; // prevent double click
    this.currentDate = new Date();
    // Danh dau la da cham de hien thi message loi
    if( (this.courseForm.controls['completealllessions'].value || this.courseForm.controls['isforcepassexam'].value ||this.courseForm.controls['isjoinsurvey'].value)){
      this.isCheckComplete = false
    }
    else{
      this.isCheckComplete = true;
    }
    if (this.courseForm.invalid) {

      CommonUtil.markFormGroupTouched(this.courseForm);
      return;
    }

    if (!this.courseFile && !this.isCreateExam && !this.courseClone) {
      this.avatarCourseErrorRequired = true;
      return;
    } else {
      this.avatarCourseErrorRequired = false;
    }

    this.course.startdate = CommonUtil.convertDateToTime(this.courseForm.value.startdate,
      this.courseForm.value.hourStartDate, this.courseForm.value.minuteStartDate);
    this.course.enddate = this.courseForm.value.enddate ? CommonUtil.convertDateToTime(this.courseForm.value.enddate,
      this.courseForm.value.hourEndDate, this.courseForm.value.minuteEndDate) : null;

    if (this.course.startdate < this.currentDate.getTime() / 1000 && (this.courseForm.value.trainingtype =="ONLR" || this.courseForm.value.trainingtype =="ONLL")) {
      this.startTimeInvalid = true;
      return;
    } else {
      this.startTimeInvalid = false;
    }

    if (this.startdateClone) {
      if (this.course.startdate < this.startdateClone) {
        this.startTimeCloneInvalid = true;
        return;
      } else {
        this.startTimeCloneInvalid = false;
      }
    }

    if (this.enddateClone) {
      if (this.course.enddate < this.enddateClone) {
        this.endTimeCloneInvalid = true;
        return;
      } else {
        this.endTimeCloneInvalid = false;
      }
    }
    if (this.course.enddate < this.course.startdate) {
      if (this.isCreateExam) {
        this.endTimeInvalid = true;
        return;
      } else {
        if (this.courseForm.value.enddate || this.courseForm.value.hourEndDate || this.courseForm.value.minuteEndDate) {
          this.endTimeInvalid = true;
          return;
        } else {
          this.endTimeInvalid = false;
        }
      }

    } else {
      this.endTimeInvalid = false;
    }


    if (this.courseForm.value.enddate && this.enddateClone && this.startdateClone) {
      if (this.course.enddate - this.course.startdate < this.enddateClone - this.startdateClone) {
        this.dateTimeCloneInvalid = true;
        return;
      } else {
        this.dateTimeCloneInvalid = false;
      }
    }
    if (this.isCreatePlanCourse) {
      var cost = this.courseForm.value.cost.length > 3 ? this.courseForm.value.cost.replace(/[^\d]/g, '') * 1 : this.courseForm.value.cost;
      if (cost > this.coursePlan.cost) {
        this.costInvalid = true;
        return;
      } else {
        this.costInvalid = false;
      }
    }

    if (!this.isCreateExam && this.courseForm.value.enddate) {
      var date1 = new Date(this.courseForm.value.startdate);
	    var date2 = new Date(this.courseForm.value.enddate);
      if(this.courseForm.value.duration > Math.floor((date2.getTime()-date1.getTime())/(1000 * 60 * 60 * 24)) + 1){
        this.durationInvalid = true;
        return;
      } else {
        this.durationInvalid = false;
      }
    }



    // Set value
    this.course.fullname = this.courseForm.value.fullname.trim();
    this.course.categoryid = this.courseForm.value.categoryid;
    this.course.summary = this.courseForm.value.summary ? this.courseForm.value.summary.trim() : '';
    this.course.coursetype = this.courseType;
    this.course.otherresource = this.courseForm.value.resource == 'OTHERS'?this.courseForm.value.other:null;

    if (!this.isCreateExam) {
      this.course.requireenglish = +this.courseForm.value.requireenglish;
      this.course.isopen = +this.courseForm.value.isopen;
      this.course.hasquiz = +this.courseForm.value.hasquiz;
      this.course.trainingtype = this.courseForm.value.trainingtype;
      this.course.istrainingforeign = +this.courseForm.value.istrainingforeign;
      this.course.isforcepassexam = +this.courseForm.value.isforcepassexam;
      this.course.isjoinsurvey = +this.courseForm.value.isjoinsurvey;
      this.course.completealllessions = +this.courseForm.value.completealllessions;
      this.course.trainingobject = this.courseForm.value.selectedPositions?.map(e => e?.id).toString();
      this.course.numberstudent = this.courseForm.value.numberstudent.length > 3 ?
        this.courseForm.value.numberstudent.replace(/[^\d]/g, '') * 1 : this.courseForm.value.numberstudent;
      // this.course.numberclass = this.courseForm.value.numberclass;
      this.course.location = this.courseForm.value.location;
      this.course.resource = this.courseForm.value.resource;
      this.course.duration = this.courseForm.value.duration;
      this.course.cost = this.courseForm.value.cost.length > 3 ? this.courseForm.value.cost.replace(/[^\d]/g, '') * 1 : this.courseForm.value.cost;
      this.course.planid = this.courseForm.value.planid;
      this.course.proposecourseid = this.courseForm.value.proposecourseid;
      this.course.badgerequire = this.courseForm.value.badgerequire?.map(e => e?.id).toString();
      // this.course.departmentid = this.courseForm.value.departmentId;
      this.course.isclass = this.isClass ? 1 : 0;
      this.course.classofcourseid = this.courseForm.value.classofcourseid;
      this.course.planid = this.courseForm.value.planid;
      this.course.proposecourseid = this.courseForm.value.planid ? (this.courseForm.value.proposecourseid ?? '') : '';
      if (this.courseClone) this.course.firstownerdepartmentid = this.courseDetail.firstownerdepartmentid ?? '';
    }
    if (this.isTeacher) {
      this.course.departmentid = this.departmentId;
    }
    if (this.courseClone) {
      this.course.courseid = this.courseClone;
    }
    this.course.iscreatebadge = +this.courseForm.value.iscreatebadge;
    if( !this.isCheckComplete){
      this.spinner.show();
      this.courseService.createNewCourse(this.course, this.courseFile, this.isCreateExam).subscribe(res => {

        this.activeModal.dismiss();


        // this.disableButton = false; // prevent double click
        console.log(this.isCreateExam)
          if (this.isCreateExam) {
            this.toastrService.success('common.noti.create_success');
            this.router.navigate(['admin/exam/detail', res.body[0].id]);
          } else {
            this.toastrService.success('common.noti.create_success');
            this.router.navigate(['admin/course/detail', res.body[0].id]);
        }



        this.spinner.hide();
        // const departmentIds = [];
        // this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
        //   data.body.forEach(department => {
        //     departmentIds.push(department.id);
        //   });
        //   this.courseService.assignDepartmentPositionTocourse(
        //     res.body[0].id, departmentIds, []
        //   ).subscribe((data) => {
        //     if (this.isCreateExam) {
        //       this.router.navigate(['admin/exam/detail', res.body[0].id]);
        //     } else {
        //       this.router.navigate(['admin/course/detail', res.body[0].id]);
        //     }
        //     this.spinner.hide();
        //   },
        //   err => {
        //     this.spinner.hide();
        //     this.toastrService.handlerError(err);
        //   });
        // });
      },
        err => {
          // this.disableButton = false; // prevent double click
          this.spinner.hide();
          this.toastrService.handlerError(err);
        }
      );
    }
  }

  handleFileInput(files: FileList) {
    if (files.length > 0) {
      this.avatarCourseErrorRequired = false;
      if (files[0].type !== 'image/jpeg'
        && files[0].type !== 'image/jpg'
        && files[0].type !== 'image/png'
        && files[0].type !== 'image/bmp'
        && files[0].type !== 'image/gif'
        && files[0].type !== 'image/jfif') {
        this.avatarCourseInvalid = true;
      } else {
        this.courseFile = files[0];
        this.avatarCourseInvalid = false;
      }
    }
  }

  startDateChange($event) {
    // this.startTimeInvalid = false;
    this.dateTimeCloneInvalid = false;
    this.startTimeCloneInvalid = false;
    // if (this.courseForm.value.startdate) {
    //   this.course.startdate = CommonUtil.convertDateToTime(this.courseForm.value.startdate,
    //     this.courseForm.value.hourStartDate, this.courseForm.value.minuteStartDate);
    // }
  }

  endDateChange($event) {
    this.endTimeInvalid = false;
    this.dateTimeCloneInvalid = false;
    this.endTimeCloneInvalid = false;
    if (this.courseForm.value.endDate) {
      this.courseForm.controls['hourEndDate'].setValidators([Validators.required])
      this.courseForm.get('hourEndDate').updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].setValidators([Validators.required])
      this.courseForm.get('minuteEndDate').updateValueAndValidity();
    }
    // if (this.courseForm.value.enddate) {
    //   this.course.enddate = CommonUtil.convertDateToTime(this.courseForm.value.enddate,
    //     this.courseForm.value.hourEndDate, this.courseForm.value.minuteEndDate);
    // }
  }
  yearSelected(value: any) {
    this.planYear = value ? value[0] : null;
    if (this.planYear) {
      this.getCourseByPlan();
    }
    else {
      this.courseForm.reset();
    }
  }
  selectOptionCre(e: any) {
    if (e.checked) {
      this.creFollwingPlan = e.checked;
    }
    else {
      // this.planYear = null;
      this.creFollwingPlan = e.checked;
      this.courseForm.reset();
    }
  }

  selectOpByTrainingType($event) {
    if ($event.target.value == "OFFI" || $event.target.value == "OFFE" || $event.target.value == "OFFP") {
      this.checkTypeTrain = true
    } else {
      this.checkTypeTrain = false
    }
    this.showOpExamCheckbox = [TRAINING_TYPE[0].value, TRAINING_TYPE[1].value, TRAINING_TYPE[2].value].includes(this.courseForm.value.trainingtype);
    this.isShowLocation = [TRAINING_TYPE[0].value, TRAINING_TYPE[1].value, TRAINING_TYPE[2].value].includes(this.courseForm.value.trainingtype);
    this.isShowCreateBadge = ![TRAINING_TYPE[0].value, TRAINING_TYPE[1].value, TRAINING_TYPE[2].value].includes(this.courseForm.value.trainingtype);
  }
  getCourseByPlan() {
    this.spinner.show();
    const params = {
      year: this.planYear,
      departmentid: this.departmentId || null
    }
    this.trainingPlanService.getListCourseInPlan(params).subscribe(
      res => {
        this.spinner.hide();
        this.listCoursePlan = res.body.results;
        // this.canEdit = res.body.canedit;
      },
      err => {
        this.spinner.hide();
      }
    )
  }

  convertCost(value: any) {
    let price = value.replace(/[^\d]/g, '') * 1;
    this.courseForm.patchValue({
      costDisplay: Intl.NumberFormat('vi-VN').format(price),
      cost: price
    });
  }
  convertDuration(value: any) {
    let number = value.replace(/[^\d]/g, '') * 1
    this.courseForm.patchValue({
      duration: Intl.NumberFormat('vi-VN').format(number)
    });
  }
  convertNumberstudent(value: any) {
    let number = value.replace(/[^\d]/g, '') * 1
    this.courseForm.patchValue({
      numberstudent: Intl.NumberFormat('vi-VN').format(number)
    });
  }
  initCourePlan() {
    let course = this.listCoursePlan.find(e => e.code == this.courseForm.value.fullname);
    this.initFormCourseDetail(course);
  }
  onChangePosition() {
    if (this.courseForm.get('selectedPositions').value) {
      if (this.courseForm.get('selectedPositions').value.length > 0) {
        this.selectedPositionIds = [];
        for (const position of this.courseForm.get('selectedPositions').value) {
          if (position) {
            this.selectedPositionIds.push(position.id);
          }
        }
      } else {
        this.selectedPositionIds = [];
      }
    } else {
      this.selectedPositionIds = [];
    }
  }
  listAllTree() {
    this.departmentService.getUserDepartmentTree(1).subscribe((data) => {
      data.body.forEach(department => {
        if (department.parentid !== null) {
          const parentName = this.departmentMap.get(department.parentid);
          let name = '';
          if (parentName) {
            name = parentName + ' / ' + department.name;
          } else {
            name = department.name;
          }
          this.departmentMap.set(department.id, name);
        } else {
          this.departmentMap.set(department.id, department.name);
        }
      });
    });
  }
  onSelectedDepartment(e: any) {
    this.departmentId = e.key;
  }
  showOptions($event) {
    this.isOpen = $event.checked;
    if ($event.checked) {
      this.courseForm.controls['numberstudent'].setValidators([Validators.maxLength(11)]);
      this.courseForm.get('numberstudent').updateValueAndValidity();
    } else {
      this.courseForm.controls['numberstudent'].setValidators([Validators.required, Validators.maxLength(11)]);
      this.courseForm.get('numberstudent').updateValueAndValidity();
    }
  }
  getListBadge(){
    this.courseService.getListBadge().subscribe(data=>{
      this.badgeList=data.body.results;
    }, err=>{
      this.toastrService.handlerError(err);
    }
    )
  }
  getListOriginalCourse(){
    this.courseService.getListOriginalCourse().subscribe(data=>{
      this.originalCourseList=data.body.results;
    }, err=>{
      this.toastrService.handlerError(err);
    }
    )
  }
  getListTrainingPlan(){
    const params = {
      limit: 0,
    };
    this.courseService.getListTrainingPlan(params).subscribe(data=>{
      this.listTrainingPlan = data.body.results;
    }, err=>{
      this.toastrService.handlerError(err);
    }
    )
  }
  onChangeBadges(){
    if (this.courseForm.get('badgerequire').value) {
      if (this.courseForm.get('badgerequire').value.length > 0) {
        this.selectedBadgeIds = [];
        for (const badged of this.courseForm.get('badgerequire').value) {
          if (badged) {
            this.selectedBadgeIds.push(badged.id);
          }
        }
      } else {
        this.selectedBadgeIds = [];
      }
    } else {
      this.selectedBadgeIds = [];
    }
  }
  onChangeResourse($event){
    if($event.target.value !== "OTHERS"){
      this.courseForm.patchValue({
        other: null,
      })
    }
  }
  onChangeLimit($event) {
    this.islimited = $event.checked;
    if(!this.islimited){
      this.courseForm.get('enddate').clearValidators();
      this.courseForm.get('hourEndDate').clearValidators();
      this.courseForm.get('minuteEndDate').clearValidators();
      this.courseForm.controls['enddate'].updateValueAndValidity();
      this.courseForm.controls['hourEndDate'].updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].updateValueAndValidity();
    }else{
      this.courseForm.controls['enddate'].setValidators([Validators.required]);
      this.courseForm.controls['enddate'].updateValueAndValidity();
      this.courseForm.controls['hourEndDate'].setValidators([Validators.required]);
      this.courseForm.controls['hourEndDate'].updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].setValidators([Validators.required]);
      this.courseForm.controls['minuteEndDate'].updateValueAndValidity();
    }
  }

  onChangeCheckClass($event) {
    this.isClass = $event.checked;
    if(!this.isClass){
      this.courseForm.get('classofcourseid').clearValidators();
      this.courseForm.controls['classofcourseid'].updateValueAndValidity();
      this.courseForm.controls['classofcourseid'].reset();
      this.classofcourseid =''
    }else{
      this.courseForm.controls['classofcourseid'].setValidators([Validators.required]);
      this.courseForm.controls['classofcourseid'].updateValueAndValidity();
    }
  }

  onChangeCheckApplyTrainingPlan($event) {
    this.checkApplyTrainingPlan = $event.checked;
    this.fullname = ''
    if(!this.checkApplyTrainingPlan){
      this.courseForm.get('planid').clearValidators();
      this.courseForm.controls['planid'].updateValueAndValidity();
      this.courseForm.controls['planid'].reset();
      this.courseForm.get('cost').clearValidators();
      this.courseForm.controls['cost'].updateValueAndValidity();
      this.getListOriginalCourse();
      this.disabledClass = false;
      this.courseForm.patchValue({
        planid: null,
        fullname: '',
        proposecourseid: null,
      })
    }else{
      this.courseForm.controls['planid'].setValidators([Validators.required]);
      this.courseForm.controls['planid'].updateValueAndValidity();
      this.originalCourseList = [];
      this.disabledClass = true;

      // reset các giá trị thuộc về lớp học
      this.isClass = false;
      this.classofcourseid = ''
      this.courseForm.patchValue({
        isClass: 0,
        classofcourseid: '',
        fullname: ''
      })
      this.courseForm.get('classofcourseid').clearValidators();
      this.courseForm.controls['classofcourseid'].updateValueAndValidity();
      this.courseForm.controls['classofcourseid'].reset();
    }
  }

  onChangePlan($event) {
    this.courseForm.patchValue({
      fullname: ''
    })
    this.fullname = ''
    this.getTrainingPlanInfo($event.target.value);
  }
  classofcourseid:string = '';
  onChangeOriginalCourse($event) {
    this.classofcourseid = $event
    let course = this.originalCourseList.find(function(item) {
      if (item.id == $event.id) {
        return item;
      }
    })
    this.courseForm.get('planid').setValue(course.planid);
  }

  calculateDiff(startdate, enddate){
    var date1 = new Date(startdate);
	  var date2 = new Date(enddate);
    return Math.floor((date2.getTime()-date1.getTime())/(1000 * 60 * 60 * 24)) + 1;
  }

  getTrainingPlanInfo(planid: any) {
    const params = {
      limit: 0,
      planid: planid,
    };
    this.trainingPlanService.getListCourseInPlan(params).subscribe(data=>{
      this.listTrainingPlanCourse=data.body.results;
    }, err=>{
      this.toastrService.handlerError(err);
    }
    )
  }

  onPickFullname($event) {
    this.maxCostValue = $event.cost - $event.totalcostused
    this.fullname = $event.fullname
    this.courseForm.patchValue({
      proposecourseid: $event.id
    })
    this.courseForm.controls['cost'].setValidators([Validators.max(this.maxCostValue)]);
  }

  onCheckIsforcepassexam(event:any){
    if(event.checked){
      this.isCheckisforcepassexam = event.checked;
      console.log(this.courseForm.get('hasquiz').value)
        this.courseForm.patchValue({
          hasquiz: true
        })
      this.courseForm.controls['hasquiz'].setValidators([Validators.requiredTrue]);
      this.courseForm.controls['hasquiz'].updateValueAndValidity();
    }
    else{
      this.courseForm.get('hasquiz').clearValidators();
      this.courseForm.controls['hasquiz'].updateValueAndValidity();
      // this.courseForm.controls['hasquiz'].reset();
    }
  }
}
