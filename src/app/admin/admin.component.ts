import {Component, OnInit} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { LocalStorageService } from 'ngx-webstorage';
@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  title = 'lms-frontend';
  constructor(
    private translate: TranslateService,
    private localStorage: LocalStorageService,
    ) {
      let language = this.localStorage.retrieve('mb-locale');
      if (!language) {
        language = 'vi';
        this.localStorage.store('mb-locale', language);
      }
      this.setLanguage(language)
  }
  ngOnInit() {
    let language = this.localStorage.retrieve('mb-locale');
    if (!language) {
      language = 'vi';
      this.localStorage.store('mb-locale', language);
    }
    this.setLanguage(language)
  }
  setLanguage(language): void {
    this.translate.setDefaultLang(language)
    this.translate.use(language);
 }
}
