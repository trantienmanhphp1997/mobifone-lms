import { Component, OnInit, OnDestroy,Input } from '@angular/core';
import { CriteriaService } from 'src/app/shared/services/criteria.service';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from '../../../shared/modal/confirm-modal/confirm-modal.component';
import { TranslateService } from '@ngx-translate/core';
import { ToastrCustomService } from '../../../shared/services/toastr-custom.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CriteriaSelectComponent} from '../../course-content/course-criteria/criteria-select/criteria-select.component';

@Component({
  selector: 'app-course-criteria',
  templateUrl: './course-criteria.component.html',
  styleUrls: ['./course-criteria.component.css']
})
export class CourseCriteriaComponent implements OnInit {
  pageSizeOptions: number[] = [10, 25, 50, 100];
  totalRecord: number;
  pageIndex = 1;
  pageSize = 10;
  listCriteria: any = [];
  constructor(
    public criteriaService: CriteriaService,
    private modalService: NgbModal,
    private translateService: TranslateService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
  ) { }

  displayDetail() {
    const paramsC = {
      page: this.pageIndex,
      limit: this.pageSize
    }
    this.criteriaService.getListCourseCriteria(paramsC).subscribe(res => {
      this.listCriteria = res.body.results;
      this.totalRecord  = res.body.total;
    });
  }
  openListCriteria(open: string) {
    const modalDep = this.modalService.open( CriteriaSelectComponent, {
      size: 'xl' as any,
      centered: false,
      backdrop: 'static'
    });

    modalDep.componentInstance.open = open;
    modalDep.componentInstance.loadAll.subscribe(($e) => {
      this.displayDetail();
    });
  }
  removeCri(courseId: number) {
    const modalDep = this.modalService.open(ConfirmModalComponent, {
      size: 'xl',
      centered: true,
      backdrop: 'static'
    });

    modalDep.componentInstance.title = this.translateService.instant('position.delete_criteria_confirm_title');
    modalDep.componentInstance.body = this.translateService.instant('position.delete_criteria_confirm_content');
    modalDep.componentInstance.confirmButton = this.translateService.instant('common.delete');
    modalDep.result.then((result) => {
      this.spinner.show();
      this.criteriaService.unAssignCriteria([courseId]).subscribe(
        res => {
          this.spinner.hide();
          this.displayDetail();
          this.toastrService.success(`common.noti.delete_success`);
        },
        err => {
          this.spinner.hide();
          this.toastrService.handlerError(err);
        }
      );
    });
  }
  ngOnInit(): void {
    this.displayDetail();
  }
  changePage(event) {
    this.pageIndex =  event.pageIndex + 1;
    this.pageSize =  event.pageSize;
    this.displayDetail();
  }
}
