import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { StaffAssessmentService } from 'src/app/shared/services/staff-assessment.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';

@Component({
  selector: 'app-evaluation-popup',
  templateUrl: './evaluation-popup.component.html',
  styleUrls: ['./evaluation-popup.component.css']
})
export class EvaluationPopupComponent implements OnInit {
  @Input() edit: any
  @Output() newStaffAss = new EventEmitter<any>();
  content: string = ''
  criterias: any = [];
  student: any = {
      id: null,
      fullname: null,
      email: null,
      department: null,
      positionid: null
    };
  manager: any = {
    id: null,
    fullname: null,
    email: null,
    department: null
  };
  st: any = [];
  haveEvaluated: boolean = false;
  constructor(
    public activeModal: NgbActiveModal,
    private staffAssessmentService :StaffAssessmentService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.content = this.student.content
    this.getEvaluationNote();
  }
  getEvaluationNote () {
    const params = {
       // positionid: 184,
       userid: this.student.id
    }
   this.staffAssessmentService.getEvaluationNote(this.student.userid, this.student.positionid).subscribe(res => {
       this.spinner.hide();
       this.criterias = res.body.criterias
       this.haveEvaluated = res.body.evaluationid ? true : false;
       this.st = this.criterias.map((c: any) => {return {criteriaid: c.id, status: c.status || 0} })
       this.student = {
           ...this.student,
           id: res.body.userid,
           fullname: res.body.fullname,
           email: res.body.email,
           department: res.body.department
         };
   
         this.manager = {
           ...this.manager,
           id: res.body.muserid,
           fullname: res.body.mfullname,
           email: res.body.memail,
           department: res.body.mdepartment
         }
   },
   err => {
       this.toastrService.handlerError(err);
       this.spinner.hide();
   })
}
createEvaluationStd () {
  const params = {
      positionid: this.student.positionid,
      userid: this.student.id,
      content: this.content,
      criteria: this.st,
      id: null
  }
  if (this.st.filter(e =>e.status !== 0).length != this.criterias.length) {
      this.toastrService.error('Bạn chưa đánh giá đủ theo các tiêu chí');
      return
  }

  this.spinner.show();
  if (this.student.evaluationid) {
      params.id = this.student.evaluationid
  }

  this.staffAssessmentService.createOrUpdateEvaluation(params).subscribe(res => {
      this.toastrService.success('Lưu đánh giá thành công');
      this.activeModal.dismiss('close');
      this.newStaffAss.emit('load');
      this.spinner.hide();
  },
  err => {
      this.toastrService.handlerError(err);
      this.spinner.hide();
  })
}
  changeHadle (criId:any, e:any) {
    this.st = this.st.map(c => {
        if (c.criteriaid == criId) {
            return {criteriaid: c.criteriaid, status: parseInt(e.target.value)}
        }
        else {
            return {criteriaid: c.criteriaid, status: c.status}
        }
    })
}

}
