import { Component, OnInit, ViewChild } from '@angular/core';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { ReportService } from 'src/app/shared/services/report.service';
import {LocalStorageService} from 'ngx-webstorage';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatInput } from '@angular/material/input';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { FileService } from '../../../shared/services/file.service';
import { CourseCategoryService } from 'src/app/shared/services/course-category.service';
import CommonUtil from 'src/app/shared/utils/common-util';
@Component({
  selector: 'app-report-teacher-internal',
  templateUrl: './report-teacher-internal.component.html',
  styleUrls: ['./report-teacher-internal.component.css']
})
export class ReportTeacherInternalComponent implements OnInit {
  @ViewChild('fromInput', {
    read: MatInput
  }) fromInput: MatInput;
  @ViewChild('toInput', {
    read: MatInput
  }) toInput: MatInput;
  @ViewChild('fromInputDV', {
    read: MatInput
  }) fromInputDV: MatInput;
  @ViewChild('toInputDV', {
    read: MatInput
  }) toInputDV: MatInput;
  @ViewChild('fromInputGV', {
    read: MatInput
  }) fromInputGV: MatInput;
  @ViewChild('toInputGV', {
    read: MatInput
  }) toInputGV: MatInput;
  departmentMap: any[] = [];
  departmentData: any[] = [];
  departmentMapDV: any[] = [];
  departmentDataDV: any[] = [];
  listTeacher:any[] = [];
  listTeacherInternalGV: any [] =[];
  listTeacherInternal: any [] =[];
  selectTeacherEvent = false;
  selectDepartmentEvent = false;
  selectDepartmentDVEvent = false;
  TeacherData: any[] = [];
  searcherTeacher ={
    pageIndex: 1,
    pageSize: 10,
    pageIndexDV: 1,
    pageSizeDV: 10,
    pageIndexGV: 1,
    pageSizeGV: 10,
  }
  height: number = 300;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  totalRecord: number;
  pageSizeOptionsGV: number[] = [10, 25, 50, 100];
  totalRecordGV: number;
  pageSizeOptionsDV: number[] = [10, 25, 50, 100];
  totalRecordDV: number;
  currentDate: Date = new Date();
  searchForm = this.fb.group({
    startDate: [],
    endDate: [],
  });
  searchFormDV = this.fb.group({
    startDateDV: [],
    endDateDV: [],
  });
  searchFormGV = this.fb.group({
    startDateGV: [],
    endDateGV: [],
  });
  validYear: Date;
  firstDay = new Date(new Date().getFullYear(), 0, 1, 1);
  validYearDV: Date;
  validYearGV: Date;
  roles: any;
  isTeacher: boolean = false;
  isAdmin: boolean = false ;
  dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 1,
    allowSearchFilter: true,
    searchPlaceholderText: 'Tìm kiếm',
    noDataAvailablePlaceholderText: 'Không có dữ liệu',
  };
  dropdownSettingTeacher = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 1,
    allowSearchFilter: true,
    searchPlaceholderText: 'Tìm kiếm',
    noDataAvailablePlaceholderText: 'Đơn vị này không có giảng viên',
  };
   searchTeacher ={
    teacherids: null,
    departmentIds: null,
    departmentIdsDV: null,
    categoryIds: null,
    startDate: null,
    endDate: null,
    startDateGV: null,
    endDateGV: null,
    startDateDV: null,
    endDateDV: null,
   }

  listCategory: any = [];
  selectedCategory: any;
  isCheckCategoryEvent: boolean = false;
  categoryids: any = [];

  constructor(
    private departmentService: DepartmentService,
    private reportService: ReportService,
    private $localStorage: LocalStorageService,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private toastrService: ToastrCustomService,
    private fileService: FileService,
    private categoryService: CourseCategoryService
  ) { }
  ngOnInit(): void {
    this.roles = this.$localStorage.retrieve('roles');
    this.listAllTree();
    this.getListTeacher();
    this.onLoadListCategory();
    for (let j = 0; j < this.roles.length; j++) {
      if (this.roles[j].shortname === 'teacher'){
          this.isTeacher =true;
          this.getListTeacherInterGV();
      }
    }
  }
  listAllTree() {
    this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
      const e = [];
      data.body.forEach(department => {
        if (department.parentid !== null) {
          const parentName = e.filter(x => x.id == department.parentid)[0]?.name;
          let name = '';
          if (parentName) {
            name = parentName + ' / ' + department.name;
          } else {
            name = department.name;
          }
          e.push({id: department.id, name: name});
        } else {
          e.push({id: department.id, name: department.name});
        }
      });
      this.departmentMap = e;
      this.departmentMapDV = e;
    });
  }
  onSelectDepartment() {
    this.selectDepartmentEvent = true;
  }
  changeDepartment() {
    if (this.selectDepartmentEvent) {
      this.selectDepartmentEvent = false;
      this.searchTeacher.departmentIds = []
      this.departmentData?.forEach((item) => {
        this.searchTeacher.departmentIds.push(item.id);
      });
      this.TeacherData = [];
      this.listTeacher =[]; 
      this.searchTeacher.teacherids = [];
      this.getListTeacher();
      this.getListTeacherInter();
    }
  }
  onSelectDepartmentDV() {
    this.selectDepartmentDVEvent = true;
  }
  changeDepartmentDV() {
    if (this.selectDepartmentDVEvent) {
      this.selectDepartmentDVEvent = false;
      this.searchTeacher.departmentIdsDV = []
      this.departmentDataDV?.forEach((item) => {
        this.searchTeacher.departmentIdsDV.push(item.id);
      });
      this.getListTeacherInterDV();
    }
  }
  onSelectTeacher() {
    this.selectTeacherEvent = true;
  }
  changeTeacher() {
    if (this.selectTeacherEvent) {
      this.selectTeacherEvent = false;
      this.searchTeacher.teacherids = []
      this.TeacherData?.forEach((item) => {
        this.searchTeacher.teacherids.push(item.id);
      });
      this.getListTeacherInter();
    }
  }
  getListTeacher(){
    const params ={
       all: 1,
       departmentids:  this.searchTeacher.departmentIds ? this.searchTeacher.departmentIds.toString() : "",
    }
     this.reportService.getListTeacherNB(params).subscribe( date =>{
       this.listTeacher= date.body.results.map(t => {
        return {id : t.id, name: t.fullname};
      });
    });
  }

  getListTeacherInterGV(){
      const param ={
        limit: this.searcherTeacher.pageSizeGV,
        page: this.searcherTeacher.pageIndexGV,
        startdate: this.searchTeacher.startDateGV ? this.searchTeacher.startDateGV.getTime() / 1000 : null,
        enddate: this.searchTeacher.endDateGV ? Math.ceil(this.searchTeacher.endDateGV.getTime() / 1000) : null,
      }
      this.spinner.show();
      this.reportService.getListTeacherIReportGV(param).subscribe( date =>{
        this.spinner.hide();
         this.listTeacherInternalGV= date.body.results;
         this.totalRecordGV=date.body.total;
      })
  }
  getListTeacherInter(){
    const params ={
      limit: this.searcherTeacher.pageSize,
      page: this.searcherTeacher.pageIndex,
      ids: this.searchTeacher.teacherids ? this.searchTeacher.teacherids.toString() : "",
      startdate: this.searchTeacher.startDate ? this.searchTeacher.startDate.getTime() / 1000 : null,
      enddate: this.searchTeacher.endDate ? Math.ceil(this.searchTeacher.endDate.getTime() / 1000) : null,
    }
    this.spinner.show();
    this.reportService.getListTeacherIReport(params).subscribe( date =>{
      this.spinner.hide();
      this.listTeacherInternal= date.body.results;
      this.totalRecord=date.body.total;
    });
  }
  getListTeacherInterDV(){
    const params ={
      limit: this.searcherTeacher.pageSizeDV,
      page: this.searcherTeacher.pageIndexDV,
      departmentids: this.searchTeacher.departmentIdsDV ? this.searchTeacher.departmentIdsDV.toString() : "",
      startdate: this.searchTeacher.startDateDV ? this.searchTeacher.startDateDV.getTime() / 1000 : null,
      enddate: this.searchTeacher.endDateDV ? Math.ceil(this.searchTeacher.endDateDV.getTime() / 1000) : null,
      categoryids: this.searchTeacher.categoryIds ? this.searchTeacher.categoryIds.toString() : ''
    }
    this.spinner.show();
    this.reportService.getListTeacherIReportDV(params).subscribe( date =>{
      this.spinner.hide();
      this.listTeacherInternalGV= date.body.results;
      this.totalRecordDV=date.body.total;
    });
  }
  changeStartDate(e: any) {
    this.searchTeacher.startDate = this.searchForm.value.startDate;
    let yearStartDate = new Date(this.searchTeacher.startDate).getFullYear();
    this.validYear = new Date(yearStartDate, 12, 0, 23, 59, 59);
    let yearEndDate = new Date(this.searchTeacher.endDate).getFullYear();
    if (yearStartDate != yearEndDate) {
      this.toInput.value = '';
      this.searchTeacher.endDate = null;
      this.searchForm.value.endDate = null;
    }
    this.getListTeacherInter();
  }
  changeStartDateGV(e: any) {
    this.searchTeacher.startDateGV = this.searchFormGV.value.startDateGV;
    let yearStartDateGV = new Date(this.searchTeacher.startDateGV).getFullYear();
    this.validYearGV = new Date(yearStartDateGV, 12, 0, 23, 59, 59);
    let yearEndDateGV = new Date(this.searchTeacher.endDateGV).getFullYear();
    if (yearStartDateGV != yearEndDateGV) {
      this.toInputGV.value = '';
      this.searchTeacher.endDateGV = null;
      this.searchFormGV.value.endDateGV = null;
    }
    this.getListTeacherInterGV();
  }
  changeStartDateDV(e: any) {
    this.searchTeacher.startDateDV = this.searchFormDV.value.startDateDV;
    let yearStartDateDV = new Date(this.searchTeacher.startDateDV).getFullYear();
    this.validYearDV = new Date(yearStartDateDV, 12, 0, 23, 59, 59);
    let yearEndDateDV = new Date(this.searchTeacher.endDateDV).getFullYear();
    if (yearStartDateDV != yearEndDateDV) {
      this.toInputDV.value = '';
      this.searchTeacher.endDateDV = null;
      this.searchFormDV.value.endDateDV = null;
    }
    this.getListTeacherInterDV();
  }
  changeEndDate(e: any) {
    this.searchTeacher.endDate = this.searchForm.value.endDate;
    this.getListTeacherInter();
  }
  changeEndDateGV(e: any) {
    this.searchTeacher.endDateGV = this.searchFormGV.value.endDateGV;
    this.getListTeacherInterGV();
  }
  changeEndDateDV(e: any) {
    this.searchTeacher.endDateDV = this.searchFormDV.value.endDateDV;
    this.getListTeacherInterDV();
  }
  clearDateDV(e: any, type: any) {
    e.stopPropagation();
    switch (type) {
      case 'endDateDV':
        this.searchTeacher.endDateDV = null;
        this.toInputDV.value = null;
        break;
      case 'startDateDV':
        this.searchTeacher.startDateDV = null;
        this.fromInputDV.value = null;
        break;
      default:
        break;
    }
    this.getListTeacherInterDV();
  }
  clearDate(e: any, type: any) {
    e.stopPropagation();
    switch (type) {
      case 'endDate':
        this.searchTeacher.endDate = null;
        this.toInput.value = null;
        break;
      case 'startDate':
        this.searchTeacher.startDate = null;
        this.fromInput.value = null;
        break;
      default:
        break;
    }
    this.getListTeacherInter();
  }
  clearDateGV(e: any, type: any) {
    e.stopPropagation();
    switch (type) {
      case 'endDateGV':
        this.searchTeacher.endDateGV = null;
        this.toInputGV.value = null;
        break;
      case 'startDateGV':
        this.searchTeacher.startDateGV = null;
        this.fromInputGV.value = null;
        break;
      default:
        break;
    }
    this.getListTeacherInterGV();
  }
  changePage(event) {
    this.searcherTeacher.pageIndex = event.pageIndex + 1;
    this.searcherTeacher.pageSize = event.pageSize;
    this.getListTeacherInter();
  }
  changePageGV(event) {
    this.searcherTeacher.pageIndexGV = event.pageIndex + 1;
    this.searcherTeacher.pageSizeGV = event.pageSize;
    this.getListTeacherInterGV();
  }
  changePageDV(event) {
    this.searcherTeacher.pageIndexDV = event.pageIndex + 1;
    this.searcherTeacher.pageSizeDV = event.pageSize;
    this.getListTeacherInterDV();
  }
  onExporDV(){
    const params = {
      departmentids: this.searchTeacher.departmentIdsDV ? this.searchTeacher.departmentIdsDV.toString() : "",
      categoryids: this.searchTeacher.categoryIds ? this.searchTeacher.categoryIds.toString() : '',
      startdate: this.searchTeacher.startDateDV ? this.searchTeacher.startDateDV.getTime() / 1000 : null,
      enddate: this.searchTeacher.endDateDV ? Math.ceil(this.searchTeacher.endDateDV.getTime() / 1000) : null,
    };
    this.spinner.show();
    this.reportService.exportTeacherNDV(params).subscribe(
      res => {
        this.spinner.hide();
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
      }, err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
    });
  }
  onExpor(){
    const params = {
      ids: this.searchTeacher.teacherids ? this.searchTeacher.teacherids.toString() : "",
      startdate: this.searchTeacher.startDate ? this.searchTeacher.startDate.getTime() / 1000 : null,
      enddate: this.searchTeacher.endDate ? Math.ceil(this.searchTeacher.endDate.getTime() / 1000) : null,
    };
    this.spinner.show();
    this.reportService.exportTeacherNN(params).subscribe(
      res => {
        this.spinner.hide();
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
      }, err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
    });
  }
  onExporGV(){
    const params = {
      startdate: this.searchTeacher.startDate ? this.searchTeacher.startDate.getTime() / 1000 : null,
      enddate: this.searchTeacher.endDate ? Math.ceil(this.searchTeacher.endDate.getTime() / 1000) : null,
    };
    this.spinner.show();
    this.reportService.exportTeacherNN(params).subscribe(
      res => {
        this.spinner.hide();
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
      }, err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
    });
  }

  onLoadListCategory(): void {
    this.categoryService.getCourseCategoryTree(null).subscribe((res) => {
      const categoryMap = new Map();
      const listData = [];
      CommonUtil.convertCategoryCodeListToMap(res.body, categoryMap);
      categoryMap.forEach((value, key) => {
        listData.push(
          {
            'id': key,
            'name': value
          }
        )
      })
      this.listCategory = listData;
    })
  }

  onSelectCategoryEvent(): void {
    this.isCheckCategoryEvent = true;
  }

  changeCategory(): void {
    if(this.isCheckCategoryEvent) {
      this.isCheckCategoryEvent = false;
      this.categoryids = [];
      this.selectedCategory.forEach((item) => {
        this.categoryids.push(item.id);
      })
      this.searchTeacher.categoryIds = this.categoryids;
      this.getListTeacherInterDV();
    }
    this.isCheckCategoryEvent = false;
  }
}
