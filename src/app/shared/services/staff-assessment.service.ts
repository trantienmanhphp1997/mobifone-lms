import { HttpResponse } from '@angular/common/http';
import { UserInfo } from '../model/user-info.model';
import { AbstractService } from './abstract.service';
import { Observable } from 'rxjs';
import CommonUtil from '../utils/common-util';
import { MOODLE_INFO } from '../constants/base.constant';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ModuleInfo } from '../model/moduleinfo.model';
import { User } from "../model/user.model";

type EntityResponseType = HttpResponse<any>;
type EntityArrayResponseType = HttpResponse<any[]>;

interface UserPageResponse {
    results: UserInfo[];
    total: number;
}

@Injectable({
    providedIn: 'root'
})
export class StaffAssessmentService {
    constructor(private abstractService: AbstractService) {}

    getListStdCplteOnB(param: any): Observable<HttpResponse<any>> {
        const data = this.abstractService.getCommonInput('mobifone_position_road_map_users');
        CommonUtil.appendDataToFormData(data, 'params', param);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }
    createOrUpdateEvaluation(param: any): Observable<HttpResponse<any>> {
        const functionName = param.id ? 'mobifone_position_update_road_map_evaluation' : 'mobifone_position_create_road_map_evaluation'
        const data = this.abstractService.getCommonInput(functionName);
        CommonUtil.appendDataToFormData(data, 'evaluation', param);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }

    getEvaluationNote(userid?: any, positionid?: any): Observable<HttpResponse<any>> {
        const data = this.abstractService.getCommonInput('mobifone_position_road_map_user_detail');
        if (positionid) data.append('positionid', positionid);
        data.append('userid', userid);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }
    
    getEvaluationNoteEndUser(positionid = null): Observable<HttpResponse<any>> {
        const data = this.abstractService.getCommonInput('mobifone_position_road_map_enduser_detail');
        if (positionid != null) data.append('positionid', positionid);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }

    assignManagerToEvaluation(positionid: any, userid: any): Observable<HttpResponse<any>> {
        const data = this.abstractService.getCommonInput('mobifone_position_assign_user_evaluation');
        data.append('positionid', positionid);
        data.append('userid', userid);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }
}
