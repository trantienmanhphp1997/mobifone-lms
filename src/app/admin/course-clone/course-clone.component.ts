import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { Sort } from '@angular/material/sort';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { CourseService } from '../../shared/services/course.service';
import { CourseSearchComponent } from '../course/course-search/course-search.component';
import CommonUtil from 'src/app/shared/utils/common-util';
import { ClonePopupComponent } from './clone-popup/clone-popup.component';
import { DetailCloneComponent } from './detail-clone/detail-clone.component';

@Component({
  selector: 'app-course-clone',
  templateUrl: './course-clone.component.html',
  styleUrls: ['./course-clone.component.css']
})
export class CourseCloneComponent implements OnInit {

  constructor(
    private courseService: CourseService,
    private modalService: NgbModal,
  ) { }
  coursesList : any = [];
  pageSizeOptions: number[] = [10, 25, 50, 100];
  totalRecord: number;
  pageSizeOptions1: number[] = [10, 25, 50, 100];
  totalRecord1: number;
  completedAll: boolean = false;
  isManage: boolean = false;
  isTeacher: boolean = false;
  role: string;
  managedDepartmentId: number;
  courseIdsChecked: number[] = [];
  courseIds: number[] = [];
  courseCanChecked: number = 0;
  pageEvent: PageEvent;
  requestCloneList:any =[];
  currentDate: Date = new Date();
  searchCourse = {
    search: '',
    search1: '',
    pageIndex: 1,
    pageSize: 10,
    pageIndex1: 1,
    pageSize1: 10,
    sortType: 'desc',
    coursetype: 1,
    status:0,
    status1:0
  };
  ngOnInit(): void {
    this.getListCourse();
    this.getListCourse1();
  }
  getListCourse(){
    const params = {
      limit: this.searchCourse.pageSize,
      page: this.searchCourse.pageIndex,
      sorttype: this.searchCourse.sortType,
      coursetype: this.searchCourse.coursetype,
      search: this.searchCourse.search,
      status: this.searchCourse.status,
    };

    this.courseService.searchCoursesClone(params).subscribe(data => {
      this.coursesList = data.body.results;
      this.totalRecord = data.body.total;
    })
  }
  
  getListCourse1(){
    const params1 = {
      limit: this.searchCourse.pageSize1,
      page: this.searchCourse.pageIndex1,
      sorttype: this.searchCourse.sortType,
      coursetype: this.searchCourse.coursetype,
      search: this.searchCourse.search1,
      type:'requested',
      status: this.searchCourse.status1,
    };
    this.courseService.searchCoursesClone(params1).subscribe(data => {
      this.requestCloneList = data.body.results;
      this.totalRecord1 = data.body.total;
    })
  }
  sortData(sort: Sort) {
    this.searchCourse.pageIndex = 1;
    this.searchCourse.sortType = sort.direction;
    this.getListCourse();
  }
  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }
  onFilterCourse(event:any){
    this.searchCourse.status = event;
    this.getListCourse();
  }
  onFilterCourse1(event:any){
    this.searchCourse.status1 = event;
    this.getListCourse1();
  }
  changePage(event) {
    this.searchCourse.pageIndex = event.pageIndex + 1;
    this.searchCourse.pageSize = event.pageSize;
    this.getListCourse();
  }
  changePage1(event) {
    this.searchCourse.pageIndex1 = event.pageIndex + 1;
    this.searchCourse.pageSize1 = event.pageSize;
    this.getListCourse1();
  }
  onChangeKeyword() {
    this.searchCourse.pageIndex = 1;
    this.getListCourse();
  }
  onChangeKeyword1() {
    this.searchCourse.pageIndex1 = 1;
    this.getListCourse1();
  }
  sendRequest(status:any, data: any){
    const modalDep = this.modalService.open(ClonePopupComponent, {
          size: 'lg',
          centered: true,
          backdrop: 'static'
    });
    modalDep.componentInstance.data = data;
    modalDep.componentInstance.status = status;
    modalDep.componentInstance.emitter.subscribe( result =>{
      if(result){
        this.getListCourse();
        this.getListCourse1();
      }
    })
  }
  detailClone(data: any, type:any){
    const modalDep = this.modalService.open(DetailCloneComponent, {
      size: 'xl',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.data = data;
    modalDep.componentInstance.type = type;
    modalDep.componentInstance.emitter1.subscribe( result =>{
      if(result){
        this.getListCourse();
        this.getListCourse1();
      }
    })
  }
  checkDisable(c) {
    return (moment.unix(c.startdate).toDate() < this.currentDate && this.currentDate < moment.unix(c.enddate).toDate() && c.published === 1) || c.published === 1;
  }
}
