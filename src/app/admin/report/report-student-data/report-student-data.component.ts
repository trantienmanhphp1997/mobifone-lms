import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ReportService } from '../../../shared/services/report.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DepartmentService } from '../../../shared/services/department.service';
import { Department } from '../../../shared/model/department.model';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { FileService } from '../../../shared/services/file.service';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { SearchCourseComponent } from './search-course/search-course.component';
import { PositionService } from '../../../shared/services/position.service';
import { ActivatedRoute } from '@angular/router';
import { BadgeCategoryService } from 'src/app/shared/services/badge-category.service';


@Component({
  selector: 'app-report-student-data',
  templateUrl: './report-student-data.component.html',
  styleUrls: ['./report-student-data.component.css']
})
export class ReportStudentDataComponent implements OnInit {
  @ViewChild('departmentTree')
  public departmentTree: TreeViewComponent;

  @ViewChild('departmentMergeTree')
  public departmentMergeTree: TreeViewComponent;

  public departmentTreeData;
  public departmentMergeTreeData;

  departmentList: Department[] = [];
  positionList: any[] = [];
  positionData: any[] = [];
  levelList: any[] = [];
  levelData: any[] = [];
  badgeList: any[] = [];
  badgeData: any[] = [];
  senirorityList: any[] = [];
  seniorityData: any[] = [];

  departmentIdsChecked: [];
  departmentMergeIdsChecked: [];
  currentSelectedId: number;
  departmentIds: number[] = [];
  departmentMergeIds : number [] = [];
  totalRecord: number;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  checkedDepartmentIds = [];
  studentList: any;
  minNumbder = 0;
  selectPositionEvent = false;
  selectSeniorityEvent = false;
  selectBadgeEvent = false;
  selectLevelEvent = false;
  searchStudent = {
    pageIndex: 1,
    pageSize: 10,
    sortColumn: 'id',
    sortType: 'desc',
    keyword: null,
    ids: null,
    positionids: null,
    seniorityids: null,
    levelids: null,
    badgeids: null,
    seniorities: null
  }
  dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 1,
    allowSearchFilter: true,
    searchPlaceholderText: 'Tìm kiếm'
  };
  constructor(
    private reportService: ReportService,
    private spinner: NgxSpinnerService,
    private departmentService: DepartmentService,
    private toastrService: ToastrCustomService,
    private fileService: FileService,
    private modalService: NgbModal,
    private positionService: PositionService,
    private categoryService: BadgeCategoryService,
  ) {
  }

  ngOnInit(): void {
    this.getListAllTree();
    this.getListPosition();
    this.listAllStudent();
    this.getListBadge();
    this.getListLevel();
    this.getListSeniority();
    this.getListDepartmentMergeTree();
  }
  // for transcluded content
  nodeSelected(e) {
    this.departmentIds = [];
    this.departmentIdsChecked = [];
    this.departmentTree.checkedNodes.forEach((node) => {
      this.departmentIds.push(parseInt(node, 10));
    });
    this.currentSelectedId = +this.departmentTree.getNode(e.node).id;
    this.searchStudent.pageSize = 10;
    this.searchStudent.pageIndex = 1;
    this.listAllStudent();
  }
  getListDepartmentMergeTree() {
    return this.departmentService.listDepartmentMergeTree().subscribe((data) => {
      const dataSource = data.body;
      this.departmentMergeTreeData = { dataSource, id: 'id', parentID: 'parent', text: 'name', hasChildren: 'haschild', selected: 'isSelected' };
    });
  }

  nodeSelectedDepartmentMerge(e) {
    this.departmentMergeIds = [];
    this.departmentMergeIdsChecked = [];
    this.departmentMergeTree.checkedNodes.forEach((node) => {
      this.departmentMergeIds.push(parseInt(node, 10));
    });
    this.currentSelectedId = +this.departmentMergeTree.getNode(e.node).id;
    this.searchStudent.pageSize = 10;
    this.searchStudent.pageIndex = 1;
    this.searchStudent.sortColumn = 'id';
    this.searchStudent.sortType = 'ASC';
    this.searchStudent.pageIndex = 1;
    this.listAllStudent();
  }

  nodeCheck(args: any): void {
    const checkedNode: any = [args.node];
    if (args.event.target.classList.contains('e-fullrow') || args.event.key == 'Enter') {
      // @ts-ignore
      const getNodeDetails: any = this.departmentTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.departmentTree.uncheckAll(checkedNode);
      } else {
        this.departmentTree.checkAll(checkedNode);
      }
    }
  }

  nodeCheckDepartmentMerge(args: any): void {
    const checkedNode: any = [args.node];
    if (args.event.target.classList.contains('e-fullrow') || args.event.key == 'Enter') {
      // @ts-ignore
      const getNodeDetails: any = this.departmentMergeTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.departmentMergeTree.uncheckAll(checkedNode);
      } else {
        this.departmentMergeTree.checkAll(checkedNode);
      }
    }
  }
  getListSeniority() {
    this.spinner.show();
    this.reportService.getSenirority().subscribe(data => {
      this.senirorityList = data.body;
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastrService.handlerError(err);
    });
  }

  listAllStudent() {
    const departmentIds = [...this.departmentIds, ...this.departmentMergeIds] ;
    const param = {
      ids: this.searchStudent.ids,
      limit: this.searchStudent.pageSize,
      page: this.searchStudent.pageIndex,
      sorttype: this.searchStudent.sortType,
      departmentids: departmentIds ? departmentIds.toString() : "",
      keyword: this.searchStudent.keyword,
      seniorities: this.searchStudent.seniorityids ? this.searchStudent.seniorityids.toString() : "",
      positionids: this.searchStudent.positionids ? this.searchStudent.positionids.toString() : "",
      levelids: this.searchStudent.levelids ? this.searchStudent.levelids.toString() : "",
      badgeids: this.searchStudent.badgeids ? this.searchStudent.badgeids.toString() : ""
    }
    this.spinner.show();
    this.reportService.getStudentData(param).subscribe(data => {
      this.studentList = data.body.results;
      this.totalRecord = data.body.total;
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastrService.handlerError(err);
    });
  }
  openListCourse(open: string) {
    const modalDep = this.modalService.open(SearchCourseComponent, {
      size: 'xl' as any,
      centered: false,
      backdrop: 'static'
    });
    modalDep.componentInstance.open = "course";
    modalDep.componentInstance.courseIdStr = this.searchStudent.ids;
    modalDep.componentInstance.transferCourseIds.subscribe((courseId) => {
      this.searchStudent.ids = courseId;
      this.listAllStudent();
    });
  }

  clear() {
    // param phuc vu tim kiem
    this.searchStudent.keyword = '';
    this.currentSelectedId = null;
    // Bo select node tren cay
    this.departmentTree.selectedNodes = [];
    this.departmentIdsChecked = [];
    this.departmentMergeIdsChecked = []
    this.departmentIds = [];
    this.departmentMergeIds = [];
    this.listAllStudent();
  }

  changePage(event) {
    this.searchStudent.pageIndex = event.pageIndex + 1;
    this.searchStudent.pageSize = event.pageSize;
    this.listAllStudent();
  }
  onSearch(e: any) {
    this.searchStudent.pageIndex = 1;
    this.searchStudent.keyword = e.target.value;
    this.listAllStudent();
  }
  onSearchSeniority(e: any) {
    this.searchStudent.pageIndex = 1;
    this.searchStudent.seniorityids = e.target.value;
    this.listAllStudent();
  }
  getListPosition() {
    return this.positionService.getPosition().subscribe(positionList => {
      this.positionList = positionList.body.results;
    });
  }
  getListBadge() {
    const params = {
      all: 1
    };
    return this.categoryService.getBadge(params).subscribe(badgeList => {
      this.badgeList = badgeList.body.results;
    });
  }
  getListLevel() {
    const params = {
      all: 1
    };
    return this.categoryService.getLevel(params).subscribe(levelList => {
      this.levelList = levelList.body.results;
    });
  }
  onSelectPosition() {
    this.selectPositionEvent = true;
  }

  onSelectLevel() {
    this.selectLevelEvent = true;
  }

  onSelectBadge() {
    this.selectBadgeEvent = true;
  }

  changeBadge() {
    if (this.selectBadgeEvent) {
      this.selectBadgeEvent = false;
      this.searchStudent.badgeids = []
      this.badgeData?.forEach((item) => {
        this.searchStudent.badgeids.push(item.id);
      })
      this.listAllStudent();
    }
  }

  changeLevel() {
    if (this.selectLevelEvent) {
      this.selectLevelEvent = false;
      this.searchStudent.levelids = []
      this.levelData?.forEach((item) => {
        this.searchStudent.levelids.push(item.id);
      })
      this.listAllStudent();
    }
  }

  changePosition() {
    if (this.selectPositionEvent) {
      this.selectPositionEvent = false;
      this.searchStudent.positionids = []
      this.positionData?.forEach((item) => {
        this.searchStudent.positionids.push(item.id);
      })
      this.listAllStudent();
    }
  }

  exportStudent() {
    const departmentIds = [...this.departmentIds, ...this.departmentMergeIds] ;
    const params = {
      ids: this.searchStudent.ids,
      departmentids: departmentIds ? departmentIds.toString() : "",
      keyword: this.searchStudent.keyword,
      seniorities: this.searchStudent.seniorityids ? this.searchStudent.seniorityids.toString() : "",
      positionids: this.searchStudent.positionids ? this.searchStudent.positionids.toString() : "",
      levelids: this.searchStudent.levelids ? this.searchStudent.levelids.toString() : "",
      badgeids: this.searchStudent.badgeids ? this.searchStudent.badgeids.toString() : "",
      all: 1,
    };
    this.spinner.show();
    this.reportService.exportStudentData(params).subscribe(
      res => {
        this.spinner.hide();
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
      }, err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      });
  }

  getListAllTree() {
    this.spinner.show();
    this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
      this.departmentList = data.body;
      if (this.departmentList) {
        this.departmentList[0].parentid = null;
      }
      this.departmentTreeData = {
        dataSource: this.departmentList,
        id: 'id',
        parentID: 'parentid',
        text: 'name',
        hasChildren: 'haschild'
      };
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      this.toastrService.handlerError(error);
    });
  }
  setUpTree(dataSource: any) {

    const indexOfCurrentSelectedId = dataSource.findIndex(x => x.id === this.currentSelectedId);
    if (indexOfCurrentSelectedId === -1) {// case currentSelectedId is deleted -> set to deault
      this.currentSelectedId = null;
    }

    dataSource.forEach(value => {
      if (value.parent === 0) // = 0 la root tree
      {
        value.parent = null; // set = null nham muc dich hien thi
        value.expanded = true; // muc dich expand root luon khi khoi tao tree
      } else {
        value.expanded = false;
      }

      if (value.id === this.currentSelectedId) { // high light selected node
        value.isSelected = true;
      }
    });
  }
  changeSeniority() {
    if (this.selectSeniorityEvent) {
      this.selectSeniorityEvent = false;
      this.searchStudent.seniorityids = []
      this.seniorityData?.forEach((item) => {
        this.searchStudent.seniorityids.push(item.id);
      })
      this.listAllStudent();
    }
  }
  onSelectSeniority() {
    this.selectSeniorityEvent = true;
  }
}
