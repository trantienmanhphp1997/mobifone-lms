import { Component, isDevMode, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ConfirmModalComponent } from 'src/app/shared/modal/confirm-modal/confirm-modal.component';
import { CriteriaService } from 'src/app/shared/services/criteria.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { CriteriaDetailComponent } from './criteria-detail/criteria-detail.component';

@Component({
    selector: 'app-criteria',
    templateUrl: './criteria.component.html',
    styleUrls: ['./criteria.component.scss']
})
export class CriteriaComponent implements OnInit {
    listCriteria: any = [];
    checkBoxAllCriDelete = false;
    criIds = [];
    criIdsChecked: any = [];
    keyword: string = '';
    /// page
    pageIndex = 1;
    pageSize = 10;

    pageSizeOptions: number[] = [10, 25, 50, 100];
    totalRecord: number;

    constructor(
        private modalService: NgbModal,
        private criteriaService: CriteriaService,
        private translateService: TranslateService,
        private toastrService: ToastrCustomService,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit(): void {
        this.getListCriteria();
    }

    getListCriteria() {
        const params = {
            keyword: this.keyword,
            page: this.pageIndex,
            limit: this.pageSize,
        }
        this.spinner.show();
        this.criteriaService.getListCriteria(params).subscribe(res => {
            this.listCriteria = res.body.results
            this.totalRecord = res.body.total;
            this.spinner.hide();
        },
            err => {
                this.spinner.hide();
                this.toastrService.handlerError(err);
            })
    }
    onCreatedCriteria() {
        const modalDep = this.modalService.open(CriteriaDetailComponent, {
            size: 'lg' as any,
            centered: true,
            backdrop: 'static'
        });
        modalDep.componentInstance.newCri.subscribe(($e) => {
            // Thuc hien insert data vao list hoac reload li
            this.getListCriteria();
        });
    }

    onUpdateCriteria(cri: any) {
        const modalDep = this.modalService.open(CriteriaDetailComponent, {
            size: 'lg' as any,
            centered: true,
            backdrop: 'static'
        });
        modalDep.componentInstance.criteria = cri;
        modalDep.componentInstance.newCri.subscribe(($e) => {
            // Thuc hien insert data vao list hoac reload li
            this.getListCriteria();
        });
    }

    checkAllUser(checked: any) {
        if (checked) {
            this.listCriteria.forEach(c => {
                c.selected = checked;
                if (!this.criIds.includes(c.id)) {
                    this.criIds.push(c.id);
                }
                if (!this.criIdsChecked?.includes(c.id)) {
                    this.criIdsChecked?.push(c.id);
                }
            });
            this.checkBoxAllCriDelete = true;
        } else {
            this.criIds?.forEach(id => {
                this.criIdsChecked?.splice(this.criIdsChecked?.indexOf(id), 1);
            });
            this.criIds = [];
            this.listCriteria?.forEach(c => {
                c.selected = false;
            });
            this.checkBoxAllCriDelete = false;
        }
    }
    checkSelection(courseId: number, checked: any) {
        if (checked) {
            this.listCriteria?.forEach(c => {
                if (c.id === courseId) {
                    c.selected = true;
                    this.criIds?.push(courseId);
                    this.criIdsChecked?.push(courseId);
                    return;
                }
            });
            if (this.criIds?.length > 0 && this.criIds?.length === this.listCriteria?.length && !this.checkBoxAllCriDelete) {
                this.checkBoxAllCriDelete = true;
            }
        } else {
            this.checkBoxAllCriDelete = false;
            this.listCriteria?.forEach(c => {
                if (c.id === courseId) {
                    c.selected = false;
                    this.criIds?.splice(this.criIds?.indexOf(courseId), 1);
                    this.criIdsChecked?.splice(this.criIdsChecked?.indexOf(courseId), 1);
                    return;
                }
            });
        }
    }

    changePage(event) {
        this.pageIndex = event.pageIndex + 1;
        this.pageSize = event.pageSize;
        this.getListCriteria();
    }

    onDeleteCri(ids: number[]) {
        const modalDep = this.modalService.open(ConfirmModalComponent, {
            size: 'lg',
            centered: true,
            backdrop: 'static'
        });
        modalDep.componentInstance.title = this.translateService.instant('criteria.delete_confirm_title');
        modalDep.componentInstance.body = this.translateService.instant('criteria.delete_confirm_content');
        modalDep.componentInstance.confirmButton = this.translateService.instant('common.delete');
        modalDep.result.then((result) => {
            if (result === 'confirm' && ids.length > 0) {
                this.spinner.show();
                this.criteriaService.deleteCriteria(ids.toString()).subscribe(
                    (res) => {
                        [...ids]?.forEach(id => {
                            this.criIds.splice(this.criIds.indexOf(id), 1);
                            this.criIdsChecked.splice(this.criIdsChecked.indexOf(id), 1);
                        });
                        this.spinner.hide();
                        this.toastrService.success('common.noti.delete_success');
                        this.getListCriteria();
                    }, (err) => {
                        this.spinner.hide();
                        this.toastrService.handlerError(err);
                    }
                );
            }
        }).catch(err => {
            if (isDevMode()) {
                console.log('Delete a Criteria', err);
            }
        });
    }

    onSearch(kw: any) {
        this.keyword = kw;
        this.pageIndex = 1;
        this.criIdsChecked = [];
        this.getListCriteria();
    }
}
