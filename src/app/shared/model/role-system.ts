export interface RoleSystem{
  id: number;
  name: string;
  shortname: string;
  shortnameTranslate: string;
  description: string;
  sortorder: string;
  cannotChooseAdmin: boolean;
  nameDisplay: string;
}
