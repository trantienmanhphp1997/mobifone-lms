import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrCustomService } from './../../../shared/services/toastr-custom.service';
import { BadgeService } from './../../../shared/services/badge.service';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { USER_INFO } from './../../../shared/constants/base.constant';
import { Component, OnInit } from '@angular/core';
import * as moment from 'moment';
import { FileService } from 'src/app/shared/services/file.service';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
@Component({
  selector: 'app-certificate-user-detail',
  templateUrl: './certificate-user-detail.component.html',
  styleUrls: ['./certificate-user-detail.component.css']
})
export class CertificateUserDetailComponent implements OnInit {

  certificate: any;
  userInfo: any;
  exportAsConfig: ExportAsConfig = {
    type: 'png',
    elementId: 'myTableElementId'
  }
  qrData: any;
  constructor(
    private $localStorage: LocalStorageService,
    private router: ActivatedRoute,
    private dadgeService: BadgeService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    private fileservice: FileService,
    private exportAsService: ExportAsService
  ) {}

  ngOnInit(): void {
    this.spinner.show();
    this.userInfo = this.$localStorage.retrieve(USER_INFO.INFO);
    this.dadgeService.getDetailBadge(+this.router.snapshot.paramMap.get('id')).subscribe(
      res => {
        this.certificate = res.body;
        this.qrData = this.certificate.id + ", " + this.certificate.name;
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      }
    );
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  viewImage(item) {
    var img = "";
    if (item.filename) {
      img = this.fileservice.badgeFileUrl(item.contextid, item.filename, '/external_badge/overviewfiles/','0/');
    }
    return img;
  }
  exportImage(certificate:any) {
    this.exportAsService.save(this.exportAsConfig, certificate.name).subscribe(() => {
    });
    this.exportAsService.get(this.exportAsConfig).subscribe(content => {
      console.log(content);
    });
  }
}
