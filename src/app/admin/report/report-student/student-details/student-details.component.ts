import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { LoginService } from 'src/app/shared/services/login.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { ReportService } from '../../../../shared/services/report.service';
import { StudentLearningHistoryComponent } from '../student-learning-history/student-learning-history.component';
import {FileService} from '../../../../shared/services/file.service';
import {NgxSpinnerService} from 'ngx-spinner';

@Component({
  selector: 'app-student-details',
  templateUrl: './student-details.component.html',
  styleUrls: ['./student-details.component.css']
})
export class StudentDetailsComponent implements OnInit, OnDestroy {
  id: any;
  sub: any;
  num: number= 1;
  listCourse: any;
  userFullname: string;
  typeSearchData: any;
  searchStatus: string;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  departmentName: string;
  totalRecord: number;
  searchType: any = {
    keyword: null,
    type: 0,
    pageSize: 10,
    pageIndex:1
  };
  constructor(
    private reportService: ReportService,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fileService: FileService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit(): void {
    this.sub = this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.reportCourseStudent();
    });
    this.typeSearchData = [
      {name: 'Khóa học theo lộ trình chức danh', id: 1},
      {name: 'Khóa học theo nhân sự đăng ký', id: 2},
      {name: 'Khóa học do quản trị hệ thống gán', id: 3},
      {name: 'Khóa học theo lãnh đạo cử đi', id: 4},
    ];
  }

  ngOnDestroy(): void {
    this.modalService.dismissAll();
  }

  reportCourseStudent() {
    const params = {
      keyword: this.searchType.keyword,
      type: this.searchType.type,
      userid: this.id,
      coursetype: 1,
      limit:  this.searchType.pageSize,
      page:  this.searchType.pageIndex,
    };
    this.spinner.show();
    this.reportService.reportStudentCourse(params).subscribe(
      (data) => {
        this.spinner.hide();
        this.listCourse = data.body.results;
        this.totalRecord = data.body.total;
        this.userFullname = data.body.userfullname;
        this.departmentName = data.body.departmentname;
      }, error => {
        this.spinner.hide();
        this.toastrService.handlerError(error);
      });
  }

  convertCompletion(completion) {
    if (completion !== null && completion === 100) {
      return 'Hoàn thành';
    }
    return 'Chưa hoàn thành';
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  openLearningHistory(courseId: any, courseName: any){
    const modalDep = this.modalService.open(StudentLearningHistoryComponent, {
      size: 'xl' as any,
      centered: false,
      backdrop: 'static',
      scrollable: true
    });
    modalDep.componentInstance.courseId = courseId;
    modalDep.componentInstance.courseName = courseName;
    modalDep.componentInstance.userId = this.id;
  }
  onFilterCourseType(typeFilter:any) {
    this.searchType.type = typeFilter;
    this.reportCourseStudent();
  }
  onSearch(event){
    this.listCourse.pageIndex = 1;
    this.searchType.keyword = event.target.value;
    this.reportCourseStudent();
  }
  onExportLearningDetail(){
    const params = {
      userid: this.id,
      coursetype: 1
    };
    this.spinner.show();
    this.reportService.exportLearningProgressDetail(params).subscribe(res => {
      window.open(this.fileService.getFileFromPathUrl(res.body.path));
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastrService.handlerError(err);
    });
  }
  changePage(event) {
    this.searchType.pageIndex = event.pageIndex + 1;
    this.searchType.pageSize = event.pageSize;
    this.reportCourseStudent();
  }

}
