import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CourseService } from '../../../../shared/services/course.service';
@Component({
  selector: 'app-course-qrcode-detail',
  templateUrl: './course-qrcode-detail.component.html',
  styleUrls: ['./course-qrcode-detail.component.css']
})
export class CourseQrcodeDetailComponent implements OnInit {
  @Input() course: any;
  @Input() qrcodeid:any;
  qrdata: any;
  constructor(
    public activeModal: NgbActiveModal,
  ) {}

  saveAsImage(parent){
    this.activeModal.dismiss();
    let canvas = parent.qrcElement.nativeElement.querySelector('canvas');
    const fileNameToDownload = 'image_qrcode';
    const url = canvas.toDataURL("image/jpeg");
    const link = document.createElement('a');
    link.href = url;
    link.download = fileNameToDownload;
    link.click();
  }
  ngOnInit(): void {
    this.qrdata = this.qrcodeid + ',' + this.course.shortname + ',' + this.course.fullname;
  }
}
