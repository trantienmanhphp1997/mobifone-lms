import { Component, OnInit,Input } from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
@Component({
  selector: 'app-detail-error',
  templateUrl: './detail-error.component.html',
  styleUrls: ['./detail-error.component.css']
})
export class DetailErrorComponent implements OnInit {
  @Input() listError: any;
  listError2: any;
  listDetailError: any =[];
  constructor(
    private activeModal:NgbActiveModal
  ) { }

  ngOnInit(): void {
   this.listDetailError = this.listError.errors.split(',');
  }
  close() {
    this.activeModal.dismiss('close');
  }
}
