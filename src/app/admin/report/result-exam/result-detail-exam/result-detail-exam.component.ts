import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { QuizContentService } from 'src/app/shared/services/quiz-content.service';
import { CourseService } from 'src/app/shared/services/course.service';
import { Course } from 'src/app/shared/model/course.model';
import { ModuleInfo } from 'src/app/shared/model/moduleinfo.model';
import { ReportService } from '../../../../shared/services/report.service';
import { ResultExamUserComponent } from '../result-exam-user/result-exam-user.component';
import { RoleSystem } from 'src/app/shared/model/role-system';
import { UserService } from 'src/app/shared/services/user.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from 'ngx-webstorage';
import { USER_ROLE } from 'src/app/shared/constants/base.constant';

@Component({
  selector: 'app-result-detail-exam',
  templateUrl: './result-detail-exam.component.html',
  styleUrls: ['./result-detail-exam.component.css']
})
export class ResultDetailExamComponent implements OnInit, OnDestroy {
  typesOfShoes: string[] = ['Boots', 'Clogs', 'Loafers', 'Moccasins', 'Sneakers'];
  courseId: number;
  modules: ModuleInfo[];
  quizId: number;
  idquiz: number;
  students = [];
  course: Course;
  totalStudents: number;
  passedStudents: number;
  failedStudents: number;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  pageIndex = 1;
  pageSize = 10;
  userIds = [];
  limit = 300;
  keywordStudent: string;
  userIdsChecked = [];
  roleList: RoleSystem[] = [];
  selectedStudent = [];
  selectRoleEvent = false;
  selectedStudents = [];
  roleListRemoveRoleManager: RoleSystem[] = [];
  showFullStudent = false;
  courses = [];
  dropdownStudentSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'fullname',
    selectAllText: 'Chọn tất cả',
    unSelectAllText: 'Bỏ chọn tất cả',
    searchPlaceholderText: 'Tìm kiếm',
    itemShowLimit: 5,
    allowSearchFilter: true,
  };
  elements: any;
  searchExam = {
    search: '',
    userids: '',
    //  startDate: null,
    //  endDate: null,
    //  categoryid: null,
    pageIndex: 1,
    pageSize: 10,
    //  sortColumn: 'timecreated',
    //  sortType: 'desc',
    coursetype: '',
  };

  constructor(
    private route: ActivatedRoute,
    public quizContentService: QuizContentService,
    private modalService: NgbModal,
    private reportService: ReportService,
    private courseService: CourseService,
    private spinner: NgxSpinnerService,
    private userService: UserService,
    private toastrService: ToastrCustomService,
    private translateService: TranslateService,
    private $localStorage: LocalStorageService,
  ) {}

  ngOnInit(): void {
    this.courseId = Number(this.route.snapshot.paramMap.get('id'));
    this.getDetailCourse();
    this.getListQuiz();
  }

  onSelectStudentEvent() {
    this.userIds = [];
    this.userIdsChecked = [];
    this.selectRoleEvent = true;
  }
  onSelectDeStudentEvent() {
    this.userIds = [];
    this.userIdsChecked = [];
    this.selectRoleEvent = true;
    this.keywordStudent = '';
    this.getListStudentData();
  }
  ngOnDestroy(): void {
    this.modalService.dismissAll();
  }

  getListQuiz() {
    this.quizContentService.listQuiz(this.courseId, 0).subscribe((data) => {
      this.modules = data.body[0]?.listquiz;
      if (this.modules.length > 0) {
        // tslint:disable-next-line: no-string-literal
        this.modules.forEach(m => m['selected'] = false);
        // tslint:disable-next-line: no-string-literal
        this.modules[0]['selected'] = true;
        this.quizId = this.modules[0].instance;
        this.getListStudentByQuiz();
      }
    });
  }

  onSearchStudent(){
    this.elements = document.getElementsByTagName('input');
    this.elements[1].addEventListener('keydown', (event) => {
      setTimeout(() => {
        this.keywordStudent =  this.elements[1].value;
        this.getListStudentData();
      }, 100);
    });
  }

  onScrollDown(){
    this.limit += 10;
    this.getListStudentData();
  }

  changeStudent() {
    if (this.selectRoleEvent) {   
      // Vi su kien onDropDownClose luon ban ra, vi vay co them co de kiem tra truoc do co hanh vi thay doi chuc danh khong
      this.selectRoleEvent = false;
      this.selectedStudents = [];
      for (const stu of this.selectedStudent) {
        this.selectedStudents.push(
          stu.id         
        );
      }
      this.pageIndex = 1;
      this.userIds = [];
      this.userIdsChecked = [];
      this.searchStudent();
    }
  }


  getListStudentData() {

    // if (this. > 0){
      const params = {
        limit: this.limit,
        keyword: this.keywordStudent,
        idquiz: this.quizId
      };
  
      return this.courseService.getUserList(params).subscribe((roleList) => {
        this.roleList = roleList.body.results;
        
        this.roleList.forEach((role) => {
          role.shortnameTranslate = this.getShortName(role.shortname);
        });
        this.roleListRemoveRoleManager = this.roleList.filter((role) => {
          return role.shortname !== USER_ROLE.STUDENT;
        });
        const roleUser = this.$localStorage.retrieve('roles')[0].shortname;
        if (roleUser === USER_ROLE.MANAGER) {
          this.showFullStudent = false;
        } else if (roleUser === USER_ROLE.STUDENT) {
          this.showFullStudent = true;
        }
      });
    // }

    
  }

  getShortName(shortname) {
    if (shortname) {
      return this.translateService.instant('user.' + shortname);
    }
    return '';
  }

  searchStudent(){
    const params = {
      limit: this.pageSize,
      page: this.pageIndex,
      userids: this.selectedStudents.toString(),
      quizid: this.quizId
    };
    this.spinner.show();
    this.userIds = [];
    this.userService.getUserListQuizUserList(params).subscribe(
      (rs) => {
        this.students = rs.body.results;
        this.keywordStudent = '';
        this.getListStudentData(); 
        this.totalStudents = rs.body.total;
        this.passedStudents = rs.body.studentpass;
        this.failedStudents = rs.body.studdentfaild;
        this.spinner.hide();
      },
      (err) => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      }
    );
  }

  openResultUser(student: any) {
    const modalDep = this.modalService.open(ResultExamUserComponent, {
      size: 'xl' as any,
      centered: false,
      backdrop: 'static',
      scrollable: true
    });
    modalDep.componentInstance.quizAttemptid = student.id;
    modalDep.componentInstance.studentName = student.fullname;
    modalDep.componentInstance.gradeuser = student.gradeuser;
    modalDep.componentInstance.gradepass = student.gradepass;
    modalDep.componentInstance.correctquestion = student.correctquestion;
    modalDep.componentInstance.incorrectquestion = student.incorrectquestion;
    modalDep.componentInstance.timestart = student.timestart;
    modalDep.componentInstance.grade = student.grade;
    modalDep.componentInstance.completionstate = student.completionstate;
  }

  onSelectQuiz(quiz: any) {
    if (quiz.instance === this.quizId){
      return;
    }
    this.quizId = quiz.instance;
    this.pageIndex = 1;
    this.pageSize = 10;
    this.getListStudentByQuiz();
  }

  getListStudentByQuiz(){
    let total1 = 0;
    const params = {
      quizid: this.quizId,
      limit: this.pageSize,
      page: this.pageIndex
    };
    this.spinner.show();
    this.reportService.getListStudentByQuiz(params).subscribe(rs => {
      this.students = rs.body.results;
      total1 = rs.body.total;
      this.totalStudents = rs.body.total;
      this.passedStudents = rs.body.studentpass;
      this.failedStudents = rs.body.studdentfaild;
      if ( rs.body.total > 0){
        this.getListStudentData();
      }
      this.spinner.hide();
    }, () => {
        this.spinner.hide();
    });
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date).format('DD/MM/YYYY - HH:mm');
    }
    return null;
  }

  getDetailCourse(){
    this.courseService.getCoursesInfo(this.courseId).subscribe(rs => {
      this.course = rs.body;
    });
  }

  changePage(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getListStudentByQuiz();
  }
}
