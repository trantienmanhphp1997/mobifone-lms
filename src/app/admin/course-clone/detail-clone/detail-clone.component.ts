import { Component, Input, OnInit, Output , EventEmitter } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CourseService } from 'src/app/shared/services/course.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { ClonePopupComponent } from '../clone-popup/clone-popup.component';
import * as moment from 'moment';
import { Item } from '@syncfusion/ej2-angular-navigations';
import { PositionService } from 'src/app/shared/services/position.service';

@Component({
  selector: 'app-detail-clone',
  templateUrl: './detail-clone.component.html',
  styleUrls: ['./detail-clone.component.css']
})
export class DetailCloneComponent implements OnInit {

  @Input() data: any;
  @Input() status: any;
  @Input() type: any;
  @Output() emitter1: EventEmitter<any> = new EventEmitter();
  requestcontent: string = '';
  nameBadge:any =[];
  namePosition:any =[];
  badgeList: any = [];
  positionList:any =[];
  constructor(
    public activeModal: NgbActiveModal,
    private toastrService: ToastrCustomService,
    private courseService: CourseService,
    private modalService: NgbModal,
    private positionService: PositionService,
  ) { }

  ngOnInit(): void {
    this.getListBadge();
    this.getListPosition();
  }
  sendRequest(status:any, data: any){
    this.activeModal.dismiss();
    const modalDep = this.modalService.open(ClonePopupComponent, {
          size: 'lg',
          centered: true,
          backdrop: 'static'
    });
    modalDep.componentInstance.data = data;
    modalDep.componentInstance.status = status;
    modalDep.componentInstance.emitter.subscribe( result =>{
      if(result){
        this.emitter1.emit({
          success: true,
        });
      }
    })
  }
  getListBadge(){
    debugger
    this.courseService.getListBadge().subscribe(data=>{
      this.badgeList = data.body.results;
      const badgeIdArray = this.data.badgerequire.split(',');
      badgeIdArray.forEach((item:any)=>{
        const foundBadge = this.badgeList.find((data: any) => {
          return data.id == item
        }); 
        if (foundBadge) {
        this.nameBadge.push(foundBadge.name);
        }
      })
    })
  }
  getListPosition() {
    return this.positionService.getPosition().subscribe(positionList => {
      this.positionList = positionList.body.results;
      const badgeIdArray = this.data.trainingobject.split(',');
      badgeIdArray.forEach((item:any)=>{
        const foundBadge = this.positionList.find((data: any) => {
          return data.id == item
        }); 
        if (foundBadge) {
        this.namePosition.push(foundBadge.name);
        }
      })
    });
  }
  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }
}
