import { Component, OnInit, ViewChild } from '@angular/core';
import { Sort } from '@angular/material/sort';
import { Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalStorageService } from 'ngx-webstorage';
import { USER_INFO, USER_ROLE } from 'src/app/shared/constants/base.constant';
import { ConfirmModalComponent } from 'src/app/shared/modal/confirm-modal/confirm-modal.component';
import { Department } from 'src/app/shared/model/department.model';
import { CourseService } from 'src/app/shared/services/course.service';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { DepartmentDetailComponent } from '../department/department-detail/department-detail.component';
import { CourseRecommedDetailComponent } from './course-recommend-detail/course-recommend-detail.component';

@Component({
    selector: 'app-course-recommend',
    templateUrl: 'course-recommend.component.html'
})

export class CourseRecommendComponent implements OnInit {
    // Tree
    @ViewChild('departmentTree')
    public departmentTree: TreeViewComponent;
    public departmentTreeData;
    currentSelectedId: number;

    // List
    courseList: Department[] = [];

    // For delete
    completedAll = false;
    departmentIds: number[] = [];
    courseRecommendIdsChecked: number[] = [];
    isDepartmentRoot: boolean = false;

    // pagination & search
    totalRecord: number;
    pageSize = 10;
    pageIndex = 1;
    pageSizeOptions: number[] = [10, 25, 50, 100];
    sortColumn = 'id';
    sortType = 'ASC';
    keyword = '';
    isAdmin = false;
    constructor(
        private modalService: NgbModal,
        private departmentService: DepartmentService,
        private toastrService: ToastrCustomService,
        private translateService: TranslateService,
        private courseService: CourseService,
        private spinner: NgxSpinnerService,
        private $localStorage: LocalStorageService,
        private router: Router
    ) { }

    ngOnInit(): void {
        const roles = this.$localStorage.retrieve(USER_INFO.ROLES);
        if (roles) {
            for (const role of roles) {
                if (role.shortname === USER_ROLE.ADMIN) {
                    this.isAdmin = true;
                }
            }
        }
        this.reLoadData();
    }

    ngOnDestroy(): void {
        this.modalService.dismissAll();
    }

    oncheckboxAll(checked: any) {
        if (checked) {
            this.courseList.forEach(c => {
                c.completed = checked;
                if (!this.departmentIds.includes(c.id)) {
                    this.departmentIds.push(c.id);
                }
                if (!this.courseRecommendIdsChecked?.includes(c.id)) {
                    this.courseRecommendIdsChecked?.push(c.id);
                }
            });
            this.completedAll = true;
        } else {
            this.departmentIds?.forEach(id => {
                this.courseRecommendIdsChecked?.splice(this.courseRecommendIdsChecked?.indexOf(id), 1);
            });
            this.departmentIds = [];
            this.courseList?.forEach(c => {
                c.completed = false;
            });
            this.completedAll = false;
        }
    }

    oncheckboxItem(courseId: number, checked: any) {
        if (checked) {
            const dpartmentNumber = this.isDepartmentRoot ? (this.courseList?.length - 1) : this.courseList?.length;
            this.courseList?.forEach(c => {
                if (c.id === courseId) {
                    c.completed = true;
                    this.departmentIds?.push(courseId);
                    this.courseRecommendIdsChecked?.push(courseId);
                    return;
                }
            });
            if (this.departmentIds?.length > 0 && this.departmentIds?.length === dpartmentNumber && !this.completedAll) {
                this.completedAll = true;
            }
        } else {
            this.completedAll = false;
            this.courseList?.forEach(c => {
                if (c.id === courseId) {
                    c.completed = false;
                    this.departmentIds?.splice(this.departmentIds?.indexOf(courseId), 1);
                    this.courseRecommendIdsChecked?.splice(this.courseRecommendIdsChecked?.indexOf(courseId), 1);
                    return;
                }
            });
        }
    }

    onCreateDepartment() {
        const current: Department = {
            name: '',
            description: '',
            parentid: this.currentSelectedId,
            code: null,
            selected: null,
        };
        const title = 'department.create_title';
        const button = 'common.add';
        this.openEditCoursePopup(current, title, button);
    }

    onEditDepartment(item) {
        // Set lại để loại bỏ các trường thừa như createduser///
        const current = {
            id: item.id,
            name: item.name,
            code: item.code,
        };
        const title = 'department.update_title';
        const button = 'common.save';
        this.openEditCoursePopup(current, title, button);
    }

    openEditCoursePopup(item: any, title: string, button: string) {
        const modalDep = this.modalService.open(CourseRecommedDetailComponent, {
            size: 'lg',
            centered: true,
            backdrop: 'static'
        });
        modalDep.componentInstance.department = item;
        // modalDep.componentInstance.courseList = this.departmentTreeData.dataSource;      
        modalDep.componentInstance.title = title;
        modalDep.componentInstance.button = button;
        modalDep.componentInstance.newDepartment.subscribe(($e) => {
            this.reLoadData();
        });

        modalDep.componentInstance.update.subscribe(($e) => {
            // console.log('test: ');
            // console.log($e);
        });
    }

    onDeleteMultipleDepartment() {
        this.onDeleteDepartment(this.courseRecommendIdsChecked);
    }

    onDeleteSingleDepartment(departmentId: number) {
        this.onDeleteDepartment([departmentId]);
    }

    onDeleteDepartment(ids: number[]) {
        const modalDep = this.modalService.open(ConfirmModalComponent, {
            size: 'lg',
            centered: true,
            backdrop: 'static'
        });
        modalDep.componentInstance.title = this.translateService.instant('department.delete_confirm_title');
        modalDep.componentInstance.body = this.translateService.instant('department.delete_confirm_content');
        modalDep.componentInstance.confirmButton = this.translateService.instant('common.delete');

        modalDep.result.then((result) => {
            // console.log('result: ', result);
            this.spinner.show();
            this.departmentService.deleteDepartment(ids).subscribe(
                res => {
                    const dataDepartment = this.departmentTree.getTreeData();
                    [...ids]?.forEach(element => {
                        const indexArr = this.courseList.findIndex(item => item.id === element);
                        dataDepartment.splice(indexArr, 1);
                        this.departmentTreeData = { dataSource: dataDepartment, id: 'id', parentID: 'parentid', text: 'name', hasChildren: 'haschild' };
                        this.departmentIds.splice(this.departmentIds.indexOf(element), 1);
                        this.courseRecommendIdsChecked.splice(this.courseRecommendIdsChecked.indexOf(element), 1);
                    });
                    this.spinner.hide();
                    this.toastrService.success(`common.noti.delete_success`);
                    this.reLoadData();
                },
                err => {
                    this.spinner.hide();
                    this.reLoadData();
                    this.toastrService.handlerError(err);
                }
            );
        });
    }

    onSearch() {
        const params = {
            limit: this.pageSize,
            page: this.pageIndex,
            keyword: this.keyword,
            id: this.currentSelectedId,
            sortcolumn: this.sortColumn,
            sorttype: this.sortType
        };
        this.spinner.show();
        this.departmentIds = [];
        this.isDepartmentRoot = false;
        this.courseService.searchCoursesRecommend(params).subscribe((data) => {
            this.courseList = data.body.results;
            this.totalRecord = data.body.total;
            this.spinner.hide();
        },
            error => {
                this.toastrService.handlerError(error);
                this.spinner.hide();
            });
    }

    reLoadData() {
        this.onSearch();
    }

    changePage(event) {
        this.pageIndex = event.pageIndex + 1;
        this.pageSize = event.pageSize;
        this.onSearch();
    }

    /**
     * clear select node
     * clear search param
     */
    clear() {
        // param phuc vu tim kiem
        this.keyword = '';
        this.currentSelectedId = null;
        this.pageSize = 10;
        this.pageIndex = 1;

        // Bo select node tren cay
        this.departmentTree.selectedNodes = [];
        this.courseRecommendIdsChecked = [];

        this.onSearch();
    }

    sortData(sort: Sort) {
        this.pageIndex = 1;
        this.sortColumn = sort.active;
        this.sortType = sort.direction;
        this.onSearch();
    }

    onChangeKeyWord() {
        this.pageIndex = 1;
        this.onSearch();
    }
    yearSelected(value: any) {
        // this.searchRank.year = value ? value[0] : null;
        // this.getRankTable();
    }
}