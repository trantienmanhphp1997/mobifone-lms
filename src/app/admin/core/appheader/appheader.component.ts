import {Component, OnInit} from '@angular/core';
import {LoginService} from '../../../shared/services/login.service';
import { LocalStorageService } from 'ngx-webstorage';
@Component({
  selector: 'app-appheader',
  templateUrl: './appheader.component.html',
  styleUrls: ['./appheader.component.css']
})
export class AppheaderComponent implements OnInit {

  lang:any;
  constructor(
    private loginService: LoginService,
    private localStorage: LocalStorageService,
    
    ) {

  }

  ngOnInit(): void {
    this.lang =  this.localStorage.retrieve('mb-locale');
  }

  logout(){
    this.loginService.logout();
  }
  onchangeLanguage(event: any){
    this.localStorage.store('mb-locale', event.target.value);
    location.reload();
  }

}
