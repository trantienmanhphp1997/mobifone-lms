import {Component} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {environment} from '../environments/environment';
import { LocalStorageService } from 'ngx-webstorage';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'lms-frontend';
  ribbon = environment.ribbon;
 
  constructor(
    private translate: TranslateService,
    private localStorage: LocalStorageService,
    ) {
      let language = this.localStorage.retrieve('mb-locale');
      if (!language) {
        language = 'vi';
        this.localStorage.store('mb-locale', language);
      }
      this.setLanguage(language)
  }
  setLanguage(language): void {
    // this language will be used as a fallback when a translation isn't found in the current language
    this.translate.setDefaultLang(language)
    // the lang to use, if the lang isn't available, it will use the current loader to get them
    this.translate.use(language);
 }
}
