import { Component, Input, OnInit, Output , EventEmitter } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CourseService } from 'src/app/shared/services/course.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';

@Component({
  selector: 'app-clone-popup',
  templateUrl: './clone-popup.component.html',
  styleUrls: ['./clone-popup.component.css']
})
export class ClonePopupComponent implements OnInit {
  @Input() data: any;
  @Input() status: any;
  @Input() type: any;
  @Output() emitter: EventEmitter<any> = new EventEmitter();
  requestcontent: string = '';
  constructor(
    public activeModal: NgbActiveModal,
    private toastrService: ToastrCustomService,
    private courseService: CourseService,
  ) { }

  ngOnInit(): void {
  }
  sendRequest(){
    if(this.requestcontent.trim() =="" && this.status !=4){
      this.toastrService.error(`Nội dung không được bỏ trống`);
    }
    else if(this.status){
      const params ={
        status: this.status,
        id:this.data.id,
        responsecontent: this.requestcontent
      }
      this.courseService.cloneCourseUpdate(params).subscribe( data =>{
        if(data){
          this.toastrService.success(`common.noti.send_request_clone`);
          this.emitter.emit({
            success: true,
          });
          this.activeModal.dismiss();

        }
      })
    }
    else if(this.type){
      const params ={
        status:this.type,
        id:this.data.id,
        responsecontent: this.requestcontent
      }
      this.courseService.updateTrainingPlanUpdate(params).subscribe( data =>{
        if(data){
          this.toastrService.success(`Phê duyệt chỉnh sửa thành công !`);
          this.emitter.emit({
            success: true,
          });
          this.activeModal.dismiss();

        }
      })
    }
  }

}
