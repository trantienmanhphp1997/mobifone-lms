import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { UnRegisterCourseConfirmModalComponent } from './../course-card/un-register-course-confirm-modal/un-register-course-confirm-modal.component';
import { PageEvent } from '@angular/material/paginator';
import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { FileService } from '../../shared/services/file.service';
import * as moment from 'moment';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { CourseService } from '../../shared/services/course.service';
import {
  DEFAULT_COURSE_IMAGE,
  TRAINING_TYPE,
} from '../../shared/constants/base.constant';
import * as ClassicEditor from 'src/assets/ckeditor5-build-classic';
import { CKEDITOR_CONFIG } from 'src/app/shared/constants/ckeditor.constant';
import { COMPLETE_TIME_TYPE } from '../../shared/constants/base.constant';
import { FormBuilder, FormGroup, Validators, FormArray } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { LocalStorage } from 'ngx-webstorage';
import { LocalStorageService } from 'ngx-webstorage';
import {
  BADGE_TYPE,
  COST_SOURCE,
  LIST_HOURS,
  LIST_MINUTES,
  PLAN_ROLE,
  USER_INFO,
  USER_ROLE,
} from '../../shared/constants/base.constant';
import CommonUtil from 'src/app/shared/utils/common-util';
import { log } from 'console';
import { EuEvaluationComponent } from './eu-evaluation/eu-evaluation.component';
import { ViewChild} from '@angular/core';
@Component({
  selector: 'app-eu-course-detail',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css'],
})
export class CourseDetailComponent implements OnInit, OnDestroy {
  editorConfig = CKEDITOR_CONFIG.NOT_UPLOAD;
  public Editor = ClassicEditor;
  currentDate: Date = new Date();
  course: any;
  progress: any = 0;
  listTeacher: any = [];
  listOtherStudent: any = [];
  openTab: number;
  sub: any;
  isTeacher = false;
  ishowtabBT = false;
  pageIndex = 1;
  pageSize = 6;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  totalStudent: number;
  permission = true;
  listCompleteTimeType = COMPLETE_TIME_TYPE;
  // MatPaginator Output
  pageEvent: PageEvent;
  courseId: number;
  courseImageUrl;
  evaluationInfo3M: any = [];
  commitmentForm: FormGroup;
  formCommitData: any = [];
  listCommitmentByUser: any = [];
  listCheckRadio = [{ value: 3 }, { value: 6 }];
  chosenItem: number;
  newArrays: any = [];
  totalRecord: number;
  userLoginId: number;
  @ViewChild('euEvaluation') euEvaluation!:EuEvaluationComponent;

  constructor(
    private activatedRoute: ActivatedRoute,
    private fileService: FileService,
    private sanitizer: DomSanitizer,
    private toastrService: ToastrCustomService,
    private courseService: CourseService,
    private fileServide: FileService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService,
    private router: Router,
    private fb: FormBuilder,
    private $localStorage: LocalStorageService
  ) {}

  ngOnInit(): void {
    this.openTab = 1;
    this.chosenItem = this.listCheckRadio[0].value;
    this.userLoginId = this.$localStorage.retrieve(USER_INFO.INFO).id;
    this.sub = this.activatedRoute.paramMap.subscribe((params) => {
      this.courseId = +params.get('id');
      this.getDetail();
      this.getCourseDetail();
      this.getListTeacher();
      this.getListStudent();
      this.initForm();
      this.getListCommitmentActionCourseByUser();
      if (!this.isTeacher) {
        this.getProgressOfCourse();
      }
    });
  }
  getDetail() {
    this.courseService
      .getDetailEvaluationEndUser(this.courseId, 3)
      .subscribe((res) => {
        this.evaluationInfo3M = res.body;
      });
  }

  initForm() {
    this.commitmentForm = this.fb.group({
      contentcommitment: ['', Validators.required],
      method: ['', Validators.required],
      objectives: ['', Validators.required],
      completetime: ['', Validators.required],
      cost: ['', Validators.required],
    });
  }

  ////gửi cam kết
  sendCommitment() {
    console.log('vào send commit');
    const params = {
      courseid: this.course.id,
      details: this.newArrays,
    };
    console.log(params);
    this.spinner.show();
    this.courseService.contentCommitment(params).subscribe(
      (res) => {
        if (res) {
          this.spinner.hide();
          this.toastrService.success('Gửi cam kết thành công');
          this.getCourseDetail();
          this.getListCommitmentActionCourseByUser();
          this.getProgressOfCourse();
          this.newArrays = [];
        } else {
          this.toastrService.error('Chưa có cam kết mới');
        }
      },
      (err) => {
        this.spinner.hide();
        this.toastrService.error('Chưa có cam kết mới');
      }
    );
  }
  ngOnDestroy(): void {
    this.modalService.dismissAll();
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  getCourseDetail() {
    return this.courseService.getCoursesInfoEnduser(this.courseId).subscribe(
      (data) => {
        this.course = data.body;
        this.isTeacher = this.course.roleincourse === 'teacher';

        if (this.course.filename) {
          const urlFile = this.fileService.getFileUrl(
            this.course.contextid,
            this.course.filename,
            '/course/overviewfiles/'
          );
          this.courseImageUrl =
            this.sanitizer.bypassSecurityTrustResourceUrl(urlFile);
        } else {
          this.courseImageUrl = DEFAULT_COURSE_IMAGE;
        }
      },
      (err) => {
        this.permission = false;
        // this.toastrService.handlerError(err);
      }
    );
  }

  getListTeacher() {
    const param = {
      courseid: this.courseId,
      useris: 'teacher',
      sortcolumn: 'roleshortnameincourse',
      sorttype: 'desc',
      page: this.pageIndex,
      limit: this.pageSize,
    };

    return this.courseService.getListUser(param).subscribe(
      (data) => {
        this.listTeacher = data.body;
        this.listTeacher.results.forEach((teacher) => {
          if (teacher.pictureid) {
            teacher.userpictureurl = this.fileServide.getFileUrlWithRevision(
              teacher.contextid,
              teacher.picturename,
              '/user/icon/boost/',
              teacher.pictureid
            );
          }
        });
      },
      (err) => {
        // this.toastrService.handlerError(err);
      }
    );
  }

  changePage($event) {
    this.pageIndex = $event.pageIndex + 1;
    this.pageSize = $event.pageSize;
    this.getListStudent();
    this.getListCommitmentActionCourseByUser();
  }

  getListStudent() {
    const param = {
      courseid: this.courseId,
      useris: 'student',
      sortcolumn: 'roleshortnameincourse',
      sorttype: 'desc',
      page: this.pageIndex,
      limit: this.pageSize,
    };

    return this.courseService.getListUser(param).subscribe(
      (data) => {
        this.listOtherStudent = data.body.results;
        this.listOtherStudent.forEach((student) => {
          if (student.pictureid) {
            student.userpictureurl = this.fileServide.getFileUrlWithRevision(
              student.contextid,
              student.picturename,
              '/user/icon/boost/',
              student.pictureid
            );
          }
        });
        this.totalStudent = data.body.total;
      },
      (err) => {
        // this.toastrService.handlerError(err);
      }
    );
  }

  getProgressOfCourse() {
    return this.courseService.getProgress(this.courseId).subscribe((data) => {
      this.progress = data.body.percentcourse;
    });
  }

  onUnRegisterCourse() {
    const modalDep = this.modalService.open(
      UnRegisterCourseConfirmModalComponent,
      {
        size: 'lg',
        centered: true,
        backdrop: 'static',
      }
    );

    modalDep.result.then((result) => {
      if (result === 'confirm') {
        this.spinner.show();
        this.courseService.userUnRegisterCourse(this.course?.id).subscribe(
          (res) => {
            this.spinner.hide();
            this.toastrService.success(
              `common.noti.unresgister_course_success`
            );
            this.router.navigate(['/my-course']);
          },
          (err) => {
            this.spinner.hide();
            this.toastrService.handlerError(err);
          }
        );
      }
    });
  }
  getTrainingType(val: any) {
    return TRAINING_TYPE.filter((e) => e.value == val)[0]?.label;
  }
  displayListData() {
    if (this.commitmentForm.invalid) {
      CommonUtil.markFormGroupTouched(this.commitmentForm);
      return;
    }
    this.listCommitmentByUser['results']?.push(this.commitmentForm.value);
    this.newArrays.push(this.commitmentForm.value);
    this.commitmentForm.reset();
    this.sendCommitment();
  }

  getListCommitmentActionCourseByUser() {
    const param = {
      courseid: this.courseId,
      page: this.pageIndex,
      limit: this.pageSize,
      userid: this.userLoginId,
    };

    return this.courseService.getListCommitment(param).subscribe(
      (data) => {
        this.listCommitmentByUser = data.body;
        console.log(this.listCommitmentByUser);
        this.totalRecord = data.body.total;
      },
      (err) => {
        // this.toastrService.handlerError(err);
      }
    );
  }

  openModal() {
    this.chosenItem = this.listCheckRadio[0].value;
  }

  loadCommit(){
    this.euEvaluation.getListCommitmentActionCourseByUser();
  }
}
