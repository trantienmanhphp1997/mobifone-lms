import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Router } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { COST_SOURCE, LIST_HOURS, LIST_MINUTES, TRAINING_TYPE } from 'src/app/shared/constants/base.constant';
import { CourseCategory } from 'src/app/shared/model/course-category.model';
import { Position } from 'src/app/shared/model/position.model';
import { CourseService } from 'src/app/shared/services/course.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import CommonUtil from 'src/app/shared/utils/common-util';
import { QUARTER } from 'src/app/shared/constants/base.constant';
import * as moment from 'moment';

@Component({
    selector: 'app-list-class-in-course-plan-detail',
    templateUrl: 'list-class-in-course-plan-detail.component.html',
    styleUrls: ['./list-class-in-course-plan-detail.component.css']
})

export class ListClassInCoursePlanDetailComponent implements OnInit {
    @Input() courseClone: any;
    @Input() action: any;
    @Input() planId: any;
    @Input() canEdit: any;
    @Input() positionList: Position[] = [];
    @Output() loadAll = new EventEmitter();
    quarterData = QUARTER;
    isCreateExam = false;
    listClass: any = [];
    courseType = 1;
    listTrainingType = TRAINING_TYPE;
    currentDate: Date = new Date();
    totalRecord: number;
    pageSize = 10;
    pageIndex = 1;
    pageSizeOptions: number[] = [10, 25, 50, 100];
    sortColumn = 'id';
    success: any;
    sortType = 'ASC';
    constructor(
        private toastrService: ToastrCustomService,
        private courseService: CourseService,
        public activeModal: NgbActiveModal,
        private router: Router,
        private spinner: NgxSpinnerService,
    ) { }

    ngOnInit(): void {
        this.loadData();
    }

    loadData() {
        const params = {
            // planid: this.courseClone?.planid,
            proposecourseid: this.courseClone?.id,
            limit: this.pageSize,
            page: this.pageIndex,
        }
        this.courseService.searchCourses(params).subscribe(data => {
            data.body?.results?.forEach(course => {
                course.teacherName = CommonUtil.convertTeacherName(course.teachers);
                if (this.currentDate > moment.unix(course.enddate).toDate()) {
                  course.status = 'Đã kết thúc';
                  course.style = 'success';
                }
                if (this.currentDate < moment.unix(course.startdate).toDate()) {
                  course.status = 'Chưa bắt đầu';
                  course.style = 'secondary';
                }
                if ((moment.unix(course.startdate).toDate() < this.currentDate && this.currentDate < moment.unix(course.enddate).toDate()) || (moment.unix(course.startdate).toDate() < this.currentDate && course.enddate == 0)) {
                  course.status = 'Đang diễn ra';
                  course.style = 'info';
                }
            });
            this.listClass = data.body.results;
            this.totalRecord = data.body.total;
            this.spinner.hide();
        }, err => {
            this.spinner.hide();
            this.toastrService.handlerError(err);
        });
    }
    
    changePage(event) {
        this.pageIndex = event.pageIndex + 1;
        this.pageSize = event.pageSize;
        this.loadData();
      }
    

    

    onViewDetail(course: any) {
        this.router.navigate(['/admin/course/detail', course?.id]);
        this.activeModal.dismiss();
        this.spinner.hide();
    }

    /**
     * Utils
     *
     * Method: getTrainingType(), getDateFromUnix(), gotoListPlan(), showCost()
     *         chooseYear(), getPositionNames()
     */

    getTrainingType(val: any) {
        return TRAINING_TYPE.filter(e => e.value == val)[0]?.label
    }

    getPositionNames(trainingobject: any) {
        let positionName = '';
        if (trainingobject) {
            const positionIdArray = trainingobject.split(',');
            for (const positionId of positionIdArray) {
                const foundPosition = this.positionList.find(value => value.id === +positionId);
                if (positionName.length > 0) {
                    positionName += ', ';
                }
                if (foundPosition && foundPosition.name !== undefined) {
                    positionName += foundPosition.name;
                }
            }
            return positionName;
        }
        return '';
    }

    showCost(value: any) {
        return Intl.NumberFormat('vi-VN').format(value);
    }

    getDateFromUnix(date) {
        if (date) {
          return moment.unix(date);
        }
        return null;
    }

    convertCost(value: any) {
        let price = value.replace(/[^\d]/g, '') * 1
        // this.courseForm.patchValue({
        //     cost: Intl.NumberFormat('vi-VN').format(price)
        // });
    }

    checkTimeStart(item) {
        if (item.planid !== null && item.startdate < (new Date().getTime() / 1000) && !item.published) {
          return "";
        }
        return "text-danger";
    }
}