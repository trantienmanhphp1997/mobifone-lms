import { ToastrCustomService } from '../../../shared/services/toastr-custom.service';
import { PageEvent } from '@angular/material/paginator';
import { NgxSpinnerService } from 'ngx-spinner';
import { BadgeService } from '../../../shared/services/badge.service';
import { Component, OnInit, Input } from '@angular/core';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { FileService } from 'src/app/shared/services/file.service';
import { ActivatedRoute } from '@angular/router';
import { Spinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { ConfirmModalComponent } from 'src/app/shared/modal/confirm-modal/confirm-modal.component';
import { BuiltinTypeName } from '@angular/compiler';
import { LocalStorageService } from 'ngx-webstorage';
import { USER_INFO } from 'src/app/shared/constants/base.constant';

@Component({
  selector: 'app-badge-offline-student',
  templateUrl: './badge-offline-student.component.html',
  styleUrls: ['./badge-offline-student.component.css']
})
export class BadgeOfflineStudentComponent implements OnInit {

  @Input() readonly: boolean;
  // @Input() badgeId: number;
  base64Image: any;
  mimetype:"";
  isShowDownload = false;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  pageEvent: PageEvent;
  studentonBadge: any[];
  totalRecord: number;
  roleChecked = false;
  searchBadgeStudent = {
    badgeid: null,
    search: '',
    page: 1,
    limit: 10,
    // sortcolumn: 'dateissued',
    sorttype: 'desc',
    status: null
  }

  constructor(
    private modalService: NgbModal,
    private badgeService: BadgeService,
    private fileservice: FileService,
    private _Activatedroute: ActivatedRoute,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    private $localStorage: LocalStorageService
  ) { }

  ngOnInit(): void {
    const roles = this.$localStorage.retrieve(USER_INFO.ROLES);
    if (roles) {
      if (roles[0].shortname == "manager") {
        this.roleChecked = true;
      } else {
        this.roleChecked = false;
      }
    }
    this._Activatedroute.paramMap.subscribe(params => {
      this.searchBadgeStudent.badgeid = params.get('id');
      this.getListStudentOnBadge();
    });

  }

  getListStudentOnBadge() {
    this.spinner.show();
    this.badgeService.getListStudentBadgeCourseOff(this.searchBadgeStudent).subscribe(
      bad => {
        this.studentonBadge = bad.body.results;
        this.totalRecord = bad.body.total;
        for(let i = 0 ;i < bad.body.total ; i++){
          if(bad.body.results[i].mimetype =="application/pdf"){
             this.isShowDownload = true ;
          }
        }
        this.spinner.hide();
      },
      err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      });
  }

  onChangeSearch(event) {
    this.searchBadgeStudent.search = event.target.value;
    this.searchBadgeStudent.page = 1;
    this.getListStudentOnBadge();
  }

  changePage(event) {
    this.searchBadgeStudent.page = event.pageIndex + 1;
    this.searchBadgeStudent.limit = event.pageSize;
    this.getListStudentOnBadge();
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  viewImage(item) {
    var img = "";
    if (item.filename) {
      img = this.fileservice.badgeFileUrl(item.contextid, item.filename, '/external_badge/overviewfiles/', '0/');
    }
    return img;
  }
  DownloadImage(item){
    var img = "";
    if (item.filename) {
      img = this.fileservice.badgeFileUrl(item.contextid, item.filename, '/external_badge/overviewfiles/', '0/');
      this.fileservice.getBase64ImageFromURL(img).subscribe(base64data => {
          this.base64Image = "data:image/jpg;base64," + base64data;
          var link = document.createElement("a");
          document.body.appendChild(link);
          link.setAttribute("href", this.base64Image);
          link.setAttribute("download", item.filename);
          link.click();
      });
    }
  }
  onApprovalBadge(status: any, issuedid: any) {
    const param = {
      status: status,
      issuedid: issuedid
    }
    const modalDep = this.modalService.open(ConfirmModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });

    let bodyModal = status == 1 ? 'phê duyệt' : "từ chối phê duyệt"
    modalDep.componentInstance.title = 'Xác nhận';
    modalDep.componentInstance.body = `Bạn có chắc chắn muốn  ${bodyModal} chứng chỉ này`;
    modalDep.componentInstance.confirmButton = 'Xác nhận';

    modalDep.result.then((result) => {
      this.spinner.show();
      this.badgeService.approvelBage(param).subscribe(
        res => {
          if (param.status == 2){
            this.toastrService.success('Từ chối phê duyệt thành công');
          }else{
            this.toastrService.success('Phê duyệt thành công');
          }
         
          this.getListStudentOnBadge();
          this.spinner.hide();
        },
        err => {
          this.spinner.hide();
          this.toastrService.handlerError(err);
        });
    });
  }

  showStatusBadgeOffStd(value: any) {
    let badge = {
      name: null,
      colorBtn: null,
    };
    switch (value) {
      case 0:
        badge.name = 'Đang chờ phê duyệt';
        badge.colorBtn = 'badge-secondary';
        break;
      case 1:
        badge.name = 'Đã phê duyệt';
        badge.colorBtn = 'badge-success';
        break;
      case 2:
        badge.name = 'Từ chối phê duyệt';
        badge.colorBtn = 'badge-danger';
        break;
      default:
        break;
    }
    return badge;
  }

  onChangeStatus(e: any) {
    this.searchBadgeStudent.status = e.target.value;
    this.getListStudentOnBadge();
  }
}
