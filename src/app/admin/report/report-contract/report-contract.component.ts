import { share } from 'rxjs/operators';
import { Component, OnInit,ViewChild } from '@angular/core';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { Department } from 'src/app/shared/model/department.model';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE_KEY } from './../../../shared/constants/base.constant';
import { ContractService } from 'src/app/shared/services/contract.service';
@Component({
  selector: 'app-report-contract',
  templateUrl: './report-contract.component.html',
  styleUrls: ['./report-contract.component.css']
})
export class ReportContractComponent implements OnInit {
  @ViewChild('departmentTree')
  public departmentTree: TreeViewComponent;
  public departmentTreeData;
  currentSelectedId: number;
  // List
  departmentList: Department[] = [];
  completedAll = false;
  departmentIds: number[] = [];
  departmentIdsChecked: number[] = [];
  isDepartmentRoot: boolean = false;
  totalRecord: number;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  inputYear:any;
  currentDate: Date = new Date();
  isAdmin = false;
  checkedDepartmentIds = [];
  listContract:any;
  searchUser = {
    keyword: '',
    pageIndex: 1,
    pageSize: 10,
    coursetype: '',
    type: '',
    year:null,
    status:''
  };
  constructor(
    private spinner: NgxSpinnerService,
    public $localStorage: LocalStorageService,
    private departmentService: DepartmentService,
    private contractService: ContractService,
  ) {
    this.searchUser.year=this.getDefault();
  }

  ngOnInit(): void {
    this.listAllTree();
    this.getdeparmentList();
  }
  getdeparmentList() {
    const params = {
      page: this.searchUser.pageIndex,
      keyword: this.searchUser.keyword,
      departmentid: this.currentSelectedId,
      year: this.searchUser.year,
      status: this.searchUser.status
    };
    this.spinner.show();
    this.departmentIds = [];
    this.isDepartmentRoot = false;
    this.contractService.listContract(params).subscribe((data) => {
      this.listContract = data.body.results;
      this.totalRecord = data.body.total;
      this.spinner.hide();
    },
      error => {
      this.spinner.hide();
      });
  }
listAllTree() {
  this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
    const dataSource = data.body;
    this.setUpTree(dataSource);
    this.departmentTreeData = {
      dataSource, id: 'id', parentID: 'parentid', text: 'name', hasChildren: 'haschild', selected: 'isSelected'
    };
  });
}

nodeSelected(e) {
  this.currentSelectedId = +this.departmentTree.getNode(e.node).id;
  this.departmentIds = [];
  this.departmentIdsChecked = [];
  this.searchUser.pageSize = 10;
  this.searchUser.pageIndex = 1;
  this.getdeparmentList();
}

setUpTree(dataSource: any) {
  const indexOfCurrentSelectedId = dataSource.findIndex(x => x.id === this.currentSelectedId);
  if (indexOfCurrentSelectedId === -1){// case currentSelectedId is deleted -> set to deault
    this.currentSelectedId = null;
  }

  // set thang cha = null de no hien thi len duoc cay
  dataSource[0].parentid = null;
  // muc dich la de expand tree
  dataSource.forEach(value => {
    if (value.parentid == null)
    {
      value.expanded = true; // value.haschild !== null;
    } else {
      value.expanded = false;
    }
    value.isChecked = true;
  });
}
onSearch(e: any){
  this.searchUser.pageIndex=1;
  this.searchUser.keyword= e.target.value;
  this.getdeparmentList();
}
onFilterStatus(contractFilter: any){
  this.searchUser.pageIndex=1;;
  this.searchUser.status = contractFilter;
  this.getdeparmentList();
}
changePage(event) {
  this.searchUser.pageIndex = event.pageIndex + 1;
  this.searchUser.pageSize = event.pageSize;
  this.getdeparmentList();
}
yearSelected(value: any) {
  if(value!==this.searchUser.year){
    this.searchUser.year = value ? value[0] : null;
    this.getdeparmentList();
  }
  
}
goToDetail(){
  this.$localStorage.store(LOCAL_STORAGE_KEY.SEARCH.REPORT_STUDENT_SEARCH, this.searchUser);
  this.$localStorage.store(LOCAL_STORAGE_KEY.SEARCH.REPORT_CHECKED_DEPARTMENT, this.checkedDepartmentIds);
}
getDefault(){
  return new Date().getFullYear();
}

}
