import {IntroeditorModel} from './introeditor.model';
import {PageModel} from './page.model';

export interface ModuleInfo {
  id?: number;
  modulename: string;
  name: string;
  course: number;
  section: number;
  intro?: string;
  content?: string;
  introeditor: IntroeditorModel;
  page?: PageModel;
  coursemodule?: number;
  quizpassword?: string;
  grade?: number;
  gradepass?: number;
  questionsperpage?: number;
  timeopen?: any;
  timeclose?: any;
  timelimit?: any;
  grademethod?: any;
  grademethodName?: string;
  completionstate?: any;
  completionStateName?: string;
  overduehandling?: any;
  preferredbehaviour?: any;
  sumgrades?: any;
  gradequiz?: any;
  attempts?: any;
  status?: string;
  instance?: number;
  happening?: string;
  // bbb field
  participants?: string;
  record?: number;
  recordings_html?: number;
  recordings_deleted?: number;
  recordings_preview?: number;
  filename?: string;
  mimetype?: string;
  contextid?: number;
  sourcecontextid?: number;
  openingtime?: number;
  closingtime?: number;
  meetingurl?: string;
  isshowgrade?: any;
  urlfile?: any;
  istopicquiz?: any;
  topicid?: any;
}


