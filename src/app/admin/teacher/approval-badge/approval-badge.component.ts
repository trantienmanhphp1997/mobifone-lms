import { Component, OnInit , Input , EventEmitter, Output} from '@angular/core';
import { BadgeService } from 'src/app/shared/services/badge.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { NgxSpinnerService } from 'ngx-spinner';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { AbstractControl, FormBuilder, FormControl, Validators } from '@angular/forms';
@Component({
  selector: 'app-approval-badge',
  templateUrl: './approval-badge.component.html',
  styleUrls: ['./approval-badge.component.css']
})
export class ApprovalBadgeComponent implements OnInit {
  @Input() issuedid: any;
  @Input() status: any;
  @Output() newBad = new EventEmitter<any>();
  constructor(
    private badgeService:BadgeService,
    private toastService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
  }

  approvalBadgeForm = this.fb.group({
    description: [''],
  });

  approvalBadge(){
    const param ={
      status :this.status,
      issuedid: this.issuedid,
      result: this.approvalBadgeForm.value.description
    }
    this.status == 1 ? 'duyệt ' : "từ chối "
    this.spinner.show();
    this.badgeService.approveExternalBadge(param).subscribe(
      res => {
        this.toastService.success('Từ chối thành công');
        this.spinner.hide();
        this.newBad.emit(this.issuedid);
        this.activeModal.close();
      },
      err => {
        this.spinner.hide();
        this.toastService.handlerError(err);
      });
  }
}
