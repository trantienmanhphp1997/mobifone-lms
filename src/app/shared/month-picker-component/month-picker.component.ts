import { ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, Output, ViewChild } from '@angular/core';
import { AbstractControl, FormControl, Validators } from '@angular/forms';
import { DateAdapter, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MatDatepicker } from '@angular/material/datepicker';

const dateValid = (c: AbstractControl) => {
  if (!c || !c.value) {
    return null
  }

  if (c.value.length === 2 && c.value.every(_ => _)) {
    return null
  }

  return { invalidDate: 'Please select month and year' }
}

const dateInPast = (c: AbstractControl) => {
  if (!c || !c.value) {
    return null
  }

  if (c.value.length === 2 && c.value.every(_ => _)) {

    const currDate = new Date();
    if (
      c.value[1] < currDate.getFullYear() ||
      (c.value[1] === currDate.getFullYear() && c.value[0] < (currDate.getMonth() + 1))

    ) {

      return { dateInPast: 'Please select a present or future date.' };

    }

  }

  return null
}

/** @title Datepicker emulating a Year and month picker */
@Component({
  selector: 'app-month-picker',
  templateUrl: './month-picker.component.html',
  styleUrls: ['./month-picker.component.scss'],
})
export class MonthPickerComponent {
  @Input() typePicker: any;
  @Input() inputValue: any;
  @Input() defaulthYear: any;
  @Output() chooseYearE = new EventEmitter<any>();
  @Output() chooseMonthE = new EventEmitter<any>();
  @ViewChild('myFormField', { read: ElementRef }) private formField: ElementRef;
  get datePlaceholder(): string {
    let ans = [];
    switch (this.typePicker) {
      case "month":
        ans = [(new Date().getMonth().toString() + 1)+'/'+new Date().getFullYear()];
        if (this.date.value) {
          
          if (this.date.value[0]) {
            const _v = this.date.value[0] < 10 ? `0${this.date.value[0]}` : this.date.value[0];
            ans[0] = _v;
            ans[1] = '/';
            ans[2] = this.date.value[1];
          }

          // if (this.date.value[1]) {
          //   ans[2] = this.date.value[1];
          // }
        }
        break;
      case "year":
          ans = ['Chọn năm']
        if (this.date.value) {
          if (this.date.value[0]) {
            ans[0] = this.date.value[0];
          }
        }
        break;
      default:
        break;
    }
    return ans.join('');
  }

  ngAfterViewInit() {
    if(this.inputValue){
      const v=[this.inputValue];
      var yr=new Date();
      if (v) {
        if (v[0] !== yr) {
          v[0] = this.inputValue;
          this.date.setValue(v);
        }
      }
    }
    this.formField.nativeElement.classList.remove("mat-form-field-disabled");
    if(this.defaulthYear){
      this.date.setValue([this.defaulthYear]);
    }
    this.chooseYearE.emit(this.defaulthYear);
  }

  date = new FormControl(null, [Validators.required, dateValid, dateInPast]);

  chosenYearHandler(d: Date, datepicker: MatDatepicker<Date>) {
    const yr = d.getFullYear();
    if (this.typePicker == 'month') {
      const v: null | [number, number] = this.date.value;
      if (v) {
        if (v[1] !== yr) {
          v[1] = yr;
          this.date.setValue(v);
        }
        return;
      }
      if (!v) {
        const _v = [undefined, yr];
        this.date.setValue(_v);
      }

    }

    if (this.typePicker == 'year') {
      const v:null | [number] = [this.date.value];
      if (v) {
        if (v[0] !== yr) {
          v[0] = yr;
          this.date.setValue(v);
        }
      }

      if (!v) {
        const _v = [yr];
        this.date.setValue(_v);
      }
      this.chooseYearE.emit(this.date.value);
      datepicker.close();
    }

  }


  chosenMonthHandler(d: Date, datepicker: MatDatepicker<Date>) {
    if(this.typePicker=='month'){
    const v: null | [number, number] = this.date.value;
    const m = d.getMonth() + 1;

    if (v) {

      if (v[0] !== m) {
        v[0] = m;
        this.date.setValue(v);
      }
    } else {
      const _v = [m, undefined];
      this.date.setValue(_v);
    }
    this.chooseMonthE.emit(this.date.value);
    datepicker.close();
  }
  }

  clearDate(e: any) {
    e.stopPropagation();
    this.date.setValue(null);
    if (this.typePicker == "month") {
      this.chooseMonthE.emit(this.date.value);
    }
    if (this.typePicker == "year") {
      this.chooseYearE.emit(this.date.value);
    }
}
}