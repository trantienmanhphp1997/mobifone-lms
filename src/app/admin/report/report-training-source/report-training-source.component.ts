import { Component, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { NgxSpinnerService } from 'ngx-spinner';
import { Department } from 'src/app/shared/model/department.model';
import { CourseCategoryService } from 'src/app/shared/services/course-category.service';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { ReportService } from 'src/app/shared/services/report.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import {FileService} from '../../../shared/services/file.service';

@Component({
  selector: 'app-report-training-source',
  templateUrl: './report-training-source.component.html',
  styleUrls: ['./report-training-source.component.css'],
})
export class ReportTrainingSourceComponent implements OnInit {
  searchTrainingReport = {
    pageIndex: 1,
    pageSize: 10,
    categoryids: [],
    departmentids: [],
    sources: [],
    year: null,
    month: null,
    limit: 0,
    sortcolumn: 'id',
    sorttype: 'ASC',
  };
  @ViewChild('categoryTree')
  public categoryTree: TreeViewComponent;
  departmentTreeData: any;
  total: number;
  pageSize = 10;
  pageIndex = 1;
  setHeight: number = 0;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  listReportTraining: any;
  categoryTreeData: any;

  @ViewChild('departmentTree')
  public departmentTree: TreeViewComponent;
  public departmentMergeTreeData;
  checkedDepartmentIds = [];
  departmentList: Department[] = [];
  listCategoryId: any = [];
  currentSelectedId: number;
  listDepartmantID: any = [];
  checkedCategoryIds = [];

  inputYear: any;
  listSources: any = [];
  sourcesDataCheckbox: any = [];
  numdeptrate: any = 0;
  deptcode: any = [];
  a: any = 1;

  @ViewChild('sourceTree')
  public sourceTree: TreeViewComponent;

  constructor(
    private reportService: ReportService,
    private spinner: NgxSpinnerService,
    private departmentService: DepartmentService,
    private toastrService: ToastrCustomService,
    private categoryService: CourseCategoryService,
    private _formBuilder: FormBuilder,
    private fileService: FileService
  ) {}

  ngOnInit(): void {
    this.getListParameter();
    this.getCategoryTree();
    this.getListAllTree();
  }

  ngAfterViewChecked() {
    this.setRow();
    window.addEventListener('resize', this.setRow);
  }

  setRow() {
    const header = document.getElementById('header1');
    const header2 = document.getElementById('header2');
    if (header && header2) {
      if (header.offsetHeight > header2.offsetHeight) {
        this.setHeight = header.offsetHeight;
      }
    }
    const table = document.querySelectorAll<HTMLElement>('.table1');
    const table2 = document.querySelectorAll<HTMLElement>('.table2');
    if (table && table2) {
      for (let i = 0; i <= table.length; i++) {
        const height_table1 = table[i]?.clientHeight;
        const height_table2 = table2[i]?.clientHeight;
        if (height_table1 && height_table2) {
          if (height_table2 < height_table1) {
            table2[i].style.height = height_table1 + 'px';
          } else {
            table[i].style.height = height_table2 + 'px';
          }
        }
      }
    }
  }

  setHeightHeader() {
    const header = document.getElementById('header1');
    const header2 = document.getElementById('header2');
    this.setHeight =
      header.offsetHeight > header2.offsetHeight
        ? header.offsetHeight
        : header2.offsetHeight;
  }

  changePage(event) {
    this.searchTrainingReport.pageIndex = event.pageIndex + 1;
    this.searchTrainingReport.pageSize = event.pageSize;
    this.getListReportTraining();
  }

  showCost(value: any) {
    return Intl.NumberFormat('vi-VN').format(value);
  }

  // Lấy tất cả đơn vị
  getListAllTree() {
    this.spinner.show();
    this.departmentService.getUserDepartmentTree(0).subscribe(
      (data) => {
        this.departmentList = data.body;
        if (this.departmentList) {
          this.departmentList[0].parentid = null;
        }
        this.departmentTreeData = {
          dataSource: this.departmentList,
          id: 'id',
          parentID: 'parentid',
          text: 'name',
          hasChildren: 'haschild',
        };
        this.getListDepartmentDefault();
        this.spinner.hide();
      },
      (error) => {
        this.spinner.hide();
        this.toastrService.handlerError(error);
      }
    );
  }
  getListDepartmentDefault() {
    this.reportService.getListDefaultParameterSource().subscribe((data) => {
     data.body.departmentids.forEach((item) =>{
        this.departmentTree.checkedNodes =[...this.departmentTree?.checkedNodes, item];
      })
      this.spinner.hide();
    });
  }
  public nodeCheck(args: any): void {
    const checkedNode: any = [args.node];
    if (
      args.event.target.classList.contains('e-fullrow') ||
      args.event.key == 'Enter'
    ) {
      // @ts-ignore
      const getNodeDetails: any = this.departmentTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.departmentTree.uncheckAll(checkedNode);
      } else {
        this.departmentTree.checkAll(checkedNode);
      }
    }
  }

  // chọn 1 đơn vị
  nodeDepartmentSelected(e: any) {
    // code
  }

  // chọn nhiều đơn vị
  nodeDepartmentChecked(e: any) {
    this.checkedDepartmentIds = [];
    this.departmentTree.checkedNodes.forEach((node) => {
      this.checkedDepartmentIds.push(parseInt(node, 10));
    });
    this.searchTrainingReport.departmentids = this.checkedDepartmentIds;
    this.searchTrainingReport.pageIndex = 1;
    this.getListReportTraining();
  }

  getCategoryTree() {
    this.categoryService.getCourseCategoryTree(null).subscribe((data) => {
        const dataSource = data.body;
        this.setUpTree(dataSource);
        this.categoryTreeData = {
          dataSource,
          id: 'id',
          parentID: 'parent',
          text: 'name',
          hasChildren: 'haschild',
          selected: 'isSelected',
        };
        this.getListCategoryIdDefault();
      });
  }
  getListCategoryIdDefault() {
    this.reportService.getListDefaultParameterSource().subscribe((data) => {
     data.body.categoryids.forEach((item) =>{
        this.categoryTree.checkedNodes =[...this.categoryTree?.checkedNodes, item];
      })
      this.spinner.hide();
    });
  }
  setUpTree(dataSource: any) {
    const indexOfCurrentSelectedId = dataSource.findIndex(
      (x) => x.id === this.currentSelectedId
    );
    if (indexOfCurrentSelectedId === -1) {
      // case currentSelectedId is deleted -> set to deault
      this.currentSelectedId = null;
    }

    dataSource.forEach((value) => {
      if (value.parent === 0) {
        // = 0 la root tree
        value.parent = null; // set = null nham muc dich hien thi
        value.expanded = true; // muc dich expand root luon khi khoi tao tree
      } else {
        value.expanded = false;
      }

      if (value.id === this.currentSelectedId) {
        // high light selected node
        value.isSelected = true;
      }
    });
  }

  getListParameter(){
    this.reportService.getListReportTrainingBySourceDefault().subscribe((data) => {
      this.listCategoryId = data.body.categoryids;
      this.listDepartmantID = data.body.departmentids;
      this.listSources = data.body.sources;
      this.searchTrainingReport.categoryids = this.listCategoryId;
      this.searchTrainingReport.departmentids = this.listDepartmantID;
      this.getListReportTraining();
      this.initForm(this.listSources);
    });
  }

  nodeCategoryChecked(e: any) {
    this.checkedCategoryIds = [];
    this.categoryTree.checkedNodes.forEach((node) => {
      this.checkedCategoryIds.push(parseInt(node, 10));
    });
    this.searchTrainingReport.categoryids = this.checkedCategoryIds;
    this.searchTrainingReport.pageIndex = 1;
    this.getListReportTraining();
  }

  public nodeCheckCategory(args: any): void {
    const checkedNode: any = [args.node];
    if (
      args.event.target.classList.contains('e-fullrow') ||
      args.event.key == 'Enter'
    ) {
      // @ts-ignore
      const getNodeDetails: any = this.categoryTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.categoryTree.uncheckAll(checkedNode);
      } else {
        this.categoryTree.checkAll(checkedNode);
      }
    }
  }

  nodeSelectedCategory(e) {

  }

  getDefault() {
    return new Date().getFullYear();
  }

  yearSelected(value: any) {
    this.searchTrainingReport.year = value ? value[0] : null;
    if(this.searchTrainingReport.year !== undefined) {
      this.getListReportTraining();
    }
  }

  getListReportTraining() {
    const param = {
      page: this.searchTrainingReport.pageIndex,
      limit: this.searchTrainingReport.pageSize,
      categoryids: this.searchTrainingReport.categoryids.toString(),
      sources: this.searchTrainingReport.sources.toString(),
      year: this.searchTrainingReport.year,
      month: this.searchTrainingReport.month,
      departmentids: this.searchTrainingReport.departmentids.toString(),
      sortcolumn: this.searchTrainingReport.sortcolumn,
      sorttype: this.searchTrainingReport.sorttype,
    };
    this.spinner.show();
    this.reportService.getListReportTrainingBySource(param).subscribe((data) => {
      this.listReportTraining = data.body.results;
      this.deptcode = data.body.deptcode;
      this.numdeptrate = this.deptcode.length;
      this.total = data.body.total;
      this.spinner.hide();
    });
  }

  monthSelected(value: any) {
    this.searchTrainingReport.month = value ? value[0] : null;
    this.searchTrainingReport.year = value ? value[1] : null; 
    this.getListReportTraining();
  }

  initForm(data: any): void {
    data.forEach((item) => {
      this.sourcesDataCheckbox.push(
        {
          name: this.formatSourceName(item),
          checked: false,
          value: item
        }
      );
    });
  }

  getCheckboxes(): void {
    const data = this.sourcesDataCheckbox.filter(x => x.checked === true).map(x => x.value);
    this.searchTrainingReport.sources = data;
    console.log(this.searchTrainingReport.sources);
    
    this.getListReportTraining();
  }

  formatSourceName(type: any): string {
    if (type === 'TRAINING_RESOURCE') {
      return 'Nguồn đào tạo';
    } else if (type === 'PROJECT_RESOURCE') {
      return 'Nguồn dự án';
    } else if (type === 'FUND_RESOURCE') {
      return 'Quỹ khóa học công nghệ';
    } else if (type === 'BUSINESS_RESOURCE') {
      return 'Sản xuất kinh doanh';
    } else if (type === 'OTHERS'){
      return 'Nguồn khác';
    }
    return type;
  }

  formatDepartmentName(id: any): string {
    const department = this.departmentTreeData.dataSource.filter((dept) => dept.id === id);
    if(!department) {
      return '';
    }
    return department[0].name;
  }

  exportExcel(): void {
    const dateCurrrent = new Date();
    const param = {
      page: this.searchTrainingReport.pageIndex,
      limit: this.searchTrainingReport.pageSize,
      categoryids: this.searchTrainingReport.categoryids.toString(),
      sources: this.searchTrainingReport.sources.toString(),
      year: this.searchTrainingReport.year ? this.searchTrainingReport.year : dateCurrrent.getFullYear(),
      month: this.searchTrainingReport.month ? this.searchTrainingReport.month : (dateCurrrent.getMonth() + 1),
      departmentids: this.searchTrainingReport.departmentids.toString(),
      sortcolumn: this.searchTrainingReport.sortcolumn,
      sorttype: this.searchTrainingReport.sorttype,
    };
    console.log(param);
    
    this.spinner.show();
    this.reportService.exportExcelReportTrainingBySource(param).subscribe((res) => {
        this.spinner.hide();
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
      },
      (err) => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      });
  }
}
