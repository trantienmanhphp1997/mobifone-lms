import { Component, OnInit, ViewChild, AfterViewChecked } from '@angular/core';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { NgxSpinnerService } from 'ngx-spinner';
import { Department } from 'src/app/shared/model/department.model';
import { Position } from 'src/app/shared/model/position.model';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { FileService } from 'src/app/shared/services/file.service';
import { PositionService } from 'src/app/shared/services/position.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { TrainingPlanService } from 'src/app/shared/services/training-plan.service';

@Component({
  selector: 'app-internal-training-plan',
  templateUrl: './internal-training-plan.component.html',
  styleUrls: ['./internal-training-plan.component.css']
})
export class InternalTrainingPlanComponent implements OnInit,AfterViewChecked {

  @ViewChild('departmentTree')
  public departmentTree: TreeViewComponent;
  public departmentTreeData;
  @ViewChild('departmentTreeNB')
  public departmentTreeNB: TreeViewComponent;
  public departmentTreeDataNB;
  
  positionList: Position[] = [];
  totalRecord: number;
  inputYear:any;
  pageSize=10;
  pageIndex=1;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  departmentList: Department[] = [];
  departmentIds: number[] = [];
  departmentId: any = '';
  departmentIdsChecked: number[] = [];
  currentSelectedId: number;
  planList: any[] = [];
  planListNB: any[] = [];
  searchName: any;
  year=null;
  setHeight: number = 0;
  setHeightFilter: number=0;
  currentSelectedDepartmentId: number;
  constructor(
    private departmentService: DepartmentService,
    private spinner: NgxSpinnerService,
    private trainingPlan: TrainingPlanService,
    private toastrService: ToastrCustomService,
    private positionService: PositionService,
    private fileService: FileService,
  ) { }
  ngOnInit(): void {
    this.year = this.getDefault();
    this.reLoadData();
  }
  reLoadData() {
    this.getListPosition();
    this.listAllTree();
    this.getListTraingPlan();
  }
  ngAfterViewChecked() {
    
    this.setRow();
    window.addEventListener('resize',this.setRow);
  }
  listAllTree() {
    this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
      this.departmentList = data.body;
      // set thang cha = null de no hien thi len duoc cay
      this.departmentList[0].parentid = null;
      this.departmentList.forEach(value => {
        this.departmentList.filter(department => department.id === value.parentid);
        value.expanded = value.parentid == null;
      });
      this.departmentTreeData = {
        dataSource: this.departmentList,
        id: 'id',
        parentID: 'parentid',
        text: 'name',
        hasChildren: 'haschild'
      };
      this.departmentTreeDataNB = {
          dataSource: this.departmentList,
          id: 'id',
          parentID: 'parentid',
          text: 'name',
          hasChildren: 'haschild'
        };
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      this.toastrService.handlerError(error);
    });
  }

  nodeSelected(e) {
    this.departmentIds = [];
    this.departmentIdsChecked = [];
    this.departmentTree.checkedNodes.forEach((node) => {
        this.departmentIds.push(parseInt(node, 10));
      });
    this.getListTraingPlan();
  }

  nodeCheck(args: any): void {
    const checkedNode: any = [args.node];
    if (args.event.target.classList.contains('e-fullrow') || args.event.key == 'Enter') {
      // @ts-ignore
      const getNodeDetails: any = this.departmentTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.departmentTree.uncheckAll(checkedNode);
      } else {
        this.departmentTree.checkAll(checkedNode);
      }
    }
  }

  setUpTree(dataSource: any) {
    const indexOfCurrentSelectedId = dataSource.findIndex(x => x.id === this.currentSelectedId);
    if (indexOfCurrentSelectedId === -1) {
        this.currentSelectedId = null;
    }
    dataSource[0].parentid = null;
    dataSource.forEach(value => {
        if (value.parentid == null) {
            value.expanded = true; 
        } else {
            value.expanded = false;
        }
        value.isChecked = true;
    });
  }
  getListTraingPlan() {
    const params = {
      limit: this.pageSize,
      page:this.pageIndex,
      departmentids: this.departmentIds? this.departmentIds.toString() : null,
      year: this.year,
      fullname:this.searchName,
      isinternal: 1,
    };
    this.spinner.show();
    this.trainingPlan.getReportTrainingPlan(params).subscribe((data) => {
      this.spinner.hide();
      this.totalRecord = data.body.total;
      this.planList = data.body.results;
    },err=>{
      this.spinner.hide();
      this.toastrService.handlerError(err);
    });
  }
  changePage(event){
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getListTraingPlan();
  }
  getListPosition() {
    return this.positionService.getPosition().subscribe(positionList => {
        this.positionList = positionList.body.results;
    });
}
  getPositionNames(positionId: any) {
    let positionName = '';
    if (positionId) {
        const positionIdArray = positionId.split(',');
        for (const positionId of positionIdArray) {
            const foundPosition = this.positionList.find(value => value.id === +positionId);
            if (positionName.length > 0) {
                positionName += ', ';
            }
            if (foundPosition && foundPosition.name !== undefined) {
                positionName += foundPosition.name;
            }
        }
        return positionName;
    }
    return '';
}
yearSelected(value: any) {
  if(value!==this.year){
    this.year = value ? value[0] : null;
    this.getListTraingPlan();
  }
}
changeName($event){
  this.searchName = $event.target.value;
  this.getListTraingPlan();
}
getDefault(){
  return new Date().getFullYear();
}
exportData(){
  const params={
    departmentids:this.departmentIds? this.departmentIds.toString() : null,
    year:this.year,
    fullname:this.searchName,
    all:1,
    isinternal: 1,
  }
  this.trainingPlan.exportTrainingPlan(params).subscribe(res=>{
    this.spinner.hide();
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
    this,this.toastrService.success('Export thành công')
  },err=>{
    this.toastrService.handlerError(err);
  })
}
setRow() {
  const table = document.querySelectorAll<HTMLElement>('.tablein1');
  const table2 = document.querySelectorAll<HTMLElement>('.tablein2');
  if (table && table2) {
    for (let i = 0; i <= table.length; i++) {
      var height_table1 = table[i]?.clientHeight;
      var height_table2 = table2[i]?.clientHeight;
      if (height_table1 && height_table2) {
        table2[i].style.height=0 +'px';
        table[i].style.height=0 +'px';
        var height_table1 = table[i]?.clientHeight;
      var height_table2 = table2[i]?.clientHeight;
        if (height_table2 < height_table1) {
          table2[i].style.height = height_table1 + 'px';
        } else if(height_table2 > height_table1) {
          table[i].style.height = height_table2 + 'px';
        }
      }
    }
    
  }
}
}
