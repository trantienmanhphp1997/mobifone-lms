import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ReportService } from '../../../shared/services/report.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { DepartmentService } from '../../../shared/services/department.service';
import { Department } from '../../../shared/model/department.model';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { LearningDataDetailComponent } from './learning-data-detail/learning-data-detail.component';
import { FileService } from 'src/app/shared/services/file.service';

@Component({
  selector: 'app-report-learning-data',
  templateUrl: './report-learning-data.component.html',
  styleUrls: ['./report-learning-data.component.css']
})
export class ReportLearningDataComponent implements OnInit {
  @ViewChild('departmentTree')
  public departmentTree: TreeViewComponent;

  public departmentTreeData;

  departmentList: Department[] = [];

  departmentIdsChecked: [];
  currentSelectedId: number;
  departmentIds: number[] = [];
  totalRecord: number;
  date: any
  pageSizeOptions: number[] = [10, 25, 50, 100];
  listData: any = [];
  listParams = {
    pageIndex: 1,
    pageSize: 10,
    keyword: null,
    ids: null,
    startdate: null,
    enddate: null,
  }

  constructor(
    private reportService: ReportService,
    private spinner: NgxSpinnerService,
    private departmentService: DepartmentService,
    private toastrService: ToastrCustomService,
    private modalService: NgbModal,
    private fileService: FileService,
  ) {
  }

  ngOnInit(): void {
    this.getListAllTree();
    this.getListData();
  }
  // for transcluded content
  nodeSelected(e) {
    this.departmentIds = [];
    this.departmentIdsChecked = [];
    this.departmentTree.checkedNodes.forEach((node) => {
      this.departmentIds.push(parseInt(node, 10));
    });
    this.currentSelectedId = +this.departmentTree.getNode(e.node).id;
    this.listParams.pageSize = 10;
    this.listParams.pageIndex = 1;
    this.getListData();
  }

  nodeCheck(args: any): void {
    const checkedNode: any = [args.node];
    if (args.event.target.classList.contains('e-fullrow') || args.event.key == 'Enter') {
      // @ts-ignore
      const getNodeDetails: any = this.departmentTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.departmentTree.uncheckAll(checkedNode);
      } else {
        this.departmentTree.checkAll(checkedNode);
      }
    }
  }

  getListData() {
    const departmentIds = [...this.departmentIds] ;
    const param = {
      ids: this.listParams.ids,
      limit: this.listParams.pageSize,
      page: this.listParams.pageIndex,
      departmentids: departmentIds ? departmentIds.toString() : "",
      keyword: this.listParams.keyword,
      startdate: this.listParams.startdate ?? null,
      enddate: this.listParams.enddate ?? null,
    }
    this.spinner.show();
    this.reportService.getReportLearningData(param).subscribe(data => {
      this.listData = data.body.results;
      this.totalRecord = data.body.total;
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastrService.handlerError(err);
    });
  }

  clear() {
    // param phuc vu tim kiem
    this.listParams.keyword = '';
    this.currentSelectedId = null;
    // Bo select node tren cay
    this.departmentTree.selectedNodes = [];
    this.departmentIdsChecked = [];
    this.departmentIds = [];
    this.getListData();
  }

  changePage(event) {
    this.listParams.pageIndex = event.pageIndex + 1;
    this.listParams.pageSize = event.pageSize;
    this.getListData();
  }

  onFilterKeyword(event: any) {
    this.listParams.pageIndex = 1;
    this.listParams.keyword = event.target.value;
    this.getListData();
  }

  getListAllTree() {
    this.spinner.show();
    this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
      this.departmentList = data.body;
      if (this.departmentList) {
        this.departmentList[0].parentid = null;
      }
      this.departmentTreeData = {
        dataSource: this.departmentList,
        id: 'id',
        parentID: 'parentid',
        text: 'name',
        hasChildren: 'haschild'
      };
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      this.toastrService.handlerError(error);
    });
  }

  onFilterDateRange(date: any) {
    this.listParams.pageIndex = 1;
    this.listParams.startdate = Math.round((date[0].getTime()) / 1000);
    this.listParams.enddate = Math.round((date[1].getTime()) / 1000);
    this.getListData();
  }

  onExportData() {
    const departmentIds = [...this.departmentIds] ;
    const param = {
      ids: this.listParams.ids,
      limit: this.listParams.pageSize,
      page: this.listParams.pageIndex,
      departmentids: departmentIds ? departmentIds.toString() : "",
      keyword: this.listParams.keyword,
      startdate: this.listParams.startdate ?? null,
      enddate: this.listParams.enddate ?? null,
    }
    this.spinner.show();
    this.reportService.exportReportLearningData(param).subscribe(
      res => {
        this.spinner.hide();
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
    }, err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
    });
  }

  viewDetail(item) {
    if (item.id) {
      const modalDep = this.modalService.open(LearningDataDetailComponent, {
        size: 'xl',
        centered: true,
        backdrop: 'static'
      });
  
      modalDep.componentInstance.itemId = item?.id;
      modalDep.componentInstance.date = {
        startdate: this.listParams.startdate ?? null,
        enddate: this.listParams.enddate ?? null,
      };
    }
  }
}
