import { ToastrCustomService } from './../../../../shared/services/toastr-custom.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Component, ElementRef, Input, OnInit, ViewChild } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ABC, abc, iii, III, T123 } from 'src/app/shared/constants/base.constant';
import CommonUtil from 'src/app/shared/utils/common-util';
import { ReportService } from '../../../../shared/services/report.service';
import { jsPDF } from 'jspdf';
import html2canvas from 'html2canvas';
import * as moment from 'moment';
@Component({
  selector: 'app-result-exam-user',
  templateUrl: './result-exam-user.component.html',
  styleUrls: ['./result-exam-user.component.css']
})
export class ResultExamUserComponent implements OnInit {
  @ViewChild('pdfContent') pdfContent: ElementRef;
  
  listQuestion = [];
  @Input() quizAttemptid: number;
  @Input() studentName: string;
  @Input() gradeuser: number;
  @Input() gradepass: string;
  @Input() correctquestion: number;
  @Input() timestart: number;
  @Input() grade: any;
  @Input() incorrectquestion: any;
  @Input() completionstate: any;


  constructor(
    public activeModal: NgbActiveModal,
    private reportService: ReportService,
    public domSanitizer2: DomSanitizer,
    private spinner: NgxSpinnerService,
    private toastrService: ToastrCustomService,
  ) {}

  ngOnInit(): void {
    this.getReportExamByUser();
  }

  exportPDF(){
    const spt = this.spinner;
    spt.show();
    const pdfContent = this.pdfContent.nativeElement;
    const filename = this.studentName + "_" + new Date().getTime() + ".pdf";
    html2canvas(pdfContent, {
      width: 800
    }).then(function(canvas) {
      const imgData = canvas.toDataURL("image/jpeg", 1.0);
      const pdf = new jsPDF("p", "px", [canvas.width, canvas.height]);
      pdf.addImage(imgData, 'JPEG', 0, 0, canvas.width, canvas.height);
      pdf.save(filename);
      spt.hide();
    });
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date).format('DD/MM/YYYY - HH:mm');
    }
    return null;
  }

  getReportExamByUser() {
    this.spinner.show();
    const params = {
      id: this.quizAttemptid
    };
    this.reportService.getReportExamByUser(params).subscribe((data) => {
      this.listQuestion = data.body.questions;
      this.listQuestion.forEach(element => {
        if (element.answernumbering === 'abc') {
          element.answernumbertype = abc;
        } else if (element.answernumbering === 'ABCD') {
          element.answernumbertype = ABC;
        } else if (element.answernumbering === '123') {
          element.answernumbertype = T123;
        } else if (element.answernumbering === 'iii') {
          element.answernumbertype = iii;
        } else if (element.answernumbering === 'IIII') {
          element.answernumbertype = III;
        }
        if (element.type === 'truefalse') {
          element.typeCheckBox = 'radio';
        } else if (element.type === 'multichoice') {
          if (element.single === 1 ) {
            element.typeCheckBox = 'radio';
          }
          if (element.single === 0 ) {
            element.typeCheckBox = 'checkbox';
          }
        }
        else if (element.type === 'multichoiceset') {
            element.typeCheckBox = 'checkbox';
        }
        element.condition = 1;
        if (element.questiontext.includes('<figure class=\"media\">')) {
          element.linkVideo = CommonUtil.linkVideoCkeditorToLinkEmbedYoutube(element.questiontext);
        }
        this.spinner.hide();
      });
    }, err => {
      this.spinner.hide();
      this.toastrService.handlerError(err);
    });
  }
}
