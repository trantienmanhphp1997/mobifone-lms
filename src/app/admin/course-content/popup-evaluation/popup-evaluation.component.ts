import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-popup-evaluation',
  templateUrl: './popup-evaluation.component.html',
  styleUrls: ['./popup-evaluation.component.css']
})
export class PopupEvaluationComponent implements OnInit {
  @Input() data:any = [];
  @Input() courseid:any = [];
  @Output() emitter: EventEmitter<any> = new EventEmitter();
  listUser: any = [];
  manager: string = '';
  constructor(
    public activeModal: NgbActiveModal,
    private userService:UserService,
    private toastrService: ToastrCustomService
  ) { }

  ngOnInit(): void {
    this.listUserIsManager();
  }
  listUserIsManager(){
    const param ={
      idstudent: this.data.id
    }
    this.userService.getListManagerDepartment(param).subscribe(data=>{
      this.listUser = data.body.results;
    })
  }
  sendManager(){
    console.log(this.data)
    if(!this.manager){
        this.toastrService.error("Lãnh đạo không được bỏ trống")
    }
    else{
      const params = {
        studentids: this.data.id,
        courseid: this.courseid,
        evaluateduserid: this.manager
      }
      this.userService.updateManager(params).subscribe(data=>{
        this.emitter.emit({
          success: true,
        });
        this.toastrService.success("Đã phân cán bộ thành công !");
        this.activeModal.dismiss();
      })
    }

  }
}
