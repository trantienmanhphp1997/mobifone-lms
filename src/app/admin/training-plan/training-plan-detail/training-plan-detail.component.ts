import { Component, HostListener, OnInit, Input } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalStorageService } from 'ngx-webstorage';
import { PLAN_ROLE, TRAINING_TYPE, USER_FUNCTIONS, USER_INFO, USER_ROLE } from 'src/app/shared/constants/base.constant';
import { ConfirmModalComponent } from 'src/app/shared/modal/confirm-modal/confirm-modal.component';
import { Position } from 'src/app/shared/model/position.model';
import { CourseCategoryService } from 'src/app/shared/services/course-category.service';
import { CourseService } from 'src/app/shared/services/course.service';
import { FileService } from 'src/app/shared/services/file.service';
import { PositionService } from 'src/app/shared/services/position.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { TrainingPlanService } from 'src/app/shared/services/training-plan.service';
import CommonUtil from 'src/app/shared/utils/common-util';
import { CustomValidators } from 'src/app/shared/utils/custom-validators';
import { environment } from 'src/environments/environment';
import { CourseCreateComponent } from '../../course/course-create/course-create.component';
import { CoursePlanDetailComponent } from '../course-plan-detail/course-plan-detail.component';
import { CourseRecommendPopup } from '../course-recommend-popup/course-recommend-popup.component';
import { DetailErrorComponent } from '../../training-plan/detail-error/detail-error.component'
import { debug } from 'console';
import { ListClassInCoursePlanDetailComponent } from '../list-class-in-course-plan-detail/list-class-in-course-plan-detail.component';
import { DetailTrainingPlanComponent } from '../detail-training-plan/detail-training-plan.component';
@Component({
    selector: 'app-training-detail',
    templateUrl: 'training-plan-detail.component.html',
    styleUrls: ['./training-plan-detail.component.css']
})

export class TrainingDetailComponent implements OnInit {
    action: any;
    planId = null;
    planForm = this.fb.group({
        code: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(200)]],
        name: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(200)]],
        description: [''],
        approverid: [null]
    });
    courseInPlan = [];
    // pagination & search
    isShowTableError = false;
    totalRecord: number;
    pageSize = 10;
    pageIndex = 1;
    pageSizeOptions: number[] = [10, 25, 50, 100];
    sortColumn = 'id';
    success: any;
    sortType = 'ASC';
    keyword = '';
    panelOpenState = false;
    listApprover = [];
    listError = [];
    isReviewPlan = false;
    isApproverPlan = false;
    planYear = new Date().getFullYear();
    listYear = [new Date().getFullYear(), new Date().getFullYear() + 1];
    positionList: Position[] = [];
    canEdit = false;
    planDetail = null;
    role = null;
    isAdmin = false;
    constructor(private fb: FormBuilder,
        private toastrService: ToastrCustomService,
        private courseService: CourseService,
        private trainingPlanService: TrainingPlanService,
        private courseCategoryService: CourseCategoryService,
        private route: ActivatedRoute,
        private fileservice: FileService,
        private translateService: TranslateService,
        private $localStorage: LocalStorageService,
        private positionService: PositionService,
        private spinner: NgxSpinnerService,
        private router: Router,

        private modalService: NgbModal) { }

    ngOnInit() {
        if (this.route.snapshot.routeConfig.path === 'training-plan-createNB') {
            this.action = 'createNB';
        } else if (this.route.snapshot.routeConfig.path === 'training-plan-create') {
            this.action = 'create';
        }
        this.role = this.courseService.getRoleUser();
        if (this.role === USER_ROLE.ADMIN) {
            this.isAdmin = true;
        }
        this.dragAreaClass = "dragarea";
        this.planId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
        this.isReviewPlan = this.$localStorage.retrieve(USER_INFO.INFO).planrole == PLAN_ROLE.REVIEW;
        this.isApproverPlan = this.$localStorage.retrieve(USER_INFO.INFO).planrole == PLAN_ROLE.APPROVE;
        if (this.planId) {
            this.action = 'edit';
            this.getInfoTrainingPlan(this.planId);
            this.getCoureInPlan();
            this.getListPosition();
        }
    }

    /**
     * Init form
     * 
     */
    initFormPlanDetail(plan: any) {
        this.planForm.patchValue({
            name: plan.name,
            code: plan.code,
            description: plan.description
        });
    }

    /**
     * Map api, fetch data
     * Api used: createOrUpdateTrainingPlan, getTrainingPlanInfo, getListCourseInPlan
     *           getPosition, importPlanFormFile
     */
    onCreateOrUpdatePlan() {
        if (this.planForm.invalid) {
            CommonUtil.markFormGroupTouched(this.planForm);
            return;
        }
        this.spinner.show();
        this.trainingPlanService.createOrUpdateTrainingPlan(this.planForm.value).subscribe(res => {
            if (this.action == 'create') {
                this.router.navigate(['/admin/training-plan-detail', res.body.id]);
                this.action = 'edit'
            }
            else if (this.action == 'createNB') {
                this.router.navigate(['/admin/training-plan-detail', res.body.id]);
                this.action = 'edit'
            }
            this.spinner.hide();
        },
            err => {
                this.toastrService.handlerError(err);
                this.spinner.hide();
            })
    }

    getInfoTrainingPlan(id: any) {
        this.trainingPlanService.getTrainingPlanInfo(id).subscribe(response => {
            // this.initFormPlanDetail(response.body);
            this.planDetail = response.body;
        },
            err => {
                this.toastrService.handlerError(err);
                this.spinner.hide();
            });
    }

    getCoureInPlan() {
        this.spinner.show();
        const params = {
            planid: this.planId,
            limit: this.pageSize,
            page: this.pageIndex,
            keyword: this.keyword
        }
        this.trainingPlanService.getListCourseInPlan(params).subscribe(
            res => {
                this.spinner.hide();
                this.courseInPlan = res.body.results;
                this.totalRecord = res.body.total;
                this.canEdit = res.body.canedit;
            },
            err => {
                this.spinner.hide();
            }
        )
    }

    getListApprover() {
        this.spinner.show();
        this.trainingPlanService.getListApprover().subscribe(
            res => {
                this.spinner.hide();
                this.listApprover = res.body.results;
            },
            err => {
                this.spinner.hide();
            }
        )
    }
    DetailError(listError: any) {
        const modalRef = this.modalService.open(DetailErrorComponent, {
            size: 'xl',
            centered: true,
            backdrop: 'static'
        });
        modalRef.componentInstance.listError = listError;
    }
    getListPosition() {
        return this.positionService.getPosition().subscribe(positionList => {
            this.positionList = positionList.body.results;
        });
    }

    impportQuestion() {
        if (!this.fileSelected) {
            this.error = "Vui lòng chọn file kế hoạch";
            return
        }
        this.spinner.show();
        const isinternal = 0;
        this.trainingPlanService.importPlanFormFile(this.fileSelected, this.planYear, isinternal).subscribe(res => {
            const file: any = res.body;
            this.isShowTableError = true;
            this.listError = file.data;
            this.success = file.success;
            this.spinner.hide();
            if (file.plansuccess) {
                this.toastrService.info(`Tải lên thành công ${file.success} bản ghi`);
                this.downloadURI(file.result);
                this.router.navigate(['/admin/training-plan-detail', res.body.planid]);
            } else {
                // this.toastrService.error('common.noti.file_create_error');
                this.toastrService.info(`Tải lên thành công ${file.success} bản ghi`);
                this.downloadURI(file.result);
            }
            this.fileSelected = null;
        }, error => {
            this.spinner.hide();
            this.toastrService.handlerError(error);
        });
    }
    impportQuestionNB() {
        if (!this.fileSelected) {
            this.error = "Vui lòng chọn file kế hoạch";
            return
        }
        const isinternal = 1;
        this.spinner.show();
        this.trainingPlanService.importPlanFormFile(this.fileSelected, this.planYear, isinternal).subscribe(res => {
            const file: any = res.body;
            this.spinner.hide();
            if (file.success) {
                this.toastrService.info(`Tải lên thành công ${file.success} bản ghi`);
                this.downloadURI(file.result);
                this.router.navigate(['/admin/training-plan-detail', res.body.planid]);
            } else {
                this.toastrService.error('common.noti.file_create_error');
                this.downloadURI(file.result);
            }
            this.fileSelected = null;
        }, error => {
            this.spinner.hide();
            this.toastrService.handlerError(error);
        });
    }
    /**
     * Process modal
     *
     * Modal used: CourseRecommend, CoursePlanDetail, ConfirmModal
     */
    openListCourseRecommend() {
        const modalRef = this.modalService.open(CourseRecommendPopup, {
            size: 'xl' as any,
            centered: false,
            backdrop: 'static'
        });

        modalRef.componentInstance.planId = this.planId;
        modalRef.componentInstance.loadAll.subscribe(($e) => {
            this.getCoureInPlan();
        });
    }

    openCoursePlanDetail(course?: any) {
        if (course?.courserun == 1 && course?.courseid) {
            this.router.navigate(['/admin/course/detail', course?.courseid]);
        } else {
            const modalRef = this.modalService.open(CoursePlanDetailComponent, {
                size: 'xl' as any,
                centered: false,
                backdrop: 'static'
            });
            modalRef.componentInstance.courseClone = course;
            modalRef.componentInstance.planId = this.planId;
            modalRef.componentInstance.canEdit = this.canEdit;
            modalRef.componentInstance.positionList = this.positionList;
            modalRef.componentInstance.loadAll.subscribe(($e) => {
                this.getCoureInPlan();
            });
        }
    }

    unAssignCourse(courseIds: number[]) {
        if (courseIds?.length > 0) {
            const modalDep = this.modalService.open(ConfirmModalComponent, {
                size: 'lg',
                centered: true,
                backdrop: 'static'
            });

            const params = {
                planid: this.planId,
                proposecourseid: courseIds.toString()
            }

            modalDep.componentInstance.title = this.translateService.instant('Xác nhận xóa khóa học');
            modalDep.componentInstance.body = 'Bạn có chắc chắn muốn xóa khóa học trong kế hoạch?';
            modalDep.componentInstance.confirmButton = this.translateService.instant('common.unassign');
            modalDep.result.then((result) => {
                if (result === 'confirm') {
                    this.spinner.show();
                    this.courseService.unAssignCourseRecommenToPlan(params).subscribe(
                        res => {
                            this.spinner.hide();
                            this.toastrService.success(`Xóa thành công`);
                            this.getCoureInPlan();
                        },
                        err => {
                            this.spinner.hide();
                            this.toastrService.handlerError(err);
                        }
                    );
                }
            });
        }
    }

    openCourseCreatePopup(course?: any) {
        const modalDep = this.modalService.open(CourseCreateComponent, {
            size: 'xl',
            centered: true,
            backdrop: 'static'
        });

        modalDep.componentInstance.creFollwingPlan = true;
        modalDep.componentInstance.coursePlan = course;
        modalDep.componentInstance.positionList = this.positionList;
    }
    /**
     * Utils
     *
     * Method: getTrainingType(), getDateFromUnix(), gotoListPlan(), showCost()
     *         chooseYear(), getPositionNames()
     */
    getTrainingType(val: any) {
        return TRAINING_TYPE.filter(e => e.value == val)[0]?.label
    }

    getDateFromUnix(date) {
        if (date) {
            return moment.unix(date);
        }
        return null;
    }

    gotoListPlan() {
        this.router.navigate(['/admin/training-plan-department']);
    }

    showCost(value: any) {
        return Intl.NumberFormat('vi-VN').format(value);
    }

    chooseYear(value: any) {
        this.planYear = value;
    }

    getPositionNames(trainingobject: any) {
        let positionName = '';
        if (trainingobject) {
            const positionIdArray = trainingobject.split(',');
            for (const positionId of positionIdArray) {
                const foundPosition = this.positionList.find(value => value.id === +positionId);
                if (positionName.length > 0) {
                    positionName += ', ';
                }
                if (foundPosition && foundPosition.name !== undefined) {
                    positionName += foundPosition.name;
                }
            }
            return positionName;
        }
        return '';
    }
    changePage(event) {
        this.pageIndex = event.pageIndex + 1;
        this.pageSize = event.pageSize;
        this.getCoureInPlan();
    }
    onChangeKeyWord() {
        this.pageIndex = 1;
        this.getCoureInPlan();
    }
    /**
     * Drag and drop file
     *
     * use HostListener: dragover, dragenter, dragend, dragleave. drop
     */
    error: string;
    dragAreaClass: string;
    fileSelected: any = null;
    onFileChange(event: any) {
        let files: FileList = event.target.files;
        this.saveFiles(files);
    }
    @HostListener("dragover", ["$event"]) onDragOver(event: any) {
        this.dragAreaClass = "droparea";
        event.preventDefault();
    }
    @HostListener("dragenter", ["$event"]) onDragEnter(event: any) {
        this.dragAreaClass = "droparea";
        event.preventDefault();
    }
    @HostListener("dragend", ["$event"]) onDragEnd(event: any) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
    }
    @HostListener("dragleave", ["$event"]) onDragLeave(event: any) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
    }
    @HostListener("drop", ["$event"]) onDrop(event: any) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
        event.stopPropagation();
        if (event.dataTransfer.files) {
            let files: FileList = event.dataTransfer.files;
            this.saveFiles(files);
        }
    }

    /**
     * Process file
     *
     * Method:
     * + saveFiles(): check validate to file and save to global variable
     * + dowloadURL(): ...
     * + dowloadTemplate(): download training plan template (excel)
     */
    saveFiles(files: FileList) {
        let allowedExtensions = /(\.xls|\.xlsx|\.xlsm)$/i;
        if (files.length > 1) {
            this.error = "Hệ thống chỉ cho phép tải lên từng tệp. Vui lòng kiểm tra lại";
        }
        else {
            if (!allowedExtensions.exec(files[0].name)) {
                this.error = "File không đúng định dạng";
                this.fileSelected = null;
            }
            else {
                this.error = "";
                this.fileSelected = files[0];
            }
        }
    }

    public downloadURI(uri: string, name?: string): void {
        const link = document.createElement('a');
        if (name) {
            link.setAttribute('download', name);
        }
        link.href = this.fileservice.getFileFromPathUrl(uri);
        document.body.appendChild(link);
        link.click();
        link.remove();
    }

    dowloadTemplate() {
        this.spinner.show();
        this.trainingPlanService.downloadTemplateTrainingPlanFile().subscribe(
            res => {
                this.spinner.hide()
                this.downloadURI(res.body.path);
            },
            err => {
                this.spinner.hide();
                this.toastrService.error('Đã có lỗi xảy ra trong quá trình tải xuống');
            }
        )
    }
    checkRoleFunction(userFunction: any, role: any): boolean {
        return USER_FUNCTIONS[userFunction].includes(role)
    }

    checkTimeStart(item) {
        var today = new Date();
        const itemYear = this.planDetail.year
        var first_day_of_first_quarter = new Date(itemYear, 0, 1);
        // console.log(first_day_of_first_quarter);
        var first_day_of_second_quarter = new Date(itemYear, 3, 1);
        var first_day_of_third_quarter = new Date(itemYear, 6, 1);
        var first_day_of_fourth_quarter = new Date(itemYear, 9, 1);
        if (item.planid !== null) {
            if (item.timeexpect == 1 && today >= first_day_of_first_quarter && !item.courseid) {
                return "text-danger";
            }
            if (item.timeexpect == 2 && today >= first_day_of_second_quarter  && !item.courseid ) {
                return "text-danger";
            }
            if (item.timeexpect == 3 && today >= first_day_of_third_quarter  && !item.courseid ) {
                return "text-danger";
            }
            if (item.timeexpect == 4 && today >= first_day_of_fourth_quarter  && !item.courseid ) {
                return "text-danger";
            }
        }
    }

    openListClassDetail(course?: any) {
        const modalRef = this.modalService.open(ListClassInCoursePlanDetailComponent, {
            size: 'xl' as any,
            centered: false,
            backdrop: 'static'
        });

        modalRef.componentInstance.courseClone = course;
        modalRef.componentInstance.planId = this.planId;
        modalRef.componentInstance.canEdit = this.canEdit;
        modalRef.componentInstance.positionList = this.positionList;
    }
    onPlanDetail(data: any){
        const modalRef = this.modalService.open(DetailTrainingPlanComponent, {
            size: 'xl' as any,
            centered: false,
            backdrop: 'static'
        });
        modalRef.componentInstance.data = data;
        modalRef.componentInstance.planId = this.planId;
        modalRef.componentInstance.emitter.subscribe( result =>{
            if(result){
                this.getCoureInPlan();
            }
          })
    }
}