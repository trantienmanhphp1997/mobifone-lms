export interface Position {
  id?: number;
  exclude?: number;
  name?: string;
  code?: string;
  description?: string;
  keyword?: string;
  ordernumber?: number;
  checked?: boolean;
  firstname?: string;
  lastname?: string;
  createdat?: number;
  checkedMany?: boolean;
  selected?: boolean;
  enableevaluation?: any;
  pcode?: any;
  hasevaluation?:number,
  completedat?:number,
}


