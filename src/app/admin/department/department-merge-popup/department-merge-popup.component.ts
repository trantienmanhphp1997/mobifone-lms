import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { Department } from 'src/app/shared/model/department.model';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import CommonUtil from 'src/app/shared/utils/common-util';

@Component({
  selector: 'app-department-merge-popup',
  templateUrl: './department-merge-popup.component.html',
  styleUrls: ['./department-merge-popup.component.css']
})
export class DepartmentMergePopupComponent implements OnInit {

  @Input() department: any;
  @Output() newDepartment = new EventEmitter<Department>();
  @Output() update = new EventEmitter<boolean>();
  public departmentList: Department[] = [];
  departmentMap = new Map();
  mergeForm = this.fb.group({
    mergeid: [null, [Validators.required]]
  })
  constructor(
    public activeModal: NgbActiveModal,
    public departmentService: DepartmentService,
    public fb: FormBuilder,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
  ) {

  }
  ngOnInit(): void {
    this.getDepartmentList()
  }
  getDepartmentList() {

    this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
      const results = data.body;
      this.departmentList = results.filter(item => item.id !== this.department.id);
      this.resetData();
    });
  }
  resetData() {
    this.departmentMap.clear();
    this.departmentList.forEach(department => {
      if (department.parentid !== null) {
        const parentName = this.departmentMap.get(department.parentid);
        let name = '';
        if (parentName) {
          name = parentName + ' / ' + department.name;
        } else {
          name = department.name;
        }
        this.departmentMap.set(department.id, name);
      } else {
        this.departmentMap.set(department.id, department.name);
      }
    });
  }
  mergeDepartment() {
    if (this.mergeForm.invalid) {
      CommonUtil.markFormGroupTouched(this.mergeForm);
      return;
    }
    const params = {
      id: this.department.id,
      mergeid: this.mergeForm.value.mergeid
    }
    this.spinner.show;
    this.departmentService.mergeDepartment(params).subscribe(data => {
      const department: Department = data.body;
      // console.log(department);
      this.newDepartment.emit(department);
      this.update.emit(true);
      this.toastrService.success('Sáp nhập đơn vị thành công');
      this.activeModal.dismiss('close');
      this.spinner.hide()
    }, err => {
      this.spinner.hide();
      this.activeModal.dismiss('close');
      this.toastrService.handlerError(err);
    })
  }

}
