export interface BadgeCategory {
    id?: number;
    name: string;
    description?: string;
    parent?: number;
    timemodified?: Date;
    depth?: number;
    path?: string;
    theme?: string;
    completed?: any;
    parentname?: string;
    islevel?: number;
  }
  
  
  