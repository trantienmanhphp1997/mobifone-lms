import {Component, isDevMode, OnInit} from '@angular/core';
import {FormBuilder, Validators} from '@angular/forms';
import {ToastrCustomService} from '../shared/services/toastr-custom.service';
import {LoginService} from '../shared/services/login.service';
import {Router} from '@angular/router';
import {NgxSpinnerService} from 'ngx-spinner';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ForgetPasswordComponent} from '../admin/forget-password/forget-password.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm = this.fb.group({
    username: [, Validators.required],
    password: [, Validators.required],
  });

  constructor(private fb: FormBuilder,
              private toastrService: ToastrCustomService,
              private loginService: LoginService,
              private spinner: NgxSpinnerService,
              private modalService: NgbModal,
              private router: Router) {
  }

  ngOnInit(): void {
  }

  login(){
    if (this.loginForm.invalid) {
      return;
    }
    // MLMS-1090
    this.loginForm.value.username = this.loginForm.value.username.toLowerCase();
    if (!this.loginForm.value.username.match('^[-\.@_a-z0-9]+$')){
      this.toastrService.error('error.invalidlogin');
      return;

    }

    this.spinner.show();
    this.loginService
      .login({
        ...this.loginForm.value
      }).then(
      data => {
        if (isDevMode()) {
          console.log('login done');
        }
        this.spinner.hide();
        this.toastrService.success(`login.success`);
        this.router.navigate(['home']);
      },
      err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      }
    );
  }

  forgetPassword() {
    this.router.navigate(['/forgot-password']);
  }

}
