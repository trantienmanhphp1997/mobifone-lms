import { Component, OnInit } from '@angular/core';
import { ContractService } from '../../shared/services/contract.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
@Component({
  selector: 'app-contract',
  templateUrl: './contract.component.html',
  styleUrls: ['./contract.component.css']
})
export class ContractComponent implements OnInit {
  listContract: any=[];
  keyword: string=null;
  totalRecord: number;
  pageSize = 10;
  pageIndex = 1;
  status:any;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  constructor(
      private contractService: ContractService,
      private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
    this.getListcontract();
  }
  getListcontract(){
    const params={
      keyword:this.keyword,
      status: this.status,
      page: this.pageIndex,
    };
    //this.spinner.show();
    this.contractService.listContractDetail(params).subscribe((data)=>{
    this.listContract= data.body.results;
    this.totalRecord = data.body.total;
    // this.spinner.hide();                  
    });
  }
  onChangeSearch(e:any){
    this.pageIndex=1;
    this.keyword= e.target.value;
    this.getListcontract();
  }
  onChangeStatus(event:any){
    this.pageIndex = 1;
    this.status= event.target.value;
    this.getListcontract();
  }
  changePage(event:any){
    this.pageIndex = 1;
    this.pageSize = event.pageSize;
    this.getListcontract();
  }
  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }
}
