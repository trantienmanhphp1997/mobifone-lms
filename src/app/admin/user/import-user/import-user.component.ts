import {AfterViewInit, Component, EventEmitter, HostListener, OnInit, Output} from '@angular/core';
import {FormBuilder} from '@angular/forms';
import {ToastrCustomService} from '../../../shared/services/toastr-custom.service';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {UserService} from '../../../shared/services/user.service';
import {URL_IMPORT_CSV_USER_TEMPLATE, USER_INFO} from '../../../shared/constants/base.constant';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';
import {NgxSpinnerService} from 'ngx-spinner';
import {FileService} from '../../../shared/services/file.service';


@Component({
  selector: 'app-import-user',
  templateUrl: './import-user.component.html',
  styleUrls: ['./import-user.component.css']
})
export class ImportUserComponent implements OnInit, AfterViewInit {

  @Output() newUser = new EventEmitter<boolean>();

  urlCSVTemplte: string;
  typeFileValid = ['application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', 'application/vnd.ms-excel'];
  fileToUpload: any = null;
  isDisable = true;

  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrCustomService,
    private userService: UserService,
    public activeModal: NgbActiveModal,
    private fileService: FileService,
    private spinner: NgxSpinnerService,
  ) {
  }

  ngOnInit(): void {
    this.urlCSVTemplte = URL_IMPORT_CSV_USER_TEMPLATE.URL;
  }

  ngAfterViewInit() {
  }

  importFileUser() {
    this.spinner.show();
    this.userService.importUserFormFile(this.fileToUpload).subscribe(res => {
      const file: any = res.body;
      this.spinner.hide();
      if (file.success) {
        this.toastrService.success('common.noti.file_create_success');
        window.open(this.fileService.getFileFromPathUrl(file.result));
        this.newUser.emit(true);
        this.activeModal.dismiss('close');

      } else {
        this.toastrService.error('common.noti.file_create_error');
      }
      this.fileToUpload = null;
      this.isDisable = true;
    }, error => {
      this.spinner.hide();
      this.toastrService.handlerError(error);
    });
  }

  dowloadTemplate() {
    this.spinner.show();
    this.userService.downloadTemplateUserFile().subscribe(
      res => {
        this.spinner.hide();
        this.downloadURI(res.body.path);
      },
      err => {
        this.spinner.hide();
        this.toastrService.error('Đã có lỗi xảy ra trong quá trình tải xuống');
      }
    );
  }

  public downloadURI(uri: string, name?: string): void {
    const link = document.createElement('a');
    if (name) {
      link.setAttribute('download', name);
    }
    link.href = this.fileService.getFileFromPathUrl(uri);
    document.body.appendChild(link);
    link.click();
    link.remove();
  }

  /**
   * Drag and drop file
   *
   * use HostListener: dragover, dragenter, dragend, dragleave. drop
   */
  error: string;
  dragAreaClass: string = "dragarea";
  onFileChange(event: any) {
    let files: FileList = event.target.files;
    this.saveFiles(files);
  }
  @HostListener("dragover", ["$event"]) onDragOver(event: any) {
    this.dragAreaClass = "droparea";
    event.preventDefault();
  }
  @HostListener("dragenter", ["$event"]) onDragEnter(event: any) {
    this.dragAreaClass = "droparea";
    event.preventDefault();
  }
  @HostListener("dragend", ["$event"]) onDragEnd(event: any) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
  }
  @HostListener("dragleave", ["$event"]) onDragLeave(event: any) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
  }
  @HostListener("drop", ["$event"]) onDrop(event: any) {
    this.dragAreaClass = "dragarea";
    event.preventDefault();
    event.stopPropagation();
    if (event.dataTransfer.files) {
      let files: FileList = event.dataTransfer.files;
      this.saveFiles(files);
    }
  }

  /**
   * Process file
   *
   * Method:
   * + saveFiles(): check validate to file and save to global variable
   * + dowloadURL(): ...
   * + dowloadTemplate(): download training plan template (excel)
   */
  saveFiles(files: FileList) {
    let allowedExtensions = /(\.xls|\.xlsx|\.xlsm)$/i;
    if (files.length > 1) {
      this.error = "Hệ thống chỉ cho phép tải lên từng tệp. Vui lòng kiểm tra lại";
      this.isDisable = true;
    }
    else {
      if (!allowedExtensions.exec(files[0].name)) {
        this.error = "File không đúng định dạng";
        this.fileToUpload = null;
        this.isDisable = true;
      }
      else {
        this.error = "";
        this.fileToUpload = files[0];
        this.isDisable = false;
      }
    }
  }
}
