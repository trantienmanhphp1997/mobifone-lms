import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-search-result',
  templateUrl: './search-result.component.html'
})

export class SearchResultComponent implements OnInit {
  keyword:any;
  constructor( private _Activatedroute: ActivatedRoute,) { }
  sub:any;
  ngOnInit():void {
    this.sub = this._Activatedroute.paramMap.subscribe(params => {
      // console.log(params);
      this.keyword = params.get('keyword');
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
