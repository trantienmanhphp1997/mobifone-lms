import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {FormBuilder, Validators} from '@angular/forms';
import {BadgeCategory} from '../../../shared/model/badge-category.model';
// import {CourseCategoryService} from '../../../shared/services/course-category.service';
import {ToastrCustomService} from '../../../shared/services/toastr-custom.service';
import {NgxSpinnerService} from 'ngx-spinner';
import CommonUtil from '../../../shared/utils/common-util';
import { BadgeCategoryService } from 'src/app/shared/services/badge-category.service';
import {Router} from '@angular/router';
@Component({
  selector: 'app-badge-level-info',
  templateUrl: './badge-level-info.component.html',
  styleUrls: ['./badge-level-info.component.css']
})
export class BadgeLevelInfoComponent implements OnInit {

  constructor(
    private fb: FormBuilder,
    private categoryService: BadgeCategoryService,
    private modalService: NgbModal,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    private router: Router
  ) {
  }
  @Input() id: any;
  @Output() seletedIndexEvent = new EventEmitter<number>();

  loadSuccess: any;
  categories: any;
  badgeId: any;
  category: BadgeCategory;
  editCategoryForm = this.fb.group({
    id: [''],
    name: ['', [Validators.required, Validators.maxLength(200)]],
    parent: ['', Validators.required],
    description: ['', [Validators.maxLength(255)]],
    islevel: 1
  });

  cloneCategory: BadgeCategory;
  categoryMap = new Map();

  ngOnInit(): void {
    this.spinner.show();
    this.categoryService.getBadgeCategoryTree(null).subscribe((data) => {
      const dataSource = data.body;
      if(dataSource.length){
        dataSource.filter(x => x.islevel == 0 && x.id !== this.id).forEach(cat => {
          if (cat.parent != null) {
            const parentName = this.categoryMap.get(cat.parent);
            let name = '';
            if (parentName) {
              name = parentName + ' / ' + cat.name;
            } else {
              name = cat.name;
            }
            this.categoryMap.set(cat.id, name);
          } else {
            this.categoryMap.set(cat.id, cat.name);
          }
        });
      }
    });

    if (this.id) { // case update
      this.categoryService.searchBadgeCategory({id: this.id, islevel: 1}).subscribe((data) => {
        if(data.body.results.length){
          this.editCategoryForm.patchValue({
            id: data.body.results[0].id,
            name: data.body.results[0].name,
            description: data.body.results[0].description,
            parent: data.body.results[0].parent
          });
        }
     }, err => {
        this.toastrService.handlerError(err);
     });
    }

    this.spinner.hide();
  }


  createOrUpdateBadgeCategory() {
    if (this.editCategoryForm.invalid) {
      // Danh dau la da cham de hien thi message loi
      CommonUtil.markFormGroupTouched(this.editCategoryForm);
      return;
    }
    this.spinner.show();
    if(this.id){// case update
      this.categoryService.updateBadgeCategory(this.editCategoryForm.value).subscribe(
        res => {
          this.toastrService.success(`common.noti.update_success`);
          this.seletedIndexEvent.emit(1);
          this.close();
          
        },
        err => {
          this.spinner.hide();
          this.toastrService.handlerError(err);
        }
      );
    }else { // case add new
      this.editCategoryForm.patchValue({
        islevel: 1
      });
      this.categoryService.createBadgeCategory(this.editCategoryForm.value).subscribe(
        res => {
          this.spinner.hide();
          if(res.body){
            this.router.navigate(['/admin/badge-level/detail', res.body[0].id]);
          }
          this.toastrService.success(`common.noti.create_success`);
          this.close();
        },
        err => {
          this.spinner.hide();
          this.toastrService.handlerError(err);
        }
      );
    }

  }

  close() {
    this.spinner.hide();
  }
}
