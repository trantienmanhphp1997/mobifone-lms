import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {CourseCategoryService} from '../../../shared/services/course-category.service';
import CommonUtil from '../../../shared/utils/common-util';
import {CourseCategory} from '../../../shared/model/course-category.model';
import { QuestionCategoryService } from 'src/app/shared/services/question-category.service';

@Component({
  selector: 'app-bank-search',
  templateUrl: './bank-search.component.html',
  styleUrls: ['./bank-search.component.css']
})
export class BankSearchComponent implements OnInit {

  searchQues: any;

  active0: string;
  active1: string;
  active2: string;
  active3: string;
  courseCategoryList: CourseCategory[] = [];
  questionCategoryList: any[] = [];
  categoryMap = new Map();
  categoryId = 0;
  qtype = '';
  level = 0;


  constructor(
    public activeModal: NgbActiveModal,
    private courseCategoryService: CourseCategoryService,
    private categoryService: QuestionCategoryService
  ) {}

  ngOnInit(): void {
    if (this.searchQues.level) {
      this.searchLevel(this.searchQues.level);
    } else {
      this.active0 = 'active2';
    }
    if (this.searchQues.categoryid) {
      this.categoryId = this.searchQues.categoryid;
    }
    if (this.searchQues.qtype) {
      this.qtype = this.searchQues.qtype;
    }

    this.categoryService.getQuestionCategoryTree(this.searchQues.courseqcategoryid).subscribe(response => {
      this.questionCategoryList = response.body;
      CommonUtil.convertCategoryListToMap(this.questionCategoryList, this.categoryMap);
    });
  }

  searchLevel(index) {
    this.active0 = '';
    this.active1 = '';
    this.active2 = '';
    this.active3 = '';
    if (index === 0) {
      this.active0 = 'active2';
      this.level = 0;
    } else if (index === 1) {
      this.active1 = 'active2';
      this.level = 1;
    } else if (index === 2) {
      this.active2 = 'active2';
      this.level = 2;
    } else if (index === 3) {
      this.active3 = 'active2';
      this.level = 3;
    }
  }

  onSearchQuestion() {
    const searchParams = {
      categoryId: this.categoryId,
      qtype: this.qtype,
      level: this.level,
    };
    this.activeModal.close(searchParams);
  }

  onChangeDataLevelSearch(type) {
    this.qtype = type;
  }

  resetSearch() {
    this.categoryId = 0;
    this.searchLevel(0);
    this.qtype = '';
  }

}
