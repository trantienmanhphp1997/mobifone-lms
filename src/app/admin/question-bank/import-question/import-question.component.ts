import { Component, EventEmitter, HostListener, OnInit, Output, Input} from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { URL_EXPORT_QUESTOIN_TEMPLATE } from 'src/app/shared/constants/base.constant';
import { FileService } from 'src/app/shared/services/file.service';
import { QuestionBankService } from 'src/app/shared/services/question-bank.service';
import { QuestionCategoryService } from 'src/app/shared/services/question-category.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import CommonUtil from 'src/app/shared/utils/common-util';

@Component({
    selector: 'app-imoprt-question',
    templateUrl: './import-question.component.html',
    styleUrls: ['./import-question.component.css']
})
export class ImportQuestion implements OnInit {
    @Output() newQuestions = new EventEmitter<boolean>();
    @Input() courseQCategoryId = null;

    questionCategoryList: any[] = [];
    categoryMap = new Map();
    categoryId = null;
    errCate = '';
    questionTempUrl: string
    constructor(
        public activeModal: NgbActiveModal,
        private questionBankService: QuestionBankService,
        private fileService: FileService,
        private toastrService: ToastrCustomService,
        private spinner: NgxSpinnerService,
        private categoryService: QuestionCategoryService,
    ) { }

    error: string;
    dragAreaClass: string;
    fileSelected: any = null;
    onFileChange(event: any) {
        let files: FileList = event.target.files;
        this.saveFiles(files);
    }
    ngOnInit() {
        this.dragAreaClass = "dragarea";
        this.questionTempUrl = URL_EXPORT_QUESTOIN_TEMPLATE.URL
        this.categoryService.getQuestionCategoryTree(this.courseQCategoryId).subscribe(response => {
            this.questionCategoryList = response.body;
            CommonUtil.convertCategoryListToMap(this.questionCategoryList, this.categoryMap);
        });
    }
    @HostListener("dragover", ["$event"]) onDragOver(event: any) {
        this.dragAreaClass = "droparea";
        event.preventDefault();
    }
    @HostListener("dragenter", ["$event"]) onDragEnter(event: any) {
        this.dragAreaClass = "droparea";
        event.preventDefault();
    }
    @HostListener("dragend", ["$event"]) onDragEnd(event: any) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
    }
    @HostListener("dragleave", ["$event"]) onDragLeave(event: any) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
    }
    @HostListener("drop", ["$event"]) onDrop(event: any) {
        this.dragAreaClass = "dragarea";
        event.preventDefault();
        event.stopPropagation();
        if (event.dataTransfer.files) {
            let files: FileList = event.dataTransfer.files;
            this.saveFiles(files);
        }
    }

    saveFiles(files: FileList) {
        let allowedExtensions = /(\.xls|\.xlsx)$/i;
        if (files.length > 1) {
            this.error = "Only one file at time allow";
        }
        else {
            // console.log(files[0].type);

            if (!allowedExtensions.exec(files[0].name)) {
                this.error = "File không đúng định dạng";
                this.fileSelected = null;
            }
            else {
                this.error = "";
                this.fileSelected = files[0];
            }

        }
    }
    downloadTemplate() {
        const url =  URL_EXPORT_QUESTOIN_TEMPLATE.URL;
        const fileName = `question_in_quiz_example1631777242.xlsx`;
        this.downloadURI(url, fileName);
    }
    
    public downloadURI(uri: string, name?: string): void {
        const link = document.createElement('a');
        // If you don't know the name or want to use
        // the webserver default set name = ''
        if (name) {
          link.setAttribute('download', name);
        }
        link.href = uri;
        document.body.appendChild(link);
        link.click();
        link.remove();
    }

    impportQuestion () {
        if (!this.categoryId) {
            this.errCate = "Chưa chọn danh mục";
            return
        }
        if (!this.fileSelected) {
            this.error = "Chưa chọn file";
            return
        }
        this.spinner.show();
        this.questionBankService.importQuestionFormFile(this.fileSelected, this.categoryId).subscribe(res => {
            const file: any = res.body;
            this.spinner.hide();
            if (file.success) {
              this.toastrService.success('common.noti.file_create_success');
              this.newQuestions.emit(true);
            } else {
              window.open(this.fileService.getFileFromPathUrl(file.result));
              this.toastrService.error('common.noti.file_create_error');
            }
            this.fileSelected = null;
          }, error => {
            this.spinner.hide();
            this.toastrService.handlerError(error);
          });
        this.activeModal.dismiss('close');
    }
}
