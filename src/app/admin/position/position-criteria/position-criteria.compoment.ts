import { PageEvent } from '@angular/material/paginator';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import { Course } from '../../../shared/model/course.model';
import { CourseService } from '../../../shared/services/course.service';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { PositionService } from '../../../shared/services/position.service';
import { ToastrCustomService } from '../../../shared/services/toastr-custom.service';
import { COURSE_TYPE } from '../../../shared/constants/base.constant';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { CriteriaService } from 'src/app/shared/services/criteria.service';

@Component({
    selector: 'app-position-criteria',
    templateUrl: './position-criteria.compoment.html',
    encapsulation: ViewEncapsulation.None,
    styleUrls: ['./position-criteria.compoment.css']
})
export class PositionCriteriaComponent implements OnInit {
    public activeIndex: any = 0;
    @Input() positionId: number;
    @Input() open: string;
    @Output() loadAll = new EventEmitter();
    completedAll = false;
    course: Course[] = [];
    exam: Course[] = [];
    courses = new Array();
    courseIds = new Array();
    coursetype: number;
    courseIdsChecked: number[] = [];

    pageSizeOptions: number[] = [10, 25, 50, 100];
    totalRecord: number;
    search = '';
    pageIndex = 1;
    pageSize = 10;
    sortcolumn = 'timecreated';
    sorttype = 'DESC';

    // MatPaginator Output
    pageEvent: PageEvent;

    @Input() isReportSreen: boolean;
    @Input() arrayidCourses: any[];

    @Input() listCourses: Course[] = [];
    @Input() listExam: Course[] = [];
    @Output() transferCourseIds = new EventEmitter<string>();
    @Input() isNotify: boolean;
    @Input() surveyId: number;

    constructor(
        public activeModal: NgbActiveModal,
        private courseService: CourseService,
        private programPositionService: PositionService,
        private toastrService: ToastrCustomService,
        private modalService: NgbModal,
        private spinner: NgxSpinnerService,
        private criteriaService: CriteriaService
    ) { }

    ngOnInit() {
        // lay ds khoa hoc cho chuc danh
        if (this.positionId) {
            this.getListCoursePosition(this.positionId);
        }
    }

    onRadiocheckboxItem(courseId: number) {
        this.courseIdsChecked[0] = courseId;
    }

    oncheckboxItem(courseId: number, checked: any) {
        if (checked) {
            this.courses?.forEach(c => {
                if (c.id === courseId) {
                    c.completed = true;
                    this.courseIds?.push(courseId);
                    this.courseIdsChecked?.push(courseId);
                    return;
                }
            });
            // nếu số khóa checked bằng số khóa học có thể checked trong page thì set checkboxall = true
            if (this.courseIds?.length > 0 && this.courseIds?.length === this.courses?.length && !this.completedAll) {
                this.completedAll = true;
            }
        } else {
            this.completedAll = false;
            this.courses?.forEach(c => {
                if (c.id === courseId) {
                    c.completed = false;
                    this.courseIds?.splice(this.courseIds?.indexOf(courseId), 1);
                    this.courseIdsChecked?.splice(this.courseIdsChecked?.indexOf(courseId), 1);
                    return;
                }
            });
        }
    }

    oncheckboxAll(checked: any) {
        if (checked) {
            this.courses.forEach(c => {
                c.completed = checked;
                if (!this.courseIds.includes(c.id)) {
                    this.courseIds.push(c.id);
                }
                if (!this.courseIdsChecked?.includes(c.id)) {
                    this.courseIdsChecked?.push(c.id);
                }
            });
            this.completedAll = true;
        } else {
            this.courseIds?.forEach(id => {
                this.courseIdsChecked?.splice(this.courseIdsChecked?.indexOf(id), 1);
            });
            this.courseIds = [];
            this.courses?.forEach(c => {
                c.completed = false;
            });
            this.completedAll = false;
        }
    }

    cancel() {
        this.activeModal.dismiss();
    }

    save() {
        if (this.courseIdsChecked?.length === 0) {
            this.toastrService.error('error.needSelectCourse');
            return;
        }
        this.spinner.show();
        this.programPositionService.assignCriteria(this.positionId, this.courseIdsChecked).subscribe(
            res => {
                this.spinner.hide();
                this.modalService.dismissAll();
                this.toastrService.success(`common.noti.ass`);
                this.loadAll.emit('load');
            }, error => {
                this.spinner.hide();
                this.toastrService.handlerError(error);
            }
        );
    }

    onSearch() {
        this.pageIndex = 1;
        this.courseIdsChecked = [];
        if (this.positionId) {
            this.getListCoursePosition(this.positionId);
        }
        // const paramPosition = {
        //   positionid: this.positionId,
        //   coursetype: this.coursetype,
        //   exclude: (this.positionId===null||this.positionId===undefined)?0:1,
        //   keyword: this.search,
        //   action: 'add',
        //   page: this.pageIndex,
        //   limit: this.pageSize
        // };
        // this.programPositionService.searchPositionCouse(paramPosition).subscribe(data => {
        //   this.courses = data.body.results;
        //   this.totalRecord = data.body.total;
        // });
    }

    // lay ds khoa hoc cho phan chuc danh
    getListCoursePosition(positionId) {
        this.spinner.show();
        const params = {
            //   positionid: positionId,
            //   coursetype: this.coursetype,
            //   exclude: 1,
            //   action: 'add',
            //   keyword: this.search,
              limit: this.pageSize,
              page: this.pageIndex,
            //   sortcolumn: this.sortcolumn,
            //   sorttype: this.sorttype
            exclude: positionId
        };
        this.courseIds = [];
        this.criteriaService.getListCriteria(params).subscribe(
            data => {
                data.body?.results?.forEach(c => {
                    if (this.courseIdsChecked?.includes(c.id)) {
                        c.completed = true;
                        this.courseIds?.push(c.id);
                    }
                });
                // nếu số khóa học được checked = số khóa học có thể checked thì set completedAll = true
                if (this.courseIds?.length > 0 && (this.courseIds?.length === data.body?.results?.length)) {
                    this.completedAll = true;
                } else {
                    this.completedAll = false;
                }
                this.courses = data.body.results;
                this.totalRecord = data.body.total;
                this.spinner.hide();
            }, err => {
                this.spinner.hide();
                this.toastrService.error(err);
            }
        );
    }


    getDateFromUnix(date) {
        if (date) {
            return moment.unix(date);
        }
        return null;
    }


    changePage(event) {
        this.pageIndex = event.pageIndex + 1;
        this.pageSize = event.pageSize;
        if (this.positionId) {
            this.getListCoursePosition(this.positionId);
        }
    }
}