import { Component, OnInit,Input, Output,EventEmitter,HostListener } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractControl, FormBuilder, FormControl, Validators } from '@angular/forms';
import {CustomValidators} from '../../../../shared/utils/custom-validators';
import { BadgeService } from '../../../../shared/services/badge.service';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import { FileService } from 'src/app/shared/services/file.service';
import CommonUtil from '../../../../shared/utils/common-util';
import {LocalStorageService} from 'ngx-webstorage';
@Component({
  selector: 'app-create-badge',
  templateUrl: './create-badge.component.html',
  styleUrls: ['./create-badge.component.css']
})
export class CreateBadgeComponent implements OnInit {
  @Input() Badge;
  @Output() newBad = new EventEmitter<any>();
  error: string;
  roles: any;
  selectedLink = '';
  fileSelected: any = null;
  imageSrc: any;
  showUpload :any;
  mimeType: string;
  fileName: string;
  currentDate: Date = new Date();
  listCategoriBadge: any[]= [];
  categoryBadgeMap= new Map();
  imageType: any = ['png', 'jpeg', 'jpg', 'csv', 'gif', 'jfif'];
  fileType: any = ['pdf'];
  pdfSrc: any;
  expiredate: any = null;
  dragAreaClass: string;
  pageVariable: number = 1;
  isUpload : boolean= false;
  isSave : boolean =false;
  isteacher: boolean = false;
  nameAdmin: any;
  ischecked = false;
  chosenItem = '';
  action: any = [];
  isActive: boolean = false;
  dateinvalid: boolean = false;
  fileinvalid: boolean = false;
  badgeForm = this.fb.group({
    name: [, [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(100)]],
    radio: [],
    expiredate: [],
    categoryid:[''],
  });
  radio: any = [
    {
      name: 'Vô thời hạn',
      value: '',
    },
    {
      name: 'Chọn thời gian hết hạn',
      value: 'expiredate',
    }
  ];
  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private badeService: BadgeService,
    private spinner:NgxSpinnerService,
    private fileservice : FileService,
    private $localStorage: LocalStorageService
  ) {
    this.showUpload=true;
   }

  ngOnInit(): void { 
    this.roles = this.$localStorage.retrieve('roles');
    for (let j = 0; j < this.roles.length; j++) {
      this.nameAdmin= this.roles[j].shortname;
      if(this.nameAdmin=='admin'){
        this.isteacher = true;
        this.badgeForm.disable();
      }
      else if(this.nameAdmin=='manager'){
        this.isteacher = true;
        this.badgeForm.disable();
      }
      else{
        this.isteacher = false;
      }
    }
    this.getCategoryBadge();
    this.isUpload=true;
    if(this.Badge?.id != null){

      this.isSave = true;
      if ((this.Badge.expiredate === null ) || (this.Badge.expiredate === 0)) {
        this.selectedLink = '';
        this.chosenItem = '';
      } else if (this.Badge.expiredate ) {
        this.selectedLink = 'expiredate';
        this.chosenItem = 'expiredate';
        this.ischecked = true;
      }
      const Expiredate = this.Badge.expiredate ? moment.unix(this.Badge.expiredate).toDate() : null;
      this.badgeForm.patchValue({
        name:this.Badge.name ,
        expiredate: Expiredate,
        categoryid: this.Badge.categoryid,
      });   
      if(this.Badge.status == 2){
          this.isUpload = true;
      }
      else{
          this.isUpload= false;
          this.badgeForm.disable();
      } 
    }
    this.showUpload = "dragarea";
    if(this.Badge && this.Badge.filename){
      var name = this.Badge.filename.split('.');
      if(name) {
          this.mimeType = name[name.length - 1];
          this.fileName = name[0].length > 170 ? name[0].substring(0,170) + '[...].' + this.mimeType :  name[0] + '.' + this.mimeType
      }
    }
    this.action = [
      {
          name: "zoomIn",
          icon: "",
          f: function() {

          }
      },
      {
          name: "zoomOut",
          icon: "",
          f: function() {
              
          }
      },
      {
          name: "page",
          icon: "",
          f: function() {
              
          }
      },
      {
          name: "zoomIn",
          icon: "",
          f: function() {
              
          }
      }
  ]
  }
  createNewBadge() {
    if (this.badgeForm.invalid) {
      CommonUtil.markFormGroupTouched(this.badgeForm);
      return;
    }
    if(!this.badgeForm.value.name){
      this.dateinvalid = true;
      return;
    }
    if(!this.fileSelected){
      this.fileinvalid = true;
      return;
    }
    const params = {
      name: this.badgeForm.value.name,
      expiredate: this.badgeForm.value.expiredate ? this.badgeForm.value.expiredate.getTime()/1000 : null,
      categoryid: this.badgeForm.value.categoryid,
    }
    this.spinner.show();
    this.badeService.uploadTeacherBadge(params,this.fileSelected).subscribe(res => {
      this.spinner.hide();
      this.activeModal.dismiss();
      this.badgeForm.reset();
      this.newBad.emit(this.Badge);
    });
    this.fileSelected=null;
  }
  EditBadge(){
    const params = {
      id: this.Badge.id,
      name: this.badgeForm.value.name,
      expiredate: this.badgeForm.value.expiredate.getTime()/1000,
      categoryid: this.badgeForm.value.categoryid,
    }
    this.spinner.show();
    this.badeService.uploadTeacherBadge(params,this.fileSelected).subscribe(res => {
      this.spinner.hide();
      this.activeModal.dismiss();
      this.badgeForm.reset();
      this.newBad.emit(this.Badge);
    });
  }
  getCategoryBadge(){
    const paramCate ={
        all:1
    }
    this.badeService.getCategoryBadgeList(paramCate).subscribe(data =>{
    this.listCategoriBadge= data.body.results;
     CommonUtil.convertCategoryListToMap(this.listCategoriBadge, this.categoryBadgeMap);
    });
  }
  saveFiles(files: FileList) {
    if (files.length > 1) {
        this.error = "Hệ thống chỉ cho phép tải lên từng tệp. Vui lòng kiểm tra lại";
    } else if(files.length) {
        this.error = "";
        this.fileSelected = files[0];
        const reader = new FileReader();
        if(this.fileSelected.type == "application/pdf"){
            if (typeof (FileReader) !== 'undefined') {
                reader.onload = (e: any) => {
                    this.imageSrc = e.target.result;
                };
                reader.readAsArrayBuffer(this.fileSelected);
            }
        }else{
            reader.onload = e => this.imageSrc = reader.result;
            reader.readAsDataURL(this.fileSelected);
        }
        this.showUpload=false;
        if(this.fileSelected.name) {
            var name = this.fileSelected.name.split('.');
            if(name) {
                this.mimeType = name[name.length - 1];
                this.fileName = name[0].length > 170 ? name[0].substring(0,170) + '[...].' + this.mimeType :  name[0] + '.' + this.mimeType
            }
        }
    }
  }
  onFileChange(event: any) {
    let files: FileList = event.target.files;
    this.saveFiles(files);
  }
  viewFile() {
    
    var file = null;
    if (this.Badge) {
        file = this.fileservice.badgeFileUrl(this.Badge.contextid, this.Badge.filename, '/external_badge/overviewfiles/', '0/');
    }
    return file;
  }
  checkType(type :any) {
    if(type.includes(this.mimeType))
      return true;
  }
  pageRendered(e: CustomEvent) {
  }
  onProgress(progressData) {
  }
  @HostListener("dragover", ["$event"]) onDragOver(event: any) {
    this.dragAreaClass = "droparea";
    event.preventDefault();
  }
  @HostListener("dragenter", ["$event"]) onDragEnter(event: any) {
      this.dragAreaClass = "droparea";
      event.preventDefault();
  }
  @HostListener("dragend", ["$event"]) onDragEnd(event: any) {
      this.dragAreaClass = "dragarea";
      event.preventDefault();
  }
  @HostListener("dragleave", ["$event"]) onDragLeave(event: any) {
      this.dragAreaClass = "dragarea";
      event.preventDefault();
  }
  @HostListener("drop", ["$event"]) onDrop(event: any) {
      this.dragAreaClass = "dragarea";
      event.preventDefault();
      event.stopPropagation();
      if (event.dataTransfer.files) {
          let files: FileList = event.dataTransfer.files;
          this.saveFiles(files);
      }
  }
  closeImg(){
      this.isActive= true;
  }
  setradio(e: string): void {
    this.selectedLink = e;
    switch (e) {
      case 'expiredate':
        this.badgeForm.addControl('expiredate', new FormControl('', Validators.required));
        break;
      default:
        this.badgeForm.removeControl('expiredate');
        break;
    }
  }
  setFormControlValue(name) {
    if (this.Badge?.id != null && this.Badge.expiredate !=null) {
      if (name === 'expiredate') {
        // return this.badgeForm.controls['expiredate'].setValue(moment.unix(this.Badge.expiredate).toDate());
      }

    }
    return null;
  }
  isSelected(name: string): boolean {
    if (!this.selectedLink) {
      return false;
    }
    this.setFormControlValue(this.selectedLink);
    return (this.selectedLink === name);
  }
}
