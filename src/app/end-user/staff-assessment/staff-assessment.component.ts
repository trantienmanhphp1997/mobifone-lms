import { Spinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StaffAssessmentService } from 'src/app/shared/services/staff-assessment.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { EvaluationPopupComponent } from './evaluation-popup/evaluation-popup.component';
import {ToastrCustomService} from '../../shared/services/toastr-custom.service';
import { PositionService } from '../../shared/services/position.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-staff-assessment',
  templateUrl: './staff-assessment.component.html',
  styleUrls: ['./staff-assessment.component.css']
})
export class StaffAssessmentComponent implements OnInit {
  positions:any=[];
  listStudent: any=[];
  keyword: string=null;
  totalRecord: number;
  pageSize = 10;
  pageIndex = 1;
  openTab: number;
  status:any;
  currentSelectedPositionId:null;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  searchPositionForm = this.fb.group({
    keyword: ['']
  });
  constructor(
    private fb: FormBuilder,
    private _router: Router,
    private staffAssessmentService: StaffAssessmentService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private toastrService: ToastrCustomService,
    private programPositionService: PositionService,

  ) {}

  ngOnInit(): void {
    this.openTab =1;
    this.getlistStudent();
    this.getlistTree();
  }
  onChangeSearch(e: any){
    this.pageIndex = 1;
    this.getlistStudent();

  }
  openEvaluationForm(std: any){
    const modalDep = this.modalService.open(EvaluationPopupComponent, {
      size: 'xl',
      centered: true,
      backdrop: 'static'
  });

  modalDep.componentInstance.student = std;
  //   modalDep.componentInstance.body = this.translateService.instant('badge.unassign_course_confirm_content');
  //   modalDep.componentInstance.confirmButton = this.translateService.instant('common.unassign');

  modalDep.componentInstance.newStaffAss.subscribe(($e) => {
      this.getlistStudent();
  });
  }
  changePage(event: any){
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getlistStudent();
  }
  onChangeStatus(event: any){
    this.pageIndex = event.pageIndex + 1;
    this.status= event.target.value;
    this.getlistStudent();
  }
  getlistStudent(){
    const params ={
      search: this.keyword,
      status: this.status,
      limit: this.pageSize,
    }
    this.spinner.show();
    this.staffAssessmentService.getListStdCplteOnB(params).subscribe( res =>{
        this.listStudent= res.body.results;
        this.totalRecord = res.body.total
        this.spinner.hide();
    });
  }
  displayDetail(positionid){
      this.listStudent = this.listStudent?.find(p => p.id === positionid);
      this.currentSelectedPositionId = positionid;
      const params = {
        positionid,
        limit: 0,
      };
      this.staffAssessmentService.getListStdCplteOnB(params).subscribe((data) => {
        this.listStudent = data.body.results;
        this.listStudent.forEach(element => {
          if (element.badges !== null && element.badges !== undefined) {
            if (element.badges.length !== 0) {
              element.certificate = 'yes';
            } else {
              element.certificate = 'no';
            }
          }
        });
      }, error => {
        this.toastrService.handlerError(error);
      });
  } 
  getlistTree(){
    const paramPosition = {
      keyword: this.searchPositionForm.value.keyword,
      limit: 0,
      hasevaluation:1
    };
    this.spinner.show();
    this.programPositionService.searchPosition(paramPosition).subscribe( data =>{
        this.spinner.hide();
        this.positions= data.body.results;
    });
  }
}
