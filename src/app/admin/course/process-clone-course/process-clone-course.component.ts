import { NgxSpinnerService } from 'ngx-spinner';
import { Router } from '@angular/router';
import { Component, ElementRef, HostListener, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CourseService } from '../../../shared/services/course.service';
import { ToastrCustomService } from '../../../shared/services/toastr-custom.service';


@Component({
  selector: 'app-process-clone-course',
  templateUrl: './process-clone-course.component.html',
  styleUrls: ['./process-clone-course.component.css']
})
export class ProcessCloneCourseComponent implements OnInit, OnDestroy {
  @Input() courseClone: any;
  @Input() backupId: any;
  @Input() restoreId: any;
  @Input() courseId: any;
  timer: any;
  process: any = 0
  status: any = 0
  operation:any = 'backup'
  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrCustomService,
    private courseService: CourseService,
    public activeModal: NgbActiveModal,
    private router: Router,
    private spinner: NgxSpinnerService,

  ) { }

  ngOnInit(): void {
    this.timer = setInterval(() => {
      this.refresh();
    }, 15000);
  }
  ngOnDestroy() {
    clearInterval(this.timer);
  }
  refresh() {
    if (this.status < 1000 && this.status != 900) {
      // this.spinner.show('temp-process');
      this.courseService.processCloneCourse(this.backupId, this.restoreId).subscribe(res => {
        this.process = Math.round(res.body[0].progress * 100 * 100) / 100
        this.status = res.body[0].status;
        this.operation = res.body[0].operation
      },
        err => {
          this.toastrService.handlerError(err);
        }
      );
    }
    else {
      clearInterval(this.timer);
    }
  }

  goToDetail() {
    this.router.navigate(['admin/course/detail', this.courseId]);
  }

  goToList () {
    window.location.reload();
  }
}
