import { ContractComponent } from './../end-user/contract/contract.component';
import {ExamDetailComponent} from './exam/exam-detail/exam-detail.component';
import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AdminComponent} from './admin.component';
import {CourseComponent} from './course/course.component';
import {CourseCategoryComponent} from './course-category/course-category.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {CourseCreateComponent} from './course/course-create/course-create.component';
import {DepartmentComponent} from './department/department.component';
import {UserComponent} from './user/user.component';
import {AuthGuard} from '../auth/auth.guard';
import {CourseContentComponent} from './course-content/course-content.component';
import {PositionComponent} from './position/position.component';
import {ExamComponent} from './exam/exam.component';
import {CreateExamComponent} from './exam/create-exam/create-exam.component';
import {SurveyComponent} from './survey/survey.component';
import {CreateSurveyComponent} from './survey/create-survey/create-survey.component';
import {SurveyDetailComponent} from './survey/survey-detail/survey-detail.component';
import {CourseDetailComponent} from './course/course-detail/course-detail.component';
import {QuestionBankComponent} from './question-bank/question-bank.component';
import {QuestionCreateComponent} from './question-bank/question-create/question-create.component';
import {BadgeComponent} from './badge/badge.component';
import {BadgeCreateComponent} from './badge/badge-create/badge-create.component';
import {BadgeDetailsComponent} from './badge/badge-details/badge-details.component';
import {NotificationComponent} from './notification/notification.component';
import {CreateNotificationComponent} from './notification/create-notification/create-notification.component';
import {RoleComponent} from './role/role.component';
import {ReportComponent} from './report/report.component';
import {ReportCourseComponent} from './report/report-course/report-course.component';
import {USER_MENU} from '../shared/constants/base.constant';
import {ReportStudentComponent} from './report/report-student/report-student.component';
import { SurveySingleDetailComponent } from './survey/survey-single-detail/survey-single-detail.component';
import { ResultExamComponent } from './report/result-exam/result-exam.component';
import { ResultDetailExamComponent } from './report/result-exam/result-detail-exam/result-detail-exam.component';
import { StudentDetailsComponent } from './report/report-student/student-details/student-details.component';
import { QuestionCategoryComponent } from './question-category/question-category.component';
import { BadgeExternalComponent } from './badge-external/badge-external.component';
import { BadgeExternalDetailsComponent } from './badge-external/badge-external-details/badge-external-details.component';
import { BadgeLevelDetailComponent } from './badge-level/badge-level-detail/badge-level-detail.component';
import { BadgeLevelComponent } from './badge-level/badge-level.component';
import { BadgeCategoryComponent } from './badge-category/badge-category.component';
import { ReportTeacherComponent } from './report/report-teacher/report-teacher.component';
import { ReportKpiComponent } from './report/report-kpi/report-kpi.component';
import { TeacherDetailsComponent } from './report/report-teacher/teacher-details/teacher-details.component';
import { StaffAssessmentComponent } from './staff-assessment/staff-assessment.component';
import { ReportContractComponent } from './report/report-contract/report-contract.component';
import { LaudatoryComponent } from './laudatory/laudatory.component';
import { CriteriaComponent } from './criteria/criteria.component';
import { ManageApproverComponent } from './manage-approver/manage-approver.component';
import { TrainingPlanComponent } from './training-plan/training-plan.component';
import { TrainingDetailComponent } from './training-plan/training-plan-detail/training-plan-detail.component';
import { TrainingPlanDepartment } from './training-plan/training-plan-department/training-plan-department.component';
import { CourseRecommendComponent } from './course-recommend/course-recommend.component';
import { BadgeOfflineStudentComponent } from './badge-external/bagde-offline-student/badge-offline-student.component';
import{ ReportDetailComponent} from './report/report-contract/report-detail/report-detail.component';
import{  ReportTeacherInternalComponent } from './report/report-teacher-internal/report-teacher-internal.component';
import { CourseQuestionBankComponent } from './course-content/course-question-bank/course-question-bank.component';
import { CourseQuestionCreateComponent } from './course-content/course-question-create/course-question-create.component';
import { CourseQuestionCategoryComponent } from './course-content/course-question-category/course-question-category.component';
import { RollCallComponent } from './roll-call/roll-call.component';
import { RollCallDetailComponent } from './roll-call/roll-call-detail/roll-call-detail.component';
import { ReportTrainingPlanComponent } from './report/report-training-plan/report-training-plan.component';
import { ReportStudentDataComponent } from './report/report-student-data/report-student-data.component';
import { TeacherComponent } from './teacher/teacher.component';
import{ DetailTeacherComponent } from './teacher/detail-teacher/detail-teacher.component';
import { CourseCloneComponent } from './course-clone/course-clone.component';
import { ReportTrainingComponent } from './report/report-training/report-training.component';
import { ReportTrainingSourceComponent } from './report/report-training-source/report-training-source.component';
import { ReportLearningDataComponent } from './report/report-learning-data/report-learning-data.component';

const routes: Routes = [{
  path: 'admin',
  component: AdminComponent,
  children: [
    {path: '', data: {breadcrumb: 'Admin'}, canActivate: [AuthGuard], component: DashboardComponent},
    {path: 'dashboard', data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT}, canActivate: [AuthGuard], component: DashboardComponent},
    {path: 'course', data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE}, canActivate: [AuthGuard], component: CourseComponent},
    {
      path: 'course/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE},
      canActivate: [AuthGuard],
      component: CourseDetailComponent
    },
    {
      path: 'course/content',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE},
      canActivate: [AuthGuard],
      component: CourseContentComponent
    },
    {
      path: 'course-clone',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE},
      canActivate: [AuthGuard],
      component: CourseCloneComponent
    },
    {
      path: 'course/content/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE},
      canActivate: [AuthGuard],
      component: CourseContentComponent
    },
    {
      path: 'new-course',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE},
      canActivate: [AuthGuard],
      component: CourseCreateComponent
    },
    {
      path: 'department',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.DEPARTMENT},
      canActivate: [AuthGuard],
      component: DepartmentComponent
    },
    {
      path: 'training-plan',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.DEPARTMENT},
      canActivate: [AuthGuard],
      component: TrainingPlanComponent
    },
    {
      path: 'training-plan-detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.DEPARTMENT},
      canActivate: [AuthGuard],
      component: TrainingDetailComponent
    },
    {
      path: 'training-plan-create',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.DEPARTMENT},
      canActivate: [AuthGuard],
      component: TrainingDetailComponent
    },
    {
      path: 'training-plan-createNB',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.DEPARTMENT},
      canActivate: [AuthGuard],
      component: TrainingDetailComponent
    },
    {
      path: 'training-plan-department',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.DEPARTMENT},
      canActivate: [AuthGuard],
      component: TrainingPlanDepartment
    },
    {
      path: 'training-plan-of-depart/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.DEPARTMENT},
      canActivate: [AuthGuard],
      component: TrainingPlanDepartment
    },
    {
      path: 'course-recommended',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE},
      canActivate: [AuthGuard],
      component: CourseRecommendComponent
    },
    {
      path: 'course-category',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE_CATEGORY},
      canActivate: [AuthGuard],
      component: CourseCategoryComponent
    },
    {
      path: 'position',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.POSITION},
      canActivate: [AuthGuard],
      component: PositionComponent
    },
    {path: 'user', data: {breadcrumb: 'Admin', authorities: USER_MENU.USER}, canActivate: [AuthGuard], component: UserComponent},
    {path: 'manage-approver', data: {breadcrumb: 'Admin', authorities: USER_MENU.MANAGE_APPROVER}, canActivate: [AuthGuard], component: ManageApproverComponent},
    {path: 'staff-assessment', data: {breadcrumb: 'Admin', authorities: USER_MENU.USER}, canActivate: [AuthGuard], component: StaffAssessmentComponent},
    {path: 'exam', data: {breadcrumb: 'Admin', authorities: USER_MENU.EXAM}, canActivate: [AuthGuard], component: ExamComponent},
    {
      path: 'create-exam',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.EXAM},
      canActivate: [AuthGuard],
      component: CreateExamComponent
    },
    {
      path: 'exam/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.EXAM},
      canActivate: [AuthGuard],
      component: ExamDetailComponent
    },
    {path: 'survey', data: {breadcrumb: 'Admin', authorities: USER_MENU.SURVEY}, canActivate: [AuthGuard], component: SurveyComponent},
    {
      path: 'create/survey',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.SURVEY},
      canActivate: [AuthGuard],
      component: CreateSurveyComponent
    },
    {
      path: 'survey/detail',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.SURVEY},
      canActivate: [AuthGuard],
      component: SurveyDetailComponent
    },
    {
      path: 'question-bank',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.QUESTION_BANK},
      canActivate: [AuthGuard],
      component: QuestionBankComponent
    },
    {
      path: 'question-bank/create',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.QUESTION_BANK},
      canActivate: [AuthGuard],
      component: QuestionCreateComponent
    },
    {
      path: 'course-question-bank/create/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.QUESTION_BANK},
      canActivate: [AuthGuard],
      component: CourseQuestionCreateComponent
    },
    {
      path: 'course-question-bank/category/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.QUESTION_BANK},
      canActivate: [AuthGuard],
      component: CourseQuestionCategoryComponent
    },
    {
      path: 'question-bank/category',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.QUESTION_BANK},
      canActivate: [AuthGuard],
      component: QuestionCategoryComponent
    },

    // {path: 'update-survey/:id', data: {breadcrumb: 'Admin'}, canActivate: [AuthGuard], component: CreateSurveyComponent}
    {
      path: 'update-survey/:id/:id2',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.SURVEY},
      canActivate: [AuthGuard],
      component: CreateSurveyComponent
    },
    {
      path: 'survey/detail/:id/:id2',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.SURVEY},
      canActivate: [AuthGuard],
      component: SurveyDetailComponent
    },
    {
      path: 'survey/single-detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.SURVEY},
      canActivate: [AuthGuard],
      component: SurveySingleDetailComponent
    },
    {path: 'badge', data: {breadcrumb: 'Admin', authorities: USER_MENU.BADGE}, canActivate: [AuthGuard], component: BadgeComponent},
    {
      path: 'badge-create',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.BADGE},
      canActivate: [AuthGuard],
      component: BadgeCreateComponent
    },
    {
      path: 'badge/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.BADGE},
      canActivate: [AuthGuard],
      component: BadgeDetailsComponent
    },
    {
      path: 'notification',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.NOTIFICATION},
      canActivate: [AuthGuard],
      component: NotificationComponent
    },
    {
      path: 'notification/details',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.NOTIFICATION},
      canActivate: [AuthGuard],
      component: CreateNotificationComponent
    },
    {path: 'role', data: {breadcrumb: 'Admin', authorities: USER_MENU.ROLE}, canActivate: [AuthGuard], component: RoleComponent},
    {path: 'report', data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT}, canActivate: [AuthGuard], component: ReportComponent},
    {
      path: 'report-course',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportCourseComponent
    },
    {
      path: 'notification/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.NOTIFICATION},
      canActivate: [AuthGuard],
      component: CreateNotificationComponent
    },
    {
      path: 'report-student',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportStudentComponent
    },
    {
      path: 'result-exam',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ResultExamComponent
    },
    {
      path: 'result-exam/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ResultDetailExamComponent
    },
    {
      path: 'report-student/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: StudentDetailsComponent
    },
    {
      path: 'teacher/detail-teacher/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: DetailTeacherComponent
    },
    {
      path: 'badge-external',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.BADGE},
      canActivate: [AuthGuard],
      component: BadgeExternalComponent
    },
    {
      path: 'badge-external/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.BADGE},
      canActivate: [AuthGuard],
      component: BadgeExternalDetailsComponent
    },
    {
      path: 'badge-offline/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.BADGE},
      canActivate: [AuthGuard],
      component: BadgeOfflineStudentComponent
    },
    {
      path: 'badge-categories',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.BADGE},
      canActivate: [AuthGuard],
      component: BadgeCategoryComponent
    },
    {
      path: 'badge-level',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE_CATEGORY},
      canActivate: [AuthGuard],
      component: BadgeLevelComponent
    },

    {
      path: 'badge-level/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE_CATEGORY},
      canActivate: [AuthGuard],
      component: BadgeLevelDetailComponent
    },

    {
      path: 'badge-level/create',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE_CATEGORY},
      canActivate: [AuthGuard],
      component: BadgeLevelDetailComponent
    },
    {
      path: 'report-teacher',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportTeacherComponent
    },
    {
      path: 'report-student-data',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportStudentDataComponent
    },
    {
      path: 'report-teacher/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: TeacherDetailsComponent
    },
    {
      path: 'report-teacher-detail',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE},
      canActivate: [AuthGuard],
      component: TeacherDetailsComponent
    },
    {
      path: 'laudatory',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: LaudatoryComponent
    },
    {
      path: 'roll-call',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.NOTIFICATION},
      canActivate: [AuthGuard],
      component: RollCallComponent
    },
    {
      path: 'roll-call-detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.NOTIFICATION},
      canActivate: [AuthGuard],
      component: RollCallDetailComponent
    },
    {
      path: 'criteria',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: CriteriaComponent
    },
    {
      path: 'report-kpi',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportKpiComponent
    },
    {
      path: 'teacher',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE},
      canActivate: [AuthGuard],
      component: TeacherComponent
    },
    {
      path: 'teacher/detail-teacher',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE},
      canActivate: [AuthGuard],
      component: DetailTeacherComponent
    },
    {
      path: 'report-contract',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportContractComponent
    },
    {
      path: 'report-teacher-internal',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.COURSE},
      canActivate: [AuthGuard],
      component: ReportTeacherInternalComponent
    },
    {
      path: 'report-training-plan',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportTrainingPlanComponent
    },
    {
      path: 'report-training',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportTrainingComponent
    },
    {
      path: 'report-contract/detail/:id',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportDetailComponent
    },
    {
      path: 'report-training-source',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportTrainingSourceComponent
    },
    {
      path: 'report-learning-data',
      data: {breadcrumb: 'Admin', authorities: USER_MENU.REPORT},
      canActivate: [AuthGuard],
      component: ReportLearningDataComponent
    },
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule {
}
