import {Injectable} from '@angular/core';
import {AbstractService} from './abstract.service';
import {MOODLE_INFO, USER_INFO} from '../constants/base.constant';
import {LocalStorageService} from 'ngx-webstorage';
import CommonUtil from '../utils/common-util';
import {environment} from '../../../environments/environment';
import { HttpResponse } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { Observable, Observer } from "rxjs";
@Injectable({providedIn: 'root'})
export class FileService {
  constructor(
    private abstractService: AbstractService,
    private $localStorage: LocalStorageService,
  ) {
  }

  getFileUrl(contextid, filename, modulename) {
    const token = this.$localStorage.retrieve(USER_INFO.TOKEN);
    return MOODLE_INFO.FILE_URL + contextid + modulename + CommonUtil.encodeURI(filename) + '?token=' + token;
  }

  badgeFileUrl(contextid, filename, modulename, courseContextId) {
    var type = filename.split('.')[1];
    let baseUrl = MOODLE_INFO.BADGE_FILE_URL;
    const token = this.$localStorage.retrieve(USER_INFO.TOKEN);
    return baseUrl + courseContextId + contextid + modulename + CommonUtil.encodeURI(filename) + '?token=' + token;
  }

  // Luu y: filepath do backend tra ve phai khong co environment.apiUrl
  getFileFromPathUrl(filepath) {
    const token = this.$localStorage.retrieve(USER_INFO.TOKEN);
    return environment.apiUrl + filepath + '?token=' + token;
  }

  getScormUrl(fileid, filename, modulename) {
    return MOODLE_INFO.SCORM_FILE_URL + fileid + modulename + CommonUtil.encodeURI(filename);
  }

  getFileUrlWithRevision(contextid, filename, modulename, revisionid){
    const token = this.$localStorage.retrieve(USER_INFO.TOKEN);
    return MOODLE_INFO.FILE_URL + contextid + modulename + CommonUtil.encodeURI(filename) + '?rev=' + revisionid + '&token=' + token;
  }
  
  getBase64ImageFromURL(url: string) {
    return Observable.create((observer: Observer<string>) => {
      const img: HTMLImageElement = new Image();
      img.crossOrigin = "Anonymous";
      img.src = url;
      if (!img.complete) {
        img.onload = () => {
          observer.next(this.getBase64Image(img));
          observer.complete();
        };
        img.onerror = err => {
          observer.error(err);
        };
      } else {
        observer.next(this.getBase64Image(img));
        observer.complete();
      }
    });
  }
  getBase64Image(img: HTMLImageElement) {
    const canvas: HTMLCanvasElement = document.createElement("canvas");
    canvas.width = img.width;
    canvas.height = img.height;
    const ctx: CanvasRenderingContext2D = canvas.getContext("2d");
    ctx.drawImage(img, 0, 0);
    const dataURL: string = canvas.toDataURL("image/png");
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, "");
  }
}
