import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {NgbActiveModal, NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';


@Component({
  selector: 'app-delete-badge-external-popup',
  templateUrl: './delete-badge-external-popup.component.html',
  providers: [NgbModal, NgbModalConfig]
})
export class DeleteBadgeExternalPopupComponent implements OnInit {

  @Input() title: string;
  @Input() body: string;
  @Input() confirmButton: string;
  @Output() onChecked: EventEmitter<any> = new EventEmitter<any>();
  isChecked : boolean =false;
  constructor(
    public activeModal: NgbActiveModal,
  ) {
  }

  ngOnInit(): void {
  }

  changeValueChecked(checked:any){
    this.onChecked.emit(checked);
  }
 
}
