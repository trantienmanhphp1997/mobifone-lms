export interface ModuleModel{
  id: number;
  course: number;
  module: number;
  name: string;
  modname: string;
  instance: number;
  intro: string;
  content: string;
  contextid: number;
  filename: string;
  filearea: string;
  timelimit: number;
}


