import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { ReportService } from 'src/app/shared/services/report.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';


@Component({
  selector: 'app-learning-data-detail',
  templateUrl: './learning-data-detail.component.html',
  styleUrls: ['./learning-data-detail.component.css']
})
export class LearningDataDetailComponent implements OnInit {
  @Input() itemId: any;
  @Input() date: any

  listCourseData: any = [];
  totalRecord: any = 0

  constructor(
    private reportService: ReportService,
    private spinner: NgxSpinnerService,
    public activeModal: NgbActiveModal,
    private toastrService: ToastrCustomService,
  ) { }

  ngOnInit(): void {
    this.getData();
  }

  getData() {
    if (this.itemId) {
      const param = {
        studentid: this.itemId,
        startdate: this.date?.startdate,
        enddate: this.date?.enddate,
      }
      this.spinner.show();
      this.reportService.getReportLearningDataDetail(param).subscribe(data => {
        this.listCourseData = data.body.results;
        this.totalRecord = data.body.results;
        if (this.totalRecord) {
          this.listCourseData.forEach(item => {
            switch (item.status) {
              case 'Đã kết thúc':
                item.style = 'success'
                break;
              case 'Chưa bắt đầu':
                item.style = 'secondary'
                break;
              case 'Đang diễn ra':
                item.style = 'info'
                break;
            }
          });
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      });
    }
  }
}
