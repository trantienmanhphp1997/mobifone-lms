import {HttpResponse} from '@angular/common/http';
import {Observable} from 'rxjs';
import {BadgeCategory} from '../model/badge-category.model';
import {MOODLE_INFO} from '../constants/base.constant';
import CommonUtil from '../utils/common-util';
import {map} from 'rxjs/operators';
import {AbstractService} from './abstract.service';
import {Injectable} from '@angular/core';

type EntityResponseType = HttpResponse<BadgeCategory>;
type EntityArrayResponseType = HttpResponse<BadgeCategory[]>;

@Injectable({providedIn: 'root'})
export class BadgeCategoryService {

    constructor(private abstractService: AbstractService) {

    }

    createBadgeCategory(badgeCategory: BadgeCategory): Observable<EntityResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_badge_create_categories_badge');
        const categoryArray: BadgeCategory[] = [];
        categoryArray.push(badgeCategory);
        CommonUtil.appendDataToFormData(data, 'categories', categoryArray);
        return this.abstractService.post<BadgeCategory>(MOODLE_INFO.SERVICE_URL, data)
        .pipe(map((res: EntityResponseType) => res));
    }

    updateBadgeCategory(badgeCategory: BadgeCategory): Observable<HttpResponse<null>> {
        const data = this.abstractService.getCommonInput('mobifone_badge_update_categories_badge');
        CommonUtil.appendDataToFormData(data, 'categories', badgeCategory);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
        .pipe(map((res: HttpResponse<null>) => res));
    }

    deleteBadgeCate(courseIds: any, skip?:number): Observable<any> {
        let strid = String(courseIds);
        let deleteIds = {
            ids: strid,
            skip: skip
        };
        const dataCourse = this.abstractService.getCommonInput('mobifone_badge_delete_categories_badge');
        CommonUtil.appendDataToFormData(dataCourse, 'params', deleteIds);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, dataCourse).pipe(map((res: any) => res));
    }

    searchBadgeCategory(category: any): Observable<any> {
        const searchData = this.abstractService.getCommonInput('mobifone_badge_get_categories_badge');
        CommonUtil.appendDataToFormData(searchData, 'params', category);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, searchData).pipe(map((res: any) => res));
    }

    getBadgeCategoryTree(params): Observable<EntityArrayResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_badge_get_all_categories_tree_badge');
        CommonUtil.appendDataToFormData(data, 'id', params);
        return this.abstractService.post<any[]>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: EntityArrayResponseType) => res));
    }
    getBadge(params): Observable<any> {
        const data = this.abstractService.getCommonInput('mobifone_badge_list_badge');
        CommonUtil.appendDataToFormData(data, 'params', params);
        return this.abstractService.post<any[]>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: any) => res));
    }
    getLevel(params): Observable<any> {
        const data = this.abstractService.getCommonInput('mobifone_badge_get_level_badge');
        CommonUtil.appendDataToFormData(data, 'params', params);
        return this.abstractService.post<any[]>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: any) => res));
    }
    getListLevel(params): Observable<any> {
        const data = this.abstractService.getCommonInput('mobifone_badge_get_level_badge');
        CommonUtil.appendDataToFormData(data, 'params', params);
        return this.abstractService.post<any[]>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: any) => res));
    }
}
