import {HttpResponse} from '@angular/common/http';
import {UserInfo} from '../model/user-info.model';
import {AbstractService} from './abstract.service';
import {Observable} from 'rxjs';
import CommonUtil from '../utils/common-util';
import {MOODLE_INFO} from '../constants/base.constant';
import {map} from 'rxjs/operators';
import {Injectable} from '@angular/core';
import {ModuleInfo} from '../model/moduleinfo.model';
import {User} from "../model/user.model";

type EntityResponseType = HttpResponse<UserInfo>;
type EntityArrayResponseType = HttpResponse<UserInfo[]>;

interface UserPageResponse {
  results: UserInfo[];
  total: number;
}

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private abstractService: AbstractService) {

  }

  searchUser(param):
    Observable<HttpResponse<UserPageResponse>> {
    const data = this.abstractService.getCommonInput('mobifone_user_list_user');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<UserPageResponse>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<UserPageResponse>) => res));
  }
  listTeacher(param):
    Observable<HttpResponse<UserPageResponse>> {
    const data = this.abstractService.getCommonInput('mobifone_user_list_teacher');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<UserPageResponse>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<UserPageResponse>) => res));
  }
  userUpdateTeacher(user):
    Observable<HttpResponse<UserPageResponse>> {
    const data = this.abstractService.getCommonInput('mobifone_user_update_teacher');
    CommonUtil.appendDataToFormData(data, 'user', user);
    return this.abstractService.post<UserPageResponse>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<UserPageResponse>) => res));
  }
  deleteUser(idUser: number[]): Observable<EntityArrayResponseType> {
    const data = this.abstractService.getCommonInput('core_user_delete_users');
    CommonUtil.appendDataToFormData(data, 'userids', idUser);
    return this.abstractService.post<UserInfo[]>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: EntityArrayResponseType) => res));
  }

  createUser(newUser: UserInfo): Observable<EntityResponseType> {
    const data = this.abstractService.getCommonInput('mobifone_user_create_user');
    CommonUtil.appendDataToFormData(data, 'user', newUser);
    return this.abstractService.post<UserInfo>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: EntityResponseType) => res));
  }

  
  getUserListUser(param): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_list_exam_result');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<any>) => res));
  }

  getUserListQuizUserList(param): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_quiz_list_user_result');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<any>) => res));
  }

  updateUser(userUpdate: any): Observable<EntityResponseType> {
    const data = this.abstractService.getCommonInput('mobifone_user_update_user');
    CommonUtil.appendDataToFormData(data, 'user', userUpdate);
    return this.abstractService.post<UserInfo>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: EntityResponseType) => res));
  }

  downloadTemplateUserFile(): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_user_generate_user_template');
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: any) => res));
  }

  importUserFormFile(file: File): Observable<EntityResponseType> {
    const data = this.abstractService.getCommonInput('mobifone_user_import_user');
    data.append('file', file, file.name);
    CommonUtil.appendDataToFormData(data, 'file', file);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: EntityResponseType) => res));
  }

  findUserByUsername(username: string): Observable<HttpResponse<boolean>> {
    const data = this.abstractService.getCommonInput('core_auth_request_password_reset');
    CommonUtil.appendDataToFormData(data, 'username', username);
    return this.abstractService.post<boolean>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<boolean>) => res));
  }

  findUserByEmail(email: string): Observable<HttpResponse<boolean>> {
    const data = this.abstractService.getCommonInput('core_auth_request_password_reset');
    CommonUtil.appendDataToFormData(data, 'email', email);
    return this.abstractService.post<boolean>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<boolean>) => res));
  }

  changeOwnPassword(params: any): Observable<any>{
    const data = this.abstractService.getCommonInput('mobifone_user_change_password');
    CommonUtil.appendRawDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: any) => res));
  }

  updateOwnDetail(file: File): Observable<EntityResponseType> {
    const data = this.abstractService.getCommonInput('mobifone_user_update_own_detail');
    data.append('repo_upload_file', file, file.name);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: any) => res));
  }

  getUserInfo(): Observable<EntityResponseType> {
    const data = this.abstractService.getCommonInput('mobifone_user_get_own_detail');
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: any) => res));
  }

  lockUser(idUser: number[]): Observable<EntityArrayResponseType> {
    const data = this.abstractService.getCommonInput('mobifone_user_lock_user');
    CommonUtil.appendDataToFormData(data, 'userids', idUser);
    return this.abstractService.post<UserInfo[]>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: EntityArrayResponseType) => res));
  }
  lockEvaluator(params: any): Observable<EntityArrayResponseType> {
    const data = this.abstractService.getCommonInput('mobifone_user_assign_role_manager');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<UserInfo[]>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: EntityArrayResponseType) => res));
  }


  unlockUser(idUser: number[]): Observable<EntityArrayResponseType> {
    const data = this.abstractService.getCommonInput('mobifone_user_unlock_user');
    CommonUtil.appendDataToFormData(data, 'userids', idUser);
    return this.abstractService.post<UserInfo[]>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: EntityArrayResponseType) => res));
  }

  getUserAchievement(param: any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_course_get_user_achievement');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: any) => res));
  }

  getListManagerDepartment(param?: any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_user_list_user_manager');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: any) => res));
  }
  updateManager(userUpdate: any): Observable<EntityResponseType> {
    const data = this.abstractService.getCommonInput('mobifone_user_save_evaluated_user');
    CommonUtil.appendDataToFormData(data, 'param', userUpdate);
    return this.abstractService.post<UserInfo>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: EntityResponseType) => res));
  }
  getUserPoint(): Observable<EntityResponseType> {
    const data = this.abstractService.getCommonInput('mobifone_user_get_point');
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: any) => res));
  }
  getHistoryChange(param?: any):
  Observable<HttpResponse<UserPageResponse>> {
  const data = this.abstractService.getCommonInput('mobifone_user_get_history_change');
  CommonUtil.appendDataToFormData(data, 'params', param);
  return this.abstractService.post<UserPageResponse>(MOODLE_INFO.SERVICE_URL, data)
    .pipe(map((res: HttpResponse<UserPageResponse>) => res));
}
}
