import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ReportTrainingSourceComponent } from './report-training-source.component';

describe('ReportTrainingSourceComponent', () => {
  let component: ReportTrainingSourceComponent;
  let fixture: ComponentFixture<ReportTrainingSourceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ReportTrainingSourceComponent ]
    })
    .compileComponents();
  }));

  

  beforeEach(() => {
    fixture = TestBed.createComponent(ReportTrainingSourceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
