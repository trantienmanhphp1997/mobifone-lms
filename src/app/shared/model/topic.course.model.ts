export interface TopicCourseModel {
  hiddenbynumsections?: number;
  id: number;
  name?: string;
  section?: number;
  summary?: string;
  summaryformat?: number;
  uservisible?: boolean;
  visible?: number;
  modname?: string;
  checked?: boolean;
  checkedMany?: boolean;
  modules?: [Module];
  ispassexam?:number;
}

export interface Module {
  ordernumber: number;
  modname: string;
  instance: number;
  id: number;
  name: any;
  mimetype?: string;
  selected?: boolean;
  createduser?: string;
  createdat?: number;
}
