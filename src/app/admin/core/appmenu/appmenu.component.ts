import {Component, Directive, Input, OnInit} from '@angular/core';
import {LocalStorageService, SessionStorageService} from 'ngx-webstorage';
import {PLAN_ROLE, USER_INFO, USER_MENU} from '../../../shared/constants/base.constant';

@Component({
  selector: 'app-appmenu',
  templateUrl: './appmenu.component.html',
  styleUrls: ['./appmenu.component.css']
})

export class AppmenuComponent implements OnInit {
  isLoggedIn: boolean;
  menus: any;
  roles: any;

  constructor(private $localStorage: LocalStorageService,) {
  }

  check: boolean;
  checkCourse: boolean;
  checkDepartment: boolean;
  checkReport: boolean;
  course: boolean;
  courseCategory: boolean;
  exam: boolean;
  badge: boolean;
  badgeCategory: boolean;
  badgeLevel: boolean;
  survey: boolean;
  questionBank: boolean;
  department: boolean;
  position: boolean;
  user: boolean;
  notification: boolean;
  role: boolean;
  report: boolean;
  badgeExternal: boolean;
  userInfo: any;
  manageapprover: boolean;
  planRole: boolean;
  staffAssessment: boolean;
  planLink: string;
  reportTeacher: boolean;
  teacher: boolean;
  isTeacher: boolean;
  rollcall: boolean;
  reportcontract: boolean;
  criteria: boolean;
  trainingplan: boolean;
  laudatory: boolean;
  ngOnInit(): void {
    this.userInfo = this.$localStorage.retrieve(USER_INFO.INFO);
    this.checkCourse = true;
    this.checkDepartment = true;
    this.checkReport = true;
    this.check = false;
    this.staffAssessment = false;
    this.planRole = false;
    this.menus = this.$localStorage.retrieve('menus');
    // this.planRole = this.userInfo && [PLAN_ROLE.CREATE, PLAN_ROLE.REVIEW].includes(this.userInfo.planrole)
    this.planLink  = this.userInfo.planrole !== null ? '/admin/training-plan-department' : '/admin/training-plan'; 
    this.reportTeacher = false;
    this.roles = this.$localStorage.retrieve('roles');
    if (this.roles !== undefined) {
      for (let j = 0; j < this.roles.length; j++) {
        if (this.roles[j].shortname === 'admin') {
          this.check = true;
          this.planRole = true;
          this.staffAssessment = false;
        }
        if (this.roles[j].shortname === 'manager') {
          this.staffAssessment = true;
          this.planRole = true;
        }
        if (this.roles[j].shortname === 'teacher') {
          this.isTeacher = true;
        }
      }
    }
    if (this.check) {
      this.course = true;
      this.courseCategory = true;
      this.exam = true;
      this.badge = true;
      this.badgeExternal = true;
      this.badgeCategory = true;
      this.badgeLevel = true;
      this.survey = true;
      this.questionBank = true;
      this.department = true;
      this.position = true;
      this.user = true;
      this.notification = true;
      this.role = true;
      this.report = true;
      this.manageapprover = true;
      this.teacher = true;
      this.reportTeacher = true;
      this.rollcall = true;
      this.reportcontract = true;
      this.criteria = true;
      this.trainingplan = true;
      this.laudatory = true
    } else {
      this.course = false;
      this.courseCategory = false;
      this.exam = false;
      this.badge = false;
      this.badgeExternal = false;
      this.badgeCategory = false;
      this.badgeLevel = false;
      this.survey = false;
      this.questionBank = false;
      this.department = false;
      this.position = false;
      this.user = false;
      this.notification = false;
      this.role = false;
      this.report = false;
      this.manageapprover = false;
      this.teacher = false;
      this.reportTeacher = false;
      this.rollcall = false;
      this.reportcontract = false;
      this.criteria = false;
      this.trainingplan = false;
      this.laudatory = false
    }
    if (!this.check) {
      for (let i = 0; i < this.menus.length; i++) {
        if (this.menus[i].code === USER_MENU.POSITION) {
          this.position = true;
        }
        if (this.menus[i].code === USER_MENU.COURSE) {
          this.course = true;
        }
        if (this.menus[i].code === USER_MENU.COURSE_CATEGORY) {
          this.courseCategory = true;
        }
        if (this.menus[i].code === USER_MENU.EXAM) {
          this.exam = true;
        }
        if (this.menus[i].code === USER_MENU.BADGE) {
          this.badge = true;
        }
        if(this.menus[i].code === USER_MENU.BADGE_CATEGORY){
          this.badgeCategory = true;
        }
        if(this.menus[i].code == USER_MENU.BADGE_LEVEL){
          this.badgeLevel = true;
        }
        if (this.menus[i].code === USER_MENU.SURVEY) {
          this.survey = true;
        }
        if (this.menus[i].code === USER_MENU.QUESTION_BANK) {
          this.questionBank = true;
        }
        if (this.menus[i].code === USER_MENU.DEPARTMENT) {
          this.department = true;
        }
        if (this.menus[i].code === USER_MENU.USER) {
          this.user = true;
        }
        if (this.menus[i].code === USER_MENU.NOTIFICATION) {
          this.notification = true;
        }
        if (this.menus[i].code === USER_MENU.ROLE) {
          this.role = true;
        }
        if (this.menus[i].code === USER_MENU.REPORT) {
          this.report = true;
        }
        if(this.menus[i].code === USER_MENU.BADGE_EXTERNAL){
          this.badgeExternal = true;
        }
        if(this.menus[i].code === USER_MENU.MANAGE_APPROVER){
          this.manageapprover = true;
        }
        if(this.menus[i].code === USER_MENU.TEACHER){
          this.teacher = true;
        }
        if(this.menus[i].code === USER_MENU.REPORT_TEACHER){
          this.reportTeacher = true;
        }
        if(this.menus[i].code === USER_MENU.ROLL_CALL){
          this.rollcall = true;
        }
        if(this.menus[i].code === USER_MENU.REPORT_CONTRACT){
          this.reportcontract = true;
        }
        if(this.menus[i].code === USER_MENU.CRITERIA){
          this.criteria = true;
        }
        if(this.menus[i].code === USER_MENU.TRAINING_PLAN){
          this.trainingplan = true;
        }
        if(this.menus[i].code === USER_MENU.LAUDATORY){
          this.laudatory = true;
        }
      }
    }
    if (!this.course && !this.courseCategory && !this.exam && !this.badge && !this.survey && !this.questionBank) {
      this.checkCourse = false;
    }
    if (!this.department && !this.position && !this.user) {
      this.checkDepartment = false;
    }
    if (!this.notification && !this.role) {
      this.checkDepartment = false;
    }
  }

}
