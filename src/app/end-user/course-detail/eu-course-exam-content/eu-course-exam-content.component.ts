import {TranslateService} from '@ngx-translate/core';
import {NgxSpinnerService} from 'ngx-spinner';
import {Component, Input, OnChanges, OnInit, SimpleChanges} from '@angular/core';
import {ToastrCustomService} from '../../../shared/services/toastr-custom.service';
import {COMPLETION_STATE, GRADE_METHOD} from '../../../shared/constants/base.constant';
import {LocalStorageService} from 'ngx-webstorage';
import * as moment from 'moment';
import {CourseService} from '../../../shared/services/course.service';
import { TopicService } from 'src/app/shared/services/topic.service';

@Component({
  selector: 'app-eu-course-exam-content',
  templateUrl: './eu-course-exam-content.component.html',
  styleUrls: ['./eu-course-exam-content.component.css']
})
export class EuCourseExamContentComponent implements OnInit, OnChanges {

  @Input() courseId: number;
  @Input() course: any;

  currentDate: Date = new Date();


  listExam: any = [];
  numberOfTopic = 0;
  numberOfTopicPassed = 0;
  numberOfTopicFailed = 0;
  numberOfTopicNotTaken = 0;

  numberOfExam = 0;
  numberOfExamPassed = 0;
  numberOfExamFailed = 0;
  numberOfExamNotTaken = 0;
  isTeacher = false;
  listQuiz: any = [];

  items: number[] = [1, 2, 3, 4, 5, 6];
  activeTopic: number = -1;
  disableExamObject: any = {};

  hiddenTopicStatistic = true;
  hiddenLastStatistic = true;
  COMPLETION_STATE = COMPLETION_STATE;
  GRADE_METHOD = GRADE_METHOD;

  constructor(
    private translateService: TranslateService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    private courseService: CourseService,
    private topicService: TopicService,
    private $localStorage: LocalStorageService,
  ) {
  }

  ngOnInit(): void {
    this.getListExamEu();
  }
  ngOnChanges(changes: SimpleChanges): void {
    if (this.course) {
      this.isTeacher = this.course.roleincourse === 'teacher';
    }
  }

  getListExamEu() {
    return this.courseService.getListExam(this.courseId, 1).subscribe((data) => {
      this.listExam = data.body;
      if (this.listExam[0]) {
        const examFirst = this.listExam[0];
        this.activeTopic = examFirst.topicid;
        this.listQuiz = this.listExam[0]?.listquiz;
        this.hiddenTopicStatistic = false;
        this.hiddenLastStatistic = true;
      } else {
        this.listQuiz = [];
      }
      this.numberOfTopic = this.listExam.length;
      this.listExam.forEach((element) => {
        if (element.listquiz.length > 0) {
          this.numberOfTopicNotTaken++;
        } else if (element.ispassexam === 1) {
          this.numberOfTopicPassed++;
        } else {
          this.numberOfTopicFailed++;
        }
      });
    });
  }

  localStore(name , overduehandling, disableQuiz: any) {
    this.$localStorage.store('quizName', name);
    this.$localStorage.store('overduehandling', overduehandling);
    console.log(disableQuiz);
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  onClickTopic(item: number, index: number) {
    this.activeTopic = item;
    const exam = this.listExam.find(
      i => i.topicid === this.activeTopic
    );

    if(index != 0 && this.listExam[index-1].ispassexam == 0)
    {
      this.disableExamObject = true;
    }
    else{
      this.disableExamObject = false;
    }
    this.listQuiz = exam.listquiz;
    this.listQuiz['disableQuiz'] = this.disableExamObject;
    console.log(this.listQuiz);
    
  }

  onChangeExamByTopic() {
    this.spinner.show()
    this.courseService.getListExam(this.courseId, 1).subscribe((data) => {
      this.listExam = data.body;
      if(this.listExam[0]) {
        const examFirst = this.listExam[0];
        this.activeTopic = examFirst.topicid;
        this.listQuiz = this.listExam[0]? this.listExam[0]?.listquiz : [];
      } else {
        this.listQuiz = [];
      }
      this.numberOfTopic = this.numberOfTopicFailed = this.numberOfTopicNotTaken = this.numberOfTopicPassed = 0;
      this.numberOfTopic = this.listExam.length;
      this.listExam.forEach((element) => {
        if(!element.quizattempts) {
          this.numberOfTopicNotTaken++;
        } else if(element.ispassexam === 1) {
          this.numberOfTopicPassed++;
        } else {
          this.numberOfTopicFailed++;
        }
      })
      this.hiddenLastStatistic = true;
      this.hiddenTopicStatistic = false;
      this.spinner.hide()
    })
  }

  onChangeExamLast() {
    this.spinner.show();
    this.courseService.getListExam(this.courseId, 0).subscribe((data) => {
      this.listQuiz = data.body[0] ? data.body[0].listquiz : [];
      const dataExam = data.body;
      console.log(this.listQuiz);
      this.numberOfExam = this.numberOfExamFailed = this.numberOfExamNotTaken = this.numberOfExamPassed = 0;
      this.numberOfExam = dataExam[0].listquiz.length;
      dataExam.forEach(element => {
        element.sumgrades = element.sumgrades ? Math.round(element.sumgrades * 100) / 100 : 0;
        element.gradequiz = element.gradequiz ? Math.ceil(element.gradequiz) : 0;
        if (element.grademethod === GRADE_METHOD.QUIZ_GRADEHIGHEST) {
          element.grademethodName = GRADE_METHOD.QUIZ_GRADEHIGHEST_DESCRIPTION;
        } else if (element.grademethod === GRADE_METHOD.QUIZ_GRADEAVERAGE) {
          element.grademethodName = GRADE_METHOD.QUIZ_GRADEAVERAGE_DESCRIPTION;
        } else if (element.grademethod === GRADE_METHOD.QUIZ_ATTEMPTFIRST) {
          element.grademethodName = GRADE_METHOD.QUIZ_ATTEMPTFIRST_DESCRIPTION;
        } else if (element.grademethod === GRADE_METHOD.QUIZ_ATTEMPTLAST) {
          element.grademethodName = GRADE_METHOD.QUIZ_ATTEMPTLAST_DESCRIPTION;
        }
        if (element.completionstate === COMPLETION_STATE.YET_EXAM) {
          this.numberOfExamNotTaken++;
          element.completionStateName = COMPLETION_STATE.YET_EXAM_DESCRIPTION;
        } else if (element.completionstate === COMPLETION_STATE.PASS_EXAM && element.isshowgrade === 1) {
          this.numberOfExamPassed++;
          element.completionStateName = COMPLETION_STATE.PASS_EXAM_DESCRIPTION;
        } else if ((element.completionstate === COMPLETION_STATE.FAIL_EXAM || element.completionstate === COMPLETION_STATE.USED_EXAM) && element.isshowgrade === 1) {
          this.numberOfExamFailed++;
          element.completionStateName = COMPLETION_STATE.FAIL_EXAM_DESCRIPTION;
        }
      });
      this.hiddenLastStatistic = false;
      this.hiddenTopicStatistic = true;
      this.spinner.hide()
    })
  }

  getCompletionStateName(state: number): string {
    if(state === COMPLETION_STATE.YET_EXAM) {
      return COMPLETION_STATE.YET_EXAM_DESCRIPTION
    } else if(state === COMPLETION_STATE.PASS_EXAM) {
      return COMPLETION_STATE.PASS_EXAM_DESCRIPTION
    } else if(state === COMPLETION_STATE.FAIL_EXAM) {
      return COMPLETION_STATE.FAIL_EXAM_DESCRIPTION
    } else {
      return COMPLETION_STATE.USED_EXAM_DESCRIPTION
    }
  }

  getGradeMethodName(state: number): string {
    if(state === GRADE_METHOD.QUIZ_GRADEHIGHEST) {
      return GRADE_METHOD.QUIZ_GRADEHIGHEST_DESCRIPTION;
    } else if(state === GRADE_METHOD.QUIZ_GRADEAVERAGE) {
      return GRADE_METHOD.QUIZ_GRADEAVERAGE_DESCRIPTION;
    } else if(state === GRADE_METHOD.QUIZ_ATTEMPTFIRST) {
      return GRADE_METHOD.QUIZ_ATTEMPTFIRST_DESCRIPTION;
    } else if(state === GRADE_METHOD.QUIZ_ATTEMPTLAST) {
      return GRADE_METHOD.QUIZ_ATTEMPTLAST_DESCRIPTION;
    } else {
      return "";
    }
  }

}
