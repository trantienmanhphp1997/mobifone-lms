import { Component, Input, OnInit } from '@angular/core';
import { FormControl, Validators } from '@angular/forms';
import { debug } from 'console';
import { NgxSpinnerService } from 'ngx-spinner';
import {map, startWith} from 'rxjs/operators';
import { CKEDITOR_CONFIG } from 'src/app/shared/constants/ckeditor.constant';
import { CourseService } from 'src/app/shared/services/course.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { UserService } from 'src/app/shared/services/user.service';
import * as ClassicEditor from 'src/assets/ckeditor5-build-classic';
import { LocalStorageService } from 'ngx-webstorage';
import { BADGE_TYPE, COST_SOURCE, LIST_HOURS, LIST_MINUTES, PLAN_ROLE, USER_INFO, USER_ROLE } from '../../../shared/constants/base.constant';
@Component({
  selector: 'app-eu-evaluation',
  templateUrl: './eu-evaluation.component.html',
  styleUrls: ['./eu-evaluation.component.css']
})
export class EuEvaluationComponent implements OnInit {

  @Input() course :any;
  public Editor = ClassicEditor;
  contentCommitment: string = '';
  commitment: any = [];
  editorConfig = CKEDITOR_CONFIG.NOT_UPLOAD;
  constructor(private userService:UserService,
    private spinner: NgxSpinnerService,
    private toartService: ToastrCustomService,
    private courseService: CourseService,
    private $localStorage: LocalStorageService,
    ) { }
  listUserEva3M: any = [];
  managerSelected3M = new FormControl('', Validators.required);
  managerFilter3M = null;
  evaluationInfo3M: any = []
  listUserEva6M: any=[];
  managerSelected6M = new FormControl('', Validators.required);
  managerFilter6M = null;
  evaluationInfo6M:any = [];
  evaluationInfo0: any = [];
  managerFilter0 = null;
  listUserEva0: any=[];
  openTab:number;
  st3M: any;
  st6M: any;
  st0: any;
  method: string = '';
  objectives: string = '';
  timeComplete: number;
  cost: string = '';
  listCommitmentByUser:any = [];
  userLoginId: number;
  pageIndex = 1;
  pageSize = 6;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  totalCommit:number;

  ngOnInit(): void {
    this.openTab=1;
    this.userLoginId = this.$localStorage.retrieve(USER_INFO.INFO).id;
    this.listUserIsManager();
    this.getDetail();
    this.getListCommitmentActionCourseByUser();
  }
  listUserIsManager(){
    //3 month
    this.spinner.show();
    this.userService.getListManagerDepartment().subscribe(data=>{
      this.spinner.hide();
      this.listUserEva3M = data.body.results;
      this.listUserEva6M = data.body.results;
      if(this.managerFilter3M){
        this.listUserEva3M=this.listUserEva3M.filter((unit) => unit.fullname.toLowerCase().indexOf(this.managerFilter3M.toLowerCase()) >-1);
      }
      if(this.managerFilter6M){
        this.listUserEva6M=this.listUserEva6M.filter((unit) => unit.fullname.toLowerCase().indexOf(this.managerFilter6M.toLowerCase()) >-1);
      }
    },err=>{
      this.spinner.hide();
      this.toartService.handlerError(err);
    })
  }
  getDetail(){
    this.spinner.show();

    this.courseService.getDetailEvaluationEndUser(this.course.id, 0).subscribe(res=>{
      this.spinner.hide();
      this.evaluationInfo0 = res.body;
      console.log(this.evaluationInfo0)
      this.managerFilter0?.setValue(
        this.listUserEva0.find(c => c.id === this.evaluationInfo0.muserid));
      this.st0 = this.evaluationInfo0.criterias.map((c: any) => {return {criteriaid: c.id, status: c.status || 0} })
    },err=>{
      this.spinner.hide();
      this.toartService.handlerError(err);
    })


    this.courseService.getDetailEvaluationEndUser(this.course.id, 3).subscribe(res=>{
      this.spinner.hide();
      this.evaluationInfo3M = res.body;
      this.contentCommitment = this.listUserEva3M.commitmentcontent
      this.managerSelected3M?.setValue(
        this.listUserEva3M.find(c => c.id === this.evaluationInfo3M.muserid));
      this.st3M = this.evaluationInfo3M.criterias.map((c: any) => {return {criteriaid: c.id, status: c.status || 0} })
    },err=>{
      this.spinner.hide();
      this.toartService.handlerError(err);
    })
    // 6M
    this.courseService.getDetailEvaluationEndUser(this.course.id, 6).subscribe(res=>{
      this.spinner.hide();
      this.evaluationInfo6M = res.body;
      this.managerSelected6M?.setValue(
        this.listUserEva6M.find(c => c.id === this.evaluationInfo6M.muserid));
      this.st6M = this.evaluationInfo6M.criterias.map((c: any) => {return {criteriaid: c.id, status: c.status || 0} })
    },err=>{
      this.spinner.hide();
      this.toartService.handlerError(err);
    })


  }
  sendCommitment(){
    const params = {
      courseid:this.course.id,
      contentcommitment: this.contentCommitment,
      method: this.method,
      objectives: this.objectives,
      completetime: this.timeComplete,
      cost: this.cost
    }
    this.spinner.show();
      this.courseService.contentCommitment(params).subscribe( res =>{
          if(res){
            this.spinner.hide();
             this.toartService.success("Gửi cam kết thành công");
             this.getDetail();
          }
      })
  }
  sendEvaluation(type){
    this.spinner.show();
    if(type==3&&this.evaluationInfo3M.criterias.length==0){
      this.spinner.hide();
        this.toartService.error('Khóa học này chưa có tiêu chí đánh giá');
        return
    }else if(type==6 &&this.evaluationInfo6M.criterias.length==0){
      this.spinner.hide();
        this.toartService.error('Khóa học này chưa có tiêu chí đánh giá');
        return
    }
    
      if(type==3){
        if(!this.managerSelected3M.value){
          this.spinner.hide();
          this.toartService.error('Bạn chưa chọn lãnh đạo đánh giá 3 tháng');
          return
        }
      }else if(type==6){
        if(!this.managerSelected6M.value){
          this.spinner.hide();
          this.toartService.error('Bạn chưa chọn lãnh đạo đánh giá 6 tháng');
          return
        }
      }
    const params ={
      courseid: this.course.id,
      userid: type==3 ? this.managerSelected3M.value.id: this.managerSelected6M.value.id,
      type: type,
    }
    this.courseService.assingEvaluationCourse(params).subscribe(data=>{
      this.spinner.hide();
      this.getDetail();
      this.toartService.success('Gửi đánh giá thành công');
    }, err=>{
      this.spinner.hide();
      this.toartService.handlerError(err);
    })
  }
  filterListCareUnit(val, type) {
    if(type==3){
      this.managerFilter3M=val;
    }else if(type==6){
      this.managerFilter6M=val;
    }
    
    this.listUserIsManager()
  }

  getListCommitmentActionCourseByUser()
  {
    const param = {
      courseid: this.course.id,
      userid: this.userLoginId,
      page: this.pageIndex,
      limit: this.pageSize,
    };

    this.courseService.getListCommitment(param).subscribe((data) => {
      this.listCommitmentByUser = data.body;
      this.totalCommit = data.body.total;
      }, err => {
        // this.toastrService.handlerError(err);
      });
  }

  changePage($event) {
    this.pageIndex = $event.pageIndex + 1;
    this.pageSize = $event.pageSize;
    this.getListCommitmentActionCourseByUser();
  }
}
