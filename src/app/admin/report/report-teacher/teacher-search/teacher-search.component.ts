import {Component, Input, OnInit, ViewChild} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { MonthPickerComponent } from 'src/app/shared/month-picker-component/month-picker.component';
import { CourseCategoryService } from 'src/app/shared/services/course-category.service';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { PositionService } from 'src/app/shared/services/position.service';
import CommonUtil from 'src/app/shared/utils/common-util';
import { runInThisContext } from 'vm';

@Component({
  selector: 'app-teacher-search',
  templateUrl: './teacher-search.component.html',
  styleUrls: ['./teacher-search.component.css']
})
export class TeacherSearchComponent implements OnInit {

  @ViewChild(MonthPickerComponent, {static : true}) child : MonthPickerComponent;
  @Input() searchCourse;
   
  searchYear :any;
  inputYear:any;
  top: any;
  grade: any;
  departmentMap: any[] = [];
  departmentMergeMap: any[] = [];
  positionList: any[] = [];
  positionData: number [] = [];
  statusSearch: any;
  departmentData: number[] = [];
  departmentMergeData: number[] = [];
  statusSearchData: any;
  isdisabled:true

  dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 5,
    allowSearchFilter: true,
    searchPlaceholderText: 'Tìm kiếm'
  };

  listCategory: any = [];
  selectedCategory: any;
  isCheckCategoryEvent: boolean = false;
  categoryids: any = [];

  constructor(
    public activeModal: NgbActiveModal,
    private positionService: PositionService,
    private departmentService: DepartmentService,
    private fb: FormBuilder,
    private categoryService: CourseCategoryService
  ) {}

  ngOnInit(): void {
    this.statusSearchData = [
      {name: 'Chưa hiển thị', id: 1},
      {name: 'Chưa diễn ra', id: 2},
      {name: 'Đang diễn ra', id: 3},
      {name: 'Đã kết thúc', id: 4}
    ];
    if(this.searchCourse.grade){
      this.grade = this.searchCourse.grade;
    }
    if(this.searchCourse.top){
      this.top = this.searchCourse.top;
    }
    if(this.searchCourse.year){
      this.inputYear = this.searchCourse.year;
      this.searchYear = this.searchCourse.year;
    }
    if(this.searchCourse.departmentData){
      this.departmentData = this.searchCourse.departmentData;
    }else{
      this.departmentData = null;
    }
    if(this.searchCourse.departmentMergeData){
      this.departmentMergeData = this.searchCourse.departmentMergeData;
    }else{
      this.departmentMergeData = null;
    }

    if(this.searchCourse.positionData){
      this.positionData = this.searchCourse.positionData;
    }else{
      this.positionData = null;
    }

    if(this.searchCourse.categories) {
      this.selectedCategory = this.searchCourse.categories;
    } else {
      this.selectedCategory = null;
    }

    this.listAllTree();
    this.getListDepartmentMerge();
    this.getListCategory();
  }

  getListPosition() {
    return this.positionService.getPosition().subscribe(positionList => {
      this.positionList = positionList.body.results;
    });
  }
  getListDepartmentMerge() {
    this.departmentService.listDepartmentMergeTree().subscribe((data) => {
      this.departmentMergeMap = data.body;
    });
  }
  onDeSelectDepartmentMerge($event) {
    this.departmentMergeData = [...this.departmentMergeData]?.filter(id => id !== $event.id);
  }
  onDeSelectAllDepartmentMerge() {
    this.departmentMergeData = [];
  }

  onSearchCourses() {
    const searchParams = {
      year: this.searchYear,
      top: this.top,
      grade: this.grade,
      departmentData: this.departmentData,
      departmentMergeData : this.departmentMergeData,
      categories: this.selectedCategory
    }
    this.activeModal.close(searchParams);
  }

  resetSearch(e:any) {
    this.positionData = [];
    this.departmentData = [];
    this.departmentMergeData = [];
    this.statusSearch = null;
    this.inputYear=null;
    this.searchYear = null;
    this.child.clearDate(e);
    this.top=null;
    this.grade=null;
    this.searchYear=null;
  }

  onDeSelectPosition($event) {
    this.positionData = [...this.positionData]?.filter(id => id !== $event.id);
  }

  onDeSelectAllPosition() {
    this.positionData = [];
  }

  onDeSelectDepartment($event) {
    this.departmentMap = [...this.departmentMap]?.filter(id => id !== $event.id);
  }
  onDeSelectAllDepartment() {
    this.departmentData = [];
  }
  

  listAllTree() {
    this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
      const e = [];
      data.body.forEach(department => {
        if (department.parentid !== null) {
          const parentName = e.filter(x => x.id == department.parentid)[0]?.name;
          let name = '';
          if (parentName) {
            name = parentName + ' / ' + department.name;
          } else {
            name = department.name;
          }
          e.push({id: department.id, name: name});
        } else {
          e.push({id: department.id, name: department.name});
        }
      });
      this.departmentMap = e;
    });
  }
  yearSelected(value: any) {
    this.searchYear = value ? value[0] : null;
  }

  getListCategory(): void {
    this.categoryService.getCourseCategoryTree(null).subscribe((res) => {
      const categoryMap = new Map();
      const listData = [];
      CommonUtil.convertCategoryCodeListToMap(res.body, categoryMap);
      categoryMap.forEach((value, key) => {
        listData.push(
          {
            'id': key,
            'name': value
          }
        )
      })
      this.listCategory = listData;
    })
  }

  onSelectCategoryEvent(): void {
    this.isCheckCategoryEvent = true;
  }

  changeCategory(): void {
    if(this.isCheckCategoryEvent) {
      this.isCheckCategoryEvent = false;
      this.searchCourse.categories = this.selectedCategory;
    }
    this.isCheckCategoryEvent = false;
  }
}
