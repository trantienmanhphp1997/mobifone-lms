import CommonUtil from 'src/app/shared/utils/common-util';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AdminComponent} from './admin.component';
import {CourseComponent} from './course/course.component';
import {CourseCategoryComponent} from './course-category/course-category.component';
import {CourseCategoryEditComponent} from './course-category/course-category-edit/course-category-edit.component';
import {AppheaderComponent} from './core/appheader/appheader.component';
import {CourseContentComponent} from './course-content/course-content.component';
import { NzSelectModule } from 'ng-zorro-antd/select';
import {AppfooterComponent} from './core/appfooter/appfooter.component';
import {AppmenuComponent} from './core/appmenu/appmenu.component';
import {AdminRoutingModule} from './admin-routing.module';
import {DashboardComponent} from './dashboard/dashboard.component';
import {TranslateModule} from '@ngx-translate/core';
import {NgSelectModule} from '@ng-select/ng-select';
import {CourseDetailComponent} from './course/course-detail/course-detail.component';
import {DepartmentDetailComponent} from './department/department-detail/department-detail.component';
import {DepartmentComponent} from './department/department.component';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppbreadcrumbComponent} from './core/appbreadcrumb/appbreadcrumb.component';
import {UserComponent} from './user/user.component';
import {UserEditComponent} from './user/user-edit/user-edit.component';
import {TreeViewModule} from '@syncfusion/ej2-angular-navigations';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatNativeDateModule} from '@angular/material/core';
import {MatSelectModule} from '@angular/material/select';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatMenuModule} from '@angular/material/menu';
import {MatIconModule} from '@angular/material/icon';
import {MatListModule} from '@angular/material/list';
import {MatDividerModule} from '@angular/material/divider';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatRadioModule} from '@angular/material/radio';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {NgxSpinnerModule} from 'ngx-spinner';
import {PageContentComponent} from './course-content/page-content/page-content.component';
import {CKEditorModule} from '@ckeditor/ckeditor5-angular';
import {ResourceContentComponent} from './course-content/resource-content/resource-content.component';
import {PositionEditComponent} from './position/position-edit/position-edit.component';
import {QuizContentComponent} from './course-content/quiz-content/quiz-content.component';
import {ShowQuizComponent} from './course-content/quiz-content/show-quiz/show-quiz.component';
import {QuizCreateComponent} from './course-content/quiz-content/quiz-create/quiz-create.component';
import {MultichoiceComponent} from './course-content/quiz-content/multichoice/multichoice.component';
import {TruefalseComponent} from './course-content/quiz-content/truefalse/truefalse.component';
import {MatchingquizComponent} from './course-content/quiz-content/matchingquiz/matchingquiz.component';
import {ShortQuizComponent} from './course-content/quiz-content/short-quiz/short-quiz.component';
import {NumbericalQuizComponent} from './course-content/quiz-content/numberical-quiz/numberical-quiz.component';
import {AddUserComponent} from './course-content/add-user/add-user.component';
import {MatTabsModule} from '@angular/material/tabs';
import {TopicDetailComponent} from './course-content/topic-detail/topic-detail.component';
import {AddTeacherComponent} from './course-content/add-teacher/add-teacher.component';
import {CourseCreateComponent} from './course/course-create/course-create.component';
import {SelectCourseComponent} from './position/position-course/select-course.component';
import {PositionComponent} from './position/position.component';
import {ExamComponent} from './exam/exam.component';
import {CreateExamComponent} from './exam/create-exam/create-exam.component';
import {NgMultiSelectDropDownModule} from 'ng-multiselect-dropdown';
import {SurveyComponent} from './survey/survey.component';
import {CreateSurveyComponent} from './survey/create-survey/create-survey.component';
import {MatCardModule} from '@angular/material/card';
import {MatPaginatorIntl, MatPaginatorModule} from '@angular/material/paginator';
import {QuestionBankComponent} from './question-bank/question-bank.component';
import {QuestionCreateComponent} from './question-bank/question-create/question-create.component';
import {MatSortModule} from '@angular/material/sort';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {MultichoiceSurveyComponent} from './survey/multichoice-survey/multichoice-survey.component';
import {TruefalseSurveyComponent} from './survey/truefalse-survey/truefalse-survey.component';
import {AddSingleUserComponent} from './course-content/add-user/add-single-user/add-single-user.component';
import {SurveyDetailComponent} from './survey/survey-detail/survey-detail.component';
import {ChartsModule} from 'ng2-charts';
import {TextQuestionSurveyComponent} from './survey/text-question-survey/text-question-survey.component';
import {SelectboxSurveyComponent} from './survey/selectbox-survey/selectbox-survey.component';
import {RateSurveyComponent} from './survey/rate-survey/rate-survey.component';
import {ChosenLibComponent} from './course-content/quiz-content/chosen-lib/chosen-lib.component';
import {ListExamComponent} from './exam/list-exam/list-exam.component';
import {CertificateComponent} from './course-content/certificate/certificate.component';
import {ExamDetailComponent} from './exam/exam-detail/exam-detail.component';
import {SurveyGeneralComponent} from './survey/survey-general/survey-general.component';
import {SurveySingleComponent} from './survey/survey-single/survey-single.component';
import {ImportUserComponent} from './user/import-user/import-user.component';
import {ImportUserToCourseComponent} from './course-content/add-user/import-user/import-user-to-course.component';
import {BadgeComponent} from './badge/badge.component';
import {BadgeCreateComponent} from './badge/badge-create/badge-create.component';
import {BadgeDetailsComponent} from './badge/badge-details/badge-details.component';
import {BadgeInfoComponent} from './badge/badge-info/badge-info.component';
import {BadgeCourseComponent} from './badge/badge-course/badge-course.component';
import {BadgeStudentComponent} from './badge/badge-student/badge-student.component';
import {QuestionDetailComponent} from './question-bank/question-detail/question-detail.component';
import {BadgeExamComponent} from './badge/badge-exam/badge-exam.component';
import {NotificationComponent} from './notification/notification.component';
import {RoleComponent} from './role/role.component';
import {RoleCreateComponent} from './role/role-create/role-create.component';
import {ReportComponent} from './report/report.component';
import {SelectItemComponent} from './badge/select-item.component';
import {ReportCourseComponent} from './report/report-course/report-course.component';
import {ReportGeneralComponent} from './report/report-general/report-general.component';
import {CourseSearchComponent} from './course/course-search/course-search.component';
import {ExamSearchComponent} from './exam/exam-search/exam-search.component';
import {BankSearchComponent} from './question-bank/bank-search/bank-search.component';
import {QuizSearchComponent} from './course-content/quiz-content/quiz-search/quiz-search.component';
import {ListSearchComponent} from './exam/list-exam/list-search/list-search.component';
import {EditQuestionComponent} from './survey/edit-question/edit-question.component';
import {InfiniteScrollModule} from 'ngx-infinite-scroll';
import {DeleteBadgePopupComponent} from './badge/delete-badge-popup/delete-badge-popup.component';
import {ForgetPasswordComponent} from './forget-password/forget-password.component';
import {ReportStudentComponent} from './report/report-student/report-student.component';
import {CreateNotificationComponent} from './notification/create-notification/create-notification.component';
import {NotificationHistoryComponent} from './notification/notification-history/notification-history.component';
import {NotificationHistoryDetailComponent} from './notification/notification-history-detail/notification-history-detail.component';
import {NotificationInfoComponent} from './notification/notification-info/notification-info.component';
import {NotificationTemplateComponent} from './notification/notification-template/notification-template.component';
import {ReportExamComponent} from './report/report-exam/report-exam.component';
import {CourseSelectComponent} from './course/course-select/course-select.component';
import {SurveySingleCreateComponent} from './survey/survey-single-create/survey-single-create.component';
import {SurveySingleDetailComponent} from './survey/survey-single-detail/survey-single-detail.component';
import {ResultExamComponent} from './report/result-exam/result-exam.component';
import {ResultDetailExamComponent} from './report/result-exam/result-detail-exam/result-detail-exam.component';
import {ResultExamUserComponent} from './report/result-exam/result-exam-user/result-exam-user.component';
import {SingleSurveySearchComponent} from './survey/single-survey-search/single-survey-search.component';
import {StudentDetailsComponent} from './report/report-student/student-details/student-details.component';
import {StudentLearningHistoryComponent} from './report/report-student/student-learning-history/student-learning-history.component';
import {BigbluebuttonContentComponent} from './course-content/bigbluebutton-content/bigbluebutton-content.component';
import {ScormContentComponent} from './course-content/scorm-content/scorm-content.component';
import { AddSingleMemberComponent } from './notification/create-notification/add-single-member/add-single-member.component';
import { MultichoiceSetComponent } from './course-content/quiz-content/multichoiceset/multichoiceset.component';
import { ChosenRanComponent } from './course-content/quiz-content/chosen-ran/chosen-ran.component';
import {SafePipe} from '../shared/utils/safe-pipe';
import { RandomQuestionComponent } from './course-content/quiz-content/random-question/random-question.component';
import { ImportQuestion } from './question-bank/import-question/import-question.component';
import { QuestionCategoryEditComponent } from './question-category/question-category-edit/question-category-edit.component';
import { QuestionCategoryComponent } from './question-category/question-category.component';
import { BadgeExternalComponent } from './badge-external/badge-external.component';
import { BadgeExternalInfoComponent } from './badge-external/badge-external-info/badge-external-info.component';
import { BadgeExternalStudentComponent } from './badge-external/badge-external-student/badge-external-student.component';
import { BadgeExternalDetailsComponent } from './badge-external/badge-external-details/badge-external-details.component';
import { BadgeExternalCreateComponent } from './badge-external/badge-external-create/badge-external-create.component';
import { DeleteBadgeExternalPopupComponent } from './badge-external/delete-badge-external-popup/delete-badge-external-popup.component';
import { BadgeLevelDetailComponent } from './badge-level/badge-level-detail/badge-level-detail.component';
import { BadgeLevelInfoComponent } from './badge-level/badge-level-info/badge-level-info.component';
import { BadgeLevelBadgeComponent } from './badge-level/badge-level-badge/badge-level-badge.component';
import { BadgeLevelBadgeAddFormComponent } from './badge-level/badge-level-badge/badge-level-badge-addForm/badege-level-badge-addForm.component';
import { BadgeCategoryCreateComponent } from './badge-category/badge-category-create/badge-category-create.component';
import { BadgeCategoryComponent } from './badge-category/badge-category.component';
import { BadgeLevelComponent } from './badge-level/badge-level.component';
import { AddSingleUserFormComponent } from './badge-external/badge-external-student/add-single-user/add-single-user-form.component';
import { ProcessCloneCourseComponent } from './course/process-clone-course/process-clone-course.component';
import { ReportTeacherComponent } from './report/report-teacher/report-teacher.component';
import { TeacherDetailsComponent } from './report/report-teacher/teacher-details/teacher-details.component';
import { StaffAssessmentComponent } from './staff-assessment/staff-assessment.component';
import { EvaluationPopupComponent } from './staff-assessment/evaluation-popup/evaluation-popup.component';
import { LaudatoryComponent } from './laudatory/laudatory.component';
import { MonthPickerComponent } from '../shared/month-picker-component/month-picker.component';
import { CriteriaComponent } from './criteria/criteria.component';
import { CriteriaDetailComponent } from './criteria/criteria-detail/criteria-detail.component';
import { PositionCriteriaComponent } from './position/position-criteria/position-criteria.compoment';
import { ManageApproverComponent } from './manage-approver/manage-approver.component';
import { TrainingPlanComponent } from './training-plan/training-plan.component';
import { TrainingDetailComponent } from './training-plan/training-plan-detail/training-plan-detail.component';
import { TrainingPlanDepartment } from './training-plan/training-plan-department/training-plan-department.component';
import { CourseRecommendComponent } from './course-recommend/course-recommend.component';
import { CourseRecommedDetailComponent } from './course-recommend/course-recommend-detail/course-recommend-detail.component';
import { CourseRecommendPopup } from './training-plan/course-recommend-popup/course-recommend-popup.component';
import { CoursePlanDetailComponent } from './training-plan/course-plan-detail/course-plan-detail.component';
import { BadgeOfflineStudentComponent } from './badge-external/bagde-offline-student/badge-offline-student.component';
import { TeacherSearchComponent } from './report/report-teacher/teacher-search/teacher-search.component';
import { ReportKpiComponent } from './report/report-kpi/report-kpi.component';
import { ReportContractComponent } from './report/report-contract/report-contract.component';
import { ReportDetailComponent } from './report/report-contract/report-detail/report-detail.component';
import { BadgeCategoryConfirmDeleteComponent } from './badge-category/badge-category-confirm-delete/badge-category-confirm-delete.component';
import { CourseQuestionBankComponent } from './course-content/course-question-bank/course-question-bank.component';
import { CourseQuestionCreateComponent } from './course-content/course-question-create/course-question-create.component';
import { CourseQuestionCategoryComponent } from './course-content/course-question-category/course-question-category.component';
import { RollCallComponent } from './roll-call/roll-call.component';
import { RollCallDetailComponent } from './roll-call/roll-call-detail/roll-call-detail.component';
import { CourseQrcodeComponent } from './course/course-qrcode/course-qrcode.component';
import { CourseQrcodeDetailComponent } from './course/course-qrcode/course-qrcode-detail/course-qrcode-detail.component';
import { ReportStudentDataComponent } from './report/report-student-data/report-student-data.component';
import { QRCodeModule } from 'angularx-qrcode';
import { RollCallExmodalComponent } from './roll-call/roll-call-exmodal/roll-call-exmodal.component';
import { ReportTrainingPlanComponent } from './report/report-training-plan/report-training-plan.component';
import { ExternalTrainingPlanComponent } from './report/report-training-plan/external-training-plan/external-training-plan.component';
import { InternalTrainingPlanComponent } from './report/report-training-plan/internal-training-plan/internal-training-plan.component';
import { TeacherComponent } from './teacher/teacher.component';
import { CourseCriteriaComponent } from './course-content/course-criteria/course-criteria.component';
import { CriteriaSelectComponent } from './course-content/course-criteria/criteria-select/criteria-select.component';
import { DetailTeacherComponent } from './teacher/detail-teacher/detail-teacher.component';
import { CreateCriteriaComponent } from './course/create-criteria/create-criteria.component';
import { SearchCourseComponent } from './report/report-student-data/search-course/search-course.component';
import { CreateBadgeComponent } from './teacher/detail-teacher/create-badge/create-badge.component';
import { ReportTeacherInternalComponent } from './report/report-teacher-internal/report-teacher-internal.component';
import { PdfViewerModule } from 'ng2-pdf-viewer';
import { ApprovalBadgeComponent } from './teacher/approval-badge/approval-badge.component';
import { DetailErrorComponent } from './training-plan/detail-error/detail-error.component';
import { MergeDepartmentComponent } from './department/merge-department/merge-department.component';
import { DepartmentMergePopupComponent } from './department/department-merge-popup/department-merge-popup.component';
import { CloneConfirmComponent } from './course/clone-confirm/clone-confirm.component';
import { CourseCloneComponent } from './course-clone/course-clone.component';
import { ClonePopupComponent } from './course-clone/clone-popup/clone-popup.component';
import { DetailCloneComponent } from './course-clone/detail-clone/detail-clone.component';
import { PopupEvaluationComponent } from './course-content/popup-evaluation/popup-evaluation.component';
import { ReportTrainingComponent } from './report/report-training/report-training.component';
import { ListClassInCoursePlanDetailComponent } from './training-plan/list-class-in-course-plan-detail/list-class-in-course-plan-detail.component';
import { ReportTrainingSourceComponent } from './report/report-training-source/report-training-source.component';
import { NzDatePickerModule } from 'ng-zorro-antd/date-picker';
import { DetailTrainingPlanComponent } from './training-plan/detail-training-plan/detail-training-plan.component';
import { ReportLearningDataComponent } from './report/report-learning-data/report-learning-data.component';
import { LearningDataDetailComponent } from './report/report-learning-data/learning-data-detail/learning-data-detail.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    NgSelectModule,
    ReactiveFormsModule,
    AdminRoutingModule,
    TranslateModule,
    NgbModule,
    TreeViewModule,
    MatDatepickerModule,
    MatFormFieldModule,
    MatInputModule,
    MatNativeDateModule,
    MatSelectModule,
    MatTooltipModule,
    MatToolbarModule,
    MatExpansionModule,
    MatIconModule,
    MatMenuModule,
    MatListModule,
    MatDividerModule,
    MatCheckboxModule,
    MatSidenavModule,
    MatRadioModule,
    MatSlideToggleModule,
    NgxSpinnerModule,
    CKEditorModule,
    MatTabsModule,
    NgMultiSelectDropDownModule.forRoot(),
    MatCardModule,
    MatPaginatorModule,
    MatSortModule,
    DragDropModule,
    ChartsModule,
    InfiniteScrollModule,
    QRCodeModule,
    PdfViewerModule,
    NzDatePickerModule,
    NzSelectModule
  ],
  declarations: [
    AdminComponent,
    CourseComponent,
    CourseCategoryComponent,
    CourseCategoryEditComponent,
    AppheaderComponent,
    AppfooterComponent,
    AppmenuComponent,
    DashboardComponent,
    CourseDetailComponent,
    CourseCreateComponent,
    DepartmentDetailComponent,
    DepartmentComponent,
    UserComponent,
    UserEditComponent,
    AppbreadcrumbComponent,
    CourseContentComponent,
    PageContentComponent,
    ResourceContentComponent,
    PositionComponent,
    PositionEditComponent,
    QuizContentComponent,
    ShowQuizComponent,
    QuizCreateComponent,
    MultichoiceComponent,
    MultichoiceSetComponent,
    TruefalseComponent,
    MatchingquizComponent,
    ShortQuizComponent,
    NumbericalQuizComponent,
    AddUserComponent,
    TopicDetailComponent,
    SelectCourseComponent,
    SelectItemComponent,
    AddTeacherComponent,
    ExamComponent,
    CreateExamComponent,
    SurveyComponent,
    CreateSurveyComponent,
    QuestionBankComponent,
    QuestionCreateComponent,
    MultichoiceSurveyComponent,
    TruefalseSurveyComponent,
    AddSingleUserComponent,
    SurveyDetailComponent,
    TextQuestionSurveyComponent,
    SelectboxSurveyComponent,
    RateSurveyComponent,
    ChosenLibComponent,
    ListExamComponent,
    CertificateComponent,
    ExamDetailComponent,
    SurveyGeneralComponent,
    SurveySingleComponent,
    ImportUserToCourseComponent,
    ImportUserComponent,
    BadgeComponent,
    BadgeCreateComponent,
    BadgeDetailsComponent,
    BadgeInfoComponent,
    BadgeCourseComponent,
    BadgeStudentComponent,
    BadgeExternalComponent,
    BadgeExternalInfoComponent,
    BadgeExternalStudentComponent,
    BadgeExternalDetailsComponent,
    BadgeLevelBadgeAddFormComponent,
    BadgeCategoryComponent,
    BadgeCategoryCreateComponent,
    BadgeLevelComponent,
    BadgeLevelDetailComponent,
    BadgeLevelInfoComponent,
    BadgeLevelBadgeComponent,
    AddSingleUserFormComponent,
    DeleteBadgePopupComponent,
    DeleteBadgeExternalPopupComponent,
    BadgeExternalCreateComponent,
    QuestionDetailComponent,
    BadgeExamComponent,
    NotificationComponent,
    RoleComponent,
    RoleCreateComponent,
    ReportComponent,
    ReportCourseComponent,
    ReportGeneralComponent,
    ReportStudentComponent,
    ReportExamComponent,
    CourseSearchComponent,
    ExamSearchComponent,
    BankSearchComponent,
    QuizSearchComponent,
    ListSearchComponent,
    EditQuestionComponent,
    NotificationTemplateComponent,
    NotificationHistoryComponent,
    NotificationHistoryDetailComponent,
    NotificationInfoComponent,
    CreateNotificationComponent,
    ForgetPasswordComponent,
    CourseSelectComponent,
    SurveySingleCreateComponent,
    SurveySingleDetailComponent,
    ResultExamComponent,
    ResultDetailExamComponent,
    ResultExamUserComponent,
    SingleSurveySearchComponent,
    StudentDetailsComponent,
    StudentLearningHistoryComponent,
    BigbluebuttonContentComponent,
    ScormContentComponent,
    AddSingleMemberComponent,
    ChosenRanComponent,
    SafePipe,
    RandomQuestionComponent,
    ImportQuestion,
    QuestionCategoryComponent,
    QuestionCategoryEditComponent,
    ProcessCloneCourseComponent,
    ReportTeacherComponent,
    TeacherDetailsComponent,
    StaffAssessmentComponent,
    EvaluationPopupComponent,
    LaudatoryComponent,
    MonthPickerComponent,
    CriteriaComponent,
    CriteriaDetailComponent,
    PositionCriteriaComponent,
    ManageApproverComponent,
    TrainingPlanComponent,
    TrainingDetailComponent,
    TrainingPlanDepartment,
    CourseRecommendComponent,
    CourseRecommedDetailComponent,
    CourseRecommendPopup,
    CoursePlanDetailComponent,
    BadgeOfflineStudentComponent,
    TeacherSearchComponent,
    ReportKpiComponent,
    ReportContractComponent,
    ReportDetailComponent,
    BadgeCategoryConfirmDeleteComponent,
    CourseQuestionBankComponent,
    CourseQuestionCreateComponent,
    CourseQuestionCategoryComponent,
    RollCallComponent,
    RollCallDetailComponent,
    CourseQrcodeComponent,
    CourseQrcodeDetailComponent,
    ReportStudentDataComponent,
    RollCallExmodalComponent,
    ReportTrainingPlanComponent,
    ExternalTrainingPlanComponent,
    InternalTrainingPlanComponent,
    ReportTrainingPlanComponent,
    TeacherComponent,
    CourseCriteriaComponent,
    CriteriaSelectComponent,
    DetailTeacherComponent,
    CreateCriteriaComponent,
    SearchCourseComponent,
    CreateBadgeComponent,
    ReportTeacherInternalComponent,
    ApprovalBadgeComponent,
    DetailErrorComponent,
    MergeDepartmentComponent,
    DepartmentMergePopupComponent,
    CloneConfirmComponent,
    CourseCloneComponent,
    ClonePopupComponent,
    DetailCloneComponent,
    PopupEvaluationComponent,
    ReportTrainingComponent,
    ListClassInCoursePlanDetailComponent,
    ReportTrainingSourceComponent,
    DetailTrainingPlanComponent,
    ReportLearningDataComponent,
    LearningDataDetailComponent
  ],
  entryComponents: [
    ForgetPasswordComponent,
    CreateNotificationComponent,
    NotificationHistoryComponent,
    NotificationHistoryDetailComponent,
    NotificationInfoComponent,
    NotificationTemplateComponent,
    CourseSelectComponent,
    TeacherSearchComponent
  ],
  providers: [
    { provide: MatPaginatorIntl, useValue: CommonUtil.overridePaginatorIntl() },
  ]
}
)
export class AdminModule {
}
