import { ToastrCustomService } from './../../../shared/services/toastr-custom.service';
import { PageEvent } from '@angular/material/paginator';
import { NgxSpinnerService } from 'ngx-spinner';
import { BadgeService } from './../../../shared/services/badge.service';
import { Component, OnInit, Input} from '@angular/core';
import * as moment from 'moment';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AddSingleUserFormComponent } from './add-single-user/add-single-user-form.component';
import { FileService } from 'src/app/shared/services/file.service';
import { ConfirmModalComponent } from 'src/app/shared/modal/confirm-modal/confirm-modal.component';

@Component({
  selector: 'app-badge-external-student',
  templateUrl: './badge-external-student.component.html',
  styleUrls: ['./badge-external-student.component.css']
})
export class BadgeExternalStudentComponent implements OnInit {

  @Input() readonly: boolean;
  @Input() badgeId: number;

  pageSizeOptions: number[] = [10, 25, 50, 100];
  pageEvent: PageEvent;
  studentonBadge: any[];
  totalRecord: number;

  searchBadgeStudent = {
    badgeid: null,
    search : '',
    page: 1,
    limit: 10,
    sortcolumn: 'dateissued',
    sorttype: 'desc'
  }

  completedAll = false;
  keepBadgeForU = 0;
  studentIdsChecked: number[] = [];
  studentIds: number[] = [];
  constructor(
    private modalService: NgbModal,
    private badgeService: BadgeService,
    private spinner: NgxSpinnerService,
    private toastrService: ToastrCustomService,
    private fileservice: FileService,
  ) {}

  ngOnInit(): void {
    this.searchBadgeStudent.badgeid = this.badgeId;
    this.getListStudentOnBadge();
  }

  getListStudentOnBadge() {
    this.spinner.show();
    this.badgeService.getListStudentOnExternalBadge(this.searchBadgeStudent).subscribe(
      bad => {
        this.studentonBadge = bad.body.results;
        this.totalRecord = bad.body.total;
        this.spinner.hide();
      },
      err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      });
  }

  onChangeSearch(event) {
    this.searchBadgeStudent.search = event.target.value;
    this.searchBadgeStudent.page = 1;
    this.getListStudentOnBadge();
  }

  changePage(event) {
    this.searchBadgeStudent.page = event.pageIndex + 1;
    this.searchBadgeStudent.limit = event.pageSize;
    this.getListStudentOnBadge();
  }

  addStudentForm(){
    const modelDep = this.modalService.open(AddSingleUserFormComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    modelDep.componentInstance.badgeId = this.badgeId;
    modelDep.result.then(res => {
      if(res === "ok"){
        this.getListStudentOnBadge();
      }
    });
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  viewImage(item) {
    var img = "";
    if (item.filename) {
      img = this.fileservice.badgeFileUrl(item.contextid, item.filename, '/external_badge/overviewfiles/','0/');
    }
    return img;
  }
  onUnassignStudentToBadge (userid: any) {
    const param = {
      badgeid: this.badgeId,
      userid: userid.toString()
    }
    const modalDep = this.modalService.open(ConfirmModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });

    modalDep.componentInstance.title = 'Xác nhận';
    modalDep.componentInstance.body = 'Bạn có chắc chắn muốn gỡ học viên khỏi chứng chỉ ?';
    modalDep.componentInstance.confirmButton = 'Xác nhận';

    modalDep.result.then((result) => {
      this.spinner.show();
      this.badgeService.unAssignStudentToBadge(param).subscribe(
        res => {
          this.toastrService.success('Gỡ thành công');
          this.getListStudentOnBadge();
          this.spinner.hide();
        },
        err => {
          this.spinner.hide();
          this.toastrService.handlerError(err);
        });
    });
  }
  onDeleteMultipleBadge () {
    this.onUnassignStudentToBadge(this.studentIdsChecked);
  }
  oncheckboxAll(checked: any) {
    if (checked) {
      this.completedAll = true;
      this.studentonBadge.forEach(c => {
          c.completed = checked;
          if (!this.studentIds.includes(c.id)) {
            this.studentIds.push(c.id);
          }
          if (!this.studentIdsChecked?.includes(c.id)) {
            this.studentIdsChecked?.push(c.id);
          }
      });
    } else {
      this.studentIds?.forEach(id => {
        this.studentIdsChecked?.splice(this.studentIdsChecked?.indexOf(id), 1);
      });
      this.studentIds = [];
      this.studentonBadge?.forEach(c => {
        c.completed = false;
      });
      this.completedAll = false;
    }
  }

  oncheckboxItem(badgeId: number, checked: any) {
    if (checked) {
      this.studentonBadge?.forEach(c => {
        if (c.id === badgeId) {
          c.completed = true;
          this.studentIds?.push(badgeId);
          this.studentIdsChecked?.push(badgeId);
          return;
        }
      });
    } else {
      this.completedAll = false;
      this.studentonBadge?.forEach(c => {
        if (c.id === badgeId) {
          c.completed = false;
          this.studentIds?.splice(this.studentIds?.indexOf(badgeId), 1);
          this.studentIdsChecked?.splice(this.studentIdsChecked?.indexOf(badgeId), 1);
          return;
        }
      });
    }
  }
}
