import CommonUtil from 'src/app/shared/utils/common-util';
import {MOODLE_INFO} from '../constants/base.constant';
import {AbstractService} from 'src/app/shared/services/abstract.service';
import {Observable} from 'rxjs';
import {Injectable} from '@angular/core';
import {map} from 'rxjs/operators';
import { HttpResponse } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class ReportService {

  constructor(private abstractService: AbstractService) {
  }

  reportCourseInSystem(courseType: any, categoryid?: number, year?: number,quarter?:number): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_course_in_system');
    CommonUtil.appendDataToFormData(data, 'params', {categoryid: categoryid,coursetype:courseType, year:year,quarter:quarter});
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  reportExportCourseInSystem(courseType: any, categoryid?: number): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_course_in_system');
    CommonUtil.appendDataToFormData(data, 'params', {categoryid: categoryid,coursetype:courseType});
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  reportCourseOrgan(params: any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_course_organization');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  reportExportCourseOrgan(courseid: number): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_course_organization');
    CommonUtil.appendDataToFormData(data, 'params', {courseid: courseid});
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  reportStudentCourse(params: any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_course_student');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }
  getStudentData(param:any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_student_data');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  getListStudent(param:any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_learning_progress_students');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  getListStudentByQuiz(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_quiz_list_user_result');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }

  getListExamReport(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_list_exam_result');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }
  getListTeacherIReportGV(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_course_of_teacher');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<any>) => res));
  }
  getListTeacherIReport(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_course_of_teacher');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }
  getListTeacherIReportDV(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_teacher_internal_department');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }
  getListTeacherNB(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_user_list_teacher');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }

  getReportExamByUser(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_quiz_result_detail');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }

  exportListExamReport(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_exam_result');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }

  exportExamReportDetail(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_quiz_result_detail');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }

  exportLearningProgressStudents(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_learning_progress_students');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }

  exportLearningProgressDetail(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_learning_progress_student_detail');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }

  getHistoryLerning(param: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_learning_progress_student_detail');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<any>) => res));
  }

  overviewSystemData(year?: any, quarter?: any, issearchbydate?: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_overview_of_system_data');
    CommonUtil.appendDataToFormData(data, 'params', {year:year, quarter:quarter, issearchbydate:issearchbydate});
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<any>) => res));
  }

  overviewSystemData2( fromdate?: any, todate?: any,  issearchbydate?: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_overview_of_system_data');
    CommonUtil.appendDataToFormData(data, 'params', { fromdate:fromdate, todate:todate, issearchbydate:issearchbydate});
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<any>) => res));
  }



  getListTeacher(param:any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_learning_progress_teachers');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }
  getListKpi(param:any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_kpi_department');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }
  exportTeacher(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_learning_progress_teachers');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }
  exportLaudatory(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_students_achievement');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }
  exportKPI(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_kpi');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }

  exportReportTraining(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_category_training_report');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }
  exportTeacherNDV(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_teacher_internal_department_export');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }
  exportTeacherNN(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_course_of_teacher_export');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }
  exportStudentData(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_student_data');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }
  exportTeacherDetail(params: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_learning_progress_teacher_detail');
    CommonUtil.appendDataToFormData(data, 'params', params);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: HttpResponse<any>) => res));
  }

  getHistoryTeacher(param: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_learning_progress_teacher_detail');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<any>) => res));
  }

  reportTeacherCourse(param: any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_course_teacher');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }
  getSenirority(): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_user_get_seniority');
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<any>) => res));
  }

  getListReportTraining(param:any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_category_training_report');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }
  getListDefaultParameter(): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_get_default_parameters_for_category_training_report');
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }
  getListDefaultParameterSource(): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_get_default_parameters_for_source_training_report');
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }
  getListReportTrainingBySource(param): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_source_training_report');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  getListReportTrainingBySourceDefault(): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_get_default_parameters_for_source_training_report');
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  exportExcelReportTrainingBySource(param): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_export_source_training_report');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  // Có thêm departemntids
  reportCourseInSystemDeparment(courseType: any, categoryid?: number, departmentids?: any, year?: number): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_course_in_system');
    CommonUtil.appendDataToFormData(data, 'params', {categoryid: categoryid, coursetype:courseType, departmentids: departmentids, year:year});
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  // Có thêm departmentids
  overviewSystemDataDeparment(year?: any, quarter? : any, departemntids?: any): Observable<HttpResponse<any>> {
    const data = this.abstractService.getCommonInput('mobifone_report_overview_of_system_data');
    CommonUtil.appendDataToFormData(data, 'params', {year:year, quarter:quarter, departmentids: departemntids});
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
      .pipe(map((res: HttpResponse<any>) => res));
  }

  getReportLearningData(param:any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_personal_learning_data');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  getReportLearningDataDetail(param:any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_report_personal_learning_data_details');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }

  exportReportLearningData(param:any): Observable<any> {
    const data = this.abstractService.getCommonInput('mobifone_export_report_personal_learning_data');
    CommonUtil.appendDataToFormData(data, 'params', param);
    return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
  }
}
