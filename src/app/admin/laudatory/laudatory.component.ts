import { Component, OnInit, ViewChild } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import * as moment from 'moment';
import { Moment } from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';
import { LocalStorageService } from 'ngx-webstorage';
import { USER_INFO } from 'src/app/shared/constants/base.constant';
import { Department } from 'src/app/shared/model/department.model';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { UserService } from 'src/app/shared/services/user.service';
import {ToastrCustomService} from 'src/app/shared/services/toastr-custom.service';
import {ReportService} from '../../shared/services/report.service';
import {FileService} from '../../shared/services/file.service';
@Component({
    selector: 'app-laudatory',
    templateUrl: './laudatory.component.html',
    styleUrls: ['./laudatory.component.scss']
})
export class LaudatoryComponent implements OnInit {
    // Tree
    @ViewChild('departmentTree')
    public departmentTree: TreeViewComponent;
    public departmentTreeData;
    currentSelectedId: number;
    // List
    departmentList: Department[] = [];
    // For delete
    completedAll = false;
    departmentIds: number[] = [];
    departmentIdsChecked: number[] = [];
    isDepartmentRoot: boolean = false;
    userInfo: any;
    rankTable: any = [];
    rankBy: any = 3;
    pageSizeOptions: number[] = [10, 25, 50, 100];
    totalRecord: number;
    checkedDepartmentIds = [];
    searchRank = {
        search: '',
        startDate: null,
        endDate: null,
        pageIndex: 1,
        pageSize: 10,
        sortColumn: 'timecreated',
        sortType: 'desc',
        month: null,
        year: null,
        day: null,
        rank: null
    };
    campaignOne: FormGroup;
    campaignTwo: FormGroup;
    // version = VERSION;
    date = new Date();
    chosenYearDate: Date;
    chosenMonthDate: Date = new Date(2020, 0, 1);
    chosenSemesterDate: Date;
    chosenWeekDate: Date;
    chosenDate: Date;
    monthInputCtrl: FormControl = new FormControl(new Date(2020, 0, 1));

    visible = true;
    currentDate: Date = new Date();
    constructor(
        private userService: UserService,
        public $localStorage: LocalStorageService,
        private departmentService: DepartmentService,
        private spinner: NgxSpinnerService,
        private toastrService: ToastrCustomService,
        private reportService: ReportService,
        private fileService: FileService,
    ) {
        const today = new Date();
        const month = today.getMonth();
        const year = today.getFullYear();

        this.campaignOne = new FormGroup({
            start: new FormControl(new Date(year, month, 13)),
            end: new FormControl(new Date(year, month, 16))
        });

        this.campaignTwo = new FormGroup({
            start: new FormControl(new Date(year, month, 15)),
            end: new FormControl(new Date(year, month, 19))
        });
    }

    ngOnInit(): void {
        this.userInfo = this.$localStorage.retrieve(USER_INFO.INFO);
        this.reLoadData();
    }

    getRankTable() {
        const param = {
            search: this.searchRank.search,
            limit: this.searchRank.pageSize,
            page: this.searchRank.pageIndex,
            month: this.searchRank.month,
            year: this.searchRank.year,
            day: this.searchRank.day ? this.searchRank.day .getTime() / 1000 : null,
            startdate: this.searchRank.startDate ? this.searchRank.startDate .getTime() / 1000 : null,
            enddate: this.searchRank.endDate ? this.searchRank.endDate .getTime() / 1000 : null,
            departmentid: this.currentSelectedId,
            rank: this.searchRank.rank
        };
          this.spinner.show();
        //   this.userIds = [];
        this.userService.getUserAchievement(param).subscribe((res) => {
            this.spinner.hide();
            this.rankTable = res.body.results;
            this.totalRecord = res.body.total;
        },
            err => {
                this.spinner.hide();
                // this.toastrService.handlerError(err);
            });
    }

    changePage(event) {
        this.searchRank.pageIndex = event.pageIndex + 1;
        this.searchRank.pageSize = event.pageSize;
        this.getRankTable();
    }

    onChangeKeyword() {
        this.searchRank.pageIndex = 1;
        // this.currentSelectedId = null; 
        this.getRankTable();
    }

    onChangeRank() {
        this.searchRank.pageIndex = 1;
        this.getRankTable();
    }

    setRankTable(by: any) {
        this.rankBy = by;
    }

    listAllTree() {
        this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
            const dataSource = data.body;
            this.setUpTree(dataSource);
            this.departmentTreeData = {
                dataSource, id: 'id', parentID: 'parentid', text: 'name', hasChildren: 'haschild', selected: 'isSelected'
            };
        });
    }

    nodeSelected(e) {
        this.currentSelectedId = +this.departmentTree.getNode(e.node).id;
        this.departmentIds = [];
        this.departmentIdsChecked = [];
        this.searchRank.pageSize = 10;
        this.searchRank.pageIndex = 1;
        // this.sortColumn = 'id';
        // this.sortType = 'ASC';
        this.getRankTable();
    }

    setUpTree(dataSource: any) {

        const indexOfCurrentSelectedId = dataSource.findIndex(x => x.id === this.currentSelectedId);
        if (indexOfCurrentSelectedId === -1) {// case currentSelectedId is deleted -> set to deault
            this.currentSelectedId = null;
        }

        // set thang cha = null de no hien thi len duoc cay
        dataSource[0].parentid = null;
        // muc dich la de expand tree
        dataSource.forEach(value => {
            if (value.parentid == null) {
                value.expanded = true; // value.haschild !== null;
            } else {
                value.expanded = false;
            }
            value.isChecked = true;
        });
    }

    reLoadData() {
        this.listAllTree();
        this.getRankTable();
    }
    /**
     * clear select node
     * clear search param
     */
    clear() {
        // param phuc vu tim kiem
        // this.keyword = '';
        this.currentSelectedId = null;
        this.searchRank.pageSize = 10;
        this.searchRank.pageIndex = 1;

        // Bo select node tren cay
        this.departmentTree.selectedNodes = [];
        this.departmentIdsChecked = [];

        this.getRankTable();
    }
    closeDatePicker(elem: MatDatepicker<any>) {
        elem.close();
    }
    yearSelected(value: any) {
        this.searchRank.year = value ? value[0] : null;
        
        this.getRankTable();
    }
    monthSelected(value: any) {
        this.searchRank.month = value ? value[0] : null
        this.searchRank.year = value ? value[1] : null
        this.getRankTable();
    }
    changeDay(e: any) {
        this.searchRank.day = e.value;
        this.getRankTable();
    }
    changeStartDate(e: any) {
        this.searchRank.startDate = e.value;
        this.getRankTable();
    }
    changeEndDate(e: any) {
        this.searchRank.endDate = e.value;
        this.getRankTable();
    }
    onExporLaudatory(){
        const params = {
            search: this.searchRank.search,
            month: this.searchRank.month,
            year: this.searchRank.year,
            day: this.searchRank.day ? this.searchRank.day .getTime() / 1000 : null,
            startdate: this.searchRank.startDate ? this.searchRank.startDate .getTime() / 1000 : null,
            enddate: this.searchRank.endDate ? this.searchRank.endDate .getTime() / 1000 : null,
            departmentid: this.currentSelectedId,
            rank: this.searchRank.rank
        };
        this.spinner.show();
        this.reportService.exportLaudatory(params).subscribe(
        res => {
            this.spinner.hide();
            window.open(this.fileService.getFileFromPathUrl(res.body.path));
        }, err => {
            this.spinner.hide();
            this.toastrService.handlerError(err);
        });
    }
    clearDate(e: any, type: any) {
        e.stopPropagation();
        switch (type) {
            case 'endDate':
                this.searchRank.endDate = null;
                break;
            case 'startDate':
                this.searchRank.startDate = null;
                // this.searchRank.endDate = null;
                break;
            case 'day':
                this.searchRank.day = null;
                break;
            default:
                break;
        }
        this.getRankTable();
    }
}
