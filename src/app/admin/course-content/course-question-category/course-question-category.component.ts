import {AfterViewInit, Component, OnInit, ViewChild, OnDestroy, Input} from '@angular/core';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import { CourseCategory } from 'src/app/shared/model/course-category.model';
import { CourseCategoryService } from 'src/app/shared/services/course-category.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import {TranslateService} from '@ngx-translate/core';
import { QuestionCategoryEditComponent } from '../../question-category/question-category-edit/question-category-edit.component';
import { ConfirmModalComponent } from 'src/app/shared/modal/confirm-modal/confirm-modal.component';
import {NgxSpinnerService} from 'ngx-spinner';
import {TreeViewComponent} from '@syncfusion/ej2-angular-navigations';
import {Sort} from '@angular/material/sort';
import { QuestionCategoryService } from 'src/app/shared/services/question-category.service';
import { CourseService } from '../../../shared/services/course.service';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-course-question-category',
  templateUrl: './course-question-category.component.html',
  styleUrls: ['./course-question-category.component.css'],
  providers: [NgbModalConfig, NgbModal],
})
export class CourseQuestionCategoryComponent implements OnInit, AfterViewInit, OnDestroy {

  // tree
  @ViewChild('categoryTree')
  public categoryTree: TreeViewComponent;
  currentSelectedId: number; // node selected
  public categoryTreeData; // data for tree
  
  // List category
  categories: CourseCategory[] = [];

  completedAll = false; // checkbox all
  // list id course checked trong cả màn
  courseCateIdsChecked: number[] = [];
  // list id course checked trong current page
  courseCateIds: number[] = [];

  // pagination
  totalRecord: number;
  pageSize = 10;
  pageIndex = 1;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  sortColumn = 'id';
  sortType = 'DESC';
  keyword = '';
  courseId:any;
  courseDetail: any;
  type: any;
  constructor(
    private modalService: NgbModal,
    private categoryService: QuestionCategoryService,
    private translateService: TranslateService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    private route: ActivatedRoute,
    private router: Router,
    private courseService: CourseService,
  ) { }

  ngOnInit(): void {
    this.courseId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.type = this.route.snapshot.queryParamMap.get('type');
    if(this.courseId){
      this.getInfoCourse(this.courseId);
    }else{
      this.onSearch();
      this.getCategoryTree();
    }

    
  }
  /**
   * @method ngAfterViewInit
   */
  ngAfterViewInit() {

  }

  ngOnDestroy(): void {
    this.modalService.dismissAll();
  }

  oncheckboxAll(checked: any) {
    if (checked) {
      this.categories.forEach(c => {
        if(c.isroot != 1){
          c.completed = checked;
        }
        if (!this.courseCateIds.includes(c.id) && c.isroot != 1) {
          this.courseCateIds.push(c.id);
        }
        if (!this.courseCateIdsChecked?.includes(c.id) && c.isroot != 1) {
          this.courseCateIdsChecked?.push(c.id);
        }
      });
      this.completedAll = true;
    } else {
      this.courseCateIds?.forEach(id => {
        this.courseCateIdsChecked?.splice(this.courseCateIdsChecked?.indexOf(id), 1);
      });
      this.courseCateIds = [];
      this.categories?.forEach(c => {
        c.completed = false;
      });
      this.completedAll = false;
    }
  }

  oncheckboxItem(courseId: number, checked: any) {
    if (checked) {
      this.categories?.forEach(c => {
        if (c.id === courseId) {
          c.completed = true;
          this.courseCateIds?.push(courseId);
          this.courseCateIdsChecked?.push(courseId);
          return;
        }
      });
      if (this.courseCateIds?.length > 0 && this.courseCateIds?.length === this.categories?.length && !this.completedAll) {
        this.completedAll = true;
      }
    } else {
      this.completedAll = false;
      this.categories?.forEach(c => {
        if (c.id === courseId) {
          c.completed = false;
          this.courseCateIds?.splice(this.courseCateIds?.indexOf(courseId), 1);
          this.courseCateIdsChecked?.splice(this.courseCateIdsChecked?.indexOf(courseId), 1);
          return;
        }
      });
    }
  }

  onSearch() {
    const params = {
      limit: this.pageSize,
      page: this.pageIndex,
      keyword: this.keyword,
      id: this.currentSelectedId,
      sortcolumn: this.sortColumn,
      sorttype: this.sortType,
      rootqcategoryid: this.courseDetail?.questioncategoryid
    };
    this.spinner.show();
    this.courseCateIds = [];
    return this.categoryService.searchQuestionCategory(params).subscribe((data) => {
      data?.body?.results.forEach(c => {
        if (this.courseCateIdsChecked?.includes(c.id)) {
          c.completed = true;
          this.courseCateIds?.push(c.id);
        }
      });
      if (this.courseCateIds?.length > 0 && (this.courseCateIds?.length === data?.body?.results?.length)) {
        this.completedAll = true;
      } else {
        this.completedAll = false;
      }
      this.categories = data.body.results;
      this.totalRecord = data.body.total;
      this.spinner.hide();
    }, error => {
      this.toastrService.handlerError(error);
      this.spinner.hide();
    });
  }

  getCategoryTree(){
    return this.categoryService.getQuestionCategoryTree(this.courseDetail?.questioncategoryid).subscribe((data) => {
      let dataSource = data.body;
      let dataRoot = null;
      if(this.courseDetail?.questioncategoryid){
        dataRoot = dataSource.filter(x => x.id == this.courseDetail?.questioncategoryid);
        dataSource = dataSource.filter(x => x.id != this.courseDetail?.questioncategoryid);
        dataSource.unshift({id: this.courseDetail?.questioncategoryid, name: dataRoot[0]?.name ?? "Root", parent: 0,  haschild: 1});
      }else{
        dataSource = [...dataSource , {id: 1, name: "Root", parent: 0,  haschild: 1}]
      }
       
      
      this.setUpTree(dataSource);
      // console.log(dataSource);
      this.categoryTreeData = { dataSource, id: 'id', parentID: 'parent', text: 'name', hasChildren: 'haschild', selected: 'isSelected' };
      // console.log(this.categoryTreeData);
      
    });
  }

  setUpTree(dataSource: any) {

    const indexOfCurrentSelectedId = dataSource.findIndex(x => x.id === this.currentSelectedId);
    if (indexOfCurrentSelectedId === -1){// case currentSelectedId is deleted -> set to deault
      this.currentSelectedId = null;
    }

    dataSource.forEach(value => {
      if (value.parent === 0) // = 0 la root tree
      {
        value.parent = null; // set = null nham muc dich hien thi
        value.expanded = true; // muc dich expand root luon khi khoi tao tree
      } else {
        value.expanded = false;
      }

      if (value.id === this.currentSelectedId){ // high light selected node
        value.isSelected = true;
      }
    });
  }

  onUpdateCourseCategory(item){
    const current: any = {
      id: item.id,
      name: item.name,
      info: item.info,
      parent: item.parent
    };
    const title = 'course_category.update_title';
    const button = 'common.save';
    this.openCourseCategoryDetailPopup(current, title, button);
  }

  onCreateCourseCategory() {
    const current: any = {
      name: '',
      info: '',
      parent: this.currentSelectedId
    };
    const title = 'course_category.create_title';
    const button = 'common.save';
    this.openCourseCategoryDetailPopup(current, title, button);
  }

  openCourseCategoryDetailPopup(courseCategory: any, title, button){
    const modalDep = this.modalService.open(QuestionCategoryEditComponent, {
      size: 'lg' as any,
      centered: true,
      backdrop: 'static'
    });

    modalDep.componentInstance.category = courseCategory;
    modalDep.componentInstance.categories = this.categoryTreeData.dataSource;
    modalDep.componentInstance.title = title;
    modalDep.componentInstance.button = button;
    modalDep.componentInstance.updateCategory.subscribe(($e) => {
      this.reloadData();
    });
  }

  onDeleteMultipleCourse() {
    this.deleteCategory([...this.courseCateIdsChecked]);
  }

  onDeleteSingleCourse(categoryId: number) {
    this.deleteCategory([categoryId]);
  }

  deleteCategory(category: number []) {
    const modalDep = this.modalService.open(ConfirmModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });

    modalDep.componentInstance.title = this.translateService.instant('course_category.delete_confirm_title');
    // tslint:disable-next-line:max-line-length
    modalDep.componentInstance.body = this.translateService.instant('course_category.delete_confirm_content');
    modalDep.componentInstance.confirmButton = this.translateService.instant('common.delete');

    modalDep.result.then((result) => {
      this.spinner.show();
      this.categoryService.deleteQuestionCate(category).subscribe(
        res => {
          this.spinner.hide();
          // if (res.body.errors.length > 0)
          // {
          //   let ids = '';
          //   res.body.errors?.forEach(element => {
          //     category.splice(category.indexOf(element.id), 1);
          //     ids += (element.name + '\n');
          //   });
          //   this.toastrService.successCate(`common.noti.delete_err_cate`,  ids );
          // } else {
          //   this.toastrService.success(`common.noti.delete_success`);
          // }
          [...category]?.forEach(courseId => {
            this.courseCateIds.splice(this.courseCateIds.indexOf(courseId), 1);
            this.courseCateIdsChecked.splice(this.courseCateIdsChecked.indexOf(courseId), 1);
          });
          this.toastrService.success(`common.noti.delete_success`);
          this.reloadData();
        },
        err => {
          // Neu co loi xay ra => giu nguyen trang thai truoc do cua checkbox
          this.spinner.hide();
          this.toastrService.handlerError(err);
        }
      );
    });
  }

  /**
   * clear select node
   * clear search param
   */
  clear(){
    // param phuc vu tim kiem
    this.keyword = '';
    this.currentSelectedId = null;
    this.pageSize = 10;
    this.pageIndex = 1;

    // Bo select node tren cay
    this.categoryTree.selectedNodes = [];

    this.onSearch();
  }

  nodeSelected(e) {
    this.courseCateIdsChecked = [];
    this.currentSelectedId = +this.categoryTree.getNode(e.node).id;
    this.pageSize = 10;
    this.pageIndex = 1;
    this.sortColumn = 'id';
    this.sortType = 'ASC';
    this.onSearch();
  }

  reloadData(){
    this.onSearch();
    this.getCategoryTree();
  }

  sortData(sort: Sort) {
    this.pageIndex = 1;
    this.sortColumn = sort.active;
    this.sortType = sort.direction;
    this.onSearch();
  }

  changePage(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  onChangeKeyWord() {
    this.pageIndex = 1;
    this.courseCateIdsChecked = [];
    this.onSearch();
  }
  backToRoute(){
    if(this.type=='course'){
      this.router.navigate(['/admin/course/detail', this.courseId],{queryParams:{selectedId:5}});
    }else if(this.type=='exam')
    this.router.navigate(['/admin/exam/detail', this.courseId],{queryParams:{selectedId:4}});
  }

  getInfoCourse(courseId: any) {
    this.courseService.getCoursesInfo(courseId).subscribe(response => {
      this.courseDetail = response.body;
      this.onSearch();
      this.getCategoryTree();
    });
  }

}
