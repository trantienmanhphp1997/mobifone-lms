import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CourseService } from 'src/app/shared/services/course.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';

@Component({
  selector: 'app-clone-confirm',
  templateUrl: './clone-confirm.component.html',
  styleUrls: ['./clone-confirm.component.css']
})
export class CloneConfirmComponent implements OnInit {
  @Input() courseClone: any;
  @Input() data: any;
  @Output() emitter: EventEmitter<any> = new EventEmitter();
  requestcontent: string = '';
  constructor(
    public activeModal: NgbActiveModal,
    private courseService: CourseService,
    private toastrService: ToastrCustomService,
  ) { }

  ngOnInit(): void {
  }
  sendRequest(){
    if(this.requestcontent.trim() ==""){
      this.toastrService.error(`Nội dung yêu cầu không được bỏ trống`);
    }
    else if(this.courseClone){
      const params ={
        courseid:this.courseClone.id,
        requestcontent: this.requestcontent
      }
      this.courseService.cloneCourseRequest(params).subscribe( data =>{
        if(data){
          this.toastrService.success(`common.noti.send_request_clone`);
          this.activeModal.dismiss();
        }
      })
    }
    else if(this.data){
      const params ={
        planid:this.data.id,
        requestcontent: this.requestcontent
      }
      this.courseService.editTrainingRequest(params).subscribe( data =>{
        if(data){
          this.toastrService.success(`common.noti.send_request_clone`);
          this.emitter.emit({
            success: true,
          });
          this.activeModal.dismiss();
        }
      })
    }
  }
}
