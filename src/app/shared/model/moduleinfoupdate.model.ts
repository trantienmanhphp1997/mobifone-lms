import {IntroeditorModel} from './introeditor.model';
import {PageModel} from './page.model';

export interface ModuleinfoupdateModel {
  modulename: string;
  name: string;
  course: number;
  section: number;
  coursemodule: number;
  introeditor: IntroeditorModel;
  page: PageModel;
  instance: number;
  timelimit: number;
  timeopen?: number;
  timeclose?: number;
  openingtime?: number;
  closingtime?: number;
}


