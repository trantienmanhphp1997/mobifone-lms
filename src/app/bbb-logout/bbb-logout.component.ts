import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bbb-logout',
  templateUrl: './bbb-logout.component.html',
  styleUrls: ['./bbb-logout.component.css']
})
export class BbbLogoutComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
    window.close();
  }

}
