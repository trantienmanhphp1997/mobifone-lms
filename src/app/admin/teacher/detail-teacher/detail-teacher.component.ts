import { Component, OnInit ,Input} from '@angular/core';
import {FormBuilder, FormControl, Validators} from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Department } from 'src/app/shared/model/department.model';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { UserService } from 'src/app/shared/services/user.service';
import { CourseService } from 'src/app/shared/services/course.service';
import { BadgeService } from 'src/app/shared/services/badge.service';
import {CustomValidators} from '../../../shared/utils/custom-validators';
import { NgxSpinnerService } from 'ngx-spinner';
import * as moment from 'moment';
import { Router } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CreateBadgeComponent } from '../../../admin/teacher/detail-teacher/create-badge/create-badge.component';
import { USER_INFO, USER_FUNCTIONS, USER_ROLE, LOCAL_STORAGE_KEY, TEACHER_ROLE } from '../../../shared/constants/base.constant';
import { ConfirmModalComponent } from 'src/app/shared/modal/confirm-modal/confirm-modal.component';
import { TranslateService } from '@ngx-translate/core';
import CommonUtil from 'src/app/shared/utils/common-util';
import { ApprovalBadgeComponent } from '../../../admin/teacher/approval-badge/approval-badge.component'
@Component({
  selector: 'app-detail-teacher',
  templateUrl: './detail-teacher.component.html',
  styleUrls: ['./detail-teacher.component.css']
})
export class DetailTeacherComponent implements OnInit {
  @Input() teacher: any;
  departmentList: Department[] = [];
  formEnable = false;
  BadgeTeacherList: any[]=[];
  currentDate: Date = new Date();
  historyDate: any;
  disabled: true;
  historyPosition: any[]=[];
  roles:any;
  role: string;
  isTeacher: boolean = false;
  isAdmin: boolean = false;
  isManager: boolean = false;
  id:any;
  departmentMap = new Map();
  selectedDepartmentFirst: any;
  teacherCouser = {
    pageIndex: 1,
    pageSize: 10,
  };
  BagdeCouser = {
    pageIndexB: 1,
    pageSizeB: 10,
  }
  pageSizeOptions: number[] = [10, 25, 50, 100];
  pageSizeOptionsB: number[] = [10, 25, 50, 100];
  TeacherCouserList: any[]=[];
  totalRecord: number;
  totalRecordBadde:number;
  roleList = TEACHER_ROLE;
  listChange: any;
  constructor(
    private fb: FormBuilder,
    private route: ActivatedRoute,
    private userService: UserService,
    private toastService: ToastrCustomService,
    private departmentService: DepartmentService,
    private spinner: NgxSpinnerService,
    private couserService:CourseService,
    private badgeService:BadgeService,
    private toastrService: ToastrCustomService,
    private router :Router,
    private $localStorage: LocalStorageService,
    private modalService: NgbModal,
    private translateService : TranslateService,
  ) { 
    this.teacherEditForm.disable();
  }

  teacherEditForm = this.fb.group({
    id: [],
    firstname: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(50)]],
    nameteacher: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(50)]],
    email: [],
    departmentid: ['', Validators.required],
    teacherrole: ['', Validators.required],
    timeconfirmed: [''],
    numberconfirmed:[''],
    durationconfirmed:[''],
  });

  ngOnInit(): void {
    this.roles=this.roleList;
    this.role = this.$localStorage.retrieve(USER_INFO.ROLES)[0].shortname;
    const roles = this.$localStorage.retrieve(USER_INFO.ROLES);
    if (roles) {
      for (const role of roles) {
        if (role.shortname === USER_ROLE.TEACHER) {
           this.isTeacher = true;
        }
        else if(role.shortname=== USER_ROLE.MANAGER){
            this.isManager = true
        }
        else if(role.shortname=== USER_ROLE.ADMIN){
            this.isAdmin = true
        }
      }
    }
    if(this.isTeacher==true){
      this.id=null;
    }
    else{
      this.id = parseInt(this.route.snapshot.paramMap.get('id'), 10) ?? 0;
    }
    
    this.roles=this.roleList;
    this.getBadgeTeacher();
    this.setDepartmentData();
    this.getInforTeacher();
    this.getTeacherCouserList();
    this.getHistoryChange();
  }

  getInforTeacher(){
    const param={
      ids:this.id,
    }
    this.userService.searchUser(param).subscribe(data=>{
      this.initFormTeacherDetail(data.body.results[0]);
    },err=>{
      this.toastService.handlerError(err);
    })
  }

  initFormTeacherDetail(teacher:any){
    const timeconfirmed = teacher.timeconfirmed ? moment.unix(teacher.timeconfirmed).toDate() : null;
    const durationconf = teacher.durationconfirmed ? moment.unix(teacher.durationconfirmed).toDate() : null;
    this.teacherEditForm.patchValue({
      id: teacher.id,
      firstname: teacher.firstname,
      nameteacher: teacher.fullname.slice(teacher.firstname.length+1,teacher.fullname.length),
      email: teacher.email,
      departmentid: teacher.departmentid,
      teacherrole: teacher.teacherrole,
      numberconfirmed: teacher.numberconfirmed,
      timeconfirmed: timeconfirmed,
      durationconfirmed: durationconf,
    })
  }

  onUpdateTeacher() {
    this.teacherEditForm.enable();
    this.disabled=this.disabled
    this.formEnable = true;
    this.enableForm();
    this.teacherEditForm.controls['firstname'].disable();
    this.teacherEditForm.controls['email'].disable();
    this.teacherEditForm.controls['nameteacher'].disable();
    this.teacherEditForm.controls['departmentid'].disable();
    this.teacherEditForm.controls['teacherrole'].disable();
  }

  cancelUpdateTeacher(){
    this.teacherEditForm.removeControl('teacherrole');
    if(this.teacherEditForm.value.teacherrole==3){
      this.teacherEditForm.removeControl('timeconfirmed');
    }
    this.teacherEditForm.addControl('teacherrole', new FormControl('', Validators.required));
    this.getInforTeacher();
    this.teacherEditForm.disable();
    this.formEnable = false;
  }

  updateTeacher(){
    const paramUD = {
      id:this.teacherEditForm.value.id,
      teacherrole:this.teacherEditForm.value.teacherrole,
      timeconfirmed:this.teacherEditForm.value.timeconfirmed ? this.teacherEditForm.value.timeconfirmed.getTime() / 1000 : null,
      numberconfirmed: this.teacherEditForm.value.numberconfirmed,
      durationconfirmed: this.teacherEditForm.value.durationconfirmed ? this.teacherEditForm.value.durationconfirmed.getTime() / 1000 : null,
    }
    this.spinner.show();
    this.userService.userUpdateTeacher(paramUD).subscribe((data) => {
      this.spinner.hide();
      this.teacherEditForm.disable();
      this.formEnable = false;
      this.getHistoryChange()
    });
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  checkTimeStart(item) {
    if(item.planid !== null && item.startdate < (new Date().getTime() / 1000) && !item.published){
      return "text-danger";
    }
  }
  
  enableForm(){
    if (this.teacherEditForm.get('departmentid').value?.toString() == null) {
      this.teacherEditForm.controls['teacherrole'].setValue(this.roleList[0].value);
      this.roles= this.roleList.filter(role =>role.value == this.roleList[0].value)
      this.teacherEditForm.controls['timeconfirmed'].disable();
      this.teacherEditForm.controls['numberconfirmed'].disable();
      this.teacherEditForm.controls['durationconfirmed'].disable();
    }
    else{
      this.roles=this.roleList.filter(x => x.value != 1);
      if(this.teacherEditForm.value.teacherrole==3){
        this.teacherEditForm.controls['timeconfirmed'].enable();
        this.teacherEditForm.controls['numberconfirmed'].enable();
      this.teacherEditForm.controls['durationconfirmed'].enable();
      }else{
        this.teacherEditForm.controls['timeconfirmed'].disable();
        this.teacherEditForm.controls['durationconfirmed'].disable();
        this.teacherEditForm.controls['numberconfirmed'].disable();
        this.teacherEditForm.controls['timeconfirmed'].setValue(null);
      }
    }
  }
  setDepartmentData() {
      this.departmentService.getUserDepartmentTree(1).subscribe((data) => {
        data.body.forEach(department => {
          if (department.parentid !== null) {
            const parentName = this.departmentMap.get(department.parentid);
            let name = '';
            if (parentName) {
              name = parentName + ' / ' + department.name;
            } else {
              name = department.name;
            }
            this.departmentMap.set(department.id, name);
          } else {
            this.departmentMap.set(department.id, department.name);
            this.selectedDepartmentFirst = {key: department.id, value: department.name};
          }
        });
    });
  }
  getTeacherCouserList(){
    const params = {
      limit: this.teacherCouser.pageSize,
      page: this.teacherCouser.pageIndex,
      teacherids: [this.id]
    };
    this.couserService.searchCourses(params).subscribe((data) => {
      data.body?.results?.forEach(course => {
        if (this.currentDate > moment.unix(course.enddate).toDate()) {
          course.status = 'Đã kết thúc';
          course.style = 'success';
        }
        if (this.currentDate < moment.unix(course.startdate).toDate()) {
          course.status = 'Chưa bắt đầu';
          course.style = 'secondary';
        }
        if (moment.unix(course.startdate).toDate() < this.currentDate && this.currentDate < moment.unix(course.enddate).toDate()) {
          course.status = 'Đang diễn ra';
          course.style = 'info';
        }
      });
      this.TeacherCouserList = data.body.results;
      this.totalRecord = data.body.total;
    });
  }
  getBadgeTeacher(){   
    const paramC = {
      limit: this.BagdeCouser.pageSizeB,
      page: this.BagdeCouser.pageIndexB,
      userid: this.id
    };
    this.badgeService.badgeListTeacher(paramC).subscribe((data) => {
      this.BadgeTeacherList = data.body.results;
      this.totalRecordBadde = data.body.total;
    });
  }
  deApprovalBadge(status: any, issuedid:any){
    const modalDep = this.modalService.open(ApprovalBadgeComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.status = status;
    modalDep.componentInstance.issuedid = issuedid;
    modalDep.componentInstance.newBad.subscribe(($e) => {
      this.getBadgeTeacher();
    });
  }
  onApprovalBadge(status: any, issuedid: any){
    const paramU ={
        status :status,
        issuedid: issuedid
    }
    const modalDep = this.modalService.open(ConfirmModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    let bodyModal = status == 1 ? 'duyệt ' : "từ chối "
    modalDep.componentInstance.title = 'Xác nhận';
    modalDep.componentInstance.body = `Bạn có chắc chắn muốn  ${bodyModal} chứng chỉ này`;
    modalDep.componentInstance.confirmButton = 'Xác nhận';

    modalDep.result.then((result) => {
      this.spinner.show();
      this.badgeService.approveExternalBadge(paramU).subscribe(
        res => {
          this.toastrService.success('Phê duyệt thành công');
          this.getBadgeTeacher();
          this.spinner.hide();
        },
        err => {
          this.spinner.hide();
          this.toastrService.handlerError(err);
        });
    });
  }

  deleteTecherBadge(ids: number[]) {
    const modalDep = this.modalService.open(ConfirmModalComponent, {
        size: 'lg',
        centered: true,
        backdrop: 'static'
    });
    modalDep.componentInstance.title = this.translateService.instant('teacherBadge.delete_confirm_title');
    modalDep.componentInstance.body = this.translateService.instant('teacherBadge.delete_confirm_content');
    modalDep.componentInstance.confirmButton = this.translateService.instant('common.delete');
    modalDep.result.then((result) => {
      this.spinner.show();
      const paramD = {
        ids: ids,
        keep: 0
      };
      this.badgeService.deleteTeacherBadge(paramD).subscribe((data) => {
        this.toastrService.success('Xóa chứng chỉ thành công');
        this.spinner.hide();
        this.getBadgeTeacher();
      },
      err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      });
    });
  }
  changePage(event) {
    this.teacherCouser.pageIndex = event.pageIndex + 1;
    this.teacherCouser.pageSize = event.pageSize;
    this.getTeacherCouserList();
  }
  changePageBadge(event){
    this.BagdeCouser.pageIndexB = event.pageIndexB + 1;
    this.BagdeCouser.pageSizeB = event.pageSizeB;
    this.getBadgeTeacher();
  }
  goToCourseDetail(courseid) {
    this.$localStorage.store(LOCAL_STORAGE_KEY.SEARCH.COURSE_SEARCH, this.teacherCouser);
    this.router.navigate(['/admin/course/detail', courseid]);
  }
  openCreateBadge(){
    const modalDep = this.modalService.open(CreateBadgeComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.newBad.subscribe(($e) => {
      this.getBadgeTeacher();
    });
  }
  editCreateBadge(Badge:any){
    const modalDep = this.modalService.open(CreateBadgeComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.Badge = Badge;
    modalDep.componentInstance.newBad.subscribe( ($e) =>{
        this.getBadgeTeacher();
    })
  }
  getHistoryChange(){
    const param ={
      id: this.id,
    }
    this.userService.getHistoryChange(param).subscribe(data=>{
      this.listChange = data.body;
    },err=>{
      this.toastService.handlerError(err);
    })
  }
}
