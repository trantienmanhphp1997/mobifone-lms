import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgxSpinner, Spinner } from 'ngx-spinner/lib/ngx-spinner.enum';
import { LocalStorageService } from 'ngx-webstorage';
import { CourseService } from 'src/app/shared/services/course.service';
import { FileService } from 'src/app/shared/services/file.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
@Component({
  selector: 'app-roll-call-exmodal',
  templateUrl: './roll-call-exmodal.component.html',
  styleUrls: ['./roll-call-exmodal.component.css']
})
export class RollCallExmodalComponent implements OnInit {
  @Input() keyword: any;
  @Input() dataMonth: any;
  @Input() selectedIds: any;
  monthOptions: number[] = [];
  selectMonth = [];
  checkedMonthIds = [];
  dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 5,
    allowSearchFilter: true,
    searchPlaceholderText: 'Tìm kiếm'
  };
  courseId: any;
  constructor(
    public activeModal: NgbActiveModal,
    private $localStorage: LocalStorageService,
    private route: ActivatedRoute,
    private courseService: CourseService,
    private spinner: NgxSpinnerService,
    private toartService: ToastrCustomService,
    private fileService: FileService) { }

  ngOnInit(): void {
    this.courseId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
  }
  onDeSelectMonth($event) {
    [...this.selectMonth]?.filter(id => id !== $event.id);
  }
  onItemSelect($event) {
    this.checkedMonthIds.push($event);
  }
  onDeSelectAllMonth() {
    this.selectMonth = [];
    this.checkedMonthIds = [];
  }
  exportDetailAttend() {
    const params = {
      id: this.courseId,
      months: this.checkedMonthIds ? this.checkedMonthIds.toString() : '',
      departmentids: this.selectedIds ? this.selectedIds.toString() : '',
      keyword: this.keyword
    }
    this.spinner.show();
    this.courseService.exportDetailAttend(params).subscribe(res => {
      this.spinner.hide();
      window.open(this.fileService.getFileFromPathUrl(res.body.path));
    }, err => {
      this.spinner.hide();
      this.toartService.handlerError(err);
    });
    this.activeModal.dismiss()
  }
}
