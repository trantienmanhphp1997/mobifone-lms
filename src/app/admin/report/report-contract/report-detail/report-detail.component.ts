import { Component, OnInit } from '@angular/core';
import {NgxSpinnerService} from 'ngx-spinner';
import { ContractService } from 'src/app/shared/services/contract.service';
import * as moment from 'moment';
import { MatTabBody } from '@angular/material/tabs';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE_KEY } from 'src/app/shared/constants/base.constant';

@Component({
  selector: 'app-report-detail',
  templateUrl: './report-detail.component.html',
  styleUrls: ['./report-detail.component.css']
})
export class ReportDetailComponent implements OnInit {
  totalRecord: number;
  id:any;
  sub: any;
  listContractDetail: any;
  fullname:string;
  inputYear:any;
  departmentName: string;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  searchUser: any = {
    keyword: '',
    type: 0,
    pageSize: 10,
    pageIndex:1,
    year: null,
    status:''
  };
  listContract: any = {
    fullname:null,
    departmentName:null,
  }
  constructor(
    private spinner: NgxSpinnerService,
    private contractService: ContractService,
    private activatedRoute: ActivatedRoute,
    public $localStorage: LocalStorageService,
  ) {
    this.searchUser.year=this.getDefault();
  }

  ngOnInit(): void {
    this.sub = this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id') || 0;
      this.getListContractdetail();
    });
  }
  getListContractdetail(){
    const params = {
      keyword: this.searchUser.keyword,
      page: this.searchUser.pageIndex,
      id:this.id,
      year:this.searchUser.year,
      status: this.searchUser.status,

    };
    this.spinner.show();
    this.contractService.listContractDetail(params).subscribe(
      (data) => {
        this.spinner.hide();
        this.listContractDetail = data.body.results;
        this.totalRecord = data.body.total;
        this.departmentName = data.body.departmentname;
        this.listContract={
          fullname: data.body.fullname,
          departmentName:data.body.departmentname
        }
      }, error => {
        this.spinner.hide();
      });
  }
  onSearch(event:any){
    this.searchUser.seapageIndex=1;
    this.searchUser.keyword= event.target.value;
    this.getListContractdetail();
  }
  changePage(event) {
    this.searchUser.pageIndex = event.pageIndex + 1;
    this.searchUser.pageSize = event.pageSize;
    this.getListContractdetail();
  }
  onFilterStatus(contractFilter: any){
    this.searchUser.pageIndex=1;
    this.searchUser.status = contractFilter;
    this.getListContractdetail()
  }
  yearSelected(value: any) {
    if(value!==this.searchUser.year){
      this.searchUser.year = value ? value[0] : null;
      this.getListContractdetail();
    }
  }
  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }
  getDefault(){
    const currenSearch = this.$localStorage.retrieve(LOCAL_STORAGE_KEY.SEARCH.REPORT_STUDENT_SEARCH)
    return currenSearch.year;
  }
}
