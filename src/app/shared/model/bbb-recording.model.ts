export interface BbbRecording {
  recordid?: string;
  playbackurl?: string;
  name?: string;
  description?: string;
  preview?: BbbPreview;
  date?: number;
  duration?: number;
}

export interface BbbPreview {
  url?: string;
  width?: number;
  height?: number;
  alt?: string;
}
