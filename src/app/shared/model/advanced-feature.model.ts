export interface AdvancedFeature {
  name: string;
  value: boolean;
}
