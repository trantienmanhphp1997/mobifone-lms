import { NgxSpinnerService } from 'ngx-spinner';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import * as moment from 'moment';
import { CourseService } from '../../shared/services/course.service';
import { ONBOARING_CODE, USER_INFO } from 'src/app/shared/constants/base.constant';
import { LocalStorageService } from 'ngx-webstorage';
import { StaffAssessmentService } from 'src/app/shared/services/staff-assessment.service';
import { FormControl, Validators } from '@angular/forms';
import { UserService } from 'src/app/shared/services/user.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { PositionService } from 'src/app/shared/services/position.service';

@Component({
  selector: 'app-course-route-detail',
  templateUrl: './course-route-detail.component.html',
  styleUrls: ['./course-route-detail.component.css']
})
export class CourseRouteDetailComponent implements OnInit {
  student: any = {
    fullname: null,
    email: null,
    department: null
  };
  manager: any = {
    id: null,
    fullname: null,
    email: null,
    department: null
  };
  positionName: string;
  id: any;
  listCourseOfPosition: any = [];
  listExamOfPosition: any = [];
  courseCompletion: any = 0;
  examCompletion: any = 0;
  sub: any;
  criterias: any = []
  haveCriteria: any;
  currPosition: any;
  isOnBoarding = false;
  constructor(
    public $localStorage: LocalStorageService,
    private activatedRoute: ActivatedRoute,
    private courseService: CourseService,
    private spinner: NgxSpinnerService,
    private staffAssessmentService: StaffAssessmentService,
    private userService: UserService,
    private positioinService: PositionService,
    private toastrService: ToastrCustomService,
  ) { }
  animalControl = new FormControl('', Validators.required);
  selectFormControl = new FormControl('', Validators.required);
  animals: any[] = [];
  ngOnInit(): void {
    this.spinner.show();
    this.positionName = history.state?.positionName;
    this.sub = this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id');
      this.getDetailPositon (this.id);
      this.getCourseByPosition(this.id);
      this.getExamByPosition(this.id);
    });
  }

  getDetailPositon (positionId: any) {
    return this.positioinService.getDetailPosition(positionId).subscribe((data => {
      this.currPosition = data.body;
      // if (this.currPosition?.pcode == ONBOARING_CODE) {
      //   this.isOnBoarding = true;
      // }
      this.getEvaluation();
      this.getListManager();
    }));
  }
  getListManager () {
    this.userService.getListManagerDepartment().subscribe(res => {
      this.spinner.hide();
     this.animals = res.body.results
    },
      err => {
        this.toastrService.handlerError(err);
        this.spinner.hide();
      })
  }
  getEvaluation() {
    this.staffAssessmentService.getEvaluationNoteEndUser(this.id ?? null).subscribe(res => {
      this.spinner.hide();
      this.criterias = res.body.criterias
      this.student = {
        ...this.student,
        fullname: res.body.fullname,
        email: res.body.email,
        department: res.body.department
      };

      this.manager = {
        ...this.manager,
        id: res.body.muserid,
        fullname: res.body.mfullname,
        email: res.body.memail,
        department: res.body.mdepartment
      }

      this.haveCriteria = res.body.evaluationid
    },
      err => {
        this.toastrService.handlerError(err);
        this.spinner.hide();
      })
  }
  assignManagerToEvaluation () {
    if(!this.animalControl.value?.id){
      this.toastrService.error('Bạn chưa chọn lãnh đạo đánh giá');
      return;
    }
    const params = {
        // positionid: 184,
        userid: this.student.id
    }
    this.spinner.show();
    this.staffAssessmentService.assignManagerToEvaluation(this.id , this.animalControl.value?.id).subscribe(res => {
        this.spinner.hide();
        // this.criterias = res.body.criterias
        this.toastrService.success('Gửi thành công');
        this.getEvaluation();
    },
    err => {
        this.toastrService.handlerError(err);
        this.spinner.hide();
    })
}
  getCourseByPosition(positionId: any) {
    const param = {
      positionid: positionId,
      coursetype: 1, // course
      sortcolumn: 'ordernumber',
      sorttype: 'ASC',
      limit: 0
    };
    return this.courseService.getCourseByPosision(param).subscribe((data => {
      this.courseCompletion = data.body.results.filter(c => c.completion === 100).length;
      this.listCourseOfPosition = data.body.results;
    }));
  }

  getExamByPosition(positionId: any) {
    const param = {
      positionid: positionId,
      coursetype: 2, // exam
      sortcolumn: 'ordernumber',
      sorttype: 'ASC',
      limit: 0
    };
    return this.courseService.getCourseByPosision(param).subscribe((data => {
      this.examCompletion = data.body.results.filter(e => e.completion === 100).length;
      this.listExamOfPosition = data.body.results;
      this.spinner.hide();
    }));
  }

  convertTotDate(date: any) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }
}
