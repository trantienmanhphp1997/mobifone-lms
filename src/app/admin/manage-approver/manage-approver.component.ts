import { Component, OnInit } from '@angular/core';
import { PageEvent } from '@angular/material/paginator';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TranslateService } from '@ngx-translate/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { AddSingleUserComponent } from '../course-content/add-user/add-single-user/add-single-user.component';

@Component({
  selector: 'app-manage-approver',
  templateUrl: './manage-approver.component.html',
  styleUrls: ['./manage-approver.component.css']
})
export class ManageApproverComponent implements OnInit {
  approvers: any[] = [];
  pageSizeOptions: number[] = [10, 25, 50, 100];
  totalRecord: number;
  pageEvent: PageEvent;
  completedAll = false;
  badgeIdsChecked: number[] = [];
  badgeIds: number[] = [];

  searchApprover = {
    keyword: '',
    pageIndex: 1,
    pageSize: 10,
    sortColumn: 'timecreated',
    sortType: 'desc',
  };
  constructor(    
    private modalService: NgbModal,
    // private badgeService: BadgeService,
    private translateService: TranslateService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
  ) { }

  ngOnInit(): void {
  }

  openAddSingleStudentPopup() {

    const modalRef = this.modalService.open(AddSingleUserComponent, {
      size: 'xl',
      centered: true,
      backdrop: 'static',
      scrollable: false
    });
    modalRef.result.then((studentIds) => {
      if (studentIds) {
        this.spinner.show();
      }
    });
    
  }
}
