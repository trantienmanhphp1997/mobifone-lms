import { Component, OnInit, ViewChild } from '@angular/core';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { Department } from 'src/app/shared/model/department.model';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { Router } from '@angular/router';
import { UserService } from 'src/app/shared/services/user.service'
import * as moment from 'moment';
import { TEACHER_ROLE } from 'src/app/shared/constants/base.constant';
@Component({
  selector: 'app-teacher',
  templateUrl: './teacher.component.html',
  styleUrls: ['./teacher.component.css']
})
export class TeacherComponent implements OnInit {
  @ViewChild('departmentTree')
  public departmentTree: TreeViewComponent;
  public departmentTreeData;
  @ViewChild('departmentTreeNB')
  public departmentTreeNB: TreeViewComponent;
  public departmentTreeDataNB;

  departmentList: Department[] = [];
  departmentIds: number[] = [];
  departmentId: any = '';
  departmentIdsChecked: number[] = [];
  currentSelectedId: number;
  teacherList: any[] = [];
  currentSelectedDepartmentId: number;
  searchTeacher ={
    pageIndex: 1,
    pageSize: 10,
    issuestatus: null,
    keyword:null,
    teacherrole:null,
  }

  roleList = TEACHER_ROLE;
  totalRecord: number;
  pageSizeOptions: number[] = [10, 25, 50, 100];

  constructor(
    private departmentService: DepartmentService,
    private spinner: NgxSpinnerService,
    private toastrService: ToastrCustomService,
    private router: Router,
    private userService: UserService,
  ) { }

  ngOnInit(): void {
    this.reLoadData();
  }

  listAllTree() {
    this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
      this.departmentList = data.body;
      // set thang cha = null de no hien thi len duoc cay
      this.departmentList[0].parentid = null;
      this.departmentList.forEach(value => {
        this.departmentList.filter(department => department.id === value.parentid);
        value.expanded = value.parentid == null;
      });
      this.departmentTreeData = {
        dataSource: this.departmentList,
        id: 'id',
        parentID: 'parentid',
        text: 'name',
        hasChildren: 'haschild'
      };
      this.departmentTreeDataNB = {
          dataSource: this.departmentList,
          id: 'id',
          parentID: 'parentid',
          text: 'name',
          hasChildren: 'haschild'
        };
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      this.toastrService.handlerError(error);
    });
  }

  nodeSelected(e) {
    this.departmentIds = [];
    this.departmentIdsChecked = [];
    this.departmentTree.checkedNodes.forEach((node) => {
        this.departmentIds.push(parseInt(node, 10));
      });
    this.getListTeacher();
  }

  nodeCheck(args: any): void {
    const checkedNode: any = [args.node];
    if (args.event.target.classList.contains('e-fullrow') || args.event.key == 'Enter') {
      // @ts-ignore
      const getNodeDetails: any = this.departmentTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.departmentTree.uncheckAll(checkedNode);
      } else {
        this.departmentTree.checkAll(checkedNode);
      }
    }
  }

  nodeDepartmentSelected(e: any) {
    this.currentSelectedDepartmentId = +this.departmentTree.selectedNodes;
    this.currentSelectedDepartmentId = +this.departmentTreeNB.selectedNodes;
    this.getListTeacher();
  }

  setUpTree(dataSource: any) {
    const indexOfCurrentSelectedId = dataSource.findIndex(x => x.id === this.currentSelectedId);
    if (indexOfCurrentSelectedId === -1) {
        this.currentSelectedId = null;
    }
    dataSource[0].parentid = null;
    dataSource.forEach(value => {
        if (value.parentid == null) {
            value.expanded = true; 
        } else {
            value.expanded = false;
        }
        value.isChecked = true;
    });
  }
  getListTeacher() {
    const params = {
      limit: this.searchTeacher.pageSize,
      page: this.searchTeacher.pageIndex,
      departmentids: this.departmentIds ? this.departmentIds.toString() : [],
      teacherrole: this.searchTeacher.teacherrole,
      keyword:this.searchTeacher.keyword,
      issuestatus:this.searchTeacher.issuestatus,
    };
    this.userService.listTeacher(params).subscribe((data) => {
      this.teacherList = data.body.results;
      this.totalRecord = data.body.total;
    });
  }

  reLoadData() {
    this.listAllTree();
    this.getListTeacher();
  }

  goToDetail(id: any){
    this.router.navigate(['/admin/teacher/detail-teacher', id]);
  }

  changePage(event) {
    this.searchTeacher.pageIndex = event.pageIndex + 1;
    this.searchTeacher.pageSize = event.pageSize;
    this.getListTeacher();
  }
  
  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  changeRole($event){
    this.searchTeacher.teacherrole = $event.target.value;
    this.getListTeacher();
  }

  changeStatus($event){
    this.searchTeacher.issuestatus = $event.target.value;
    this.getListTeacher();
  }

  changeName(){
    // this.searchTeacher.keyword = $event.target.value;
    this.searchTeacher.pageIndex=1;
    this.getListTeacher();
  }
}
