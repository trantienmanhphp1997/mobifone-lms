export interface Badge {
  id?: number;
  name: string;
  image?: string;
  description?: string;
  version?: number;
  expiredate?: number;
  expireperiod?: number;
  status?:number;
}


