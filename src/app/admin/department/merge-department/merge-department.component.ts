import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import {Sort} from '@angular/material/sort';
@Component({
  selector: 'app-merge-department',
  templateUrl: './merge-department.component.html',
  styleUrls: ['./merge-department.component.css']
})
export class MergeDepartmentComponent implements OnInit {

  @Input() updateMerge: any;
  keyword = '';
  totalRecord: number;
  pageSize = 10;
  pageIndex = 1;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  sortColumn = 'id';
  sortType = 'ASC';

  departmentList:any;
  constructor(
    public departmentService: DepartmentService,
    public fb: FormBuilder,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
  ) {
    
  }
  ngOnInit(): void {
    this.onSearch();
  }
  onChangeKeyWord(){
    this.pageIndex = 1;
    this.onSearch();
  }
  sortData(sort: Sort) {
    this.pageIndex = 1;
    this.sortColumn = sort.active;
    this.sortType = sort.direction;
    this.onSearch();
  }
  onSearch() {
    const params = {
      limit: this.pageSize,
      page: this.pageIndex,
      keyword: this.keyword,
      sortcolumn: this.sortColumn,
      sorttype: this.sortType,
      merged: 1
    };
    this.spinner.show();
    this.departmentService.searchDepartment(params).subscribe((data) => {
      this.departmentList=data.body.results
      this.totalRecord = data.body.total;
      this.spinner.hide();
    },
      error => {
      this.toastrService.handlerError(error);
      this.spinner.hide();
      });
  }
  changePage(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

}
