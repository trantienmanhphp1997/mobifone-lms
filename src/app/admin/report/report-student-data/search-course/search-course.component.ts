import { PageEvent } from '@angular/material/paginator';
import { Component, EventEmitter, Input, OnInit, Output, ViewEncapsulation } from '@angular/core';
import {Course} from '../../../../shared/model/course.model';
import {CourseService} from '../../../../shared/services/course.service';
import {NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ToastrCustomService} from '../../../../shared/services/toastr-custom.service';
import {COURSE_TYPE, ONBOARING_CODE} from '../../../../shared/constants/base.constant';
import * as moment from 'moment';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-search-course',
  templateUrl: './search-course.component.html',
  styleUrls: ['./search-course.component.css']
})
export class SearchCourseComponent implements OnInit {

  public activeIndex: any = 0;
  @Output() transferCourseIds = new EventEmitter<any>();
  @Input() courseIdStr: any;
  courseIdsChecked = [];
  completedAll = false;
  course: Course[] = [];
  courses = new Array();
  courseIds = new Array();
  coursetype: number;

  
  pageSizeOptions: number[] = [10, 25, 50, 100];
  totalRecord: number;
  search = '';
  pageIndex = 1;
  pageSize = 10;
  sortcolumn = 'timecreated';
  sorttype = 'DESC';

  // MatPaginator Output
  pageEvent: PageEvent;

  constructor(
    public activeModal: NgbActiveModal,
    private courseService: CourseService,
    private toastrService: ToastrCustomService,
    private modalService: NgbModal,
    private spinner: NgxSpinnerService
  ) {}

  ngOnInit() {
    if(this.courseIdStr){
      this.courseIdsChecked = this.courseIdStr.split(',').map(id=> {return parseInt(id);});
    }
    this.coursetype = COURSE_TYPE.COURSE;
    this.getReportCourses();
  }

  oncheckboxItem(courseId: number, checked: any) {
    if (checked) {
      this.courses.forEach(c => {
        if (c.id === courseId) {
          c.completed = true;
          this.courseIds.push(courseId);
          this.courseIdsChecked?.push(courseId);
          return;
        }
      });
      // nếu số khóa checked bằng số khóa học có thể checked trong page thì set checkboxall = true
      if (this.courseIds?.length > 0 && this.courseIds?.length === this.courses?.length && !this.completedAll) {
        this.completedAll = true;
      }
    } else {
      this.completedAll = false;
      this.courses?.forEach(c => {
        if (c.id === courseId) {
          c.completed = false;
          this.courseIds?.splice(this.courseIds?.indexOf(courseId), 1);
          this.courseIdsChecked?.splice(this.courseIdsChecked?.indexOf(courseId), 1);
          return;
        }
      });
    }
  }

  oncheckboxAll(checked: any) {
    if (checked) {
      this.courses.forEach(c => {
        c.completed = checked;
        if (!this.courseIds.includes(c.id)) {
          this.courseIds.push(c.id);
        }
        if (!this.courseIdsChecked?.includes(c.id)) {
          this.courseIdsChecked?.push(c.id);
        }
      });
      this.completedAll = true;
    } else {
      this.courseIds?.forEach(id => {
        this.courseIdsChecked?.splice(this.courseIdsChecked?.indexOf(id), 1);
      });
      this.courseIds = [];
      this.courses?.forEach(c => {
        c.completed = false;
      });
      this.completedAll = false;
    }
  }

  cancel() {
    this.activeModal.dismiss();
  }

  save() {
    this.activeModal.close(this.courseIds[0]);
    this.modalService.dismissAll();
    this.transferCourseIds.emit(this.courseIdsChecked?.toString());
  }

  onSearch() {
    this.pageIndex = 1;
    this.getReportCourses();
  }

  // lay ds khoa hoc cho bao cao
  getReportCourses() {
    this.spinner.show();
    const params = {
      limit: this.pageSize,
      page: this.pageIndex,
      search: this.search,
      showmanagedcourse: 1,
      status: 5
    };
    this.courseIds = [];
    this.courseService.searchCourses(params).subscribe(
      data => {
        this.courses = data.body.results;
        this.totalRecord = data.body.total;
        
        this.courses?.forEach(c => {
          if (this.courseIdsChecked?.includes(c.id)) {
            c.completed = true;
            if (!this.courseIds?.includes(c.id)) {
              this.courseIds?.push(c.id);
            }
          }
        });
        // nếu số khóa học được checked = số khóa học có thể checked thì set completedAll = true
        if (this.courseIds?.length > 0 && (this.courseIds?.length === this.courses?.length)) {
          this.completedAll = true;
        } else {
          this.completedAll = false;
        }
        this.spinner.hide();
      }, err => {
        this.spinner.hide();
        this.toastrService.error(err);
      }
    );
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }


  changePage(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.getReportCourses();
  }

}
