import { HttpResponse } from '@angular/common/http';
import { UserInfo } from '../model/user-info.model';
import { AbstractService } from './abstract.service';
import { Observable } from 'rxjs';
import CommonUtil from '../utils/common-util';
import { MOODLE_INFO } from '../constants/base.constant';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ModuleInfo } from '../model/moduleinfo.model';
import { User } from "../model/user.model";

type EntityResponseType = HttpResponse<any>;
type EntityArrayResponseType = HttpResponse<any[]>;

interface UserPageResponse {
    results: UserInfo[];
    total: number;
}

@Injectable({
    providedIn: 'root'
})
export class CriteriaService {
    constructor(private abstractService: AbstractService) {}

    getListCriteria(param: any): Observable<HttpResponse<any>> {
        const data = this.abstractService.getCommonInput('mobifone_position_list_criteria');
        CommonUtil.appendDataToFormData(data, 'params', param);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }
    getListCourseCriteria(param: any): Observable<HttpResponse<any>> {
        const data = this.abstractService.getCommonInput('mobifone_course_list_criteria');
        CommonUtil.appendDataToFormData(data, 'params', param);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }
    assignCriteria(criteriaids): Observable<EntityArrayResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_course_assign_criteria');
        CommonUtil.appendDataToFormData(data, 'criteriaids', criteriaids);
        return this.abstractService.post(MOODLE_INFO.SERVICE_URL, data)
          .pipe(map((res: EntityArrayResponseType) => res));
    }
    unAssignCriteria(criteriaids): Observable<EntityArrayResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_course_unassign_criteria');
        CommonUtil.appendDataToFormData(data, 'criteriaids', criteriaids);
        return this.abstractService.post(MOODLE_INFO.SERVICE_URL, data)
          .pipe(map((res: EntityArrayResponseType) => res));
      }
    createOrUpdateCriteria(param: any): Observable<HttpResponse<any>> {
        const functionName = param.id ? 'mobifone_position_update_criteria' : 'mobifone_position_create_criteria'
        const data = this.abstractService.getCommonInput(functionName);
        CommonUtil.appendDataToFormData(data, 'criteria', param);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }

    deleteCriteria(idCri: any): Observable<EntityArrayResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_position_delete_criteria');
        CommonUtil.appendDataToFormData(data, 'ids', idCri);
        return this.abstractService.post<UserInfo[]>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: EntityArrayResponseType) => res));
    }

    createUser(newUser: UserInfo): Observable<EntityResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_user_create_user');
        CommonUtil.appendDataToFormData(data, 'user', newUser);
        return this.abstractService.post<UserInfo>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: EntityResponseType) => res));
    }

    updateUser(userUpdate: any): Observable<EntityResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_user_update_user');
        CommonUtil.appendDataToFormData(data, 'user', userUpdate);
        return this.abstractService.post<UserInfo>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: EntityResponseType) => res));
    }

    importUserFormFile(file: File): Observable<EntityResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_user_import_user');
        data.append('file', file, file.name);
        CommonUtil.appendDataToFormData(data, 'file', file);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: EntityResponseType) => res));
    }

    findUserByUsername(username: string): Observable<HttpResponse<boolean>> {
        const data = this.abstractService.getCommonInput('core_auth_request_password_reset');
        CommonUtil.appendDataToFormData(data, 'username', username);
        return this.abstractService.post<boolean>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<boolean>) => res));
    }

    findUserByEmail(email: string): Observable<HttpResponse<boolean>> {
        const data = this.abstractService.getCommonInput('core_auth_request_password_reset');
        CommonUtil.appendDataToFormData(data, 'email', email);
        return this.abstractService.post<boolean>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<boolean>) => res));
    }

    changeOwnPassword(params: any): Observable<any> {
        const data = this.abstractService.getCommonInput('mobifone_user_change_password');
        CommonUtil.appendRawDataToFormData(data, 'params', params);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: any) => res));
    }

    updateOwnDetail(file: File): Observable<EntityResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_user_update_own_detail');
        data.append('repo_upload_file', file, file.name);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: any) => res));
    }

    getUserInfo(): Observable<EntityResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_user_get_own_detail');
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: any) => res));
    }

    lockUser(idUser: number[]): Observable<EntityArrayResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_user_lock_user');
        CommonUtil.appendDataToFormData(data, 'userids', idUser);
        return this.abstractService.post<UserInfo[]>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: EntityArrayResponseType) => res));
    }

    unlockUser(idUser: number[]): Observable<EntityArrayResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_user_unlock_user');
        CommonUtil.appendDataToFormData(data, 'userids', idUser);
        return this.abstractService.post<UserInfo[]>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: EntityArrayResponseType) => res));
    }
}
