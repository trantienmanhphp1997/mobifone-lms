import { LOCAL_STORAGE_KEY } from './../../../shared/constants/base.constant';
import { Department } from 'src/app/shared/model/department.model';
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import {ReportService} from '../../../shared/services/report.service';
import {ToastrCustomService} from 'src/app/shared/services/toastr-custom.service';
import { DepartmentService } from '../../../shared/services/department.service';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { LocalStorageService } from 'ngx-webstorage';
import {FileService} from '../../../shared/services/file.service';
import {NgxSpinnerService} from 'ngx-spinner';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { TeacherSearchComponent } from './teacher-search/teacher-search.component';
import { UserService } from 'src/app/shared/services/user.service';

@Component({
  selector: 'app-report-teacher',
  templateUrl: './report-teacher.component.html',
  styleUrls: ['./report-teacher.component.css']
})
export class ReportTeacherComponent implements OnInit, AfterViewInit {

  @ViewChild('keyword') keyword: ElementRef;
  // @ViewChild('departmentTree')
  // public departmentTree: TreeViewComponent;
  // public departmentTreeData;
  dropdownTeacherSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'fullname',
    selectAllText: 'Chọn tất cả',
    unSelectAllText: 'Bỏ chọn tất cả',
    searchPlaceholderText: 'Tìm kiếm',
    itemsShowLimit: 5,
    allowSearchFilter: true
  };
  listTeacher: any = [];
  selectedTeacher: any;
  isCheckTeacherEvent: boolean = false;
  paramTeacher = {
    keyword: '',
    page: 1,
    limit: 10,
  }

  constructor(
    private reportService: ReportService,
    private toastrService: ToastrCustomService,
    private departmentService: DepartmentService,
    private $localStorage: LocalStorageService,
    private fileService: FileService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private userService: UserService,
  ) {}

  searchUser = {
    keyWord: '',
    pageIndex: 1,
    pageSize: 10,
    coursetype: '',
    departmentids: [],
    departmentData: [],
    departmentMergeData : [],
    departmentMergeIds: [],
    year: null,
    top: null,
    grade :null,
    categoryids: [],
    categories: [],
    teacherids: []
  };
  totalRecord: number;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  departmentList: Department[] = [];
  checkedDepartmentIds = [];
  listStudent: any;
  selectStudent: any;
  currentSelectedDepartmentId: number;
  userId: any;
  listCourse: any;
  listExam: any;
  courseType = [
    {
      id: 1,
      name: 'Khóa học',
    },
    {
      id: 2,
      name: 'Kỳ thi'
    }
  ];

  dropdownStudentsSettings = {
    allowSearchFilter: true,
    singleSelection: true,
    idField: 'id',
    textField: 'fullname',
    itemsShowLimit: 10,
  };

  selected: any = 1;
  elements: any;

  ngOnInit(): void {
    const currentSearch = this.$localStorage.retrieve(LOCAL_STORAGE_KEY.SEARCH.REPORT_TEACHER_SEARCH);
    const depart = this.$localStorage.retrieve(LOCAL_STORAGE_KEY.SEARCH.REPORT_CHECKED_DEPARTMENT);
    if (currentSearch && depart) {
      // Kiem tra xem truoc do co luu du lieu tim kiem khong
      this.searchUser.keyWord = currentSearch.keyWord;
      this.searchUser.pageIndex = currentSearch.pageIndex;
      this.searchUser.pageSize = currentSearch.pageSize;
      this.searchUser.departmentMergeIds = currentSearch.departmentMergeIds;
      this.searchUser.departmentMergeData = currentSearch.departmentMergeData;
      this.checkedDepartmentIds  = depart;
      // Xoa di sau khi su dung
      this.$localStorage.clear(LOCAL_STORAGE_KEY.SEARCH.REPORT_TEACHER_SEARCH);
      this.$localStorage.clear(LOCAL_STORAGE_KEY.SEARCH.REPORT_CHECKED_DEPARTMENT);
    }
    // this.listAllTree();
    this.getListStudent();
    this.onloadDropdownTeacher();
  }

  ngAfterViewInit() {
    // this.keyword.nativeElement.value = this.searchUser?.keyWord;
  }

  getListStudent() {
    let departmentIds = this.searchUser.departmentids;
    if(this.searchUser.departmentMergeIds){
      departmentIds = [...this.searchUser.departmentids,this.searchUser.departmentMergeIds];
    }
    const params = {
      top: this.searchUser.top ,
      page:  this.searchUser.pageIndex,
      keyword: this.searchUser.keyWord,
      departmentids: departmentIds ? departmentIds.toString() : "",
      limit: this.searchUser.pageSize,
      year: this.searchUser.year,
      grade: this.searchUser.grade,
      categoryids: this.searchUser.categoryids ? this.searchUser.categoryids.toString() : "",
      teacherids: this.searchUser.teacherids ? this.searchUser.teacherids.toString() : ""
      // sortcolumn: this.sortcolumn,
      // sorttype: this.sorttype
    };
    this.spinner.show();
    this.reportService.getListTeacher(params).subscribe((data) => {
      this.listStudent = data.body.results;
      this.totalRecord = data.body.total;
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      this.toastrService.handlerError(error);
    });
  }

  onItemChange(value) {
    this.userId = value;
  }

  onSearch(event){
    this.searchUser.pageIndex = 1;
    this.searchUser.keyWord = event.target.value;
    this.getListStudent();
  }

  changePage(event) {
    this.searchUser.pageIndex = event.pageIndex + 1;
    this.searchUser.pageSize = event.pageSize;
    this.getListStudent();
  }


  onSelectStudentEvent(event?: any) {
    this.onItemChange(event.id);
  }

  onDeSelectStudentEvent(event?: any) {
    if (event) {
      this.listCourse = [];
    }
  }

  // listAllTree() {
  //   this.spinner.show();
  //   this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
  //     this.departmentList = data.body;
  //     // set thang cha = null de no hien thi len duoc cay
  //     this.departmentList[0].parentid = null;
  //     this.departmentList.forEach(value => {
  //       // value.expanded = value.haschild !== null;
  //       this.departmentList.filter(department => department.id === value.parentid);
  //       value.expanded = value.parentid == null;
  //       // value.isChecked = true;
  //       // this.checkedDepartmentIds.push(value.id);
  //     });
  //     this.departmentTreeData = {
  //       dataSource: this.departmentList,
  //       id: 'id',
  //       parentID: 'parentid',
  //       text: 'name',
  //       hasChildren: 'haschild'
  //     };
  //     // this.loading.next(true);
  //     this.spinner.hide();
  //   }, error => {
  //     this.spinner.hide();
  //     this.toastrService.handlerError(error);
  //   });
  // }

  // chọn 1 đơn vị
  //  nodeDepartmentSelected(e: any) {
  //   this.currentSelectedDepartmentId = +this.departmentTree.selectedNodes;
  //   this.getListStudent();
  // }

  // public nodeCheck(args: any): void {
  //   const checkedNode: any = [args.node];
  //   if (args.event.target.classList.contains('e-fullrow') || args.event.key == 'Enter') {
  //     // @ts-ignore
  //     const getNodeDetails: any = this.departmentTree.getNodeData(args.node);
  //     if (getNodeDetails.isChecked == 'true') {
  //       this.departmentTree.uncheckAll(checkedNode);
  //     } else {
  //       this.departmentTree.checkAll(checkedNode);
  //     }
  //   }
  // }

  // chọn nhiều đơn vị
  // nodeDepartmentChecked(e) {
  //   this.checkedDepartmentIds = [];
  //   this.departmentTree.checkedNodes.forEach((node) => {
  //     this.checkedDepartmentIds.push(parseInt(node, 10));
  //   });
  //   this.searchUser.pageIndex = 1;
  //   this.getListStudent();
  //   // debugger;
  // }

  onExportLearningProgressStudents(){
    const params = {
      top: this.searchUser.top ,
      page:  this.searchUser.pageIndex,
      keyword: this.searchUser.keyWord,
      departmentids: this.searchUser.departmentids ? this.searchUser.departmentids.toString() : "",
      limit: this.searchUser.pageSize,
      year: this.searchUser.year,
      grade: this.searchUser.grade,

      
      // sortcolumn: this.sortcolumn,
      // sorttype: this.sorttype
    };
    this.spinner.show();
    this.reportService.exportTeacher(params).subscribe(
      res => {
        this.spinner.hide();
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
    }, err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
    });
  }

  goToDetail(year: any){
    this.searchUser.year = year;
    this.$localStorage.store(LOCAL_STORAGE_KEY.SEARCH.REPORT_TEACHER_SEARCH, this.searchUser);
    this.$localStorage.store(LOCAL_STORAGE_KEY.SEARCH.REPORT_CHECKED_DEPARTMENT, this.checkedDepartmentIds);
  }
  openTeacherSearchPopup() {
    const modalDep = this.modalService.open(TeacherSearchComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.searchCourse = this.searchUser;

    modalDep.result.then((result) => {
      if (result) {
        const departmentMergeIds = [];
        const departmentIds = [];
        const categoryids = [];
        if (result.departmentData) {
          this.searchUser.departmentData = result.departmentData;
        } else {
          this.searchUser.departmentData = null;
        }

        result.departmentData?.forEach((department) => {
          departmentIds.push(department.id);
        })

        if (departmentIds.length > 0) {
          this.searchUser.departmentids = departmentIds;
        } else {
          this.searchUser.departmentids = [];
        }

        if (result.departmentMergeData) {
          this.searchUser.departmentMergeData = result.departmentMergeData;
        } else {
          this.searchUser.departmentMergeData = null;
        }
        result.departmentMergeData?.forEach((departmentmerge) => {
          departmentMergeIds.push(departmentmerge.id);
        });
        if (departmentMergeIds.length > 0) {
          this.searchUser.departmentMergeIds = departmentMergeIds;
        } else {
          this.searchUser.departmentMergeIds = null;
        }
        if(result.categories) {
          this.searchUser.categories = result.categories;
        } else {
          this.searchUser.categories = null;
        }
        result.categories?.forEach((category) => {
          categoryids.push(category.id);
        })
        if(categoryids.length > 0) {
          this.searchUser.categoryids = categoryids;
        } else {
          this.searchUser.categoryids = null;
        }
        this.searchUser.year = result.year || null; 
        this.searchUser.top = result.top || null;
        this.searchUser.grade = result.grade || null;
        this.searchUser.pageIndex = 1;
        this.getListStudent();
      }
    });
  }

  onloadDropdownTeacher(): void {
    const param = {
      keyword: this.paramTeacher.keyword,
      page: this.paramTeacher.page,
      limit: this.paramTeacher.limit
    }
    this.userService.listTeacher(param).subscribe((res) => {
      this.listTeacher = res.body.results;
    })
  }

  changeTeacher(value: any): void {
    this.searchUser.teacherids = this.selectedTeacher;
    this.getListStudent();
  }

  onSearchTeacher(value: any): void {
    this.paramTeacher.keyword = value;
    setTimeout(() => {
      this.onloadDropdownTeacher();
    }, 800);
  }

  onScrollDown(): void {
    const limit = this.paramTeacher.limit + 10;
    this.paramTeacher.limit = limit
    this.onloadDropdownTeacher();
  }
}
