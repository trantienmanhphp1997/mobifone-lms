FROM nginx:alpine

LABEL MAINTAINER tudo247@gmail.com

# Set working directory
WORKDIR /var/www

# Copy config nginx
COPY ./docker/nginx-config/lms_frontend.conf /etc/nginx/conf.d/lms_frontend.conf
# Copy existing application directory contents
COPY ./dist/lms-frontend /var/www/lms-frontend
