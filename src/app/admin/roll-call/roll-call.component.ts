import { AfterViewChecked, Component, Input, OnInit, ViewChild } from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { MatInput } from '@angular/material/input';
import { ActivatedRoute, Router } from '@angular/router';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { Department } from '../../shared/model/department.model';
import { NgxSpinnerService } from 'ngx-spinner';
import { CourseService } from 'src/app/shared/services/course.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import * as moment from 'moment';
import { LocalStorageService } from 'ngx-webstorage';
import { USER_INFO, USER_ROLE } from './../../shared/constants/base.constant';

@Component({
  selector: 'app-roll-call',
  templateUrl: './roll-call.component.html',
  styleUrls: ['./roll-call.component.css']
})
export class RollCallComponent implements OnInit {


  @ViewChild('departmentTree')
  public departmentTree: TreeViewComponent;
  @ViewChild('fromInput', {
    read: MatInput
  }) fromInput: MatInput;
  @ViewChild('toInput', {
    read: MatInput
  }) toInput: MatInput;

  public departmentTreeData;
  public currentSelectedDepartmentId;
  checkedDepartmentIds = [];
  departmentData = [];
  departmentMap: any[] = [];
  role: any;
  isTeacher = false;
  isManage = false;
  dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 5,
    allowSearchFilter: true,
    searchPlaceholderText: 'Tìm kiếm'
  };
  validYear: Date;
  pageIndex = 1;
  currentDate: Date = new Date();
  departmentList: Department[] = [];
  pageSizeOptions: number[] = [10, 25, 50, 100];
  typeCourseOption: any[] = ['OFFI', 'OFFP', 'OFFE'];
  listCourse: any;
  totalRecord: any;
  searchFilter = {
    pageIndex: 1,
    pageSize: 10,
    sortColumn: 'timecreated',
    sortType: 'desc',
    startDate: null,
    endDate: null,
    keyword: null,
    type: "",
  }
  startDate: any;
  constructor(private departmentService: DepartmentService,
    private router: Router,
    private fb: FormBuilder,
    private spinner: NgxSpinnerService,
    private courseService: CourseService,
    private toastrService: ToastrCustomService,
    private $localStorage: LocalStorageService,
  ) { }


  ngOnInit(): void {
    this.role = this.$localStorage.retrieve(USER_INFO.ROLES)[0].shortname;
    const roles = this.$localStorage.retrieve(USER_INFO.ROLES);
    if (roles) {
      for (const role of roles) {
        if (role.shortname === USER_ROLE.MANAGER || role.shortname === USER_ROLE.ADMIN) {
          this.isManage = true;
        }
        if (role.shortname === USER_ROLE.TEACHER) {
          this.isTeacher = true
        }
      }
    }

    this.listAllTree();
    this.listAllCourse();
  }

  nodeCheck(args: any): void {
    const checkedNode: any = [args.node];
    if (args.event.target.classList.contains('e-fullrow') || args.event.key == 'Enter') {
      // @ts-ignore
      const getNodeDetails: any = this.departmentTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.departmentTree.uncheckAll(checkedNode);
      } else {
        this.departmentTree.checkAll(checkedNode);
      }
    }
  }

  nodeDepartmentChecked(e) {
    this.checkedDepartmentIds = [];
    this.departmentTree.checkedNodes.forEach((node) => {
      this.checkedDepartmentIds.push(parseInt(node, 10));
    });
    this.pageIndex = 1;
    this.listAllCourse();
  }

  goToDetail(id) {
    this.router.navigate(['/admin/roll-call-detail', id]);
  }
  
  listAllTree() {
    this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
      this.departmentList = data.body;
      if (this.departmentList.length > 0) {
        // set thang cha = null de no hien thi len duoc cay
        this.departmentList[0].parentid = null;
        this.departmentList.forEach(value => {
          this.departmentList.filter(department => department.id === value.parentid);
          value.expanded = value.parentid == null;
          if (value.parentid !== null) {
            const parentName = this.departmentMap.filter(x => x.id == value.parentid)[0]?.name;
            let name = '';
            if (parentName) {
              name = parentName + ' / ' + value.name;
            } else {
              name = value.name;
            }
            this.departmentMap.push({ id: value.id, name: name });
          } else {
            this.departmentMap.push({ id: value.id, name: value.name });
          }
        });
        this.departmentTreeData = {
          dataSource: this.departmentList,
          id: 'id',
          parentID: 'parentid',
          text: 'name',
          hasChildren: 'haschild'
        };
      }
    });
  }
  changeStartDate(e) {

    this.searchFilter.startDate = Math.round(e.target.value.getTime() / 1000);
    this.startDate = moment.unix(this.searchFilter.startDate).toDate();
    this.listAllCourse();
  }
  changeEndDate(e) {
    this.searchFilter.endDate = Math.round(e.target.value.getTime() / 1000);
    this.currentDate = moment.unix(this.searchFilter.endDate).toDate();
    this.listAllCourse();
  }
  clearDate(e, value: any) {
    switch (value) {
      case 'startDate':
        this.searchFilter.startDate = null;
        this.fromInput.value = null;
        break;
      case 'endDate':
        this.searchFilter.endDate = null;
        this.toInput.value = null;
        break;
      default:
        break;
    }
    this.listAllCourse();
  }
  listAllCourse() {
    const param = {
      limit: this.searchFilter.pageSize,
      page: this.searchFilter.pageIndex,
      search: this.searchFilter.keyword,
      startdate: this.searchFilter.startDate,
      enddate: this.searchFilter.endDate,
      sortcolumn: this.searchFilter.sortColumn,
      sorttype: this.searchFilter.sortType,
      departmentids: this.checkedDepartmentIds ? this.checkedDepartmentIds.toString() : "",
      trainingtype: this.searchFilter.type ?? null,
      isofflinecourse: 1
    }
    this.spinner.show();
    this.courseService.searchCourses(param).subscribe(data => {
      this.listCourse = data.body.results;
      this.totalRecord = data.body.total;
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastrService.handlerError(err);
    });
  }
  sortData(e) {
    this.searchFilter.pageIndex = 1;
    this.searchFilter.sortColumn = e.active;
    this.searchFilter.sortType = e.direction;
    this.listAllCourse();
  }
  changePage(event) {
    this.searchFilter.pageIndex = event.pageIndex + 1;
    this.searchFilter.pageSize = event.pageSize;
    this.listAllCourse();
  }
  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }
  changeName() {
    // this.searchFilter.keyword = e.target.value;
    this.searchFilter.pageIndex = 1;
    this.listAllCourse();
  }
  onDeSelectDepartment(e) {
    this.departmentMap = [...this.departmentMap]?.filter(id => id !== e.id);
  }
  onDeSelectAllDepartment() {
    this.departmentData = [];
  }
  onItemSelect($event) {
    if (this.isTeacher) {
      this.checkedDepartmentIds = [];
      this.departmentData.forEach(value => {
        this.checkedDepartmentIds.push(parseInt(value.id));
      });
    }
    this.listAllCourse();
  }
  changeTypeCourse($event) {
    this.searchFilter.type = $event.target.value;
    this.listAllCourse();
  }
}
