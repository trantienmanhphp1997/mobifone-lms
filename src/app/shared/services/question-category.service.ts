import { HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
// import { questionCategory } from '../model/course-category.model';
import { MOODLE_INFO } from '../constants/base.constant';
import CommonUtil from '../utils/common-util';
import { map } from 'rxjs/operators';
import { AbstractService } from './abstract.service';
import { Injectable } from '@angular/core';

type EntityResponseType = HttpResponse<any>;
type EntityArrayResponseType = HttpResponse<any[]>;

@Injectable({ providedIn: 'root' })
export class QuestionCategoryService {

    constructor(private abstractService: AbstractService) {}

    createQuestionCategory(questionCategory: any): Observable<EntityResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_question_create_categories');
        const categoryArray: any[] = [];

        categoryArray.push(questionCategory);
        CommonUtil.appendDataToFormData(data, 'categories', categoryArray);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: EntityResponseType) => res));
    }

    updateQuestionCategory(questionCategory: any): Observable<HttpResponse<null>> {

        const data = this.abstractService.getCommonInput('mobifone_question_update_category');
        CommonUtil.appendDataToFormData(data, 'category', questionCategory);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<null>) => res));
    }

    deleteQuestionCate(courseIds: any): Observable<any> {
        let strid = String(courseIds);
        let deleteIds = {
            ids: strid
        };
        const dataCourse = this.abstractService.getCommonInput('mobifone_question_delete_categories');
        CommonUtil.appendDataToFormData(dataCourse, 'params', deleteIds);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, dataCourse).pipe(map((res: any) => res));
    }

    searchQuestionCategory(category: any): Observable<any> {
        const searchData = this.abstractService.getCommonInput('mobifone_question_get_categories');
        CommonUtil.appendDataToFormData(searchData, 'params', category);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, searchData).pipe(map((res: any) => res));
    }

    getQuestionCategoryTree(params): Observable<EntityArrayResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_question_get_all_categories_tree');
        CommonUtil.appendDataToFormData(data, 'id', params);
        return this.abstractService.post<any[]>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: EntityArrayResponseType) => res));
    }
}
