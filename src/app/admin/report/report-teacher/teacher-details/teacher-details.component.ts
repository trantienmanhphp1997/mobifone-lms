import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import * as moment from 'moment';
import { LoginService } from 'src/app/shared/services/login.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { ReportService } from '../../../../shared/services/report.service';
import {FileService} from '../../../../shared/services/file.service';
import {NgxSpinnerService} from 'ngx-spinner';
import { LocalStorageService } from 'ngx-webstorage';
import { LOCAL_STORAGE_KEY } from 'src/app/shared/constants/base.constant';


@Component({
  selector: 'app-teacher-details',
  templateUrl: './teacher-details.component.html',
  styleUrls: ['./teacher-details.component.css']
})
export class TeacherDetailsComponent implements OnInit, OnDestroy {
  id: any;
  sub: any;
  currentSearch:any;
  listCourse: any;
  searchCourse: any = {
    keyword: null,
    status: 0,
    year: null,
    pageIndex: 1,
    pageSize: 10,
  };
  totalRecord: number;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  userFullname: string;
  departmentName: string;
  teacherInfo: any = {
    fullname: null,
    departmentName: null,
    email: null,
    grade: null,
    rank: null
  };
  statusSearchData: any;
  searchStatus: string;
  currentDate: Date = new Date();
  constructor(
    private reportService: ReportService,
    private activatedRoute: ActivatedRoute,
    private modalService: NgbModal,
    private fileService: FileService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    private $localStorage: LocalStorageService,
  ) {}

  ngOnInit(): void {
    debugger
    this.currentSearch = this.$localStorage.retrieve(LOCAL_STORAGE_KEY.SEARCH.REPORT_TEACHER_SEARCH);
    this.sub = this.activatedRoute.paramMap.subscribe(params => {
      this.id = params.get('id') || 0;
      this.reportCourseStudent();
    });
    this.statusSearchData = [
      {name: 'Chưa hiển thị', id: 1},
      {name: 'Chưa diễn ra', id: 2},
      {name: 'Đang diễn ra', id: 3},
      {name: 'Đã kết thúc', id: 4}
    ];
  }
  getDefault() {
    return this.currentSearch.year;
  }
  ngOnDestroy(): void {
    this.modalService.dismissAll();
  }

  reportCourseStudent() {
      const params = {
        keyword: this.searchCourse.keyword,
        status: this.searchCourse.status,
        userid: this.id,
        coursetype: 1,
        year: this.searchCourse.year ==null ? this.currentSearch.year : this.searchCourse.year,
        page: this.searchCourse.pageIndex,
      };
      this.spinner.show();
      this.reportService.reportTeacherCourse(params).subscribe(
      (data) => {
        this.spinner.hide();
        this.listCourse = data.body.results;
        this.totalRecord = data.body.total;
        this.listCourse?.forEach(course => {
          if (this.currentDate > moment.unix(course.enddate).toDate()) {
            course.status = 'Đã kết thúc';
            course.style = 'success';
          }
          if (this.currentDate < moment.unix(course.startdate).toDate()) {
            course.status = 'Chưa bắt đầu';
            course.style = 'secondary';
          }
          if (moment.unix(course.startdate).toDate() < this.currentDate && this.currentDate < moment.unix(course.enddate).toDate()) {
            course.status = 'Đang diễn ra';
            course.style = 'info';
          }
        });
        this.userFullname = data.body.userfullname;
        this.departmentName = data.body.departmentname;
        this.teacherInfo = {
          fullname:  data.body.userfullname,
          departmentName:  data.body.departmentname,
          email:  data.body.email,
          grade:  data.body.grade,
          rank:  data.body.ordernumber,
        };
      }, error => {
        this.spinner.hide();
        this.toastrService.handlerError(error);
      });
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  onExportLearningDetail(){
    const params = {
      userid: this.id,
      coursetype: 1
    };
    this.spinner.show();
    this.reportService.exportTeacherDetail(params).subscribe(res => {
      window.open(this.fileService.getFileFromPathUrl(res.body.path));
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastrService.handlerError(err);
    });
  }
  onSearch(event){
    this.listCourse.pageIndex = 1;
    this.searchCourse.keyword = event.target.value;
    this.reportCourseStudent();
  }
  onFilterStatus(statusFilter: string) {
    this.searchStatus = statusFilter;
    this.searchCourse.status = statusFilter;
    this.reportCourseStudent();
  }
  yearSelected(value: any) {
    this.searchCourse.year = value ? value[0] : null;
    this.reportCourseStudent();
  }
  changePage(event) {
    this.searchCourse.pageIndex = event.pageIndex + 1;
    this.searchCourse.pageSize = event.pageSize;
    this.reportCourseStudent();
  }
}
