export interface Question {
  id?: number;
  name?: string;
  questiontext?: string;
  qtype?: string;
  categoryid?: number;
  categoryname?: string;
  defaultmark?: number;
  level?: number;
  checked?: boolean;
  answercorrect?: number;
  answers?: any[];
  answernumbertype?: string[];
  type?: string;
  slot?: any;
  single?: any;
  typeCheckBox: string;
  condition: number;
  linkVideo: string | any;
  answernumbering: string;
  anwser?: [{
    id?: number;
    text?: string;
    grade?: number;
    type?: string;
    checked?: boolean;
  }];
  answeruser?: any[];
  disabled?: boolean;
  flagged?: boolean;
}
