export interface Token {
  token: string;
  privatetoken: string;
}
