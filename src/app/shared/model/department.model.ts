export interface Department{
  id?: number;
  code: string;
  name: string;
  parentid: number;
  description?: string;
  selected: boolean;
  expanded?: boolean;
  isChecked?: boolean;
  completed?: any;
  results?: any;
  parent?: any;
  haschild?: any;
  parentname?: any;
  isadministrationdept?: number;
}
