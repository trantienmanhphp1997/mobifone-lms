import { Location } from '@angular/common';
import { filter } from 'rxjs/operators';
import { BADGE_TYPE, COST_SOURCE, DEFAULT_COURSE_IMAGE, TRAINING_TYPE, USER_ROLE } from './../../../shared/constants/base.constant';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { ConfirmModalComponent } from 'src/app/shared/modal/confirm-modal/confirm-modal.component';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { FileService } from '../../../shared/services/file.service';
import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import { CourseService } from '../../../shared/services/course.service';
import { ToastrCustomService } from '../../../shared/services/toastr-custom.service';
import { CourseCategory } from '../../../shared/model/course-category.model';
import { CourseCategoryService } from '../../../shared/services/course-category.service';
import CommonUtil from '../../../shared/utils/common-util';
import * as moment from 'moment';
import { Course } from '../../../shared/model/course.model';
import { SafeResourceUrl } from '@angular/platform-browser';
import { ModuleInfo } from '../../../shared/model/moduleinfo.model';
import { LIST_HOURS, LIST_MINUTES, USER_FUNCTIONS, ONBOARING_CODE } from 'src/app/shared/constants/base.constant';
import { CustomValidators } from '../../../shared/utils/custom-validators';
import { PositionService } from 'src/app/shared/services/position.service';
import { DepartmentService } from '../../../shared/services/department.service';
import { isNull } from 'util';
import { TrainingPlanService } from 'src/app/shared/services/training-plan.service';
import { QuizCreateComponent } from '../../course-content/quiz-content/quiz-create/quiz-create.component';

@Component({
  selector: 'app-course-create',
  templateUrl: './course-detail.component.html',
  styleUrls: ['./course-detail.component.css']
})
export class CourseDetailComponent implements OnInit {
  @ViewChild('quizCreate') quizCreate!: QuizCreateComponent;
  role: string;
  placeHolder: 'user.choose_department2';
  courseForm = this.fb.group({
    fullname: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(200)]],
    summary: [''],
    requireenglish: [],
    isopen: [],
    categoryid: [null, Validators.required],
    startdate: [, Validators.required],
    enddate: ['', Validators.required],
    hourStartDate: ['', Validators.required],
    minuteStartDate: ['', Validators.required],
    hourEndDate: ['', Validators.required],
    minuteEndDate: ['', Validators.required],
    location: [],
    trainingtype: [, Validators.required],
    trainingobject: [],
    numberstudent: ['', [Validators.required]],
    cost: [, Validators.required],
    resource: [],
    hasquiz: [],
    selectedPositions: [, Validators.required],
    duration: [null,[Validators.min(0)]],
    departmentid: [],
    other: [''],
    badgerequire: [],
    islimited: [true],
    istrainingforeign:[],
    isforcepassexam:[],
    isjoinsurvey:[],
    completealllessions: [],
    isclass: [],
    classofcourseid: [],
    planid: [],
    isApplyTrainingPlan: [],
  });
  checkTypeTrain = false;
  hoursList = LIST_HOURS;
  minutesList = LIST_MINUTES;
  listCostSource = COST_SOURCE;
  listTrainingType = TRAINING_TYPE;

  quizOutput: ModuleInfo;
  // Luu thong tin hien tai cua course
  // MLMS 668
  courseDetail: any = {
    id: null,
    summary: '',
    fullname: '',
    startdate: null,
    enddate: null,
    published: null,
    requireenglish: null,
    isopen: null,
    categoryid: null,
    publishedat: null,
    readonly: false,
    planid: null,
  };
  urlSafe: SafeResourceUrl;
  courseCategoryList: CourseCategory[] = [];
  categoryMap = new Map();
  currentDate: Date = new Date();
  courseId: number;
  avatarCourse: any;
  avatarCourseInvalid: any;
  type: any;
  showCreateQuiz = false;
  formEnable = false;
  checkC = false;
  startTimeInvalid = false;
  endTimeInvalid = false;
  isTeacher = false;
  showOpExamCheckbox = false;
  dropdownPositionSettings = {
    singleSelection: false,
    idField: 'id',
    searchPlaceholderText: 'Tìm kiếm',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 5,
    allowSearchFilter: true
  };
  isCreateExam = false;
  isShowLocation = false;
  positionList = [];
  selectedPositions = [];
  selectedPositionIds = [];
  selectedBadge = [];
  badgeList: any;
  canEditCourse = false;
  departmentMap = new Map();
  changeDepartment = false;
  costInvalid = false;
  durationInvalid = false;
  selectedId = null;
  isOpen = false;
  maxDuration: any;
  originalCourseList = [];
  isClass : boolean = false;
  isApplyTrainingPlan = false;
  listTrainingPlan = [];
  listTrainingPlanCourse = []
  fullname: any = ''
  disabledClass: boolean = false;
  maxCostValue: any
  requireConfirmUpdate: boolean = false;
  courseDisplayText = 'khóa học'; // khóa học, lớp học
  isCheckisforcepassexam = false
  @ViewChild('imgPreview') imgPreview: ElementRef;

  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrCustomService,
    private courseService: CourseService,
    private courseCategoryService: CourseCategoryService,
    private route: ActivatedRoute,
    private fileservice: FileService,
    private translateService: TranslateService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private positionService: PositionService,
    private departmentService: DepartmentService,
    private trainingPlanService: TrainingPlanService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.getListOriginalCourse()
    this.getListTrainingPlan()
    this.getListBadge();
    this.getListPosition();
    this.listAllTree();
    this.role = this.courseService.getRoleUser();
    if (this.role === USER_ROLE.TEACHER) {
      this.isTeacher = true;
    }
    this.type = 'course';
    this.courseId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.selectedId = parseInt(this.route.snapshot.queryParamMap.get('selectedId'), 10);

    this.courseCategoryService.getCourseCategoryTree(null).subscribe(response => {
      this.courseCategoryList = response.body;
      CommonUtil.convertCategoryListToMap(this.courseCategoryList, this.categoryMap);
    });
    this.getInfoCourse(this.courseId);
    this.courseForm.disable();
  }
  loadQuiz(){
    this.quizCreate.getContenTopic();
    this.quizCreate.getFinalQuiz();
  }
  initFormCourseDetail(course: any) {
    if (course.filename) {
      this.courseDetail.img = this.fileservice.getFileUrl(course.contextid, course.filename, '/course/overviewfiles/');
    } else {
      this.courseDetail.img = DEFAULT_COURSE_IMAGE;
    }
    this.imgPreview.nativeElement.src = this.courseDetail.img;

    const cost = CommonUtil.converCostWithDots(course.cost);
    const startDate = moment.unix(course.startdate).toDate();
    const endDate = course.enddate ? moment.unix(course.enddate).toDate() : null;
    this.courseForm.patchValue({
      fullname: course.fullname,
      summary: course.summary,
      startdate: startDate,
      hourStartDate: startDate.getHours(),
      minuteStartDate: startDate.getMinutes(),
      enddate: endDate,
      hourEndDate: endDate?.getHours(),
      minuteEndDate: endDate?.getMinutes(),
      requireenglish: course.requireenglish,
      categoryid: course.categoryid,
      isopen: course.isopen,
      trainingtype: course.trainingtype || 1,
      // trainingobject: course.trainingobject,
      numberstudent: course.numberstudent,
      numberclass: course.numberclass,
      location: course.location,
      resource: course.resource,
      hasquiz: course.hasquiz,
      cost: cost == '' ? 0 : cost,
      duration: course.duration,
      departmentid: course.departmentid,
      other: course.otherresource,
      islimited : endDate ? true : false,
      istrainingforeign: course.istrainingforeign ? 1 : 0,
      isforcepassexam: course.isforcepassexam ? 1 : 0,
      isjoinsurvey: course.isjoinsurvey ? 1: 0,
      completealllessions: course.completealllessions ? 1 : 0,
      isclass: course.isclass == 0 ? false : true,
      classofcourseid: course.classofcourseid,
      classofcoursename: course.classofcoursename,
      planid: course.planid,
      isApplyTrainingPlan: course.planid == null ? false : true

    });
    this.isClass = (course.isclass == 1) ? true : false;
    this.courseDisplayText = (course.isclass == 1) ? 'lớp học' : 'khóa học';
    this.isApplyTrainingPlan = (course.planid == null) ? false : true;
    this.disabledClass = true;
    this.fullname = course.fullname
    if (course.planid) this.getListOriginalCourse({planid: course.planid})
    this.isOpen = course.isopen || false;
    this.placeHolder = null;
    this.showOpExamCheckbox = [TRAINING_TYPE[0].value, TRAINING_TYPE[1].value, TRAINING_TYPE[2].value, TRAINING_TYPE[3].value, TRAINING_TYPE[4].value].includes(course.trainingtype);
    this.isShowLocation = [TRAINING_TYPE[0].value, TRAINING_TYPE[1].value, TRAINING_TYPE[2].value, TRAINING_TYPE[3].value, TRAINING_TYPE[4].value].includes(course.trainingtype);
   debugger
    if (course.trainingobject) {
      this.selectedPositions = [];
      const positionIdArray = course.trainingobject.split(',');
      for (const positionId of positionIdArray) {
        const foundPosition = this.positionList.find(value => value.id === +positionId);
        if (foundPosition) {
          this.selectedPositions.push(foundPosition);
        }
      }
      this.courseForm.patchValue({
        selectedPositions: this.selectedPositions,
      });
    } else {
      this.selectedPositions = [];
    }

    if (course.badgerequire) {
      this.selectedBadge = [];
      const badgeIdArray = course.badgerequire.split(',');
      for (const badgeId of badgeIdArray) {
        const foundBadge = this.badgeList?.find(value => value.id === +badgeId);
        if (foundBadge) {
          this.selectedBadge.push(foundBadge);
        }
      }
      this.courseForm.patchValue({
        badgerequire: this.selectedBadge,
      });
    } else {
      this.selectedBadge = [];
    }



    if (course.isopen) {
      this.selectedBadge = [];
      const badgeIdArray = course.badgerequire?.split(',');
      if (badgeIdArray) {
        for (const badgeId of badgeIdArray) {
          if ( badgeId !== '')
          {
            const foundBadge = this.badgeList.find(value => value.id === +badgeId);
            this.selectedBadge.push(foundBadge);
          }
        }
      }

      this.courseForm.patchValue({
        badgerequire: this.selectedBadge,
      });
      this.courseForm.controls['numberstudent'].setValidators([Validators.maxLength(11)]);
      this.courseForm.get('numberstudent').updateValueAndValidity();
    } else {
      this.courseForm.controls['numberstudent'].setValidators([Validators.required, Validators.maxLength(11)]);
      this.courseForm.get('numberstudent').updateValueAndValidity();
    }

    this.courseForm.get('departmentid').disable();

    if(!endDate){
      this.courseForm.get('enddate').clearValidators();
      this.courseForm.get('hourEndDate').clearValidators();
      this.courseForm.get('minuteEndDate').clearValidators();
      this.courseForm.controls['enddate'].updateValueAndValidity();
      this.courseForm.controls['hourEndDate'].updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].updateValueAndValidity();
    }else{
      this.courseForm.controls['enddate'].setValidators([Validators.required]);
      this.courseForm.controls['enddate'].updateValueAndValidity();
      this.courseForm.controls['hourEndDate'].setValidators([Validators.required]);
      this.courseForm.controls['hourEndDate'].updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].setValidators([Validators.required]);
      this.courseForm.controls['minuteEndDate'].updateValueAndValidity();
    }

    if(endDate){
      this.maxDuration = Math.floor((endDate.getTime()-startDate.getTime())/(1000 * 60 * 60 * 24)) + 1;
    }
  }

  getInfoCourse(courseId: any) {
    this.courseService.getCoursesInfo(courseId).subscribe(response => {
      this.courseDetail = response.body;
      this.courseId = this.courseDetail.id;
      // kiểm tra role được hiển thị btn hiển thị/ẩn khóa học
      if (this.isTeacher) {
        this.canEditCourse = response.body.teachercreate;
      }
      else {
        this.canEditCourse = !response.body.readonly
      }
      this.courseDetail.canEditCourse = this.canEditCourse;
      this.initFormCourseDetail(response.body);
    });
  }

  getListOriginalCourse(params: any = null){
    this.courseService.getListOriginalCourse(params).subscribe(data=>{
      this.originalCourseList=data.body.results;
    }, err=>{
      this.toastrService.handlerError(err);
    }
    )
  }
  getListTrainingPlan(){
    const params = {
      limit: 0,
    };
    this.courseService.getListTrainingPlan(params).subscribe(data=>{
        console.log(data);
      this.listTrainingPlan = data.body.results;
    }, err=>{
      this.toastrService.handlerError(err);
    }
    )
  }
  onChangeCheckClass($event) {
    this.isClass = $event.checked;
    if(!this.isClass){
      this.courseForm.get('classofcourseid').clearValidators();
      this.courseForm.controls['classofcourseid'].updateValueAndValidity();
      this.courseForm.get('classofcourseid').disable();
      this.courseDisplayText = 'khóa học';
    }else{
      this.courseForm.get('classofcourseid').enable();
      this.courseForm.controls['classofcourseid'].setValidators([Validators.required]);
      this.courseForm.controls['classofcourseid'].updateValueAndValidity();
      this.courseDisplayText = 'lớp học';
    }
  }

  onChangeIsApplyTrainingPlan($event) {
    this.isApplyTrainingPlan = $event.checked;
    if(!this.isApplyTrainingPlan){
      this.courseForm.get('planid').clearValidators();
      this.courseForm.controls['planid'].updateValueAndValidity();
      this.courseForm.get('planid').disable();
      this.courseForm.patchValue({
        planid: null
      })
    }else{
      this.courseForm.controls['planid'].setValidators([Validators.required]);
      this.courseForm.controls['planid'].updateValueAndValidity();
      this.courseForm.get('planid').enable();
      this.courseForm.patchValue({
        planid: this.courseDetail.planid
      })
    }
    this.getListOriginalCourse({planid: this.courseForm.value.planid ?? null})
    this.autoLoadOldDataNamePlanClass();
  }

  onChangePlan($event) {
    this.courseForm.patchValue({
      fullname: ''
    })
    this.fullname = ''
    this.getTrainingPlanInfo($event.target.value);
    this.autoLoadOldDataNamePlanClass()
  }

  getTrainingPlanInfo(planid: any) {
    const params = {
      limit: 0,
      planid: planid,
    };
    this.trainingPlanService.getListCourseInPlan(params).subscribe(data=>{
      this.listTrainingPlanCourse=data.body.results;
    }, err=>{
      this.toastrService.handlerError(err);
    }
    )
  }

  onChangeOriginalCourse($event) {
    let course = this.originalCourseList.find(function(item) {
      if (item.id == $event.id) {
        return item;
      }
    })
    this.courseForm.get('planid').setValue(course.planid);
  }

  onPublishCourse() {
    const modalDep = this.modalService.open(ConfirmModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.title = this.translateService.instant('course.publish_confirm_title');
    modalDep.componentInstance.body = this.translateService.instant('course.publish_confirm_content');
    modalDep.componentInstance.confirmButton = this.translateService.instant('common.show');
    modalDep.result.then((result) => {
      if (result === 'confirm') {
        this.spinner.show();
        this.courseService.publishCourse(this.courseId).subscribe(
          res => {
            this.courseDetail.published = 1;
            if (!this.courseDetail.publishedat) {
              this.courseDetail.publishedat = Math.round(this.currentDate.getTime() / 1000);
            }
            if (this.formEnable) {
              this.cancelUpdateCourse();
            }
            this.spinner.hide();
            this.toastrService.success(`common.noti.publish_success`);
          },
          err => {
            if (err.error.errorcode == "coursequiztimeinvalid" || err.error.errorcode == "noquestioninquiz") {
              this.selectedId = 4;
            }
            this.spinner.hide();
            this.toastrService.handlerError(err);
          }
        );
      }
    });
  }

  onUnPublishCourse() {
    const modalDep = this.modalService.open(ConfirmModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.title = this.translateService.instant('course.unpublish_confirm_title');
    modalDep.componentInstance.body = this.translateService.instant('course.unpublish_confirm_content');
    modalDep.componentInstance.confirmButton = this.translateService.instant('common.hide');
    modalDep.result.then((result) => {
      if (result === 'confirm') {
        this.spinner.show();
        this.courseService.unPublishCourse(this.courseId).subscribe(
          res => {
            this.spinner.hide();
            this.toastrService.success(`common.noti.un_publish_success`);
            this.courseDetail.published = 0;
          },
          err => {
            this.spinner.hide();
            this.toastrService.handlerError(err);
          }
        );
      }
    });

  }

  onNotificationCourse() {
    localStorage.setItem("idCourseNoti", this.courseId + '');
    console.log(localStorage.getItem("idCourseNoti"));
    this.router.navigate(['/admin/notification/details']);
  }

  handleFileInput(files: FileList) {
    if (files.length > 0) {
      if (files[0].type !== 'image/jpeg'
        && files[0].type !== 'image/jpg'
        && files[0].type !== 'image/png'
        && files[0].type !== 'image/bmp'
        && files[0].type !== 'image/gif') {
        const param = { file_name: files[0].name };
        this.translateService.get(`common.noti.img_invalid`, param).subscribe(message => {
          this.toastrService.error(message);
        });
      } else {
        this.avatarCourse = files[0];
        this.imgPreview.nativeElement.src = URL.createObjectURL(files[0]);
        // this.courseDetail.img = URL.createObjectURL(files[0]);
        this.avatarCourseInvalid = false;
      }
    }
  }
  selectOpByTrainingType($event) {
    if ($event.target.value == "OFFI" || $event.target.value == "OFFE" || $event.target.value == "OFFP") {
      this.checkTypeTrain = true
    } else {
      this.checkTypeTrain = false
    }
    this.showOpExamCheckbox = [TRAINING_TYPE[0].value, TRAINING_TYPE[1].value, TRAINING_TYPE[2].value].includes(this.courseForm.value.trainingtype);
    this.isShowLocation = [TRAINING_TYPE[0].value, TRAINING_TYPE[1].value, TRAINING_TYPE[2].value].includes(this.courseForm.value.trainingtype);
  }
  updateCourse() {
    if (this.courseForm.invalid) {
      CommonUtil.markFormGroupTouched(this.courseForm);
      return;
    }

    const startdate = CommonUtil.convertDateToTime(
      this.courseForm.value.startdate,
      this.courseForm.value.hourStartDate,
      this.courseForm.value.minuteStartDate
    );
    const enddate = this.courseForm.value.enddate ? CommonUtil.convertDateToTime(
      this.courseForm.value.enddate,
      this.courseForm.value.hourEndDate,
      this.courseForm.value.minuteEndDate
    ) : null;

    // bỏ validate thời gian bắt đầu không được nhỏ hơn thời gian hiện tại
    // if (!this.courseDetail.publishedat && (startdate < this.currentDate.getTime() / 1000)) {
    //   this.startTimeInvalid = true;
    //   return;
    // } else {
    //   this.startTimeInvalid = false;
    // }

    if (enddate < startdate && (this.courseForm.value.enddate || this.courseForm.value.hourEndDate || this.courseForm.value.minuteEndDate)) {
      this.endTimeInvalid = true;
      return;
    } else {
      this.endTimeInvalid = false;
    }
    var cost = this.courseForm.value.cost.length > 3 ? this.courseForm.value.cost.replace(/[^\d]/g, '') * 1 : this.courseForm.value.cost;
    if (cost > this.courseDetail.cost && this.courseDetail.planid) {
      this.costInvalid = true;
      return;
    } else {
      this.costInvalid = false;
    }

    if (!this.isCreateExam && this.courseForm.value.enddate) {
      var date1 = new Date(this.courseForm.value.startdate);
	    var date2 = new Date(this.courseForm.value.enddate);
      if(this.courseForm.value.duration > Math.floor((date2.getTime()-date1.getTime())/(1000 * 60 * 60 * 24)) + 1){
        this.durationInvalid = true;
        return;
      } else {
        this.durationInvalid = false;
      }
    }

    this.spinner.show();

    const updateCourse: Course = {
      id: this.courseDetail.id,
      summary: this.courseForm.value.summary ? this.courseForm.value.summary.trim() : '',
      fullname: this.courseForm.value.fullname.trim(),
      startdate: startdate,
      enddate: enddate,
      requireenglish: + this.courseForm.value.requireenglish,
      isopen: + this.courseForm.value.isopen,
      categoryid: this.courseForm.value.categoryid,
      publishedat: this.courseDetail.publishedat,
      hasquiz: + this.courseForm.value.hasquiz,
      istrainingforeign: +this.courseForm.value.istrainingforeign,
      isforcepassexam: +this.courseForm.value.isforcepassexam,
      isjoinsurvey: +this.courseForm.value.isjoinsurvey,
      completealllessions: +this.courseForm.value.completealllessions,
      trainingtype: this.courseForm.value.trainingtype,
      trainingobject: this.courseForm.value.selectedPositions?.map(e => e?.id).toString(),
      numberstudent: this.courseForm.value.numberstudent.length > 3 ?
        this.courseForm.value.numberstudent.replace(/[^\d]/g, '') * 1 : this.courseForm.value.numberstudent,
      location: this.courseForm.value.location,
      resource: this.courseForm.value.resource,
      cost: cost,
      duration: this.courseForm.value.duration,
      otherresource: this.courseForm.value.resource == 'OTHERS' ? this.courseForm.value.other : null,
      badgerequire: this.courseForm.value.badgerequire?.map(e => e?.id).toString(),
      planid: this.isApplyTrainingPlan ? this.courseForm.value.planid : null,
      isclass: this.isClass ? 1 : 0,
      classofcourseid: this.isClass ? this.courseForm.value.classofcourseid : null,
    };
    if (this.isTeacher && !this.courseDetail.publishedat) {
      updateCourse.departmentid = this.courseForm.value.departmentid;
    }


      this.courseService.updateCourse(updateCourse, this.avatarCourse).subscribe(
        res => {
            this.spinner.hide();
            this.toastrService.success('common.noti.update_success');
            this.getInfoCourse(this.courseId);
            this.courseForm.disable();
            this.formEnable = false;
            if (this.isTeacher && !this.courseDetail.publishedat && this.courseForm.value.departmentid != this.courseDetail.departmentid) {
              this.changeDepartment = true;
            }
          this.spinner.hide();
        },
        err => {
          this.spinner.hide();
          this.toastrService.handlerError(err);
        }
      );

  }

  check(event) {
    this.checkC = event;
  }

  onUpdateCourse() {
    this.isClass = (this.courseDetail.isclass == 0) ? false : true;
    this.disabledClass = (this.courseDetail.planid != null) ? true : (this.courseDetail.isclass != 1 ? true : false);
    if (this.courseDetail.trainingtype == "OFFI" || this.courseDetail.trainingtype == "OFFE" || this.courseDetail.trainingtype == "OFFP"
    || this.courseDetail.trainingtype == "OFFF" || this.courseDetail.trainingtype == "OFFD") {
      this.checkTypeTrain = true
    } else {
      this.checkTypeTrain = false
    }
    this.currentDate = new Date();
    if (this.courseDetail.published === 1) {
      const modalDep = this.modalService.open(ConfirmModalComponent, {
        size: 'lg',
        centered: true,
        backdrop: 'static'
      });
      modalDep.componentInstance.title = this.translateService.instant('common.caution');
      modalDep.componentInstance.body = this.translateService.instant('common.noti.required_un_publish_course');
      modalDep.componentInstance.confirmButton = this.translateService.instant('common.hide');
      modalDep.result.then((result) => {
        if (result === 'confirm') {
          this.courseService.unPublishCourse(this.courseId).subscribe(
            res => {
              this.toastrService.success(`common.noti.un_publish_success`);
              this.courseDetail.published = 0;
              this.courseForm.enable();
              this.formEnable = true;
            },
            err => {
              this.toastrService.handlerError(err);
            }
          );
        }
      });
    } else {
      this.courseForm.enable();
      if (this.courseDetail.planid) {
        this.disabledClass = true;
        this.courseForm.get('classofcourseid').disable();
        this.courseForm.get('isclass').disable();
      }
      this.formEnable = true;
      if (this.courseForm.invalid) {
        CommonUtil.markFormGroupTouched(this.courseForm);
        return;
      }
    }
  }

  cancelUpdateCourse() {
    this.courseForm.disable();
    this.formEnable = false;
    this.initFormCourseDetail(this.courseDetail);
    this.avatarCourse = null;
    this.avatarCourseInvalid = false;
    this.startTimeInvalid = false;
    this.endTimeInvalid = false;
  }

  handleShowCreateQuiz(event) {
    this.showCreateQuiz = true;
  }

  showQuiz(value) {
    this.quizOutput = value;
  }

  handleShowListQuiz(event) {
    this.showCreateQuiz = false;
  }

  back() {
    window.history.back();
  }

  checkRoleFunction(userFunction: any, role: any): boolean {
    return USER_FUNCTIONS[userFunction].includes(role);
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date).toDate();
    }
    return null;
  }
  convertCost(value: any) {
    let price = value.replace(/[^\d]/g, '') * 1;
    this.courseForm.patchValue({
      cost: Intl.NumberFormat('vi-VN').format(price)
    });
  }
  getListPosition() {
    return this.positionService.getPosition().subscribe(positionList => {
      this.positionList = positionList.body.results.filter(e => e.pcode != ONBOARING_CODE);
    });
  }
  onChangePosition() {
    if (this.courseForm.get('selectedPositions').value) {
      if (this.courseForm.get('selectedPositions').value.length > 0) {
        this.selectedPositionIds = [];
        for (const position of this.courseForm.get('selectedPositions').value) {
          if (position) {
            this.selectedPositionIds.push(position.id);
          }
        }
      } else {
        this.selectedPositionIds = [];
      }
    } else {
      this.selectedPositionIds = [];
    }
  }
  listAllTree() {
    this.departmentService.getUserDepartmentTree(1).subscribe((data) => {
      data.body.forEach(department => {
        if (department.parentid !== null) {
          const parentName = this.departmentMap.get(department.parentid);
          let name = '';
          if (parentName) {
            name = parentName + ' / ' + department.name;
          } else {
            name = department.name;
          }
          this.departmentMap.set(department.id, name);
        } else {
          this.departmentMap.set(department.id, department.name);
        }
      });
    });
  }
  onSelectedDepartment(e: any) {
    this.courseForm.patchValue({
      departmentid: e ? e.key : null
    });
  }
  convertNumberstudent(value: any) {
    let number = value.replace(/[^\d]/g, '') * 1;
    this.courseForm.patchValue({
      numberstudent: Intl.NumberFormat('vi-VN').format(number)
    });
  }

  startDateChange($event, name) {
    this.startTimeInvalid = false;
    if(name == 'startdate'){
      var date1 = new Date($event.target.value.setHours(23, 59, 59, 999));
      var date2 = new Date(this.courseForm.value.enddate);
      this.maxDuration = Math.floor((date2.getTime()-date1.getTime())/(1000 * 60 * 60 * 24)) + 1;
    }
  }

  endDateChange($event, name) {
    if(name == 'enddate'){
      var date1 = new Date(this.courseForm.value.startdate);
      var date2 = new Date($event.target.value.setHours(23, 59, 59, 999));
      this.maxDuration = Math.floor((date2.getTime()-date1.getTime())/(1000 * 60 * 60 * 24)) + 1;
    }
    this.endTimeInvalid = false;
    if (this.courseForm.value.endDate) {
      this.courseForm.controls['hourEndDate'].setValidators([Validators.required])
      this.courseForm.get('hourEndDate').updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].setValidators([Validators.required])
      this.courseForm.get('minuteEndDate').updateValueAndValidity();
    } else {
      this.courseForm.controls['hourEndDate'].clearValidators();
      this.courseForm.controls['minuteEndDate'].clearValidators();
    }
  }
  showOptions($event) {
    this.isOpen = $event.checked;
    if ($event.checked) {
      this.courseForm.controls['numberstudent'].setValidators([Validators.maxLength(11)]);
      this.courseForm.get('numberstudent').updateValueAndValidity();
    } else {
      this.courseForm.controls['numberstudent'].setValidators([Validators.required, Validators.maxLength(11)]);
      this.courseForm.get('numberstudent').updateValueAndValidity();
    }
  }
  getListBadge() {
    this.courseService.getListBadge().subscribe(data => {
      this.badgeList = data.body.results;
    }, err => {
      this.toastrService.handlerError(err);
    }
    )
  }
  onChangeBadges() {
    if (this.courseForm.get('badgerequire').value) {
      if (this.courseForm.get('badgerequire').value.length > 0) {
        this.selectedBadge = [];
        for (const badged of this.courseForm.get('badgerequire').value) {
          if (badged) {
            this.selectedBadge.push(badged.id);
          }
        }
      } else {
        this.selectedBadge = [];
      }
    } else {
      this.selectedBadge = [];
    }
  }
  onChangeResourse($event) {
    if ($event.target.value !== "OTHERS") {
      this.courseForm.patchValue({
        other: null,
      })
    }
  }
  onChangeLimit($event) {
    this.courseForm.patchValue({
      islimited: $event.checked,
    })
    if(!$event.checked){
      this.courseForm.get('enddate').clearValidators();
      this.courseForm.get('hourEndDate').clearValidators();
      this.courseForm.get('minuteEndDate').clearValidators();
      this.courseForm.controls['enddate'].updateValueAndValidity();
      this.courseForm.controls['hourEndDate'].updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].updateValueAndValidity();
      this.courseForm.patchValue({
        enddate: 0,
        hourEndDate: 0,
        minuteEndDate: 0,
      })
    }else{
      this.courseForm.controls['enddate'].setValidators([Validators.required]);
      this.courseForm.controls['enddate'].updateValueAndValidity();
      this.courseForm.controls['hourEndDate'].setValidators([Validators.required]);
      this.courseForm.controls['hourEndDate'].updateValueAndValidity();
      this.courseForm.controls['minuteEndDate'].setValidators([Validators.required]);
      this.courseForm.controls['minuteEndDate'].updateValueAndValidity();
    }
  }

  onCheckIsforcepassexam(event:any){
    if(event.checked){
      this.isCheckisforcepassexam = event.checked;
      this.courseForm.patchValue({
        hasquiz: true
      })

      this.courseForm.controls['hasquiz'].setValidators([Validators.requiredTrue]);
      this.courseForm.controls['hasquiz'].updateValueAndValidity();
    }
    else{
      this.courseForm.get('hasquiz').clearValidators();
      this.courseForm.controls['hasquiz'].updateValueAndValidity();
      // this.courseForm.controls['hasquiz'].reset();
    }
  }

  onPickFullname($event) {
    this.fullname = $event.fullname
    this.courseForm.patchValue({
      proposecourseid: $event.id
    })
  }

  autoLoadOldDataNamePlanClass() {
    this.disabledClass = this.isApplyTrainingPlan ? true : false
    if (this.courseDetail.planid != this.courseForm.value.planid && this.isApplyTrainingPlan){
      this.fullname = ''
      this.courseForm.patchValue({
        fullname: null,
        isclass: null,
        classofcourseid: null
      })
      this.courseForm.get('classofcourseid').clearValidators();
      this.courseForm.controls['classofcourseid'].updateValueAndValidity();
    } else if (this.courseDetail.planid == this.courseForm.value.planid) {
      this.fullname = this.courseDetail.fullname
      this.courseForm.patchValue({
        fullname: this.courseDetail.fullname,
        isclass: this.courseDetail.isclass,
        classofcourseid: this.courseDetail.classofcourseid
      })
    }
    this.isClass = this.courseForm.controls['classofcourseid'].value ? true : false
  }

  onConfirmUpdateCourse() {
    this.requireConfirmUpdate = false;
    if (this.courseForm.controls['fullname'].value != this.courseDetail.fullname ||
          this.courseForm.controls['planid'].value != this.courseDetail.planid ||
          this.courseForm.controls['isclass'].value != this.courseDetail.isclass ||
          this.courseForm.controls['classofcourseid'].value != this.courseDetail.classofcourseid) {
      this.requireConfirmUpdate = true;
    }

    if (!this.requireConfirmUpdate) {
      this.updateCourse()
    } else {
      const modalDep = this.modalService.open(ConfirmModalComponent, {
        size: 'lg',
        centered: true,
        backdrop: 'static'
      });
      modalDep.componentInstance.title = "Xác nhận";
      modalDep.componentInstance.body = "Thông tin bạn thay đổi có thể sẽ ảnh hưởng đến các khóa học, lớp học khác. Bạn có muốn tiếp tục không?";
      modalDep.componentInstance.confirmButton = "Xác nhận";
      modalDep.result.then((result) => {
        if (result === 'confirm') {
          this.spinner.show();
          modalDep.dismiss()
          this.updateCourse()
        } else {
          modalDep.dismiss()
        }
      });
    }
  }

  checkDisabledNotification(): boolean {
    if(!((new Date().getTime() / 1000) < this.courseDetail.enddate)) {
      return true;
    }
    return false;
  }
}
