export interface Error {
  exception: string;
  error: string;
  errorcode: string;
  message: string;
  stacktrace: string;
  debuginfo: string;
  reproductionlink: string;
}
