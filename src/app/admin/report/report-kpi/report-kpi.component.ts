import { Subscription } from 'rxjs';
import { Component, Input, OnInit, ViewChild, AfterContentInit, AfterViewChecked } from '@angular/core';
import { ReportService } from '../../../shared/services/report.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { CourseCategoryService } from '../../../shared/services/course-category.service';
import { CourseCategory } from '../../../shared/model/course-category.model';
import CommonUtil from '../../../shared/utils/common-util';
import { DepartmentService } from '../../../shared/services/department.service';
import { CourseService } from '../../../shared/services/course.service';
import { Department } from '../../../shared/model/department.model';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { PositionService } from '../../../shared/services/position.service';
import { FileService } from '../../../shared/services/file.service';
import { FormBuilder, FormControl, FormGroup } from '@angular/forms';
import { MatDatepicker } from '@angular/material/datepicker';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { MatInput } from '@angular/material/input';
import { formatDate } from '@angular/common';
@Component({
  selector: 'app-report-kpi',
  templateUrl: './report-kpi.component.html',
  styleUrls: ['./report-kpi.component.css']
})
export class ReportKpiComponent implements OnInit, AfterViewChecked {
  @ViewChild('categoryTree')
  public categoryTree: TreeViewComponent;
  @ViewChild('fromInput', {
    read: MatInput
  }) fromInput: MatInput;
  @ViewChild('toInput', {
    read: MatInput
  }) toInput: MatInput;
  listKpi: any;
  searchKpi = {
    departmentid: null,
    pageIndex: 1,
    pageSize: 10,
    categoryid: null,
    year: null,
    startDate: null,
    endDate: null,
    yearstartDate: null,
    limit: 0
  };
  searchForm = this.fb.group({
    startDate: [],
    endDate: [],
  });
  firstDay = new Date(new Date().getFullYear(), 0, 1, 1);
  @ViewChild('departmentTree')
  public departmentTree: TreeViewComponent;
  public departmentMergeTreeData;

  @ViewChild('departmentMergeTree')
  public departmentMergeTree: TreeViewComponent;
  campaignOne: FormGroup;
  campaignTwo: FormGroup;
  positions: any;
  keyword = '';
  departmentTreeData: any;
  public categoryTreeData;
  positionIds: number[] = [];
  sortColumn = 'id';
  sortType = 'ASC';
  selectedDepartment: boolean;
  departmentIdsChecked: number[] = [];
  departmentMergeIdsChecked: [];
  departmentMergeIds : number [] = [];
  pageSize = 10;
  pageIndex = 1;
  currentSelectedId: number;
  courseId: number;
  departmentList: Department[] = [];
  categoryList: CourseCategory[] = [];
  departmentIds: number[] = [];
  isTeacher: false;
  validYear: Date;
  currentDate: Date = new Date();
  totalRecord: number;
  categoryMap = new Map();
  departmentMap = new Map();
  courseCategoryList: CourseCategory[] = [];
  pageSizeOptions: number[] = [10, 25, 50, 100];
  checkedDepartmentIds = [];
  setHeight: number = 0;
  constructor(
    private reportService: ReportService,
    private spinner: NgxSpinnerService,
    private programPositionService: PositionService,
    private categoryService: CourseCategoryService,
    private courseService: CourseService,
    private courseCategoryService: CourseCategoryService,
    private departmentService: DepartmentService,
    private toastrService: ToastrCustomService,
    private fileService: FileService,
    private fb: FormBuilder,
  ) {
    const today = new Date();
    const month = today.getMonth();
    const year = today.getFullYear();
    const day = today.getDate();
    this.searchKpi.startDate = this.firstDay;
    this.searchKpi.endDate = today;
    this.searchForm.patchValue({
      startDate: this.firstDay,
      endDate: today,
    });
    this.campaignOne = new FormGroup({
      start: new FormControl(new Date(year, month, 13)),
      end: new FormControl(new Date(year, month, 16))
    });

    this.campaignTwo = new FormGroup({
      start: new FormControl(new Date(year, month, 15)),
      end: new FormControl(new Date(year, month, 19))
    });
  }

  ngOnInit(): void {
    this.getListKpi();
    this.listAllTree();
    this.getCategoryTree();
    this.getListAllTree();
    this.getListDepartmentMergeTree();
    // this.courseCategoryService.getCourseCategoryTree(null).subscribe(response => {
    //   this.courseCategoryList = response.body;
    //   CommonUtil.convertCategoryListToMap(this.courseCategoryList, this.categoryMap);
    // });
  }
  // for transcluded content
  ngAfterViewChecked() {
    this.setRow();
    window.addEventListener('resize', this.setRow);
  }

  getListDepartmentMergeTree() {
    return this.departmentService.listDepartmentMergeTree().subscribe((data) => {
      const dataSource = data.body;
      this.departmentMergeTreeData = { dataSource, id: 'id', parentID: 'parent', text: 'name', hasChildren: 'haschild', selected: 'isSelected' };
    });
  }

  nodeSelectedDepartmentMerge(e) {
    this.departmentMergeIds = [];
    this.departmentMergeIdsChecked = [];
    this.departmentMergeTree.checkedNodes.forEach((node) => {
      this.departmentMergeIds.push(parseInt(node, 10));
    });
    this.currentSelectedId = +this.departmentMergeTree.getNode(e.node).id;
    this.pageSize = 10;
    this.pageIndex = 1;
    this.sortColumn = 'id';
    this.sortType = 'ASC';
    this.pageIndex = 1;
    this.onSearch(this.currentSelectedId);
  }

  onFilterDepartmentMerge(departmentMergeFilter: any) {
    this.pageIndex = 1;
    this.keyword = departmentMergeFilter;
    this.getListKpi();
  }

  nodeCheckDepartmentMerge(args: any): void {
    const checkedNode: any = [args.node];
    if (args.event.target.classList.contains('e-fullrow') || args.event.key == 'Enter') {
      // @ts-ignore
      const getNodeDetails: any = this.departmentMergeTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.departmentMergeTree.uncheckAll(checkedNode);
      } else {
        this.departmentMergeTree.checkAll(checkedNode);
      }
    }
  }

  public nodeCheck(args: any): void {
    const checkedNode: any = [args.node];
    if (args.event.target.classList.contains('e-fullrow') || args.event.key == 'Enter') {
      // @ts-ignore
      const getNodeDetails: any = this.departmentTree.getNodeData(args.node);
      if (getNodeDetails.isChecked == 'true') {
        this.departmentTree.uncheckAll(checkedNode);
      } else {
        this.departmentTree.checkAll(checkedNode);
      }
    }
  }



  nodeSelected(e) {
    this.currentSelectedId = +this.departmentTree.getNode(e.node).id;
    this.departmentIds = [];
    this.departmentIdsChecked = [];
    this.pageSize = 10;
    this.pageIndex = 1;
    this.sortColumn = 'id';
    this.sortType = 'ASC';
    this.onSearch(this.currentSelectedId);
  }
  nodeSelectedCategory(e) {
    this.departmentIdsChecked = [];
    this.currentSelectedId = +this.categoryTree.getNode(e.node).id;
    this.pageSize = 10;
    this.pageIndex = 1;
    this.sortColumn = 'id';
    this.sortType = 'ASC';
    this.onFilterCategory(this.currentSelectedId);
  }
  getListKpi() {
    const param = {
      page: this.searchKpi.pageIndex,
      limit: this.searchKpi.pageSize,
      id: this.searchKpi.departmentid,
      categoryid: this.searchKpi.categoryid,
      year: this.searchKpi.year,
      startdate: this.searchKpi.startDate ? this.searchKpi.startDate.getTime() / 1000 : null,
      enddate: this.searchKpi.endDate ? Math.ceil(this.searchKpi.endDate.getTime() / 1000) : null,
      courselimit: this.searchKpi.limit
    };
    this.spinner.show();
    this.reportService.getListKpi(param).subscribe((data) => {
      this.spinner.hide();
      this.listKpi = data.body.results;
      this.totalRecord = data.body.total;
    });
  }
  
  onSearch(departmentFilter: any) {
    this.searchKpi.pageIndex = 1;
    this.searchKpi.departmentid = departmentFilter;
    this.getListKpi();
  }

  onFilterCategory(courseFilter: any) {
    this.searchKpi.pageIndex = 1;
    this.searchKpi.categoryid = courseFilter;
    this.getListKpi();
  }

  clear() {
    // param phuc vu tim kiem
    this.keyword = '';
    this.currentSelectedId = null;
    this.pageSize = 10;
    this.pageIndex = 1;

    // Bo select node tren cay
    this.departmentTree.selectedNodes = [];
    this.departmentIdsChecked = [];
    this.departmentMergeIdsChecked = [];
    this.departmentMergeIds = [];

    this.onSearch(this.keyword);
  }

  changePage(event) {
    this.searchKpi.pageIndex = event.pageIndex + 1;
    this.searchKpi.pageSize = event.pageSize;
    this.getListKpi();
  }

  changeStartDate(e: any) {
    this.searchKpi.startDate = this.searchForm.value.startDate;
    let yearStartDate = new Date(this.searchKpi.startDate).getFullYear();
    this.validYear = new Date(yearStartDate, 12, 0, 23, 59, 59);
    let yearEndDate = new Date(this.searchKpi.endDate).getFullYear();
    if (yearStartDate != yearEndDate) {
      this.toInput.value = '';
      this.searchKpi.endDate = null;
      this.searchForm.value.endDate = null;
    }
    this.getListKpi();
  }

  changeEndDate(e: any) {
    this.searchKpi.endDate = this.searchForm.value.endDate;
    this.getListKpi();
  }

  onFilterCourseLimit(limit: any) {
    this.pageIndex = 1;
    this.searchKpi.limit = limit;
    this.getListKpi();
  }

  closeDatePicker(elem: MatDatepicker<any>) {
    elem.close();
  }

  onExporKPI() {
    const params = {
      id: this.searchKpi.departmentid,
      categoryid: this.searchKpi.categoryid,
      year: this.searchKpi.year,
      startdate: this.searchKpi.startDate ? this.searchKpi.startDate.getTime() / 1000 : null,
      enddate: this.searchKpi.endDate ? Math.ceil(this.searchKpi.endDate.getTime() / 1000) : null,
      courselimit: this.searchKpi.limit
    };
    this.spinner.show();
    this.reportService.exportKPI(params).subscribe(
      res => {
        this.spinner.hide();
        window.open(this.fileService.getFileFromPathUrl(res.body.path));
      }, err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      });
  }
  getListAllTree() {
    this.spinner.show();
    this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
      this.departmentList = data.body;
      if (this.departmentList) {
        this.departmentList[0].parentid = null;
      }
      this.departmentTreeData = {
        dataSource: this.departmentList,
        id: 'id',
        parentID: 'parentid',
        text: 'name',
        hasChildren: 'haschild'
      };
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      this.toastrService.handlerError(error);
    });
  }
  getCategoryTree() {
    return this.categoryService.getCourseCategoryTree(null).subscribe((data) => {
      const dataSource = data.body;
      this.setUpTree(dataSource);
      this.categoryTreeData = { dataSource, id: 'id', parentID: 'parent', text: 'name', hasChildren: 'haschild', selected: 'isSelected' };
    });
  }
  setUpTree(dataSource: any) {

    const indexOfCurrentSelectedId = dataSource.findIndex(x => x.id === this.currentSelectedId);
    if (indexOfCurrentSelectedId === -1) {// case currentSelectedId is deleted -> set to deault
      this.currentSelectedId = null;
    }

    dataSource.forEach(value => {
      if (value.parent === 0) // = 0 la root tree
      {
        value.parent = null; // set = null nham muc dich hien thi
        value.expanded = true; // muc dich expand root luon khi khoi tao tree
      } else {
        value.expanded = false;
      }

      if (value.id === this.currentSelectedId) { // high light selected node
        value.isSelected = true;
      }
    });
  }
  getListCategoryOnCourse() {
    if (!this.courseId) {
      return;
    }
    const params = { courseid: this.courseId };
    this.spinner.show();
    this.courseService.listCategoryOnCourse(params).subscribe((data) => {
      data.body.results?.forEach((item) => {
        this.categoryTreeData.checkedNodes = [...this.categoryTreeData?.checkedNodes, item.id.toString()];
      });
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
      this.toastrService.handlerError(error);
    });
  }
  listAllTree() {
    this.departmentService.getUserDepartmentTree(0).subscribe((data) => {
      data.body.forEach(department => {
        if (department.parentid !== null) {
          const parentName = this.departmentMap.get(department.parentid);
          let name = '';
          if (parentName) {
            name = parentName + ' / ' + department.name;
          } else {
            name = department.name;
          }
          this.departmentMap.set(department.id, name);
        } else {
          this.departmentMap.set(department.id, department.name);
        }
      });
    });
  }
  clearDate(e: any, type: any) {

    e.stopPropagation();
    switch (type) {
      case 'endDate':
        this.searchKpi.endDate = null;
        this.toInput.value = null;
        break;
      case 'startDate':
        this.searchKpi.startDate = null;
        this.fromInput.value = null;
        break;
      default:
        break;
    }
    this.getListKpi();
  }
  setRow() {
    const header = document.getElementById('header1');
    const header2 = document.getElementById('header2');
    if (header && header2) {
      if (header.offsetHeight > header2.offsetHeight) {
        this.setHeight = header.offsetHeight;
      }
    }
    const table = document.querySelectorAll<HTMLElement>('.table1');
    const table2 = document.querySelectorAll<HTMLElement>('.table2');
    if (table && table2) {
      for (let i = 0; i <= table.length; i++) {
        const height_table1 = table[i]?.clientHeight;
        const height_table2 = table2[i]?.clientHeight;
        if (height_table1 && height_table2) {
          if (height_table2 < height_table1) {
            table2[i].style.height = height_table1 + 'px';
          } else {
            table[i].style.height = height_table2 + 'px';
          }
        }
      }
    }
  }

  setHeightHeader() {
    const header = document.getElementById('header1');
    const header2 = document.getElementById('header2');
    this.setHeight = header.offsetHeight > header2.offsetHeight ? header.offsetHeight : header2.offsetHeight;
  }

}
