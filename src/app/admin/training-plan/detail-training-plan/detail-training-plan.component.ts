import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ONBOARING_CODE, TRAINING_TYPE } from 'src/app/shared/constants/base.constant';
import { PositionService } from 'src/app/shared/services/position.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { TrainingPlanService } from 'src/app/shared/services/training-plan.service';
import CommonUtil from 'src/app/shared/utils/common-util';
import { CustomValidators } from 'src/app/shared/utils/custom-validators';

@Component({
  selector: 'app-detail-training-plan',
  templateUrl: './detail-training-plan.component.html',
  styleUrls: ['./detail-training-plan.component.css']
})
export class DetailTrainingPlanComponent implements OnInit {
  @Input() data: any;
  @Input() planId: any;
  form: FormGroup = new FormGroup({});
  positionList: any = [];
  positionName: any = []
  selectedPositionIds : any = [];
  quy = '' ;
  dropdownPositionSettings = {
    singleSelection: false,
    idField: 'id',
    searchPlaceholderText: 'Tìm kiếm',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 5,
    allowSearchFilter: true
  };
  listTrainingType = TRAINING_TYPE;
  @Output() emitter: EventEmitter<any> = new EventEmitter();
  constructor(
    public activeModal: NgbActiveModal,
    private fb: FormBuilder,
    private positionService: PositionService,
    private trainingPlanService: TrainingPlanService,
    private toastrService: ToastrCustomService,
  ) { }

  ngOnInit(): void {
    this.getListPosition();
    this.initForm();
  }
  initForm(){
    this.form = this.fb.group({
      name: [{value: this.data.name, disabled: false }, [Validators.required]],
      code: [{value: this.data.code, disabled: false }, [Validators.required , Validators.pattern('^([A-Za-z0-9]+)$')]],
      trainingtype: [{value:  this.data.trainingtype, disabled: false }, [Validators.required]],
      trainingobject: [{value: this.positionName, disabled: false }, [Validators.required]],
      duration: [{value: this.data.duration, disabled: false }, [Validators.required ,Validators.min(0)]],
      timeexpect: [{value: this.data.timeexpect, disabled: false }, [Validators.required ]],
      location: [{value: this.data.location, disabled: false }, [Validators.required]],
      numberstudent: [{value: this.data.numberstudent, disabled: false }, [Validators.required,Validators.min(0)]],
      numberclass: [{value: this.data.numberclass, disabled: false }, [Validators.required , Validators.min(0)]],
      cost: [{value: this.data.cost, disabled: false }, [Validators.required,Validators.pattern('^[0-9]*$')]],
     
    })
  }
  showCost(value: any) {
    return Intl.NumberFormat('vi-VN').format(value);
  }
  getListPosition() {
    debugger
     this.positionService.getPosition().subscribe(res => {
      this.positionList = res?.body?.results;
      this.positionName = [];
      const positionIdArray = this.data.trainingobject.split(',');
      for (const positionId of positionIdArray) {
        const foundPosition = this.positionList?.find(value => value.id === +positionId);
        if (foundPosition) {
          this.positionName.push(foundPosition);
          this.form.patchValue({
                    trainingobject: this.positionName,
            });
        }
      }
    });
    
      // this.positionList?.forEach((item:any) =>{
      //     if(item.id == this.data.trainingobject){
      //       this.positionName.push(item);
      //       this.form.patchValue({
      //         trainingobject: this.positionName,
      //       });
           
      //     }
      // })
  
  }
  selectOpByTrainingType($event) {
 
  }
  onChangePosition() {
    if (this.form?.get('selectedPositions').value) {
      if (this.form.get('selectedPositions').value.length > 0) {
        this.selectedPositionIds = [];
        for (const position of this.form.get('selectedPositions').value) {
          if (position) {
            this.selectedPositionIds.push(position.id);
          }
        }
      } else {
        this.selectedPositionIds = [];
      }
    } else {
      this.selectedPositionIds = [];
    }
  }
  onUpdate(){
    if (this.form.invalid ) {
      CommonUtil.markFormGroupTouched(this.form);
      return;
    }
    if( this.form.value.duration > 91){
      this.toastrService.error(`Thời gian tạo nhỏ hơn 90`);
    }
    else {
    const trainingobjectList: any  = []
    this.form.value.trainingobject.forEach((item:any)=>{
      trainingobjectList.push(item.id)
    })
    const data = {
      ...this.form.value,
      planid: this.planId,
      proposecourseid: this.data.id,
      trainingobject: trainingobjectList.toString()
    }
    this.trainingPlanService.updateTrainingPlan(data).subscribe( data => {
      this.toastrService.success(`Cập nhật thành công !`);
      this.emitter.emit({
        success: true,
      });
      this.activeModal.dismiss();
    })
  }
}
}
