import {BadgeService} from '../../../shared/services/badge.service';
import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {ActivatedRoute} from '@angular/router';
import {BadgeCategory} from '../../../shared/model/badge-category.model';
import {ToastrCustomService} from 'src/app/shared/services/toastr-custom.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {Sort} from '@angular/material/sort';
import CommonUtil from 'src/app/shared/utils/common-util';
import * as moment from 'moment';
import { STATUS_BADGES } from 'src/app/shared/constants/base.constant';
import { BadgeLevelBadgeAddFormComponent } from './badge-level-badge-addForm/badege-level-badge-addForm.component';
import { ConfirmModalComponent } from 'src/app/shared/modal/confirm-modal/confirm-modal.component';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-level-badge',
  templateUrl: './badge-level-badge.component.html',
  styleUrls: ['./badge-level-badge.component.css']
})
export class BadgeLevelBadgeComponent implements OnInit {
  @Input() id: any;
  totalRecord: number;
  pageSize = 10;
  pageIndex = 1;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  sortColumn = 'id';
  sortType = 'ASC';
  keyword = '';
  listBadge = [];
  badgeActive = 1;
  badgeIds: any = [];
  completeAll = false;
  selectedRow = 0;
  constructor(
    private _Activatedroute: ActivatedRoute,
    private badgeService: BadgeService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    private modalService: NgbModal,
    private translateService: TranslateService
  ) {
  }

  ngOnInit(): void {
    this.onChangeContent(this.badgeActive);
  }

  onLoadBadge(){
    this.spinner.show();
    const params = {
      limit: this.pageSize,
      page: this.pageIndex,
      keyword: this.keyword,
      sortcolumn: this.sortColumn,
      sorttype: this.sortType,
      includelevelid: this.id,
      type: this.badgeActive == 1 ? 1 : 0 
    };
    this.badgeService.getBadgeByLevel(params).subscribe(data => {
      if(data && data.body && data.body.results){
        this.listBadge = data.body.results;
      }
      this.spinner.hide();
    }, error => {
      this.spinner.hide();
    });
  }

  onSearchLBadge(search: string) {
    this.pageIndex = 1;
    this.keyword = search;
    this.onLoadBadge();
  }
  onClickShowModel(){
    const modalDep = this.modalService.open(BadgeLevelBadgeAddFormComponent, {
      size: 'lg' as any,
      centered: true,
      backdrop: 'static'
    });

    modalDep.componentInstance.id = this.id;
    modalDep.componentInstance.badgeActive = this.badgeActive;
    modalDep.result.then(result => {
      if(result == "close"){
        this.onLoadBadge();
      }
    });
  }

  unAssignSingleBadge(id){
    const p = {
      badgeids: id,
      levelid: this.id
    }
    this.unAssignBadge(p);
  }

  unAssignMultiBadge(){
    const p = {
      badgeids: this.badgeIds.toString().replace(/\,$/,""),
      levelid: this.id
    }
    this.unAssignBadge(p);
  }

  unAssignBadge(params){
    const modalDep = this.modalService.open(ConfirmModalComponent, {
      size: 'xl',
      centered: true,
      backdrop: 'static'
    });
    

    modalDep.componentInstance.title = this.translateService.instant('badge_level.delete_confirm_title');
    modalDep.componentInstance.body = this.translateService.instant('badge_level.delete_confirm_content');
    modalDep.componentInstance.confirmButton = this.translateService.instant('common.delete');
    modalDep.result.then((result) => {
      if (result == "confirm") {
        this.spinner.show();
        this.badgeService.unsignBadgeToLevel(params).subscribe(res => {
          this.toastrService.success('common.noti.delete_success');
          this.onChangeContent(this.badgeActive);
          this.spinner.hide();
        }, error => {
          this.toastrService.handlerError(error);
          this.spinner.hide();
        });
      }
    });
  }

  onChangeContent(id){
    this.badgeActive = id;
    this.badgeIds = [];
    this.completeAll = false;
    this.onLoadBadge();
  }

  oncheckboxItem(id: any, event: any){
    const e = this.badgeIds.indexOf(id);
    if(e == -1){
      this.badgeIds.push(id);
    }else{
      this.badgeIds.splice(e, 1);
    }
    this.completeAll = (this.badgeIds.length == this.listBadge.length);
    this.completed();
  }

  oncheckboxAll(event){
    this.badgeIds = new Array();
    if(this.completeAll){
      for(var i = 0; i < this.listBadge.length; i++){
        const b_id = this.listBadge[i].id;
        const e = this.listBadge.includes(b_id);
        if(!e)
          this.badgeIds.push(b_id);
      }
    }
    this.completed();
  }

  completed(){
    for(var i = 0; i < this.listBadge.length; i++){
      var e = this.badgeIds.includes(this.listBadge[i].id);
      this.listBadge[i]['completed'] = e ? true: false;
    }
    // console.log(this.listBadge);
    this.selectedRow = this.badgeIds.length;
  }

  clear(){
    // param phuc vu tim kiem
    this.keyword = '';
    this.pageSize = 10;
    this.pageIndex = 1;

    this. onLoadBadge();
  }

  nodeSelected(e) {
    this.pageSize = 10;
    this.pageIndex = 1;
    this.sortColumn = 'id';
    this.sortType = 'ASC';
    this.onLoadBadge();
  }

  reloadData(){
    this.onLoadBadge();
  }

  sortData(sort: Sort) {
    this.pageIndex = 1;
    this.sortColumn = sort.active;
    this.sortType = sort.direction;
    this.onLoadBadge();
  }

  changePage(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.onLoadBadge();
  }

  onChangeKeyWord() {
    this.pageIndex = 1;
    this.onLoadBadge();
  }

  limitString(str: string){
    return CommonUtil.limitWord(str, 30);
  }
  
  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  convertDays(sec: any) {
    if (sec !== null) {
      return Math.floor(sec / 86400);
    }
    return '';
  }

  getStatusBadge(status: number) {
    const statusBadge = STATUS_BADGES.find(s => s.statusId === status);
    return statusBadge;
  }
}
