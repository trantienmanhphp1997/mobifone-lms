export interface Input {
  wstoken: string;
  wsfunction: string;
  moodlewsrestformat: string;
  [key: string]: any;
}


