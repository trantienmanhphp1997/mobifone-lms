import {Component, Input, isDevMode, OnInit} from '@angular/core';
import {UserInfo} from '../../shared/model/user-info.model';
import {FormBuilder, FormGroup} from '@angular/forms';
import {ToastrCustomService} from '../../shared/services/toastr-custom.service';
import {NgxSpinnerService} from 'ngx-spinner';
import {UserService} from '../../shared/services/user.service';
import {LocalStorageService} from 'ngx-webstorage';
import {USER_INFO} from '../../shared/constants/base.constant';
import {AccountService} from '../../shared/services/account.service';
import {FileService} from '../../shared/services/file.service';

@Component({
  selector: 'app-user-detail',
  templateUrl: './user-detail.component.html',
  styleUrls: ['./user-detail.component.css']
})
export class UserDetailComponent implements OnInit {

  user: UserInfo;
  isChangeAvatar = false;
  isValidAvatarFormat = true;
  avatar: File;

  updateUserInfoForm: FormGroup;

  constructor(
    private fb: FormBuilder,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    public userService: UserService,
    private $localStorage: LocalStorageService,
    private fileServide: FileService,
  ) {
  }

  ngOnInit(): void {
    this.user = this.$localStorage.retrieve(USER_INFO.INFO);
    this.setFormData();
  }

  setFormData() {
    // TODO -> update other field
    this.updateUserInfoForm = this.fb.group({

    });
  }

  changeAvatar(event: any) {
    const files = event.target.files;
    if (files.length > 0) {
      this.isChangeAvatar = true;
      if (files[0].type === 'image/jpeg'
        || files[0].type === 'image/jpg'
        || files[0].type === 'image/png'
        || files[0].type === 'image/bmp'
        || files[0].type === 'image/gif') {

        this.avatar = files[0];
        this.isValidAvatarFormat = true;
        // preview avatar
        this.previewAvatarBeforeUpdate(event);

      } else {
        this.isValidAvatarFormat = false;
      }
    }
  }

  previewAvatarBeforeUpdate(event: any) {

    const reader = new FileReader();
    reader.readAsDataURL(event.target.files[0]);
    // tslint:disable-next-line:no-shadowed-variable
    reader.onload = (event: any) => {
      this.user.userpictureurl = event.target.result;
    };

  }

  updateUserInfo() {
    this.spinner.show();
    this.userService.updateOwnDetail(this.avatar).subscribe(res => {
        const userInfo: UserInfo = res.body;
        if (userInfo.pictureid){ // Neu user da thay doi anh dai dien thi lay url cua anh dai dien
          userInfo.userpictureurl = this.fileServide.getFileUrlWithRevision(
            userInfo.contextid, userInfo.picturename, '/user/icon/boost/', userInfo.pictureid
          );
        } else { // default -> tranh loi xay ra
          userInfo.userpictureurl = 'assets/graduated.svg';
        }
        this.$localStorage.store(USER_INFO.INFO, userInfo);
        this.user = {...userInfo};
        this.toastrService.success('common.noti.update_success');
        // update localstorage
        this.spinner.hide();
    },
      error => {
        this.toastrService.handlerError(error);
        this.spinner.hide();
      });
  }
}
