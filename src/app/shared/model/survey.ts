export interface Survey {
  cmid: any;
  name: string;
  sectionid: number;
  introeditor: string;
  ismaster: number;
  published: any;
  publishedat: any;
  startdate: any;
  enddate: any;
  id: number;
  qtype: number;
  questionnaireid: number;
  intro: string;
  listquestion: any;
  course: any;
  isactive: any;
  refdepartmentid: any;
  activatedat: any;
  departmentid: any;
  selected: boolean;
  isteacher: any;
  sid: number;
  hasresponse?: boolean;
}
