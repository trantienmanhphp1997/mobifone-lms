import { HttpResponse } from '@angular/common/http';
import { UserInfo } from '../model/user-info.model';
import { AbstractService } from './abstract.service';
import { Observable } from 'rxjs';
import CommonUtil from '../utils/common-util';
import { MOODLE_INFO } from '../constants/base.constant';
import { map } from 'rxjs/operators';
import { Injectable } from '@angular/core';
import { ModuleInfo } from '../model/moduleinfo.model';
import { User } from "../model/user.model";

type EntityResponseType = HttpResponse<any>;
type EntityArrayResponseType = HttpResponse<any[]>;

interface UserPageResponse {
    results: UserInfo[];
    total: number;
}

@Injectable({
    providedIn: 'root'
})
export class TrainingPlanService {
    constructor(private abstractService: AbstractService) { }

    getListTrainingPlan(param: any): Observable<HttpResponse<any>> {
        const data = this.abstractService.getCommonInput('mobifone_plan_list_plan');
        CommonUtil.appendDataToFormData(data, 'params', param);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }
    getListTrainingPlanRequests(param: any): Observable<HttpResponse<any>> {
        const data = this.abstractService.getCommonInput('mobifone_plan_list_plan_update_requests');
        CommonUtil.appendDataToFormData(data, 'params', param);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }

    createOrUpdateTrainingPlan(param: any): Observable<HttpResponse<any>> {
        const functionName = param.id ? 'mobifone_plan_update_plan' : 'mobifone_plan_create_plan'
        const data = this.abstractService.getCommonInput(functionName);
        CommonUtil.appendDataToFormData(data, 'plan', param);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }
    updateTrainingPlan(params): Observable<any> {
        const data = this.abstractService.getCommonInput('mobifone_plan_update_plan_course');
        CommonUtil.appendDataToFormData(data, 'params', params);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
    }
    getTrainingPlanInfo(id: any): Observable<any> {
        const data = this.abstractService.getCommonInput('mobifone_plan_detail_plan');
        data.append('id', id);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
    }

    getListCourseInPlan(params: any): Observable<any> {
        const data = this.abstractService.getCommonInput('mobifone_plan_list_propose_course');
        CommonUtil.appendDataToFormData(data, 'params', params);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
    }

    getListApprover(params?: any): Observable<any> {
        const data = this.abstractService.getCommonInput('mobifone_plan_list_user_approve');
        CommonUtil.appendDataToFormData(data, 'params', params);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
    }

    assignManagerToEvaluation(positionid: any, userid: any): Observable<HttpResponse<any>> {
        const data = this.abstractService.getCommonInput('mobifone_position_assign_user_evaluation');
        data.append('positionid', positionid);
        data.append('userid', userid);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }

    downloadTemplateTrainingPlanFile(): Observable<any> {
        const data = this.abstractService.getCommonInput('mobifone_plan_export_plan_template');
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: any) => res));
    }

    importPlanFormFile(file: File, year?: any, isinternal?: any): Observable<EntityResponseType> {
        const data = this.abstractService.getCommonInput('mobifone_plan_import_plan');
        data.append('file', file, file.name);
        data.append('year', year);
        data.append('isinternal', isinternal);
        // data.append('categoryid', categoryId);
        CommonUtil.appendDataToFormData(data, 'file', file);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: EntityResponseType) => res));
    }

    getOverviewPlanOfDepartment(params: any): Observable<HttpResponse<any>> {
        const searchData = this.abstractService.getCommonInput('mobifone_plan_list_department');
        CommonUtil.appendDataToFormData(searchData, 'params', params);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, searchData)
            .pipe(map((res: HttpResponse<any>) => res));

    }

    deleteTrainingPlan(planid: any) : Observable<HttpResponse<any>> {
        const searchData = this.abstractService.getCommonInput('mobifone_plan_delete_plan');
        CommonUtil.appendDataToFormData(searchData, 'ids', planid);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, searchData)
            .pipe(map((res: HttpResponse<any>) => res));
    }
    getReportTrainingPlan(params: any){
        const data = this.abstractService.getCommonInput('mobifone_report_training_plan');
        CommonUtil.appendDataToFormData(data, 'params', params);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data)
            .pipe(map((res: HttpResponse<any>) => res));
    }
    exportTrainingPlan(params: any): Observable<any> {
        const data = this.abstractService.getCommonInput('mobifone_report_training_plan_export');
        CommonUtil.appendDataToFormData(data, 'params', params);
        return this.abstractService.post<any>(MOODLE_INFO.SERVICE_URL, data).pipe(map((res: any) => res));
    }
}
