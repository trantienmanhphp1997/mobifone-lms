import {ExamLessonService} from '../../../shared/services/exam-lesson.service';
import {Component, OnInit} from '@angular/core';
import {ModuleInfo} from '../../../shared/model/moduleinfo.model';
import {TopicModel} from '../../../shared/model/topic.model';
import {ActivatedRoute} from '@angular/router';
import {COMPLETION_STATE, GRADE_METHOD} from '../../../shared/constants/base.constant';
import {element} from 'protractor';
import * as moment from 'moment';
import {LocalStorageService} from 'ngx-webstorage';

@Component({
  selector: 'app-exam-lesson',
  templateUrl: './exam-lesson.component.html',
  styleUrls: ['./exam-lesson.component.css']
})
export class ExamLessonComponent implements OnInit {
  modules: any[];
  topicContent: TopicModel;
  courseId: any;
  countQuizPass: number;
  countQuizFail: number;
  countQuizInComplete: number;
  countQuizNoyAnnounced: number;
  examName: string;
  finished: boolean;
  permission = true;

  constructor(
    private examLessonService: ExamLessonService,
    private route: ActivatedRoute,
    private $localStorage: LocalStorageService
  ) {
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  localStore(name, overduehandling) {
    this.$localStorage.store('quizName', name);
    this.$localStorage.store('overduehandling', overduehandling);
  }

  ngOnInit(): void {
    // this.examName = this.$localStorage.retrieve('examName');
    this.finished = this.$localStorage.retrieve('finished');
    this.countQuizInComplete = 0;
    this.countQuizPass = 0;
    this.countQuizFail = 0;
    // tslint:disable-next-line:radix
    this.courseId = parseInt(this.route.snapshot.paramMap.get('id'));
    this.examLessonService.listQuiz(this.courseId, 0 ,0).subscribe((data) => {
      this.examName = data.body.coursename;
      this.topicContent = data.body;

      this.modules = this.topicContent[0]?.listquiz;
      // tslint:disable-next-line:no-shadowed-variable
      this.modules.forEach(element => {
        element.sumgrades = element.sumgrades ? Math.round(element.sumgrades * 100) / 100 : 0;
        element.gradequiz = element.gradequiz ? Math.ceil(element.gradequiz) : 0;
        if (element.grademethod === GRADE_METHOD.QUIZ_GRADEHIGHEST) {
          element.grademethodName = GRADE_METHOD.QUIZ_GRADEHIGHEST_DESCRIPTION;
        } else if (element.grademethod === GRADE_METHOD.QUIZ_GRADEAVERAGE) {
          element.grademethodName = GRADE_METHOD.QUIZ_GRADEAVERAGE_DESCRIPTION;
        } else if (element.grademethod === GRADE_METHOD.QUIZ_ATTEMPTFIRST) {
          element.grademethodName = GRADE_METHOD.QUIZ_ATTEMPTFIRST_DESCRIPTION;
        } else if (element.grademethod === GRADE_METHOD.QUIZ_ATTEMPTLAST) {
          element.grademethodName = GRADE_METHOD.QUIZ_ATTEMPTLAST_DESCRIPTION;
        }
        if (element.completionstate === COMPLETION_STATE.YET_EXAM) {
          this.countQuizInComplete++;
          element.completionStateName = COMPLETION_STATE.YET_EXAM_DESCRIPTION;
        } else if (element.completionstate === COMPLETION_STATE.PASS_EXAM && element.isshowgrade === 1) {
          this.countQuizPass++;
          element.completionStateName = COMPLETION_STATE.PASS_EXAM_DESCRIPTION;
        } else if ((element.completionstate === COMPLETION_STATE.FAIL_EXAM || element.completionstate === COMPLETION_STATE.USED_EXAM) && element.isshowgrade === 1) {
          this.countQuizFail++;
          element.completionStateName = COMPLETION_STATE.FAIL_EXAM_DESCRIPTION;
        }
        else if (element.completionstate === COMPLETION_STATE.NOT_ANNOUNCED_EXAM) {
          this.countQuizNoyAnnounced++;
          element.completionStateName = COMPLETION_STATE.NOT_ANNOUNCED_EXAM_DESCREIPTION;
        }
      });
    },
    (err) => {
      this.permission = false;
      // this.spinner.hide();
    });
  }
}
