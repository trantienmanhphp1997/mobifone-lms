import { Component, OnInit, Input } from '@angular/core';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { NgxSpinnerService } from 'ngx-spinner';
import { PageEvent } from '@angular/material/paginator';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { BadgeService } from 'src/app/shared/services/badge.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-add-single-user-form',
  templateUrl: './add-single-user-form.component.html',
  styleUrls: ['./add-single-user-form.component.css']
})
export class AddSingleUserFormComponent implements OnInit {
  
  @Input() badgeId: number;
  
  studentsOutBadge: any[] = [];
  studentId: number[] = [];
  pageSizeOptions: number[] = [5, 10, 25, 50, 100];
  totalRecord: number;
  completedAll = false;
  pageEvent: PageEvent;
  cerFile: any;
  requestData: any;
  searchStudent = {
    limit: 5,
    page: 1,
    search: ''
  }

  fileData: any;
  fileName: string;
  isDisable: boolean = true;

  constructor(
    private modalService: NgbModal,
    public activeModal: NgbActiveModal,
    private badgeService: BadgeService,
    private spinner: NgxSpinnerService,
    private toastrService: ToastrCustomService,
    private translateService: TranslateService,
  ) {}

  ngOnInit(): void {
    this.getListStudentsOutBadge();
  }

  getListStudentsOutBadge() {
    this.spinner.show();
    const params = {
      limit: this.searchStudent.limit,
      page: this.searchStudent.page,
      search: this.searchStudent.search,
      badgeid: this.badgeId
    };
    this.studentId = [];
    this.badgeService.listStudentsOutBadge(params).subscribe(
      (data) => {
        data.body.results?.forEach((item) => {
          item.fullname = item.firstname + ' ' + item.lastname;
        });
        this.studentsOutBadge = data.body.results;
        this.totalRecord = data.body.total;
        this.spinner.hide();
      }, (err) => {
        this.toastrService.handlerError(err);
        this.spinner.hide();
      }
    );
  }

  onSearchStudent(search: string) {
    this.searchStudent.page = 1;
    this.searchStudent.search = search;
    this.getListStudentsOutBadge();
  }

  onCheckBoxItem(studentId: number) {
    this.studentId = [];
    this.studentId.push(studentId);
  }

  changePage(event) {
    this.studentId = [];
    this.searchStudent.page = event.pageIndex + 1;
    this.searchStudent.limit = event.pageSize;
    this.getListStudentsOutBadge();
  }

  onSubmitForm(){
    this.spinner.show();
    const params = {
      userid: this.studentId.toString(),
      badgeid: this.badgeId
    }
    this.badgeService.assignStudentToBadge(params, this.fileData).subscribe(res => {
      this.spinner.hide();
      this.toastrService.success("Thêm học viên thành công!");
      this.onreloadPage();
    }, err => {
      this.spinner.hide();
      this.toastrService.handlerError(err);
    })
  }

  handleFileInput(files: any) {
    const fileType = ['image/jpeg','image/png', 'image/webp', 'image/bmp', 'application/pdf'];
    
    if (files.length > 0) {
      if (fileType.includes(files[0].type) === false) {
        const param = {file_name: files[0].name};
        this.translateService.get(`common.noti.file_invalid`, param).subscribe(message => {
          this.toastrService.error(message);
        });
       } else {
        this.fileData = files[0];
        if(this.fileData?.name){
          const name = this.fileData.name.split(".");
          this.fileName = name[0].length > 30 ? name[0].substring(0, 30) +"[...]." + name[1] : this.fileData.name;
        }
      }
    }
  }

  onreloadPage(){
    this.fileData = undefined;
    this.fileName = undefined;
    this.getListStudentsOutBadge();
    this.studentId = [];
  }

  removeFile(){
    this.fileData = undefined;
    this.fileName = undefined;
  }
}
