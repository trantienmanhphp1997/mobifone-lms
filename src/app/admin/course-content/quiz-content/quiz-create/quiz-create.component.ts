import { EXAM_ATTEMPS, GRADE_METHOD } from './../../../../shared/constants/base.constant';
import {NgxSpinnerService} from 'ngx-spinner';
import {QuestionBankService} from '../../../../shared/services/question-bank.service';
import {TranslateService} from '@ngx-translate/core';
import {ConfirmModalComponent} from '../../../../shared/modal/confirm-modal/confirm-modal.component';
import {Component, EventEmitter, Input, OnInit, Output, OnDestroy, OnChanges, SimpleChanges} from '@angular/core';
import * as ClassicEditor from 'src/assets/ckeditor5-build-classic';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ModuleInfo} from '../../../../shared/model/moduleinfo.model';
import {ToastrCustomService} from '../../../../shared/services/toastr-custom.service';
import {IntroeditorModel} from '../../../../shared/model/introeditor.model';
import {QuizContentService} from '../../../../shared/services/quiz-content.service';
import {CdkDragDrop, moveItemInArray} from '@angular/cdk/drag-drop';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {ChosenLibComponent} from '../chosen-lib/chosen-lib.component';
import {Question} from '../../../../shared/model/question.model';
import * as moment from 'moment';
import {LIST_HOURS, LIST_MINUTES} from '../../../../shared/constants/base.constant';
import { OVER_DUE_HANDLING_2, GRADE_METHOD_2} from '../../../../shared/constants/base.constant';
import CommonUtil from '../../../../shared/utils/common-util';
import {DataService} from 'src/app/shared/services/data.service';
import {CustomValidators} from '../../../../shared/utils/custom-validators';
import { v1 as uuidv1 } from 'uuid';
import {ChosenRanComponent} from '../chosen-ran/chosen-ran.component';
import {MatRadioChange} from '@angular/material/radio';
import {CKEDITOR_CONFIG} from '../../../../shared/constants/ckeditor.constant';
import { USER_INFO, USER_ROLE } from 'src/app/shared/constants/base.constant';
import { LocalStorageService } from 'ngx-webstorage';
import { QuestionCategoryService } from 'src/app/shared/services/question-category.service';
import { TopicModel } from 'src/app/shared/model/topic.model';
import { TopicCourseModel } from 'src/app/shared/model/topic.course.model';
import { TopicService } from 'src/app/shared/services/topic.service';
import { CourseService } from 'src/app/shared/services/course.service';

@Component({
  selector: 'app-quiz-create',
  templateUrl: './quiz-create.component.html',
  styleUrls: ['./quiz-create.component.css']
})
export class QuizCreateComponent implements OnInit, OnDestroy, OnChanges {

  courseDetail;
  @Input() checkCreate = false;
  @Input() courseId;
  @Input() quizInput;
  @Output() showCreateQuizz = new EventEmitter();

  OVER_DUE_HANDLING_2 = OVER_DUE_HANDLING_2;
  GRADE_METHOD_2 = GRADE_METHOD_2;
  formModule: FormGroup = new FormGroup({});
  listQuestion: Question[] = [];
  public Editor = ClassicEditor;

  editorConfig = CKEDITOR_CONFIG.NOT_UPLOAD;

  idModule: number;
  quizId: number;
  module: any = [];
  introeditor: IntroeditorModel;
  titleNumber: any = 1;
  questionsTrueFalse: any = [];
  questionsMulti: any = [];
  questionsMultiset: any = [];
  questionTypeMulti = 'multichoice';
  questionTypeMultiset = 'multichoiceset';
  questionTypeTrueFalse = 'truefalse';
  accordions: any = [];
  defauttAttemp = '0';
  categories = new Map();
  questionCategoryList;

  idCheck: number;
  saveEnabled = false;
  endDateCourseDate: any;
  startDateCourseDate: any;
  minCloseDate: any;
  minOpenDate: any;
  current = new Date();
  isQuiz: boolean;
  EXAM_ATTEMPS = EXAM_ATTEMPS;

  totalTopic: number;
  topicContent: any = [];
  topicContent1: any = [];
  hoursList = LIST_HOURS.filter(hour => hour.value !== '');
  minutesList = LIST_MINUTES.filter(minute => minute.value !== '');

  panelOpenState = true;
  isShowRGrade = 1;
  isTeacher = false;
  role: any;
  isCheckCreateQuiz: boolean = false;
  istopicquiz: any;
  topicid:any;
  listFinalQuiz: any = [];
  ischeckDetail: boolean = false;
  isCheckshowB: boolean = false ;
  isCheckDetailQuiz : boolean = false;
  isCheckAddQuiz: boolean = false;
  isCheckUpdateQuiz: boolean = false;
  isCheckTable: boolean = true;
  isAdmin: boolean = false;
  isManager: boolean = false;
  idofquiz:any;
  section:any;
  active: any ;
  activeTopic: any;

  listQuiz: any = [];
  listExam: any = [];

  constructor(
    private fb: FormBuilder,
    private modalService: NgbModal,
    private quizContentService: QuizContentService,
    private toastrService: ToastrCustomService,
    private translateService: TranslateService,
    private questionBankService: QuestionBankService,
    public dataService: DataService,
    private spinner: NgxSpinnerService,
    private $localStorage: LocalStorageService,
    private categoryService: QuestionCategoryService,
    private topicService: TopicService,
    private courseService: CourseService
  ) {}

  ngOnInit() {
    this.role = this.$localStorage.retrieve(USER_INFO.ROLES)[0].shortname;
    if(this.role == USER_ROLE.TEACHER){
      this.isTeacher = true;
    }
    if(this.role == USER_ROLE.ADMIN){
      this.isAdmin = true;
    }
    if(this.role == USER_ROLE.MANAGER){
      this.isManager = true;
    }
    this.setDefaultPage();
    this.getContenTopic();
    this.getCourseDetail();
    this.getFinalQuiz();
    this.setFormValue();
    // this.categoryService.getQuestionCategoryTree(this.courseDetail?.questioncategoryid).subscribe(response => {
    //   this.questionCategoryList = response.body;
    //   CommonUtil.convertCategoryListToMap(this.questionCategoryList, this.categories);
    //   this.spinner.hide();
    // }, error => {
    //   this.spinner.hide();
    //   this.toastrService.handlerError(error);
    // });
  }

  ngOnChanges(changes: SimpleChanges) {
    this.setDefaultPage();
    this.setFormValue();
  }
  OpenCreateQuiz(data?:any,type?:any){
    this.isCheckTable = false;
    this.isCheckDetailQuiz = false
    this.isCheckAddQuiz = false;
    this.isCheckCreateQuiz= true;
    this.isCheckUpdateQuiz = false;
    this.checkCreate = true;
    this.idCheck = undefined;
    if(type == 1){
       this.istopicquiz= 1;
       this.topicid = data?.id;
       this.section= data.section
    }
    else{
      this.istopicquiz =0;
    }
    const dataDefault = this.getDefaultFormData();
    this.setFormValue(dataDefault);
  }
  setFormValue(dataTopic?:any, type?:any): void  {
    const data = dataTopic ? dataTopic :[];
    // const startDate = data.timeopen ? moment.unix(data.timeopen).toDate(): null;
    // const endDate = data.timeclose ? moment.unix(data.timeclose).toDate() : null;
    this.formModule = this.fb.group({
      isquiz: [{ value: dataTopic? data?.isquiz : true, disabled: type ? true : false }],
      name: [ { value: dataTopic? data.name:null, disabled: type==1 ? true : false }, [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(200)]],
      description: [{ value: data?.description ? data?.description : '' , disabled: type ? true : false }, Validators.required],
      // start date
      // openingtime: [ { value: dataTopic ? startDate: null, disabled: type ? true : false }], 
      // houropeningtime: [ { value: dataTopic? startDate.getHours() : null, disabled: type ? true : false }],
      // minuteopeningtime: [ { value: dataTopic ? startDate.getMinutes() : null, disabled: type ? true : false }],
      // end date
      // closingtime: [ { value:dataTopic? endDate : null, disabled: type ? true : false }],
      // hourclosingtime: [ { value:  dataTopic ? endDate?.getHours() : null, disabled: type ? true : false }],
      // minuteclosingtime: [ { value: dataTopic ? endDate?.getMinutes() : null, disabled: type ? true : false }],

      // start date
      openingtime: [ { value: dataTopic ? data?.openingtime: null, disabled: type ? true : false }], 
      houropeningtime: [ { value: dataTopic? data?.houropeningtime: null, disabled: type ? true : false }],
      minuteopeningtime: [ { value: dataTopic ? data?.minuteopeningtime : null, disabled: type ? true : false }],
      // end date
      closingtime: [ { value:dataTopic? data?.closingtime : null, disabled: type ? true : false }],
      hourclosingtime: [ { value:  dataTopic ? data?.hourclosingtime : null, disabled: type ? true : false }],
      minuteclosingtime: [ { value: dataTopic ? data?.minuteclosingtime : null, disabled: type ? true : false }],

      timelimit: [  { value: dataTopic ? data.timelimit : null, disabled: type ? true : false }, [Validators.required, CustomValidators.isPositiveInteger, CustomValidators.checkLimit(1, 6)]],
      overduehandling: [ { value: dataTopic? data.overduehandling : null , disabled: type ? true : false }, Validators.required],
      gradepass:  data.gradepass>0 ? [  { value: dataTopic?  Math.floor(data.gradepass) : null, disabled: type ? true : false },
        [Validators.required, CustomValidators.isPositiveInteger, CustomValidators.checkLimit(1, 6)] 
      ] : [],

      grade: [ { value: dataTopic ? Math.floor(data.grade) : null, disabled:type ? true : false }, [Validators.required, CustomValidators.isPositiveInteger, CustomValidators.checkLimit(1, 6)]],
      attempts: [ { value: dataTopic ? data.attempts : null, disabled: type ? true : false }, Validators.required],
      grademethod: [ { value: dataTopic ? data.grademethod : null, disabled: type ? true : false }, Validators.required],
      isShowRGrade: [ { value: dataTopic ? data.isshowgrade  : true, disabled: type ? true : false }]
    }
    // ,
    // {
    //   validators: CustomValidators.startTimeEndTimeValidator(this.courseDetail?.startdate, this.courseDetail.enddate),
    // }
    );

    // nếu bài thi đã có học viên làm thì chỉ được sửa các thông tin: thời gian/tên/mô tả
    if (this.quizInput?.quizhasstudent) {
      const feilds = ['name', 'timelimit', 'description', 'openingtime', 'houropeningtime', 'minuteopeningtime', 'closingtime', 'hourclosingtime', 'minuteclosingtime'];
      const keyForm = Object.keys(this.formModule.value);
      for (const key of keyForm) {
        if (!feilds.includes(key)) {
          this.formModule.controls[key].disable();
        }
      }
    }
  }

  getContenTopic(){
    const param = {
      section:0,
      courseid: this.courseId,
      istopicquiz: 1
    }
    this.topicService.getCourseTopict(param).subscribe( res =>{
        this.topicContent = res?.body
        this.activeTopic = this.topicContent.length ? this.topicContent[0].id : undefined;
        this.listQuiz = this.topicContent.length ? this.topicContent[0].listquiz : [];
    })
  }

  openCreateQuiz(type:any){
    this.isCheckshowB = false;
    this.isCheckCreateQuiz = false;
    this.isCheckUpdateQuiz = false;
    this.isCheckDetailQuiz = false;
    this.isCheckTable = true;
    this.checkCreate = true;
    this.idCheck = undefined;
    this.istopicquiz = type;
    this.accordions = [];
    if(type === 0) {
      this.isCheckshowB = true;
      this.onChangeExamLast();
    } else {
      this.onChangeExamByTopic();
    }
    
  }

  getFinalQuiz(){
    this.getCourseDetail()
    const param = {
      section:0,
      courseid: this.courseId,
      istopicquiz: 0
    }
    this.topicService.getCourseTopict(param).subscribe( res =>{
      const listFinal =  res?.body
      this.listFinalQuiz = listFinal[0]?.listquiz
      // if (this.listFinalQuiz.length > 0) this.isCheckshowB = false
      // else this.isCheckshowB = true
    })
  }

  getCourseDetail() {
    this.courseService.getCoursesInfo(this.courseId).subscribe(response => {
      this.courseDetail = response.body;
    });
  }

  OpenDetailQuiz(data:any , type?: any){
    this.isCheckAddQuiz = false;
    this.isCheckDetailQuiz = true;
    this.active = data.id
    this.isCheckCreateQuiz = true;
    this.isCheckUpdateQuiz = false; 
    this.isCheckTable = false;
    this.idofquiz = data.idofquiz;
    this.idCheck = data.id;
    this.listQuestions(this.idofquiz);
    this.ischeckDetail = true;
    this.setFormValue(this.getDataDetail(data), type)
    this.quizId = this.idofquiz; 
  }

  openUpdateQuiz(data:any , type?: any){
    this.isCheckAddQuiz = true;
    this.isCheckDetailQuiz = false;
    this.isCheckUpdateQuiz = true; 
    this.isCheckTable = false;
    this.active = data.id
    this.isCheckCreateQuiz = true;
    this.idofquiz = data.idofquiz;
    this.idCheck = data.id;
    this.listQuestions(this.idofquiz);
    // this.ischeckDetail = true;
    this.setFormValue(this.getDataDetail(data), type)
    this.quizId = this.idofquiz; 
    
  }

  getDefaultFormData(): any{
    let isquiz = true; // create
    if (!this.checkCreate){ // update
      if (this.module.gradepass === 0) {
        isquiz = false;
      }
    }
    const openingtime = this.checkCreate ?
      this.getDateFromDatetime(this.courseDetail?.startdate) : // create
      this.getDateFromDatetime(this.module.timeopen); // update

    const houropeningtime = this.checkCreate ?
      this.getHourFromDatetime(this.courseDetail?.startdate) : // create
      this.getHourFromDatetime(this.module.timeopen); // update

    const minuteopeningtime = this.checkCreate ?
      this.getMinusFromDatetime(this.courseDetail?.startdate) : // create
      this.getMinusFromDatetime(this.module.timeopen); // update

    const closingtime = this.checkCreate ?
      this.getDateFromDatetime(this.courseDetail?.enddate) : // create
      this.getDateFromDatetime(this.module.timeclose); // update

    const hourclosingtime = this.checkCreate ?
      this.getHourFromDatetime(this.courseDetail?.enddate) : // create
      this.getHourFromDatetime(this.module.timeclose); // update

    const minuteclosingtime = this.checkCreate ?
      this.getMinusFromDatetime(this.courseDetail?.enddate) : // create
      this.getMinusFromDatetime(this.module.timeclose); // update

    const name = this.checkCreate ?
      null : // create
      this.module.name; // update

    const description = this.checkCreate ?
      null :
      this.module.intro;

    const timelimit = this.checkCreate ?
      null :
      this.module.timelimit * 60;

    const overduehandling = this.checkCreate ?
      this.OVER_DUE_HANDLING_2[0].key : // create
      this.module.overduehandling; // update

    const grade = this.checkCreate ?
      null : // create
      this.module.grade; // update

    const gradepass = this.checkCreate ?
      null : // create
      this.module.gradepass; // update

    const attempts = this.checkCreate ?
      this.defauttAttemp : // create
      this.module.attempts; // update

    const grademethod = this.checkCreate ?
      GRADE_METHOD_2[0].key : // create
      this.module.grademethod; // update

    const isshowgrade = this.checkCreate ?
      1 : // create
      this.module.isshowgrade; // update


    const result = {
      isquiz, // before comment
      name,
      description,
      openingtime,
      houropeningtime,
      minuteopeningtime,
      closingtime,
      hourclosingtime,
      minuteclosingtime,
      timelimit,
      overduehandling,
      gradepass,
      grade,
      attempts,
      grademethod,
      isshowgrade
    };
    return result;
  }

  getDataDetail(data: any) {
    const startDate = data.timeopen ? moment.unix(data.timeopen).toDate(): null;
    const endDate = data.timeclose ? moment.unix(data.timeclose).toDate() : null;

    const openingtime = startDate;

    const houropeningtime = startDate.getHours();

    const minuteopeningtime = startDate.getMinutes();

    const closingtime = endDate

    const hourclosingtime = endDate.getHours();

    const minuteclosingtime = endDate.getMinutes();

    const name = data?.name;

    const description = data?.intro || '';

    const timelimit = data?.timelimit / 60;

    const overduehandling = data?.overduehandling;

    const grade = data?.gradequiz ? data?.gradequiz : data?.grade;

    const gradepass = data?.gradepass;

    const attempts = data?.attempts;

    const grademethod = data?.grademethod;

    const isshowgrade = data?.isshowgrade;

    const isquiz = data?.gradepass > 0 ? true : false;

    const result = {
      isquiz, // before comment
      name,
      description,
      openingtime,
      houropeningtime,
      minuteopeningtime,
      closingtime,
      hourclosingtime,
      minuteclosingtime,
      timelimit,
      overduehandling,
      gradepass,
      grade,
      attempts,
      grademethod,
      isshowgrade
    };
    return result;
  }

  isShowGadepass(event: MatRadioChange){
    this.isQuiz = event.value;
    // if (this.isQuiz){
    //   this.formModule.get('gradepass').setValidators(
    //     [Validators.required, CustomValidators.isPositiveInteger, CustomValidators.checkLimit(1, 6)]
    //   );
    // } else { // Tu kiem tra -> bo qua khong can validate truong nay
    //   this.formModule.get('gradepass').clearValidators();
    // }
    // this.formModule.get('gradepass').updateValueAndValidity();
  }

  isShowGrade(event: MatRadioChange){
    this.isShowRGrade = event.value;
  }

  getDateFromDatetime(dateTime: any) {
    return dateTime != 0 ? moment.unix(dateTime).toDate() : new Date();
  }

  getHourFromDatetime(dateTime: any) {
    return dateTime != 0 ? moment.unix(dateTime).toDate().getHours() : 1;
  }

  getMinusFromDatetime(dateTime: any) {
    return dateTime != 0 ? moment.unix(dateTime).toDate().getMinutes() : 1;
  }

  onChangGradepass(value: any) {
    const validate = Number.isInteger(+value);
    if (validate && this.formModule.controls.grade.value) {
      if (value && value > this.formModule.controls.grade.value) {
        this.formModule.get(['gradepass']).setErrors({gradepassError: true});
      } else if (value && value <= this.formModule.controls.grade.value) {
        this.formModule.get(['gradepass']).setErrors(null);
      }
    }
    return validate;
  }

  onChangGrade(value: any) {
    const validate = Number.isInteger(+value);
    if (validate && this.formModule.controls.gradepass.value && this.isQuiz) {
      if (value && value < this.formModule.controls.gradepass.value) {
        this.formModule.get(['gradepass']).setErrors({gradepassError: true});
      } else if (value && value >= this.formModule.controls.gradepass.value) {
        this.formModule.get(['gradepass']).setErrors(null);
      }
    }
    return validate;
  }

  numericOnly(event: any){
    const patt = /^([0-9])$/;
    return patt.test(event.key);
  }

  setDefaultPage(){
    this.isQuiz = true;
    if (!this.checkCreate){
      if (!this.module){
        this.module = this.quizInput;
      }
      if ((this.courseDetail?.published === 1 || this.courseDetail?.readonly) && this.quizInput) { // case update after create
        this.idModule = this.quizInput.id;
      }
      // if (this.module.gradepass === 0) { // update
      //   this.isQuiz = false;
      // }
    }else {
      this.quizInput = undefined;
    }

    this.endDateCourseDate = this.courseDetail?.enddate == 0 ? 0 : moment.unix(this.courseDetail?.enddate).toDate();
    this.startDateCourseDate = moment.unix(this.courseDetail?.startdate).toDate();
    this.minCloseDate = this.startDateCourseDate;
    this.minOpenDate = this.current < moment.unix(this.startDateCourseDate).toDate() ? this.current : this.startDateCourseDate;
  }

  ngOnDestroy(): void {
    this.modalService.dismissAll();
  }

  listQuestions(quizId?:any) {
    this.accordions = [];
    this.listQuestion = [];
    this.titleNumber = 1;
    if (this.quizInput !== null && this.quizInput !== undefined) {
      this.quizId = this.quizInput.instance;
    } else {
      this.quizId = this.module.id;
    }
    // chưa thêm phân trang
    const params = {
      quizid: quizId ?? this.idofquiz,
      sortcolumn: 'slot',
      sorttype: 'asc',
      limit: 0
    };
    this.spinner.show('listquestion');
    this.questionBankService.getListQuestion(params).subscribe((data) => {
      this.listQuestion = data.body.results;
      data.body.results?.forEach((item) => {
        this.addQues(item.qtype, item);
        this.dataService.changeMessage(this.titleNumber, item.qtype);
      });
      this.spinner.hide('listquestion');
    },
      error => {
        this.spinner.hide('listquestion');
        this.toastrService.handlerError(error);
      });
  }

  openLibaryCreatePopup() {
    const modalDep = this.modalService.open(ChosenLibComponent, {
      size: 'xl',
      centered: true,
      backdrop: 'static'
    });
    if (this.listQuestion !== undefined) {
      modalDep.componentInstance.listQuestions = this.listQuestion;
    }
    const id = this.quizId;
    if (this.quizInput === undefined) {
      modalDep.componentInstance.quizId = this.quizId;
    } else {
      modalDep.componentInstance.quizId = this.quizInput.instance;
    }
    modalDep.componentInstance.courseQCategoryId = this.courseDetail?.questioncategoryid;
    modalDep.componentInstance.loadAll.subscribe(($e) => {
      this.listQuestions(this.quizId);
      this.quizId = id;
    });
  }

  openRandomCreateQuestionPopup() {
    const modalDep = this.modalService.open(ChosenRanComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    const id = this.quizId;
    if (this.quizInput === undefined) {
      modalDep.componentInstance.quizId = this.quizId;
    } else {
      modalDep.componentInstance.quizId = this.quizInput.instance;
    }
    modalDep.componentInstance.courseQCategoryId = this.courseDetail?.questioncategoryid;
    modalDep.componentInstance.loadAll.subscribe(($e) => {
      this.listQuestions(this.quizId);
      this.quizId = id;
    }); 
  }

  submitForm(){
    if (this.formModule.invalid) {
      CommonUtil.markFormGroupTouched(this.formModule);
      return;
    }
    const data = this.getDataForCreateOrUpdate();
    if (this.idCheck === undefined) {
      this.createQuiz(data);
    }else {
      // if (this.quizInput !== null && this.quizInput !== undefined) {
      //   data.coursemodule = this.quizInput.id;
      // } else {
      //   data.coursemodule = this.idCheck;
      // }
      data.coursemodule = this.idCheck;
      this.updateQuiz(data);
    }
  }

  getDataForCreateOrUpdate() {
    this.introeditor = {text: '', format: 1, itemid: 1598597784614};
    const createOrUpdate: ModuleInfo = {
      attempts: this.formModule.getRawValue().attempts,
      course: this.courseId,
      grade: this.formModule.getRawValue().grade,
      grademethod: this.formModule.getRawValue().grademethod,
      gradepass: null,
      introeditor: null,
      modulename: 'quiz',
      name: this.formModule.getRawValue().name,
      overduehandling: this.formModule.getRawValue().overduehandling,
      section: this.section ? this.section : 0,
      timeclose: Math.floor(this.getClosingtime()),
      timelimit: this.formModule.getRawValue().timelimit * 60,
      timeopen: Math.floor(this.getOpeningtime()),
      isshowgrade: this.isShowRGrade,
      istopicquiz: this.istopicquiz,
      topicid: this.topicid ? this.topicid : null,
    };
    if (this.isQuiz) {
      createOrUpdate.gradepass = this.formModule.getRawValue().gradepass;
    } else {
      createOrUpdate.gradepass = 0;
    }
    this.introeditor.text = this.formModule.getRawValue().description;
    createOrUpdate.introeditor = this.introeditor;
    return createOrUpdate;
  }

  getOpeningtime() {
    return CommonUtil.convertDateToTime(
      this.formModule.value.openingtime,
      this.formModule.value.houropeningtime,
      this.formModule.value.minuteopeningtime,
    );
  }

  getClosingtime() {
    return CommonUtil.convertDateToTime(
      this.formModule.value.closingtime,
      this.formModule.value.hourclosingtime,
      this.formModule.value.minuteclosingtime,
    );
  }

  createQuiz(data){
    this.spinner.show();
    this.quizContentService.createQuizContent(data).subscribe(res => {
      this.toastrService.success(`common.noti.create_success`);
      this.setDataAfterUpdateOrCreate(res);
      this.getContenTopic();
      this.getFinalQuiz();
      this.checkCreate = true;
      this.isCheckCreateQuiz = true;
      // this.isCheckshowB = false;
      this.spinner.hide();
    }, error => {
      this.toastrService.handlerError(error);
      this.spinner.hide();
    });
  }

  updateQuiz(data){
    this.spinner.show();
    this.quizContentService.updateQuizContent(data).subscribe(res => {
      this.toastrService.success(`common.noti.update_success`);
      this.setDataAfterUpdateOrCreate(res);
      this.getContenTopic();
      this.getFinalQuiz();
      this.checkCreate = true;
      this.isCheckCreateQuiz = true;
      this.spinner.hide();
    }, error => {
      this.toastrService.handlerError(error);
      this.spinner.hide();
    });
  }

  deleteQuiz(id: any) {
    this.isCheckCreateQuiz = false;
    const modalModule = this.modalService.open(ConfirmModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    const cmids: number [] = [];
    cmids.push(id);
    modalModule.componentInstance.title = this.translateService.instant('Xóa bài thi');
    modalModule.componentInstance.body = this.translateService.instant('Bạn có chắc chắn muốn xóa bài thi này');
    modalModule.componentInstance.confirmButton = this.translateService.instant('common.delete');
    modalModule.result.then((result) => {
        this.spinner.show();
        this.quizContentService.deleteQuizContent(cmids).subscribe(
          res => {
            this.getContenTopic();
            if(!this.istopicquiz) {
              this.getFinalQuiz();
              this.ischeckDetail = false;
            }
            this.spinner.hide();
            this.toastrService.success(`common.noti.delete_success`)
          },
          err => {
            this.spinner.hide();
            this.toastrService.handlerError(err);
          }
        );
      },
    );
  }

  setDataAfterUpdateOrCreate(response: any){
    // this.formModule.patchValue({
    //   gradepass: response.body.gradepass,
    // });
    // if (response.body.gradepass !== 0){
    //   this.formModule.patchValue({
    //     isquiz: true,
    //   });
    //   this.formModule.get('gradepass').setValidators([Validators.required]);
    //   this.isQuiz = true;
    // } else {
    //   this.formModule.patchValue({
    //     isquiz: false,
    //   });
    //   this.formModule.get('gradepass').setValidators([]);
    //   this.isQuiz = false;
    // }
    // this.formModule.get('gradepass').updateValueAndValidity();
    // this.formModule.get('isquiz').updateValueAndValidity();
    this.module = response.body;
    this.isCheckAddQuiz = true;
    this.checkCreate = false;
    this.idCheck = this.module.coursemodule;
    this.module.grade = Math.ceil(this.module.grade);
    this.idModule = this.module.id;
    this.quizId = this.module.id;
    this.isCheckUpdateQuiz = false; 
    this.listQuestions(this.quizId);
    this.setFormValue(this.getDataDetail(this.module));
    this.quizId = this.module.id;
  }

  back() {
    this.isCheckAddQuiz = false;
    this.isCheckDetailQuiz = false;
    this.isCheckUpdateQuiz = false;
    this.isCheckCreateQuiz = false;
    if(this.isCheckshowB) {
      this.onChangeExamLast();
    }
    this.isCheckTable = true;
  }

  edit() {
    this.idModule = null;
    if (this.module.gradepass === 0) {
      this.isQuiz = false;
    } else {
      this.isQuiz = true;
    }
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.listQuestion, event.previousIndex, event.currentIndex);
  }

  addQuestion(typeValue: any, index?: number, ques?: any, isViewMode = false) {
    let question = null;
    if (!ques && typeValue === this.questionTypeMulti && this.questionsMulti.length > 0 && index && !ques) {
      // tslint:disable-next-line:no-shadowed-variable
      this.questionsMulti.forEach(ques => {
        if (ques.index === index) {
          question = {...ques};
          question.index = this.titleNumber;
          this.receiveMulti(question);
        }
      });
    } else if (!ques && typeValue === this.questionTypeTrueFalse && this.questionsTrueFalse.length > 0 && index && !ques) {
      // tslint:disable-next-line:no-shadowed-variable
      this.questionsTrueFalse.forEach(ques => {
        if (ques.index === index) {
          question = {...ques};
          question.index = this.titleNumber;
          this.receiveTrueFalse(question);
        }
      });
    } else if (typeValue === this.questionTypeMultiset && this.questionsMultiset.length > 0 && index && !ques) {
      // tslint:disable-next-line:no-shadowed-variable
      this.questionsMultiset.forEach(ques => {
        if (ques.index === index) {
          question = {...ques};
          question.index = this.titleNumber;
          this.receiveMultiset(question);
        }
      });
    } else if (ques) {
      question = {...ques};
      question.index = this.titleNumber;
      switch (typeValue){
        case this.questionTypeMulti:
          this.questionsMulti.push(question);
          break;
        case this.questionTypeMultiset:
          this.questionsMultiset.push(question);
          break;
        case this.questionTypeTrueFalse:
          this.questionsTrueFalse.push(question);
          break;
      }
      // typeValue === this.questionTypeMulti ? this.questionsMulti.push(question) : this.questionsTrueFalse.push(question);
    }
    if (question?.answer) {
      question.answer.forEach(a => {
        delete a.id;
      });
    }
    if (question?.id && isViewMode){
      this.prepareQuestiontext(typeValue, question);
    } else {
      delete question?.id;
      this.addQues(typeValue, question);
    }
  }

  addQues(typeValue: any, ques?: any) {
    const newAco = [
      {
        index: this.titleNumber,
        questionNumber: this.titleNumber,
        // title: this.titleNumber,
        description: typeValue === 'random' ? 'Câu hỏi ' + this.titleNumber + ' (Câu hỏi ngẫu nhiên)' : 'Câu hỏi ' + this.titleNumber,
        type: typeValue,
        defaultmark: 4,
        question: ques ? ques : null,
        enable: ques?.id ? false : true,
        btnSaveDisable: ques ? false : true,
        idofquiz: this.quizId
      }
    ];
    this.accordions.push(...newAco);
    this.titleNumber++;
    this.resetAccordions();
  }

  removeQuestion(accordion: any) {
    if (this.questionsMulti.length > 0 || this.questionsTrueFalse.length > 0 || this.accordions) {
      const modalDep = this.modalService.open(ConfirmModalComponent, {
        size: 'lg',
        centered: true,
        backdrop: 'static'
      });
      modalDep.componentInstance.title = this.translateService.instant('question.delete_confirm_title');
      modalDep.componentInstance.body = this.translateService.instant('question.delete_confirm_content');
      modalDep.componentInstance.confirmButton = this.translateService.instant('common.delete');
      modalDep.result.then((result) => {
        if (result === 'confirm') {
          if (accordion.question?.id) {
            const params = {
              questionids: accordion.question.id,
              quizid: this.quizId
            };
            this.spinner.show();
            this.questionBankService.deleteQuestionOnExam(params).subscribe(
              res => {
                this.toastrService.success(`common.noti.delete_success`);
                this.accordions.splice(this.accordions.indexOf(accordion), 1);
                // this.titleNumber -= 1;
                // this.listQuestions();
                this.spinner.hide();
              },
              err => {
                this.spinner.hide();
                this.toastrService.handlerError(err);
              });
          } else {
            this.accordions.splice(this.accordions.indexOf(accordion), 1);
          }
        }
      });
    }
  }

  saveQuestions(accordion?: any) {
    if (this.questionsTrueFalse.length <= 0 && this.questionsMulti.length <= 0 && this.questionsMultiset.length <= 0) {
      return;
    }
    if (accordion) {
      if (accordion.type === this.questionTypeMulti) {
        const ques = this.questionsMulti.filter(question => question.index === accordion.index);
        if (ques.lenght === 0) {
          return;
        }
        if (accordion.question?.id) {
          ques[0].id = accordion.question.id;
        }
        this.questionsMulti = this.questionsMulti.filter(question => question.index !== accordion.index);
        this.saveQuestion(ques, accordion);
      } else if (accordion.type === this.questionTypeTrueFalse) {
        const ques = this.questionsTrueFalse.filter(question => question.index === accordion.index);
        if (ques.lenght === 0) {
          return;
        }
        if (accordion.question?.id) {
          ques[0].id = accordion.question.id;
        }
        this.questionsTrueFalse = this.questionsTrueFalse.filter(question => question.index !== accordion.index);
        this.saveQuestion(ques, accordion);
      } else if (accordion.type === this.questionTypeMultiset) {
        const ques = this.questionsMultiset.filter(question => question.index === accordion.index);
        if (ques.lenght === 0) {
          return;
        }
        if (accordion.question?.id) {
          ques[0].id = accordion.question.id;
        }
        this.questionsMultiset = this.questionsMultiset.filter(question => question.index !== accordion.index);
        this.saveQuestion(ques, accordion);
      }
    } else {
      this.saveQuestion(this.questionsMulti);
      this.saveQuestion(this.questionsTrueFalse);
      this.saveQuestion(this.questionsMultiset);
      this.questionsMulti = [];
      this.questionsTrueFalse = [];
      this.questionsMultiset = [];
    }
  }

  private saveQuestion(questions: any[], accordion?: any) {
   
    questions.forEach((question: any) => {
      question.quizid = this.idModule ? this.idModule : this.quizInput.instance;
      question.categoryid = this.courseDetail?.categoryid;
      const index = question.index;
      const single = question.single;
      const qtype = question.qtype;
      if (question.questiontext.search('<img') !== -1) {
        question.questiontext = question.questiontext.replaceAll('<img', '<img class="img-fluid"');
      }
      question.answer?.forEach(ans => {
        if (ans.text.search('<img') !== -1) {
          // nếu ảnh chưa có alt thì add thêm, nếu ảnh đã có alt thì replate để tránh gây lỗi khi làm bài thi có 2 ảnh có alt bị trùng
          if (ans.text.search('alt="') === -1) {
            ans.text = ans.text.replaceAll('<img', '<img class="img-fluid" alt="' + uuidv1() + '"');
          } else {
            ans.text = ans.text.replaceAll('<img', '<img class="img-fluid"').replaceAll('></figure>', ' alt="' + uuidv1() + '"></figure>');
          }
        }
      });
      if (!question.id && !accordion?.question?.id) {
        this.spinner.show();
        this.questionBankService.createQuestion(question).subscribe(
          res => {
            this.toastrService.success(`common.noti.create_success`);
            if (accordion) {
              this.accordions.forEach(acc => {
                if (acc.index === accordion.index) {
                  acc.question = question;
                  acc.question.id = res.body.id;
                  acc.question.index = index;
                  acc.question.single = single;
                  acc.question.qtype = qtype;
                  acc.enable = false;
                  this.dataService.changeMessage(index, qtype, true);
                }
              });
            } else {
              this.dataService.changeMessage(index, qtype, true);
            }
            this.spinner.hide();
          },
          err => {
            question.index = index;
            question.single = single;
            question.qtype = qtype;
            this.toastrService.handlerError(err);
            // tslint:disable-next-line:no-shadowed-variable
            this.accordions.forEach((accordion) => {
              if (accordion.index === question.index) {
                accordion.btnSaveDisable = true;
              }
            });
            this.spinner.hide();
          });
      } else {
        this.spinner.show();
        this.questionBankService.updateQuestion(question).subscribe(
          res => {
            this.toastrService.success(`common.noti.update_success`);
            if (accordion) {
              this.accordions.forEach(acc => {
                if (acc.index === accordion.index) {
                  acc.question = question;
                  acc.question.index = index;
                  acc.question.single = single;
                  acc.question.qtype = qtype;
                  acc.enable = false;
                  this.dataService.changeMessage(index, qtype);
                }
              });
            } else {
              this.dataService.changeMessage(index, qtype);
            }
            this.spinner.hide();
          },
          err => {
            question.index = index;
            question.single = single;
            question.qtype = qtype;
            this.toastrService.handlerError(err);
            // tslint:disable-next-line:no-shadowed-variable
            this.accordions.forEach((accordion) => {
              if (accordion.index === question.index) {
                accordion.btnSaveDisable = true;
              }
            });
            this.spinner.hide();
          });
      }
    });
  }

  cancelCreateQues() {
    if (this.accordions.length > 0
      && (this.questionsTrueFalse.length > 0 || this.questionsMulti.length > 0 || this.questionsMultiset.length > 0)
    ) {
      const modalDep = this.modalService.open(ConfirmModalComponent, {
        size: 'lg',
        centered: true,
        backdrop: 'static'
      });
      modalDep.componentInstance.title = this.translateService.instant('question.cancel_confirm_title');
      modalDep.componentInstance.body = this.translateService.instant('question.cancel_confirm_content');
      modalDep.componentInstance.confirmButton = this.translateService.instant('common.continute');
      modalDep.result.then((result) => {
        if (result === 'confirm') {
          // const confirm = this.accordions;
          // this.accordions = [];
          // confirm.forEach((accordion) => {
          //   if(accordion.question && accordion.question.id) this.accordions.push(accordion);
          // });
          // this.questionsTrueFalse = [];
          // this.questionsMulti = [];
          // this.titleNumber = this.accordions.length === 0 ? 1 : this.accordions.length + 1;
          this.listQuestions();
        }
      });
    } else {
      this.listQuestions();
      // const confirm = this.accordions;
      // this.accordions = [];
      // confirm.forEach((accordion) => {
      //   if(accordion.question && accordion.question.id) this.accordions.push(accordion);
      // });
      // this.titleNumber = this.accordions.length === 0 ? 1 : this.accordions.length + 1;
    }
  }

  receiveTrueFalse($ques) {
    if (!$ques.questiontext) {
      this.accordions.forEach((accordion) => {
        if (accordion.index === $ques.index) {
          accordion.btnSaveDisable = true;
        }
      });
      this.saveEnabled = false;
      return;
    }
    this.saveEnabled = true;
    this.accordions.forEach((accordion) => {
      if (accordion.index === $ques.index) {
        accordion.btnSaveDisable = false;
      }
    });
    let recover = false;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.questionsTrueFalse.length; i++) {
      if (this.questionsTrueFalse[i].index === $ques.index) {
        // this.questionsTrueFalse[i].name = $ques.name;
        this.questionsTrueFalse[i].questiontext = $ques.questiontext;
        this.questionsTrueFalse[i].level = $ques.level;
        this.questionsTrueFalse[i].defaultmark = $ques.defaultmark;
        this.questionsTrueFalse[i].answercorrect = $ques.answercorrect;
        recover = true;
      }
    }
    $ques.qtype = this.questionTypeTrueFalse;
    $ques.categoryid = this.courseDetail?.categoryid;
    if (!recover) {
      this.questionsTrueFalse.push($ques);
    }
  }

  receiveMulti($ques) {
    if (!$ques.questiontext || !$ques.answer[0].text) {
      this.accordions.forEach((accordion) => {
        if (accordion.index === $ques.index) {
          accordion.btnSaveDisable = true;
        }
      });
      this.saveEnabled = false;
      return;
    }
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < $ques.answer.length; i++) {
      if (!$ques.answer[i].text || !$ques.answer[i].grade) {
        this.accordions.forEach((accordion) => {
          if (accordion.index === $ques.index) {
            accordion.btnSaveDisable = true;
          }
        });
        this.saveEnabled = false;
        return;
      }
    }
    this.saveEnabled = true;
    this.accordions.forEach((accordion) => {
      if (accordion.index === $ques.index) {
        accordion.btnSaveDisable = false;
      }
    });
    let recover = false;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.questionsMulti.length; i++) {
      if (this.questionsMulti[i].index === $ques.index) {
        // this.questionsMulti[i].name = $ques.name;
        this.questionsMulti[i].questiontext = $ques.questiontext;
        this.questionsMulti[i].defaultmark = $ques.defaultmark;
        this.questionsMulti[i].answernumbering = $ques.answernumbering;
        this.questionsMulti[i].answer = $ques.answer;
        this.questionsMulti[i].level = $ques.level;
        this.questionsMulti[i].single = $ques.single;
        recover = true;
      }
    }
    $ques.qtype = this.questionTypeMulti;
    $ques.categoryid = this.courseDetail?.categoryid;
    if (!recover) {
      this.questionsMulti.push($ques);
    }
  }

  receiveMultiset($ques) {
    if (!$ques.questiontext || !$ques.answer[0].text) {
      this.accordions.forEach((accordion) => {
        if (accordion.index === $ques.index) {
          accordion.btnSaveDisable = true;
        }
      });
      this.saveEnabled = false;
      return;
    }
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < $ques.answer.length; i++) {
      if (!$ques.answer[i].text || !$ques.answer[i].grade) {
        this.accordions.forEach((accordion) => {
          if (accordion.index === $ques.index) {
            accordion.btnSaveDisable = true;
          }
        });
        this.saveEnabled = false;
        return;
      }
    }
    this.saveEnabled = true;
    this.accordions.forEach((accordion) => {
      if (accordion.index === $ques.index) {
        accordion.btnSaveDisable = false;
      }
    });
    let recover = false;
    // tslint:disable-next-line:prefer-for-of
    for (let i = 0; i < this.questionsMultiset.length; i++) {
      if (this.questionsMultiset[i].index === $ques.index) {
        // this.questionsMulti[i].name = $ques.name;
        this.questionsMultiset[i].questiontext = $ques.questiontext;
        this.questionsMultiset[i].defaultmark = $ques.defaultmark;
        this.questionsMultiset[i].answernumbering = $ques.answernumbering;
        this.questionsMultiset[i].answer = $ques.answer;
        this.questionsMultiset[i].level = $ques.level;
        this.questionsMultiset[i].single = $ques.single;
        recover = true;
      }
    }
    $ques.qtype = this.questionTypeMultiset;
    $ques.categoryid = this.courseDetail?.categoryid;
    if (!recover) {
      this.questionsMultiset.push($ques);
    }
  }

  editQuestion($index) {
    for (const item of this.accordions) {
      if (item.index === $index) {
        this.spinner.show();
        this.questionBankService.prepareQuestiontextForUpdate(item.question.id).subscribe(
          res => {
            item.question.questiontext = res.body.questiontext;
            item.enable = true;
            this.spinner.hide();
          }, error => {
            this.toastrService.handlerError(error);
            this.spinner.hide();
          }
        );
      }
    }
  }

  getGradepassValueByKey(key){
    return this.GRADE_METHOD_2.find(obj => obj.key === key).value;
  }

  getOverDueHandlingValueByKey(key){
    return this.OVER_DUE_HANDLING_2.find(obj => obj.key === key).value;
  }

  setMinEndDate(startDate: any) {
    this.minCloseDate = startDate;
  }

  setDefaultStartDate(){
    this.formModule.patchValue({
      // start date
      openingtime: this.getDateFromDatetime(this.courseDetail?.startdate),
      houropeningtime: this.getHourFromDatetime(this.courseDetail?.startdate),
      minuteopeningtime: this.getMinusFromDatetime(this.courseDetail?.startdate),
    });
  }

  setDefaultEndDate(){
    this.formModule.patchValue({
      // end date
      closingtime: this.getDateFromDatetime(this.courseDetail?.enddate),
      hourclosingtime: this.getHourFromDatetime(this.courseDetail?.enddate),
      minuteclosingtime: this.getMinusFromDatetime(this.courseDetail?.enddate),
    });
  }

  onRemoveQuestion(index: number) {
    this.accordions.splice(index, 1);
    this.resetAccordions();
  }
  resetAccordions(){
    var k=1;
    for (var accordion of this.accordions) {
      accordion.index = k ;
      accordion.questionNumber = k ;
      if(accordion.type == "random"){
        accordion.description = "Câu hỏi "+k + " (Câu hỏi ngẫu nhiên)";
      }else{
        accordion.description = "Câu hỏi "+k;
      }
      
      k++;
    }
  }
  prepareQuestiontext(typeValue, question){
    this.spinner.show();
    this.questionBankService.prepareQuestiontextForUpdate(question.id).subscribe(
      res => {
        delete question?.id;
        question.questiontext = res.body.questiontext;
        this.addQues(typeValue, question);
        this.spinner.hide();
      }, error => {
        this.toastrService.handlerError(error);
        this.spinner.hide();
      }
    );
  }

  setRootTree(tree: any){
    tree.forEach(value => {
      if (value.parent === 0) // = 0 la root tree
      {
        value.parent = null; // set = null nham muc dich hien thi
        value.expanded = true; // muc dich expand root luon khi khoi tao tree
      } else {
        value.expanded = false;
      }
      value.isChecked = true;
    });
  }

  getTitleQuiz(): string {
    if(this.isCheckDetailQuiz) {
      return "Chi tiết thông tin bài thi";
    } else {
      if(this.isCheckUpdateQuiz) {
        return "Cập nhật thông tin bài thi";
      }
      return "Tạo mới thông tin bài thi"
    }
  }

  getTitleQuestion(): string {
    if(this.isCheckDetailQuiz) {
      return "Chi tiết nội dung bài thi";
    } else {
      if(this.isCheckUpdateQuiz) {
        return "Cập nhật nội dung bài thi";
      }
      return "Tạo mới nội dung bài thi"
    }
  }

  onChangeExamByTopic() {
    this.getContenTopic();
  }

  onChangeExamLast() {
    const param = {
      section:0,
      courseid: this.courseId,
      istopicquiz: 0
    }
    this.topicService.getCourseTopict(param).subscribe( res =>{
      const listFinal =  res?.body
      this.listFinalQuiz = listFinal[0]?.listquiz
      this.listQuiz = this.listFinalQuiz;
    })
  }

  onClickTopic(item: any) {
    this.activeTopic = item.id;
    this.listQuiz = item.listquiz;
    this.isCheckDetailQuiz = false;
    this.isCheckCreateQuiz = false;
    this.isCheckUpdateQuiz = false;
    this.isCheckTable = true;
  }

  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  getGradeMethodName(state: number): string {
    if(state === GRADE_METHOD.QUIZ_GRADEHIGHEST) {
      return GRADE_METHOD.QUIZ_GRADEHIGHEST_DESCRIPTION;
    } else if(state === GRADE_METHOD.QUIZ_GRADEAVERAGE) {
      return GRADE_METHOD.QUIZ_GRADEAVERAGE_DESCRIPTION;
    } else if(state === GRADE_METHOD.QUIZ_ATTEMPTFIRST) {
      return GRADE_METHOD.QUIZ_ATTEMPTFIRST_DESCRIPTION;
    } else if(state === GRADE_METHOD.QUIZ_ATTEMPTLAST) {
      return GRADE_METHOD.QUIZ_ATTEMPTLAST_DESCRIPTION;
    } else {
      return "";
    }
  }

  getTypeQuiz(gradepass: any): string {
    const result = Number.parseInt(gradepass);
    if(result === 0) {
      return "Tự kiểm tra";
    }
    return "Bài thi";
  }

  getStatusQuiz(timeStart: any): string {
    let dateCurrent = new Date();
    let dateOpen = new Date(timeStart * 1000)
    let message = '';
    if(dateOpen < dateCurrent) {
      message = 'Đã hiển thị'
    } else {
      message = 'Chưa hiển thị';
    }
    return message;
  }

}
