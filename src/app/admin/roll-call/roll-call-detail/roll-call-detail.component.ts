import { Component, OnInit, ViewChild,AfterViewChecked } from '@angular/core';
import { NgbModal, NgbModalConfig } from '@ng-bootstrap/ng-bootstrap';
import { DepartmentService } from 'src/app/shared/services/department.service';
import { Department } from 'src/app/shared/model/department.model';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { TreeViewComponent } from '@syncfusion/ej2-angular-navigations';
import { NgxSpinnerService } from 'ngx-spinner';
import { TranslateService } from '@ngx-translate/core';
import { LocalStorageService } from 'ngx-webstorage';
import { CourseService } from 'src/app/shared/services/course.service';
import { ActivatedRoute } from '@angular/router';
import { RollCallExmodalComponent } from '../roll-call-exmodal/roll-call-exmodal.component';

@Component({
  selector: 'app-roll-call-detail',
  templateUrl: './roll-call-detail.component.html',
  styleUrls: ['./roll-call-detail.component.css'],
  providers: [NgbModal, NgbModalConfig]
})
export class RollCallDetailComponent implements OnInit,AfterViewChecked {

  setHeight: number;
  // Tree
  @ViewChild('departmentTree')
  public departmentTree: TreeViewComponent;
  public departmentTreeData;
  departmentList: Department[] = [];

  // List
  departmentData = [];
  checkedDepartmentIds = [];
  // pagination & search
  listSchedule: any;
  listStudent: any;
  totalRecord: number;
  pageSize = 10;
  pageIndex = 1;
  courseId: any;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  monthOptions: number[] = [];
  month: number;
  sortColumn = 'id';
  sortType = 'ASC';
  keyword = '';
  nameCourse: any;
  courseDetail: any;
  departmentMap: any[] = [];
  dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'name',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 2,
    allowSearchFilter: true,
    searchPlaceholderText: 'Tìm kiếm'
  };
  fromMonth:any;
  toMonth:any;
  constructor(
    private modalService: NgbModal,
    private departmentService: DepartmentService,
    private toastrService: ToastrCustomService,
    private courseService: CourseService,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,
    private $localStorage: LocalStorageService,
  ) {
  }

  ngOnInit(): void {
    this.courseId = parseInt(this.route.snapshot.paramMap.get('id'), 10);
    this.getMonthCourse();
    this.listAllTree();
    if(this.courseId && this.month){
      this.listAllDetail();
    }
  }

  ngAfterViewChecked() {
    this.setRow();
    // this.setHeightBody();
    window.addEventListener('resize',this.setRow);
  }

  getMonthCourse() {
    this.courseService.getCoursesInfo(this.courseId).subscribe(data => {
      this.courseDetail = data.body;
      this.nameCourse = this.courseDetail.fullname;
      const fromYear = new Date(parseInt(this.courseDetail.startdate) * 1000).getFullYear();
      const toYear = new Date(parseInt(this.courseDetail.enddate) * 1000).getFullYear();
       
      if(fromYear != toYear){
         this.fromMonth=1;
         this.toMonth =12;
      }else{
        this.fromMonth = new Date(parseInt(this.courseDetail.startdate) * 1000).getMonth() + 1;
        this.toMonth = new Date(parseInt(this.courseDetail.enddate) * 1000).getMonth() + 1;
      }
      for (let i = this.fromMonth; i <= this.toMonth; i++) {
        this.monthOptions.push(i);
      }
      this.month = this.fromMonth;
      this.listAllDetail();
    }, err => {
      this.toastrService.handlerError(err);
    });
  }

  listAllTree() {
    const e = [];
    this.departmentService.getUserDepartmentTree(1).subscribe((data) => {
      this.departmentList = data.body;
      if (this.departmentList.length > 0) {
        // set thang cha = null de no hien thi len duoc cay
        this.departmentList[0].parentid = null;
        this.departmentList.forEach(value => {
          this.departmentList.filter(department => department.id === value.parentid);
          value.expanded = value.parentid == null;
          if (value.parentid !== null) {
            const parentName = e.filter(x => x.id == value.parentid)[0]?.name;
            let name = '';
            if (parentName) {
              name = parentName + ' / ' + value.name;
            } else {
              name = value.name;
            }
            e.push({ id: value.id, name: name });
          } else {
            e.push({ id: value.id, name: value.name });
          }
        });
      }
      this.departmentMap = e;
    });
  }

  listAllDetail() {
    this.checkedDepartmentIds=[]
    this.departmentData.forEach(value => {
      this.checkedDepartmentIds.push(parseInt(value.id));
    });
    const params = {
      limit: this.pageSize,
      page: this.pageIndex,
      id: this.courseId,
      keyword: this.keyword,
      departmentids: this.checkedDepartmentIds ? this.checkedDepartmentIds.toString() : '',
      month: this.month,
    }
    this.spinner.show();
    this.courseService.getListStudentDetailAttendance(params).subscribe(data => {
      this.listSchedule = data.body.schedule;
      this.totalRecord = data.body.total;
      this.listStudent = data.body.results;
      this.spinner.hide();
    }, err => {
      this.spinner.hide();
      this.toastrService.handlerError(err);
    });
  }
  onDeSelectDepartment(e) {
    this.departmentMap = [...this.departmentMap]?.filter(id => id !== e.id);
    this.listAllDetail();
  }
  onDeSelectAllDepartment() {
    this.departmentData = [];
    this.listAllDetail();
  }

  changePage(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.listAllDetail();
  }

  changeName() {
    this.pageIndex = 1;
    this.listAllDetail();
  }

  changeMonth($event) {
    this.month = $event.target.value;
    this.listAllDetail();
  }

  openModalExport(keyword, selectedIds) {
    const modalDep = this.modalService.open(RollCallExmodalComponent, {
      size: 'md',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.dataMonth = this.monthOptions;
    modalDep.componentInstance.keyword = keyword;
    modalDep.componentInstance.selectedIds = selectedIds;
    modalDep.result.then((result) => {
      if (result) { }
    });
  }

  onItemSelect($event){
    this.listAllDetail();
  }

  setRow() {
    const header = document.getElementById('header1');
    const header2 = document.getElementById('header2');
    const height_body1 = document.getElementById('body1');
    const height_body2 = document.getElementById('body2');
    if (header && header2) {
      if (header.offsetHeight > header2.offsetHeight) {
        this.setHeight = header.offsetHeight;
      }
    }
    if (height_body1 && height_body2) {
      if (height_body1.offsetHeight > height_body2.offsetHeight) {
        this.setHeight = height_body1.offsetHeight;
      }
    }
    const table = document.querySelectorAll<HTMLElement>('.table1');
    const table2 = document.querySelectorAll<HTMLElement>('.table2');
    if (table && table2) {
      for (let i = 0; i <= table.length; i++) {
        const height_table1 = table[i]?.clientHeight;
        const height_table2 = table2[i]?.clientHeight;
        if (height_table1 && height_table2) {
          if (height_table2 < height_table1) {
            table2[i].style.height = height_table1 + 'px';
          } else {
            table[i].style.height = height_table2 + 'px';
          }
        }
      }
    }
  }

  setHeightHeader() {
    const header = document.getElementById('header1');
    const header2 = document.getElementById('header2');
    this.setHeight = header.offsetHeight > header2.offsetHeight ? header.offsetHeight : header2.offsetHeight;
  }

  setHeightBody(){
    const height_body1 = document.getElementById('body1');
    const height_body2 = document.getElementById('body2');
    this.setHeight = height_body1.offsetHeight > height_body2.offsetHeight ? height_body1.offsetHeight : height_body2.offsetHeight;
  }
}