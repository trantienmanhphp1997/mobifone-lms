import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CourseAssessmentDetailComponent } from '../course-assessment-detail/course-assessment-detail.component';
import { CourseEvaluationUsersComponent } from '../course-evaluation-users/course-evaluation-users.component';
import { CourseService } from 'src/app/shared/services/course.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { PopupEvaluationComponent } from 'src/app/admin/course-content/popup-evaluation/popup-evaluation.component';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-course-assessment',
  templateUrl: './course-assessment.component.html',
  styleUrls: ['./course-assessment.component.css']
})
export class CourseAssessmentComponent implements OnInit {
  fullname: any;
  year: any;
  isShowForm = false;
  totalRecord: number;
  userEvaluationList: any[] = [];
  listCourse: any[] = [];
  pageSizeOptions: number[] = [10, 25, 50, 100];
  userId: any;
  courseList: any;
  searchCourse = {
    pageIndex: 1,
    pageSize: 10,
    sortColumn: 'id',
    sortType: 'desc',
  }
  user: any;

  constructor(
    private modalService: NgbModal,
    private courseService: CourseService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
  ) {
  }

  ngOnInit(): void {
    this.getListUserEvaluation();
    // this.listCourseBeAsssessed();
    // this.listCourseByUserId();
  }

  changePage(event) {
    this.searchCourse.pageIndex = event.pageIndex + 1;
    this.searchCourse.pageSize = event.pageSize;
  }

  showDetail(item) {
    const modalDep = this.modalService.open(CourseAssessmentDetailComponent, {
      size: 'xl',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.courseId = item.id;
    modalDep.componentInstance.user = this.user;
    modalDep.componentInstance.emitService.subscribe((emmitedValue) => {
      if(this.userId){
        this.listCourseByUserId();
      }
    });
  }

  evaluationUsers() {
    const modalDep = this.modalService.open(CourseEvaluationUsersComponent, {
      size: 'xl',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.emitService.subscribe((emmitedValue) => {
      if(this.userId){
        this.listCourseByUserId();
      }
    });
  }

  getListUserEvaluation() {
    this.courseService.getListUserEvaluationEndUser().subscribe(res => {
      this.userEvaluationList = res.body
    },
      err => {
        this.toastrService.handlerError(err);
      })
  }

  listCourseByUserId() {
    const params = {
      userid: this.userId,
      keyword: this.fullname,
      year: this.year,
    };
    this.spinner.show();
    this.courseService.getListCourseEvaluation(params).subscribe(res => {
      this.spinner.hide();
      this.courseList = res.body.results;
      this.totalRecord = res.body.total;
    },
      err => {
        this.toastrService.handlerError(err);
      });
  }

  changeUserId(item) {
    this.user = item;
    this.userId = item.id;
    this.listCourseByUserId();
  }

  SearchText() {
    this.listCourseByUserId();
  }

  SearchYear() {
    this.listCourseByUserId();
  }
  addLD(data:any , courseid: any ){
    const modalDep = this.modalService.open(PopupEvaluationComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    console.log(modalDep) 
    modalDep.componentInstance.data = data;
    modalDep.componentInstance.courseid = courseid;
    modalDep.componentInstance.emitter.subscribe( result =>{
      this.listCourseByUserId();
    })
  }
}
