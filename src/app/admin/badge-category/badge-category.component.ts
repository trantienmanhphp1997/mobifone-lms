import {AfterViewInit, Component, OnInit, ViewChild, OnDestroy} from '@angular/core';
import {NgbModal, NgbModalConfig} from '@ng-bootstrap/ng-bootstrap';
import {BadgeCategory} from '../../shared/model/badge-category.model';
import {ToastrCustomService} from '../../shared/services/toastr-custom.service';
import {TranslateService} from '@ngx-translate/core';
import {BadgeCategoryCreateComponent} from './badge-category-create/badge-category-create.component';
import {ConfirmModalComponent} from '../../shared/modal/confirm-modal/confirm-modal.component';
import {NgxSpinnerService} from 'ngx-spinner';
import {TreeViewComponent} from '@syncfusion/ej2-angular-navigations';
import {Sort} from '@angular/material/sort';
import { BadgeCategoryService } from 'src/app/shared/services/badge-category.service';
import {LocalStorageService} from 'ngx-webstorage';
import {Router} from '@angular/router';
import { BadgeCategoryConfirmDeleteComponent } from './badge-category-confirm-delete/badge-category-confirm-delete.component';

@Component({
  selector: 'app-badge-category',
  templateUrl: './badge-category.component.html',
  styleUrls: ['./badge-category.component.css'],
  providers: [NgbModalConfig, NgbModal],
})
export class BadgeCategoryComponent implements OnInit, AfterViewInit, OnDestroy {

  // tree
  @ViewChild('categoryTree')
  public categoryTree: TreeViewComponent;
  currentSelectedId: number; // node selected
  public categoryTreeData; // data for tree

  // List category
  categories: BadgeCategory[] = [];

  completedAll = false; // checkbox all
  // list id course checked trong cả màn
  badgeCateIdsChecked: number[] = [];
  // list id course checked trong current page
  badgeCateIds: number[] = [];
  // pagination
  totalRecord: number;
  pageSize = 10;
  pageIndex = 1;
  pageSizeOptions: number[] = [10, 25, 50, 100];
  sortColumn = 'id';
  sortType = 'ASC';
  keyword = '';


  constructor(
    private modalService: NgbModal,
    private categoryService: BadgeCategoryService,
    private translateService: TranslateService,
    private toastrService: ToastrCustomService,
    private spinner: NgxSpinnerService,
    private $localStorage: LocalStorageService,
    private router: Router
  ) { }

  ngOnInit(): void {
    this.onSearch();
    this.getCategoryTree();
  }
  /**
   * @method ngAfterViewInit
   */
  ngAfterViewInit() {
    this.onSearch();
  }

  ngOnDestroy(): void {
    this.modalService.dismissAll();
  }

  oncheckboxAll(checked: any) {
    if (checked) {
      this.categories.forEach(c => {
        c.completed = checked;
        if (!this.badgeCateIds.includes(c.id)) {
          this.badgeCateIds.push(c.id);
        }
        if (!this.badgeCateIdsChecked?.includes(c.id)) {
          this.badgeCateIdsChecked?.push(c.id);
        }
      });
      this.completedAll = true;
    } else {
      this.badgeCateIds?.forEach(id => {
        this.badgeCateIdsChecked?.splice(this.badgeCateIdsChecked?.indexOf(id), 1);
      });
      this.badgeCateIds = [];
      this.categories?.forEach(c => {
        c.completed = false;
      });
      this.completedAll = false;
    }
  }

  oncheckboxItem(courseId: number, checked: any) {
    if (checked) {
      this.categories?.forEach(c => {
        if (c.id === courseId) {
          c.completed = true;
          this.badgeCateIds?.push(courseId);
          this.badgeCateIdsChecked?.push(courseId);
          return;
        }
      });
      if (this.badgeCateIds?.length > 0 && this.badgeCateIds?.length === this.categories?.length && !this.completedAll) {
        this.completedAll = true;
      }
    } else {
      this.completedAll = false;
      this.categories?.forEach(c => {
        if (c.id === courseId) {
          c.completed = false;
          this.badgeCateIds?.splice(this.badgeCateIds?.indexOf(courseId), 1);
          this.badgeCateIdsChecked?.splice(this.badgeCateIdsChecked?.indexOf(courseId), 1);
          return;
        }
      });
    }
  }

  onSearch() {
    const params = {
      limit: this.pageSize,
      page: this.pageIndex,
      keyword: this.keyword,
      id: this.currentSelectedId,
      sortcolumn: this.sortColumn,
      sorttype: this.sortType
    };
    this.spinner.show();
    this.badgeCateIds = [];
    return this.categoryService.searchBadgeCategory(params).subscribe((data) => {
      data?.body?.results.forEach(c => {
        if (this.badgeCateIdsChecked?.includes(c.id)) {
          c.completed = true;
          this.badgeCateIds?.push(c.id);
        }
      });
      if (this.badgeCateIds?.length > 0 && (this.badgeCateIds?.length === data?.body?.results?.length)) {
        this.completedAll = true;
      } else {
        this.completedAll = false;
      }
      this.categories = data.body.results;
      this.totalRecord = data.body.total;
      this.spinner.hide();
    }, error => {
      this.toastrService.handlerError(error);
      this.spinner.hide();
    });
  }

  getCategoryTree(){
    return this.categoryService.getBadgeCategoryTree(null).subscribe((data) => {
      const dataSource = data.body;
      this.setUpTree(dataSource);
      this.categoryTreeData = { dataSource, id: 'id', parentID: 'parent', text: 'name', hasChildren: 'haschild', selected: 'isSelected' };
    });
  }

  setUpTree(dataSource: any) {
    const indexOfCurrentSelectedId = dataSource.findIndex(x => x.id === this.currentSelectedId);

    if (indexOfCurrentSelectedId === -1){// case currentSelectedId is deleted -> set to deault
      this.currentSelectedId = null;
    }

    dataSource.forEach(value => {
      if (value.parent === 0) // = 0 la root tree
      {
        value.parent = null; // set = null nham muc dich hien thi
        value.expanded = true; // muc dich expand root luon khi khoi tao tree
      } else {
        value.expanded = false;
      }

      if (value.id === this.currentSelectedId){ // high light selected node
        value.isSelected = true;
      }
    });
  }

  onUpdateBadgeCategory(item){
    const current: BadgeCategory = {
      id: item.id,
      name: item.name,
      description: item.description,
      parent: item.parent
    };

    const modalDep = this.modalService.open(BadgeCategoryCreateComponent, {
      size: 'lg' as any,
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.title = "badge_category.update_title";
    modalDep.componentInstance.category = current;
    modalDep.componentInstance.categories = this.categoryTreeData.dataSource;
    modalDep.componentInstance.updateCategory.subscribe(($e) => {
      this.reloadData();
    });
  }

  onCreateBadgeCategory() {
    const current: BadgeCategory = {
      name: '',
      description: '',
      parent: this.currentSelectedId
    };

    const modalDep = this.modalService.open(BadgeCategoryCreateComponent, {
      size: 'lg' as any,
      centered: true,
      backdrop: 'static'
    });

    modalDep.componentInstance.title = "badge_category.create_title";
    modalDep.componentInstance.category = current;
    modalDep.componentInstance.categories = this.categoryTreeData.dataSource;
    modalDep.componentInstance.updateCategory.subscribe(($e) => {
      this.reloadData();
    });
  }

  onDeleteMultipleCourse() {
    this.deleteCategory([...this.badgeCateIdsChecked]);
  }

  onDeleteSingleCourse(categoryId: number) {
    this.deleteCategory([categoryId]);
  }

  deleteCategory(category: number [], skip: number = null) {
    const modalDep = this.modalService.open(ConfirmModalComponent, {
      size: 'lg',
      centered: true,
      backdrop: 'static'
    });
    modalDep.componentInstance.title = this.translateService.instant('badge_category.delete_confirm_title');
    modalDep.componentInstance.body = this.translateService.instant('badge_category.delete_confirm_content');
    modalDep.componentInstance.confirmButton = this.translateService.instant('common.delete');
    modalDep.result.then((result) => {
      if(result == 'confirm'){
        this.onDelete(category, null);
      }
    });
  }

  onDelete(category, skip) {
    this.spinner.show();
    this.categoryService.deleteBadgeCate(category, skip).subscribe(
      res => {
        this.spinner.hide();
        if(res?.body?.status == "success" && !res?.body?.data){
          this.reloadData();
          this.toastrService.success(`common.noti.delete_success`);
        }else{
          const modalDele = this.modalService.open(BadgeCategoryConfirmDeleteComponent, {
            size: 'lg',
            centered: true,
            backdrop: 'static'
          });
          modalDele.componentInstance.body = this.translateService.instant(JSON.stringify(res?.body?.data));
          modalDele.componentInstance.confirmButton = this.translateService.instant('common.continute');
          modalDele.result.then((result) => {
            if(result == "confirm"){
              this.onDelete(category, 1);
            }
          })
        }
      },
      err => {
        this.spinner.hide();
        this.toastrService.handlerError(err);
      }
    );
  }

  /**
   * clear select node
   * clear search param
   */
  clear(){
    // param phuc vu tim kiem
    this.keyword = '';
    this.currentSelectedId = null;
    this.pageSize = 10;
    this.pageIndex = 1;

    // Bo select node tren cay
    this.categoryTree.selectedNodes = [];

    this.onSearch();
  }

  nodeSelected(e) {
    this.badgeCateIdsChecked = [];
    this.currentSelectedId = +this.categoryTree.getNode(e.node).id;
    this.pageSize = 10;
    this.pageIndex = 1;
    this.sortColumn = 'id';
    this.sortType = 'ASC';
    this.onSearch();
  }

  reloadData(){
    this.onSearch();
    this.getCategoryTree();
  }

  sortData(sort: Sort) {
    this.pageIndex = 1;
    this.sortColumn = sort.active;
    this.sortType = sort.direction;
    this.onSearch();
  }

  changePage(event) {
    this.pageIndex = event.pageIndex + 1;
    this.pageSize = event.pageSize;
    this.onSearch();
  }

  onChangeKeyWord() {
    this.pageIndex = 1;
    this.badgeCateIdsChecked = [];
    this.onSearch();
  }

  isLevel(){
    const e = this.categories.filter(x => x.id == this.currentSelectedId && !x.islevel);
    return e.length || !this.currentSelectedId ? true : false;
  }
}
