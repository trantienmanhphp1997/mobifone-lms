export interface NotifycationModel {
  title: string;
  content: any;
  userids: string;
  id: any;
  useridfrom: any;
  useridto: any;
  subject: string;
  smallmessage: string;
  timecreated: any;
  timeread: any;
  fullmessage: any;
  fullmessagehtml: any;
  customdata: any;
  createdat: any;
  recipient: any;
  selected: any;
  description?: string;
}
