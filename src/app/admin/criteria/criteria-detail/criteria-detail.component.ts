import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { AbstractControl, FormBuilder, Validators } from '@angular/forms';
import { ToastrCustomService } from '../../../shared/services/toastr-custom.service';
import CommonUtil from '../../../shared/utils/common-util';
import { CustomValidators } from '../../../shared/utils/custom-validators';
import { MatRadioChange } from '@angular/material/radio';
import { Criteria } from 'src/app/shared/model/criteria.model';
import { CriteriaService } from 'src/app/shared/services/criteria.service';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
    selector: 'app-criteria-detail',
    templateUrl: './criteria-detail.component.html',
    styleUrls: ['./criteria-detail.component.css']
})
export class CriteriaDetailComponent implements OnInit {

    @Input() positions: Criteria[];
    // @Input() courseCategoryList: CourseCategory[];
    @Input() criteria: Criteria;

    @Output() newCri = new EventEmitter<any>();
    isEnableEvaluation: any = 0;
    constructor(
        public activeModal: NgbActiveModal,
        private fb: FormBuilder,
        private modalService: NgbModal,
        private toastrService: ToastrCustomService,
        private criteriaService: CriteriaService,
        private spinner: NgxSpinnerService,
    ) {
    }

    validateCodePos = true;
    positionsName: any;
    editProgramForm = this.fb.group({
        name: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(255)]],
        description: [''],
        code: ['', [Validators.required, CustomValidators.notBlankValidator, Validators.maxLength(50)]],
        // enableevaluation: 0
    });
    positionClone: Criteria;

    titleForm = 'common.update';

    ngOnInit(): void {
        this.positionClone = Object.assign({}, this.criteria);
        // this.positionClone = this.position.enableevaluation ? {...this.position} : {...this.position, enableevaluation: 0}
        // this.fetch();
        if (!this.criteria) {
            this.titleForm = 'common.create';
        }
    }

    mustTypeIdDepartment() {
        const code = this.positionClone.code;
        if (code === '') {
            this.validateCodePos = true;
        } else if (!code.match(/^[A-Za-z0-9-_]+$/g)) {
            this.validateCodePos = false;
        } else {
            this.validateCodePos = true;
        }
    }

    createOrUpdateCri() {
        if (this.editProgramForm.invalid) {
            // Danh dau la da cham de hien thi message loi
            CommonUtil.markFormGroupTouched(this.editProgramForm);
            return;
        }
        if (this.validateCodePos === false) {
            return;
        }
        this.spinner.show();
        this.editProgramForm.value.id = this.positionClone.id;
        this.criteriaService.createOrUpdateCriteria(this.editProgramForm.value).subscribe(res => {
            this.toastrService.success('Lưu tiêu chí đánh giá thành công');
            this.activeModal.dismiss('close');
            this.newCri.emit('load');
            this.spinner.hide();
        },
        err => {
            this.toastrService.handlerError(err);
            this.spinner.hide();
        })
    }
    enableEvaluation(event: MatRadioChange) {
        this.isEnableEvaluation = event.value;
    }
}
