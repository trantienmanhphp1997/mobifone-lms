import { CompileShallowModuleMetadata } from '@angular/compiler';
import {
  Component,
  OnInit,
  NgModule,
  Output,
  EventEmitter,
} from '@angular/core';
import { FormBuilder, FormControl, Validators } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { CourseService } from 'src/app/shared/services/course.service';
import { ToastrCustomService } from 'src/app/shared/services/toastr-custom.service';
import { log } from 'console';

@Component({
  selector: 'app-course-evaluation-users',
  templateUrl: './course-evaluation-users.component.html',
  styleUrls: ['./course-evaluation-users.component.css'],
})
export class CourseEvaluationUsersComponent implements OnInit {
  @Output() emitService: EventEmitter<any> = new EventEmitter();
  defaultChecked = true;
  IsmodelShow: boolean;
  errorCourse = true;
  nameCourse: any;
  errorMonth = true;
  courseList: any[] = [];
  studentData: any[] = [];
  studentSelected: any[] = [];
  courseId: any;
  courseListCriteria: any[] = [];
  totalRecord: number;
  type = 3;
  checkstudentcanwatchevaluation: boolean = true;
  searchCourse = {
    pageIndex: 1,
    pageSize: 10,
    sortColumn: 'id',
    sortType: 'desc',
  };
  evaluationInfo3M = {
    fullname: null,
    email: null,
    department: null,
    criterias: [],
    type: 3,
    muserid: null,
    mfullname: null,
    memail: null,
    mdepartment: null,
  };
  st3M: any;
  evaluationInfo6M = {
    fullname: null,
    email: null,
    department: null,
    criterias: [],
    type: 6,
    muserid: null,
    mfullname: null,
    memail: null,
    mdepartment: null,
  };
  st6M: any;
  dropdownSettings = {
    singleSelection: false,
    idField: 'id',
    textField: 'fullname',
    selectAllText: 'Chọn hết',
    unSelectAllText: 'Bỏ chọn hết',
    itemsShowLimit: 5,
    allowSearchFilter: true,
    searchPlaceholderText: 'Tìm kiếm',
  };

  constructor(
    private fb: FormBuilder,
    public activeModal: NgbActiveModal,
    private spinner: NgxSpinnerService,
    private courseService: CourseService,
    private toastrService: ToastrCustomService,
    private toartService: ToastrCustomService
  ) {}

  ngOnInit(): void {
    this.listCourseBeAsssessed();
    this.getListCourseCriteria();
  }

  listCourseBeAsssessed() {
    this.courseService.getListCourseBeAssessed().subscribe(
      (res) => {
        this.courseList = res.body;
      },
      (err) => {
        this.toastrService.handlerError(err);
      }
    );
  }

  changeStudentWatch(event: any) {
    this.checkstudentcanwatchevaluation = event.checked;
  }

  close() {
    this.IsmodelShow = true;
  }

  listStudentOfCourse() {
    const params = {
      id: this.courseId,
      type: this.type,
    };
    this.courseService.getListStudentOfCourseBeAssessed(params).subscribe(
      (res) => {
        this.studentData = res.body;
        this.studentSelected = this.studentData;
        console.log('student selected');
        console.log(this.studentSelected);
      },
      (err) => {
        this.toastrService.handlerError(err);
      }
    );
  }
  changeIdCourse($event) {
    this.getListCourseCriteria();
    this.courseId = $event.target.value;
    if (this.courseId) {
      this.listStudentOfCourse();
    }
    if (!$event.target.value) {
      this.studentData = null;
      this.studentSelected = null;
    }
    this.courseList.find((x) => {
      if (x.id == this.courseId) {
        this.nameCourse = x.fullname;
      }
    });
  }

  getListCourseCriteria() {
    const params = {
      limit: 10,
      page: 1,
      all: 1,
    };
    this.courseService.getListCourseCriteriaEndUser(params).subscribe(
      (res) => {
        this.courseListCriteria = res.body.results;
        this.evaluationInfo3M.criterias = this.courseListCriteria;
        this.evaluationInfo6M.criterias = this.courseListCriteria;
        this.st3M = this.evaluationInfo3M.criterias.map((c: any) => {
          return { criteriaid: c.id, status: c.status || 0 };
        });
        this.st6M = this.evaluationInfo6M.criterias.map((c: any) => {
          return { criteriaid: c.id, status: c.status || 0 };
        });
        this.totalRecord = res.body.total;
      },
      (err) => {
        this.toastrService.handlerError(err);
      }
    );
  }
  setValueRadio($event) {
    this.type = $event.target.value;

    this.getListCourseCriteria();
    if (this.courseId) {
      this.listStudentOfCourse();
    }
  }
  changeEvaluation(id, $event) {

    if (this.type == 3) {
      this.st3M = this.st3M.map((c) => {
        if (c.criteriaid == id) {
          return {
            criteriaid: c.criteriaid,
            status: parseInt($event.target.value),
          };
        } else {
          return { criteriaid: c.criteriaid, status: c.status };
        }
      });
    } else if (this.type == 6) {

      this.st6M = this.st6M.map((c) => {
        if (c.criteriaid == id) {
         

          return {
            criteriaid: c.criteriaid,
            status: parseInt($event.target.value),
          };
        } else {

          return { criteriaid: c.criteriaid, status: c.status };
        }
      });
    }
  }
  evaluatationUser() {
    if (!this.courseId) {
      this.errorCourse = false;
    } else {
      this.errorCourse = true;
    }
    if (!this.type) {
      this.errorMonth = false;
    } else {
      this.errorMonth = true;
    }
    if (this.errorCourse == false || this.errorMonth == false) {
      return;
    }


    this.spinner.show();
    if (this.studentSelected.length <= 0) {
      this.spinner.hide();
      this.toartService.error('Chưa có học viên nào được chọn đánh giá');
      return;
    }

    if (
      this.type == 3 &&
      this.evaluationInfo3M.criterias.length !=
        this.st3M.filter((e) => e.status !== 0).length
    ) {
      this.spinner.hide();
      this.toartService.error('Bạn chưa đánh giá đủ theo các tiêu chí');
      return;
    } else if (
      this.type == 6 &&
      this.evaluationInfo6M.criterias.length !=
        this.st6M.filter((e) => e.status !== 0).length
    ) {
      this.spinner.hide();
      this.toartService.error('Bạn chưa đánh giá đủ theo các tiêu chí');
      return;
    }
    let userids = this.studentSelected.map((x) => x.id);
    const evaluation = {
      courseid: this.courseId,
      userids: userids.length > 0 ? userids.toString() : [],
      criteria: this.type === 3 ? this.st3M : this.st6M,
      type: this.type,
      checkstudentcanwatchevaluation:
        this.checkstudentcanwatchevaluation === true ? 1 : 0,
    };


    this.courseService.evaluatationUser(evaluation).subscribe(
      (res) => {
        this.spinner.hide();
        this.emitService.next(null);
        this.toartService.success('Đánh giá thành công');
        this.courseId = null;
        this.studentSelected = null;
        this.getListCourseCriteria();
      },
      (err) => {
        this.spinner.hide();
        this.toartService.handlerError(err);
      }
    );
  }
}
