import { Component, Input, OnInit } from '@angular/core';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrCustomService } from './../../../shared/services/toastr-custom.service';
import { BadgeService } from './../../../shared/services/badge.service';
import { ActivatedRoute } from '@angular/router';
import { LocalStorageService } from 'ngx-webstorage';
import { USER_INFO } from './../../../shared/constants/base.constant';
import * as moment from 'moment';
import { FileService } from 'src/app/shared/services/file.service';
import { ExportAsService, ExportAsConfig } from 'ngx-export-as';
@Component({
  selector: 'app-eu-certificate',
  templateUrl: './eu-certificate.component.html',
  styleUrls: ['./eu-certificate.component.css']
})
export class EuCertificateComponent implements OnInit {
  @Input() course :any;
  userInfo:any ;
  certificateList: any ;
  certificate:any
  isShow1: boolean = false;
  exportAsConfig: ExportAsConfig = {
    type: 'png', 
    elementId: 'myTableElementId' 
  }
  constructor(
    private badgeService: BadgeService,
    private fileservice : FileService,
    private exportAsService: ExportAsService,
    private spinner: NgxSpinnerService,
    private $localStorage: LocalStorageService,
    private dadgeService: BadgeService,
  ) { }

  ngOnInit(): void {
    console.log(this.course)
    this.userInfo = this.$localStorage.retrieve(USER_INFO.INFO);
    this.dadgeService.getDetailBadge(this.course).subscribe( res=>{
      this.certificate = res.body;
    })
    this.spinner.hide();
  }
  getDateFromUnix(date) {
    if (date) {
      return moment.unix(date);
    }
    return null;
  }

  viewImage(item) {
    var img = "";
    if (item.filename) {
      img = this.fileservice.badgeFileUrl(item.contextid, item.filename, '/external_badge/overviewfiles/','0/');
    }
    return img;
  }
  exportImage(certificate:any) {
    this.exportAsService.save(this.exportAsConfig, certificate.name).subscribe(() => {
    });
    this.exportAsService.get(this.exportAsConfig).subscribe(content => {
      console.log(content);
    });
  }

}
